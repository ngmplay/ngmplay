﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncPC.Utility
{
    static class ParametriDaFile
    {

        public static int ID_int_Stadio { get; set; } = 0;
        public static int int_IdentificazionePC { get; set; } = 0;
        public static void GetParametri() { 

            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(@"c:\parametriPC.txt");
            while ((line = file.ReadLine()) != null)
            {

                line = line.ToString().ToLower().Replace(" ", "").Replace(";", "");
                if ((line.Length > 0) && (line.Contains("=")))
                {
                    string[] chiaveval = line.Split('=');
                        string chiave = chiaveval[0].ToLower();
                    string val = chiaveval[1];

                    if (chiave == "identificazionepc")
                    {
                        int_IdentificazionePC = Convert.ToInt32(val);
                    }
                    if (chiave == "idstadio")
                    {
                        ID_int_Stadio = Convert.ToInt32(val);
                    }
                    Console.WriteLine(line);
                }

            }
        }
    }
}
