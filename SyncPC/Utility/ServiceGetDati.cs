﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace SyncPC.Utility
{
    static class ServiceGetDati
    {


        private static void ArchiviaIdSquadre()
        {

            XElement xelement = XElement.Load("https://www.goalserve.com/getfeed/552a653da38d4bdf9cbef861d1a6baf0/soccerleague/1265");
            var teams = xelement.Descendants("team").ToList();
            foreach (var t in teams)
            {
                 DB.EseguiSQL("update tb_squadre set IdService = '"+ t.Attribute("id").Value + "' where nva_Nome like '"+ t.Attribute("name").Value + "' and IdService IS NULL");
                Console.WriteLine( t.Attribute("name").Value);
            }
        }




        public static void ArchiviaInDBRisultati()
        {
            try
            {
                XElement xelement = XElement.Load(ParametriDaDB.Parametro_nva_LiveScoreUrlServer + ParametriDaDB.Parametro_nva_LiveScoreUserGuid + "/soccernew/live");
                var allCategory = xelement.Descendants("category").ToList();
                var category = (from cat in allCategory
                                where cat.Attribute("id").Value.Equals(ParametriDaDB.Parametro_nva_LiveScoreIdSerieB)
                                select cat).FirstOrDefault();
                if (category != null)
                {   
                    var matchs = category.Descendants("match").ToList();

                    if (matchs != null)
                    {
                        foreach (var match in matchs)
                        {
                            var matchId = match.Attribute("static_id").Value;

                            String IdSquadraCasa    = match.Element("localteam").Attribute("id").Value;
                            String IdSquadraOspiti  = match.Element("visitorteam").Attribute("id").Value;

                            String int_GoalCasa     = match.Element("localteam").Attribute("goals").Value;
                            String int_GoalOspiti   = match.Element("visitorteam").Attribute("goals").Value;

                            DB.EseguiSQL("exec sp_InserisciGoalDaService @IdSquadraCasaService = "+ IdSquadraCasa + ", @IdSquadraOspitiService = "+ IdSquadraOspiti + ", @int_GoalCasa = "+ int_GoalCasa + ", @int_GoalOspiti = "+ int_GoalOspiti);
                        
                        }
                    }
                    else
                    {
                        Log.AggiungiLog("nessun match trovato", "SyncPC.ServiceGetDati.ArchiviaInDBRisultati");
                    }
                }
                else
                {
                    Log.AggiungiLog("nessuna Categoria di statisticaGoal trovata", "SyncPC.ServiceGetDati.ArchiviaInDBRisultati");
                }
            }catch (Exception ex){
                Log.AggiungiErrore(ex.Message, "SyncPC.ServiceGetDati.ArchiviaInDBRisultati");
            }
        }







    }







}
