﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


    public static class fg
    {

        public static string PulisciStringa(string Stringa)
        {

            Stringa = Stringa.Replace("  ", " ").Replace("  ", " ").Replace("  ", " ").ToLower().Trim().Replace(" ","_");
            Regex rgx = new Regex("[^a-z0-9_]");
            Stringa = rgx.Replace(Stringa, "");
            return Stringa;
        }


        public static Boolean IsYes(string Stringa)
        {
            Stringa = Stringa.ToLower();
            if (Stringa == "si") return true;
            if (Stringa == "yes") return true;
            if (Stringa == "ok") return true;
            if (Stringa == "1") return true;
            return false;
        }
        public static Boolean IsYes(int i)
        {
            if (i > 0) return true;
            return false;
        }



    }
