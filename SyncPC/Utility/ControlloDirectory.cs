﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncPC.Utility
{
    class ControlloDirectory
    {

        public static void  ControllaConsistenzaDir(String nva_PathCompletaBaseDirLocale )
        {
            if (!Directory.Exists(nva_PathCompletaBaseDirLocale + @"\Scene"))
                Directory.CreateDirectory(nva_PathCompletaBaseDirLocale + @"\Scene");

            if (!Directory.Exists(nva_PathCompletaBaseDirLocale + @"\Gif250"))
                Directory.CreateDirectory(nva_PathCompletaBaseDirLocale + @"\Gif250");
        }

    }
}
