﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncPC.Utility
{
    class ParametriDaDB
    {

        public static String Parametro_nva_IPScaler;
        public static String Parametro_nva_IPSelettore;
        public static String Parametro_nva_FileEseguibileConsoleHash;
        public static String Parametro_nva_DirRootVideo;
        public static String Parametro_nva_DirFFMpeg;
        public static String Parametro_nva_DirRootServer;
        public static String Parametro_nva_DirOutVideoScene;
        public static String Parametro_nva_FileEseguibileFFMPEG;
        public static String Parametro_nva_DirOutGif250Scene;
        public static String Parametro_nva_DirVideoSorgenti;
        public static String Parametro_nva_DirTemp;
        public static String Parametro_nva_DirVideoCompositi;
        public static String Parametro_nva_FileEseguibileFFPROBE;
        public static String Parametro_nva_EseguiChiamateScaler;
        public static String Parametro_nva_EstensioniConsentiteDentroCartellaEventi;
        public static String Parametro_nva_DirPlaylist;
        public static String Parametro_nva_LiveScoreSerieA;
        public static String Parametro_nva_LiveScoreSerieB;
        public static String Parametro_nva_LiveScoreBackground;
        public static String Parametro_nva_LiveScoreHeight;
        public static String Parametro_nva_LiveScoreIdSerieA;
        public static String Parametro_nva_LiveScoreIdSerieB;
        public static String Parametro_nva_LiveScoreIdTeam;
        public static String Parametro_nva_LiveScorePolling;
        public static String Parametro_nva_LiveScorePositionX;
        public static String Parametro_nva_LiveScorePositionY;
        public static String Parametro_nva_LiveScoreTimeInSeconds;
        public static String Parametro_nva_LiveScoreTypeOfStatus;
        public static String Parametro_nva_LiveScoreUrlFixtureSerieA;
        public static String Parametro_nva_LiveScoreUrlFixtureSerieB;
        public static String Parametro_nva_LiveScoreUrlH2H;
        public static String Parametro_nva_LiveScoreUrlImgLiveScoreEnd;
        public static String Parametro_nva_LiveScoreUrlImgLiveScoreStart;
        public static String Parametro_nva_LiveScoreUrlLiveScore;
        public static String Parametro_nva_LiveScoreUrlServer;
        public static String Parametro_nva_LiveScoreUrlSoundLiveScore;
        public static String Parametro_nva_LiveScoreUrlStandingSerieA;
        public static String Parametro_nva_LiveScoreUrlStandingSerieB;
        public static String Parametro_nva_LiveScoreUrlStatsSerieA;
        public static String Parametro_nva_LiveScoreUrlStatsSerieB;
        public static String Parametro_nva_LiveScoreUserGuid;
        public static String Parametro_nva_LiveScoreEnable;




        public static void GetParametri()
        {

            Parametro_nva_IPSelettore = GetParametro("nva_IPSelettore");
            Parametro_nva_IPScaler = GetParametro("nva_IPScaler");
            Parametro_nva_FileEseguibileConsoleHash = GetParametro("nva_IPSelettore");
            Parametro_nva_DirRootVideo = GetParametro("nva_DirRootVideo");
            Parametro_nva_DirFFMpeg = GetParametro("nva_DirFFMpeg");
            Parametro_nva_DirRootServer = GetParametro("nva_DirRootServer");
            Parametro_nva_DirOutVideoScene = GetParametro("nva_DirOutVideoScene");
            Parametro_nva_FileEseguibileFFMPEG = GetParametro("nva_FileEseguibileFFMPEG");
            Parametro_nva_DirOutGif250Scene = GetParametro("nva_DirOutGif250Scene");
            Parametro_nva_DirVideoSorgenti = GetParametro("nva_DirVideoSorgenti");
            Parametro_nva_DirTemp = GetParametro("nva_DirTemp");
            Parametro_nva_DirVideoCompositi = GetParametro("nva_DirVideoCompositi");
            Parametro_nva_FileEseguibileFFPROBE = GetParametro("nva_FileEseguibileFFPROBE");
            Parametro_nva_EseguiChiamateScaler = GetParametro("nva_EseguiChiamateScaler");
            Parametro_nva_DirPlaylist = GetParametro("nva_DirPlaylist");
            Parametro_nva_LiveScoreBackground = GetParametro("nva_LiveScoreBackground");
            Parametro_nva_LiveScoreHeight = GetParametro("nva_LiveScoreHeight");
            Parametro_nva_LiveScoreIdSerieA = GetParametro("nva_LiveScoreIdSerieA");
            Parametro_nva_LiveScoreIdSerieB = GetParametro("nva_LiveScoreIdSerieB");
            Parametro_nva_LiveScoreIdTeam = GetParametro("nva_LiveScoreIdTeam");
            Parametro_nva_LiveScoreSerieA = GetParametro("nva_LiveScoreSerieA");
            Parametro_nva_LiveScoreSerieB = GetParametro("nva_LiveScoreSerieB");
            Parametro_nva_LiveScorePolling = GetParametro("nva_LiveScorePolling");
            Parametro_nva_LiveScorePositionX = GetParametro("nva_LiveScorePositionX");
            Parametro_nva_LiveScorePositionY = GetParametro("nva_LiveScorePositionY");
            Parametro_nva_LiveScoreTimeInSeconds = GetParametro("nva_LiveScoreTimeInSeconds");
            Parametro_nva_LiveScoreTypeOfStatus = GetParametro("nva_LiveScoreTypeOfStatus");
            Parametro_nva_LiveScoreUrlFixtureSerieA = GetParametro("nva_LiveScoreUrlFixtureSerieA");
            Parametro_nva_LiveScoreUrlFixtureSerieB = GetParametro("nva_LiveScoreUrlFixtureSerieB");
            Parametro_nva_LiveScoreUrlH2H = GetParametro("nva_LiveScoreUrlH2H");
            Parametro_nva_LiveScoreUrlImgLiveScoreEnd = GetParametro("nva_LiveScoreUrlImgLiveScoreEnd");
            Parametro_nva_LiveScoreUrlImgLiveScoreStart = GetParametro("nva_LiveScoreUrlImgLiveScoreStart");
            Parametro_nva_LiveScoreUrlLiveScore = GetParametro("nva_LiveScoreUrlLiveScore");
            Parametro_nva_LiveScoreUrlServer = GetParametro("nva_LiveScoreUrlServer");
            Parametro_nva_LiveScoreUrlSoundLiveScore = GetParametro("nva_LiveScoreUrlSoundLiveScore");
            Parametro_nva_LiveScoreUrlStandingSerieA = GetParametro("nva_LiveScoreUrlStandingSerieA");
            Parametro_nva_LiveScoreUrlStandingSerieB = GetParametro("nva_LiveScoreUrlStandingSerieB");
            Parametro_nva_LiveScoreUrlStatsSerieA = GetParametro("nva_LiveScoreUrlStatsSerieA");
            Parametro_nva_LiveScoreUrlStatsSerieB = GetParametro("nva_LiveScoreUrlStatsSerieB");
            Parametro_nva_LiveScoreUserGuid = GetParametro("nva_LiveScoreUserGuid");
            Parametro_nva_LiveScoreEnable = GetParametro("nva_LiveScoreEnable");
        }


        private static String GetParametro(String NomeParametro)
        {
            try
            {
                DataTable dt = DB.GetDataFromSQL("select top(1) nva_ValoreParametro from tb_Parametri where nva_NomeParametro  = '" + NomeParametro + "'");
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows[0][0].ToString();
                }else{
                    Log.AggiungiLog("Parametro NON trovato in DB:" + NomeParametro, "SyncPC-GetParametro");
                    return "";
                }
            }catch (Exception ex){
                Log.AggiungiLog("catch StaticStartConfig errore Parametro:" + NomeParametro + " exception:" + ex.Message, "SyncPC-GetParametro");
                return "";
            }
        }


    }
}
