﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SyncPC
{
    public partial class SincronizzoFileTraPc : Form
    {
        String nva_PathBaseDirRemota;
        String nva_PathBaseDirLocale;
        String nva_NomeDirectoryStadio;
        String nva_IpRemoto;
        String nva_IpLocale;
        String nva_PathCompletaBaseDirRemota;
        String nva_PathCompletaBaseDirLocale;
        Boolean IsOnline = false;
        Boolean IsMaster = false;
        Dictionary<String, DateTime> ListaFilesDaCopiare = new Dictionary<String, DateTime>();
        Timer t = new Timer();
        int DurataCountdownSecondi = 5;
        int ContatoreSecondi = 0;
        DataTable dt = new DataTable();
        Boolean IsPcAttivo=false;

        public SincronizzoFileTraPc()
        {
            InitializeComponent();

            try
            {
                string line;

            // Read the file and display it line by line.  
            System.IO.StreamReader file = new System.IO.StreamReader(@"c:\parametriPC.txt");
            while ((line = file.ReadLine()) != null)
            {

                line = line.ToString().ToLower().Replace(" ", "").Replace(";", "");
                if ((line.Length > 0) && (line.Contains("=")))
                {

                    string[] chiaveval = line.Split('=');
                    string chiave = chiaveval[0].ToLower();
                    string val = chiaveval[1];

                    if (chiave == "identificazionepc")
                    {
                        StaticStartConfig.int_IdentificazionePC = Convert.ToInt32(val);
                    }
                    if (chiave == "idstadio")
                    {
                        StaticStartConfig.ID_int_Stadio = Convert.ToInt32(val);
                    }
                    Console.WriteLine(line);
                }

            }
  

            DataRow drStadio = DB.GetDataFromSQL("select * from tb_Stadi where Id= " + StaticStartConfig.ID_int_Stadio).Rows[0];

            nva_NomeDirectoryStadio = drStadio["nva_NomeDirectory"].ToString();



            DataRow drPcLocale = DB.GetDataFromSQL("select * from tb_Pc where Id= " + StaticStartConfig.int_IdentificazionePC).Rows[0];
            DataRow drPcRemoto = DB.GetDataFromSQL("select * from tb_Pc where Id <> " + StaticStartConfig.int_IdentificazionePC).Rows[0];
            if (Convert.ToInt32(drPcLocale["tin_IsMaster"]) == 1)
            {
                IsPcAttivo = true;
            }


            nva_IpRemoto = drPcRemoto["nva_IP"].ToString();
                nva_PathBaseDirLocale = @"C:\NGMPlay\publish\wwwroot";
                nva_PathBaseDirRemota = @"\\" + nva_IpRemoto ;

                nva_PathCompletaBaseDirLocale = @"C:\NGMPlay\publish\wwwroot\video\"+ nva_NomeDirectoryStadio+@"\";
            nva_PathCompletaBaseDirRemota = @"\\" + nva_IpRemoto + @"\video\"+ nva_NomeDirectoryStadio + @"\";

            dgView.AutoGenerateColumns = false;
            //dt.Clear();
            //dt.Columns.Add(new DataColumn("DataOra"));
            //dt.Columns.Add("NomeFile");
        

            t.Interval = 1000;
            t.Tick += T_Tick;


            RiavviaTimer();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }

        }

        private void T_Tick(object sender, EventArgs e)
        {
            IsOnline = false;
            IsMaster = false;
            ContatoreSecondi = ContatoreSecondi - 1;
            lbTimer.Text = ContatoreSecondi.ToString()+" sec";
            if (ContatoreSecondi == 0)
            {
                t.Enabled = false;


                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
                lbTimer.Text = "drPcLocale";
                DataRow drPcLocale =  DB.GetDataFromSQL("select isnull(tin_IsMaster,0) tin_IsMaster, nva_IP , id, dtt_Modifica from tb_Pc where Id= " + StaticStartConfig.int_IdentificazionePC).Rows[0];

                lbTimer.Text = "drPcRemoto";
                DataRow drPcRemoto = DB.GetDataFromSQL("select  isnull(tin_IsMaster,0) tin_IsMaster, nva_IP , id , dtt_Modifica from tb_Pc where Id <> " + StaticStartConfig.int_IdentificazionePC).Rows[0];

      

                nva_IpRemoto = drPcRemoto["nva_IP"].ToString();
                nva_IpLocale = drPcLocale["nva_IP"].ToString();

                IsMaster = Convert.ToBoolean(drPcLocale["tin_IsMaster"]);

                String nva_IpLocaleReale = localIPs[(localIPs.Count() - 1)].ToString();


                if (nva_IpLocaleReale != nva_IpLocale)
                {
                    MessageBox.Show("Ip effettivo diverso da quello archiviato! (effettivo: "+ nva_IpLocaleReale + " - archiviato: " + nva_IpLocale+")");
                }



   
                try
                {
                    lbTimer.Text = "proc146";
                    DB.EseguiSQL( "update pcR set tin_IsMaster = pcL.tin_IsMaster, dtt_Modifica = pcL.dtt_Modifica, nva_IP=pcL.nva_IP  from [" + nva_IpRemoto + "].[NgmStudioDevelop].dbo.tb_PC pcR join tb_Pc pcL on pcR.nva_Nome=pcL.nva_Nome where pcL.dtt_Modifica > pcR.dtt_Modifica;" +
                            "update pcL set tin_IsMaster = pcR.tin_IsMaster, dtt_Modifica = pcR.dtt_Modifica, nva_IP=pcR.nva_IP  from tb_PC pcL join [" + nva_IpRemoto + "].[NgmStudioDevelop].dbo.tb_Pc pcR on pcR.nva_Nome=pcL.nva_Nome where pcR.dtt_Modifica > pcL.dtt_Modifica;");
                    lbTimer.Text = "proc150";
                    DB.EseguiSQL("update tb_PC set tin_IsOnline = 1, dtt_UltimoControlloOnline = getdate() where nva_IP='" + nva_IpLocale + "'");

                    lbStatoConnessioneRemota.Text = "OK";

                    IsOnline = true;

                }
                catch (Exception ex)
                {

                    lbStatoConnessioneRemota.Text = "Errore";
                    lbTimer.Text = "proc161";
                    DB.EseguiSQL("update tb_PC set tin_IsOnline = 0, dtt_UltimoControlloOnline = getdate() where nva_IP='" + nva_IpLocale + "'");

                }


                lbDataOraControlloConnessione.Text = DateTime.Now.ToString();


                lbIpPcRemoto.Text = nva_IpRemoto;
                lbIpPcLocale.Text = nva_IpLocale;


                if (((Convert.ToInt32(drPcLocale["tin_IsMaster"]) + Convert.ToInt32(drPcRemoto["tin_IsMaster"])) > 1) || ((Convert.ToInt32(drPcLocale["tin_IsMaster"]) + Convert.ToInt32(drPcRemoto["tin_IsMaster"])) == 0))
                {
                    lbTipoPcLocale.Text = "Errore!";
                    lbTipoPcRemoto.Text = "Errore!";
                }
                else
                {
                    if (IsMaster)
                    {
                        lbTipoPcLocale.Text = "Attivo";
                        lbTipoPcRemoto.Text = "Backup";
                    }
                    else
                    {
                        lbTipoPcLocale.Text = "Backup";
                        lbTipoPcRemoto.Text = "Attivo";
                    }
                }




                /*
                try
                {

                    DB.EseguiSQL("update pcL set tin_IsMaster = pcR.tin_IsMaster, dtt_Modifica = pcR.dtt_Modifica, nva_IP=pcR.nva_IP  from tb_PC pcL join [" + nva_IpRemoto + "].dbo.tb_Pc pcR on pcR.nva_Nome=pcL.nva_Nome where pcR.dtt_Modifica > pcL.dtt_Modifica ");
                    DB.EseguiSQL("update pcR set tin_IsMaster = pcL.tin_IsMaster, dtt_Modifica = pcL.dtt_Modifica, nva_IP=pcL.nva_IP  from [" + nva_IpRemoto + "].dbo.tb_PC pcR join tb_Pc pcL on pcR.nva_Nome=pcL.nva_Nome where pcL.dtt_Modifica > pcR.dtt_Modifica ");

                    lbStatoConnessioneRemota.Text="OK";
                }catch(Exception ex)
                {
                    lbStatoConnessioneRemota.Text = "Errore";
                }





                

                */

                lbTimer.Text = "sp_TrovaNuoviFileSorgenti";
                ListaFilesDaCopiare =  sp_TrovaNuoviFileSorgenti();




                if (IsOnline)
                {
                    lbTimer.Text = "exec sp_SyncDB_AggiornaDBLocaleConNuoviRecord";
                    DB.EseguiSQL("exec sp_SyncDB_AggiornaDBLocaleConNuoviRecord");
                    lbTimer.Text = "AvviaCopiaFileTraPC";
                    AvviaCopiaFileTraPC(ListaFilesDaCopiare);
                }
                



                if ((!IsOnline) || (IsMaster))
                {
                    lbTimer.Text = "exec sp_GetHashComplessivoTutteLeSceneDaFileSorgenti";
                    DB.EseguiSQL("exec sp_GetHashComplessivoTutteLeSceneDaFileSorgenti");
                }


                lbTimer.Text = "generazione scene";
                //AvviaGenerazioneScene();



                if (IsOnline)
                {
                    lbTimer.Text = "TrovaTuttiIFileDaAcquisireInSpecificaCartella scene";
                    ListaFilesDaCopiare = TrovaTuttiIFileDaAcquisireInSpecificaCartella("Scene");
                    lbTimer.Text = "AvviaCopiaFileTraPC scene";
                    AvviaCopiaFileTraPC(ListaFilesDaCopiare);

                    lbTimer.Text = "TrovaTuttiIFileDaAcquisireInSpecificaCartella gif250";
                    ListaFilesDaCopiare = TrovaTuttiIFileDaAcquisireInSpecificaCartella("gif250");
                    lbTimer.Text = "AvviaCopiaFileTraPC gif250";
                    AvviaCopiaFileTraPC(ListaFilesDaCopiare);
                }




             t.Enabled = true;
             RiavviaTimer();
            }

        }




        public async void AvviaGenerazioneScene()
        {
          int rimanenti = 0;

         //   while (rimanenti > 0)
        //    {
                rimanenti = Convert.ToInt32(DB.GetDataFromSQL("sp_SperimentoGenerazioneComposizioneScena_v11").Rows[0][0]);


                lbTimer.Text = "rimanenti " + rimanenti.ToString();
           // }


        }






        public async void AvviaCopiaFileTraPC(Dictionary<String, DateTime> lf)
        {
            progressBar1.Value = 0;
            lbTimer.Text = "avvio copia file";

            await CopyFiles(lf, nva_PathCompletaBaseDirRemota, nva_PathCompletaBaseDirLocale,(prog, s) => AvanzaProgressBar(prog,s));
           
        }




         void AvanzaProgressBar(int prog,string s)
        {


        
    
            if(s!=""){

                dgView.Rows.Add( DateTime.Now.ToString("yyyyMMddHHmmssfff"), s );
                dgView.Sort(DataOra, ListSortDirection.Descending);
                if(dgView.Rows.Count>1000)
                    dgView.Rows.RemoveAt(dgView.Rows.Count-1);
            



            }



            lbOperazioneSvolta.Text = "copio: " + s;
            progressBar1.Value = prog;

            if (prog == 100)
            {


              //Task.Run(() =>
              //  {
              //      AggiungiLog("exec sp_ControllaFileAcquisisciHashFileTutteLeScene");
              //      DB.EseguiSQL("exec sp_ControllaFileAcquisisciHashFileTutteLeScene");
              //      AggiungiLog("RiavviaTimer()");
              //      RiavviaTimer();
              //  });

                
               // progressBar1.Value = 0;
            //    
            }
        }





        void RiavviaTimer()
        {
            lbOperazioneSvolta.Text = "-";
            ContatoreSecondi = DurataCountdownSecondi;
            t.Enabled=true;
            t.Start();
        }



        //public async void TrovaTuttiIFileDaAcquisire()
        //{



        //    ListaFilesDaCopiare.Clear();

        //    SincronizzaFileSorgenti();




        //   // TrovaTuttiIFileDaAcquisireInSpecificaCartella("Scene");
        //  //  TrovaTuttiIFileDaAcquisireInSpecificaCartella("gif250");
        //    //TrovaTuttiIFileDaAcquisireInSpecificaCartella("Sorgenti");

        //}




        private  Dictionary<String, DateTime>  sp_TrovaNuoviFileSorgenti()
        {
            Dictionary<String, DateTime> lf = new Dictionary<String, DateTime>();
            DataTable dt =  DB.GetDataFromSQL("exec sp_TrovaNuoviFileSorgenti");
            foreach (DataRow r in dt.Rows)
            {
                AggiungiLog("sorgenti/" + r["nva_NomeFile"].ToString());
                lf.Add("sorgenti/"+r["nva_NomeFile"].ToString(), Convert.ToDateTime(r["dtt_UltimaScritturaSulFile"]));
            }
            return lf;
        }




            /*

            private void TrovaTuttiIFileDaAcquisireInSpecificaCartella(String StrCartella)
            {
                String Tabella;
                if (IsPcAttivo)
                {
                    Tabella = "tb_LogFileSync";
                }
                else
                {
                    Tabella = "[" + nva_IpRemoto + "].[NgmStudioDevelop].dbo.tb_LogFileSync";
                }


                Int64 i = 0;
                DirectoryInfo DirInfoCartellaLocale = new DirectoryInfo(Path.Combine(nva_PathCompletaBaseDirLocale, StrCartella));
                DirectoryInfo DirInfoCartellaRemota = new DirectoryInfo(Path.Combine(nva_PathCompletaBaseDirRemota, StrCartella));


                foreach (FileInfo FileInfoRemoto in DirInfoCartellaRemota.GetFiles())
                {
                    i = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssfff")) + 1;

                    String HashFileRemoto = BytesToString(GetHashSha256(FileInfoRemoto.FullName));


                    DataTable dtFile = DB.GetDataFromSQL("select * from " + Tabella + " where nva_PathFile like '" + StrCartella + "/" + FileInfoRemoto.Name + "' ");

                    if (dtFile.Rows.Count == 0)
                    {
                        DB.EseguiSQL("insert into " + Tabella + " (nva_PathFile,nva_HashPC1,nva_HashPC2,)");

                    }

                    if (!FileInfoRemoto.Name.ToLower().Contains("_tmp.mp4"))
                    {
                        if (File.Exists(Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name)))
                        {
                            string StringaFileLocale = Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name);
                            FileInfo FileInfoLocale = new FileInfo(StringaFileLocale);
                            String HashFileLocale = BytesToString(GetHashSha256(FileInfoLocale.FullName));
                            if(HashFileRemoto != HashFileLocale)
                            {
                                dgControlli.Rows.Add(i, FileInfoRemoto.FullName, FileInfoLocale.FullName, UltimaModificaRemoto, UltimaModificaLocale);
                                dgControlli.Rows[dgControlli.Rows.Count - 1].DefaultCellStyle.BackColor = Color.Red;
                                ListaFilesDaCopiare.Add(FileInfoRemoto.FullName, FileInfoLocale.FullName);
                            }

                        }
                        else
                        {
                            dgControlli.Rows.Add(i, FileInfoRemoto.FullName, Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name), UltimaModificaRemoto, "File non presente localmente");
                            dgControlli.Rows[dgControlli.Rows.Count - 1].DefaultCellStyle.BackColor = Color.Red;

                            ListaFilesDaCopiare.Add(FileInfoRemoto.FullName, Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name));
                        }
                    }



                    dgControlli.Sort(DataOraIncrementale, ListSortDirection.Descending);
                    if (dgControlli.Rows.Count > 20)
                        dgControlli.Rows.RemoveAt(dgControlli.Rows.Count - 1);


                }
            }

        */










            private Dictionary<String, DateTime> TrovaTuttiIFileDaAcquisireInSpecificaCartella(String StrCartella)
        {

            Dictionary<String, DateTime> lf = new Dictionary<String, DateTime>();
            Int64 i = 0;

            DirectoryInfo DirInfoCartellaLocale = new DirectoryInfo(Path.Combine(nva_PathCompletaBaseDirLocale, StrCartella));
            DirectoryInfo DirInfoCartellaRemota = new DirectoryInfo(Path.Combine(nva_PathCompletaBaseDirRemota, StrCartella));


            foreach (FileInfo FileInfoRemoto in DirInfoCartellaRemota.GetFiles())
            {

                i = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssfff")) + 1;
                DateTime CreazioneRemoto = FileInfoRemoto.CreationTime;
                DateTime UltimaModificaRemoto = FileInfoRemoto.LastWriteTime;

                if (!FileInfoRemoto.Name.ToLower().Contains("_tmp.mp4"))
                {

                    if (File.Exists(Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name)))
                    {


                        string StringaFileLocale = Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name);

                        FileInfo FileInfoLocale = new FileInfo(StringaFileLocale);


                        DateTime UltimaModificaLocale = FileInfoLocale.LastWriteTime;

       
                            try
                            {
                                dgControlli.Rows.Add(i, FileInfoRemoto.FullName, FileInfoLocale.FullName, UltimaModificaRemoto, UltimaModificaLocale);
                                if (UltimaModificaRemoto > UltimaModificaLocale)
                                {
                                    lf.Add(Path.Combine(StrCartella + "/" + FileInfoRemoto.Name), UltimaModificaRemoto);
                                    dgControlli.Rows[dgControlli.Rows.Count - 1].DefaultCellStyle.BackColor = Color.Yellow;
                                }else{
                                    dgControlli.Rows[dgControlli.Rows.Count - 1].DefaultCellStyle.BackColor = Color.Green;
                                }
                            }
                            catch (Exception ex)
                            {
                                AggiungiLog("215:" + ex.Message);
                            }

              
                    }
                    else
                    {
                        dgControlli.Rows.Add(i, FileInfoRemoto.FullName, Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name), UltimaModificaRemoto, "File non presente localmente");
                        dgControlli.Rows[dgControlli.Rows.Count - 1].DefaultCellStyle.BackColor = Color.Red;

                        lf.Add(Path.Combine(StrCartella + "/" + FileInfoRemoto.Name), UltimaModificaRemoto);
                    }
                }



                dgControlli.Sort(DataOraIncrementale, ListSortDirection.Descending);
                if (dgControlli.Rows.Count > 1000)
                    dgControlli.Rows.RemoveAt(dgControlli.Rows.Count - 1);


            }


            return lf;
        }




        void AggiungiLog(String Log)
        {
            dgLog.Rows.Add(DateTime.Now.ToString("yyyyMMddHHmmssfff"),Log);

            dgLog.Sort(DataOraLog, ListSortDirection.Descending);

            if (dgLog.Rows.Count > 1000)
                dgLog.Rows.RemoveAt(dgLog.Rows.Count - 1);
        }


        //private static SHA256 Sha256 = SHA256.Create();


        //private static byte[] GetHashSha256(string filename)
        //{
        //    using (FileStream stream = File.OpenRead(filename))
        //    {
        //        return Sha256.ComputeHash(stream);
        //    }
        //}

        //public static string BytesToString(byte[] bytes)
        //{
        //    string result = "";
        //    foreach (byte b in bytes) result += b.ToString("x2");
        //    return result;
        //}

        private void btn_PcAttivo_Click(object sender, EventArgs e)
        {

            try
            {

                DB.EseguiSQL("exec [sp_ImpostaQuestoPCComeMaster]");

                lbStatoConnessioneRemota.Text = "OK";
            }
            catch (Exception ex)
            {
                lbStatoConnessioneRemota.Text = "Errore";
            }


        }





        public  async Task CopyFiles(Dictionary<string, DateTime> files, String nva_PathCompletaBaseDirRemota, string nva_PathCompletaBaseDirLocale, Action<int, string> progressCallback)
        {



            for (int x = 0; x < files.Count; x++)
            {
                
                try
                {



                    var item = files.ElementAt(x);
                    lbTimer.Text = "File avvio copia " +item.Key  ;


                 
                    String from = nva_PathCompletaBaseDirRemota + item.Key;
                    String to = nva_PathCompletaBaseDirLocale + item.Key;


                    DateTime DataOraModificaFinale = item.Value;

              

                    using (var outStream = new FileStream(to, FileMode.Create, FileAccess.Write, FileShare.Read))
                    {
                        using (var inStream = new FileStream(from, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            await inStream.CopyToAsync(outStream);

                        }

                    }

                    File.SetLastWriteTime(to, DataOraModificaFinale);

                    double d = (Convert.ToDouble((x + 1)) / Convert.ToDouble(files.Count)) * 100;
                    progressCallback(Convert.ToInt32(Math.Round(d, 0)), to.ToString());
                    
                }
                catch (Exception e)
                {
                    lbTimer.Text=("548:" + e.Message.ToString());

                }
            }
            progressCallback(100, "");



        }
    }

}



    //public static class Copier
    //{
    //    public static async Task CopyFiles(Dictionary<string, DateTime> files, String nva_PathCompletaBaseDirRemota,string nva_PathCompletaBaseDirLocale, Action<int,string> progressCallback)
    //    {



    //        for (int x = 0; x < files.Count; x++)
    //        {

    //            try
    //            {

              

    //            var item = files.ElementAt(x);
    //            String from = nva_PathCompletaBaseDirRemota+ item.Key ;
    //            String to = nva_PathCompletaBaseDirLocale + item.Key;


    //            DateTime DataOraModificaFinale = item.Value;



    //            using (var outStream = new FileStream(to, FileMode.Create, FileAccess.Write, FileShare.Read))
    //            {
    //                using (var inStream = new FileStream(from, FileMode.Open, FileAccess.Read, FileShare.Read))
    //                {
    //                    await inStream.CopyToAsync(outStream);
                        
    //                }
                    
    //            }

    //          File.SetLastWriteTime(to, DataOraModificaFinale);

    //        double d = (Convert.ToDouble((x + 1)) / Convert.ToDouble( files.Count)) * 100;
    //        progressCallback(Convert.ToInt32( Math.Round(d,0)), to.ToString());

    //            }
    //            catch (Exception e)
    //            {
    //                AggiungiLog("548:" + e.Message.ToString());

    //            }
    //        }
    //        progressCallback(100,"");



    //    }
    //}








