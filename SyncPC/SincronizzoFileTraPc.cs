﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace SyncPC
{



        public partial class SincronizzoFileTraPc : Form
        {





        String nva_PathBaseDirRemota;
        String nva_PathBaseDirLocale;
        String nva_NomeDirectoryStadio;
        String nva_IpRemoto;
        String nva_IpLocale;
        String nva_NomePcLocale;
        String nva_NomePcRemoto;

        String nva_PathCompletaBaseDirRemota;
        String nva_PathCompletaBaseDirLocale;
        Boolean IsOnline = false;
        Boolean IsMaster = false;
        Dictionary<String, DateTime> ListaFilesDaCopiare = new Dictionary<String, DateTime>();
        Timer t = new Timer();
        int DurataCountdownSecondi = 1;
        int ContatoreSecondi = 1;
        DataTable dt = new DataTable();
        Boolean IsPcAttivo=false;

        int DurataIntervalloControlloConnessione = 50;
        int DurataIntervalloControlloPriorita = 10;

        int IntervalloControlloConnessione = 0;
        int IntervalloControlloPriorita = 0;

        public SincronizzoFileTraPc()
        {
            InitializeComponent();


            dgStorico.Columns["nva_Esito"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dgStorico.Columns["nva_DescrizioneCompleta"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dgStorico.AutoGenerateColumns = false;
            try
            {

                string line;

          
                System.IO.StreamReader file = new System.IO.StreamReader(@"c:\parametriPC.txt");
                while ((line = file.ReadLine()) != null)
                {

                    line = line.ToString().ToLower().Replace(" ", "").Replace(";", "");
                    if ((line.Length > 0) && (line.Contains("=")))
                    {
                        string[] chiaveval = line.Split('=');
                        string chiave = chiaveval[0].ToLower();
                        string val = chiaveval[1];

                        if (chiave == "identificazionepc")
                        {
                            StaticStartConfig.int_IdentificazionePC = Convert.ToInt32(val);
                        }
                        if (chiave == "idstadio")
                        {
                            StaticStartConfig.ID_int_Stadio = Convert.ToInt32(val);
                        }
                        Console.WriteLine(line);
                    }

                }

            

                DataRow drStadio = DB.GetDataFromSQL("select * from tb_Stadi where Id= " + StaticStartConfig.ID_int_Stadio).Rows[0];

                nva_NomeDirectoryStadio = drStadio["nva_NomeDirectory"].ToString();

                DataRow drPcLocale = DB.GetDataFromSQL("select * from tb_Pc where Id= " + StaticStartConfig.int_IdentificazionePC).Rows[0];
                DataRow drPcRemoto = DB.GetDataFromSQL("select * from tb_Pc where Id <> " + StaticStartConfig.int_IdentificazionePC).Rows[0];
                if (Convert.ToInt32(drPcLocale["tin_IsMaster"]) == 1)
                {
                    IsPcAttivo = true;
                }


                nva_IpRemoto = drPcRemoto["nva_IP"].ToString();
                nva_PathBaseDirLocale = @"C:\NGMPlay\publish\wwwroot";
                nva_PathBaseDirRemota = @"\\" + nva_IpRemoto ;

                nva_PathCompletaBaseDirLocale = @"C:\NGMPlay\publish\wwwroot\video\"+ nva_NomeDirectoryStadio+@"\";
                nva_PathCompletaBaseDirRemota = @"\\" + nva_IpRemoto + @"\video\"+ nva_NomeDirectoryStadio + @"\";

                ControllaConsistenzaDir();
                t.Interval = 1000;
                t.Tick += T_Tick;


                RiavviaTimer();

            }
            catch (Exception e)
            {
                using (StreamWriter sw = File.AppendText(DateTime.Now.ToString("yyyy-MM-dd") + ".txt"))
                {
                    sw.WriteLine(DateTime.Now.ToString(@"hh\:mm\:ss\:fff") + " " + "SincronizzoFileTraPc" + ": " + "catch 125: " + e.Message.ToString());
                }

                // MessageBox.Show("preavviso dettagli errore");
                // MessageBox.Show(e.Message.ToString());
            }
          
        }


        private void Nascondi(object sender, EventArgs e)
        {
            this.Hide();
        }



        private void T_Tick(object sender, EventArgs e)
        {
            

            ContatoreSecondi = ContatoreSecondi - 1;
            //  lbTimer.Text = ContatoreSecondi.ToString()+" sec";
            if (ContatoreSecondi == 0)
            {
                t.Enabled = false;

                //  dgStorico.DataSource = Storico;




                /*
                //try
                //{
                //    DB.EseguiSQL("update pcL set tin_IsMaster = pcR.tin_IsMaster, dtt_Modifica = pcR.dtt_Modifica, nva_IP=pcR.nva_IP  from tb_PC pcL join [" + nva_IpRemoto + "].dbo.tb_Pc pcR on pcR.nva_Nome=pcL.nva_Nome where pcR.dtt_Modifica > pcL.dtt_Modifica ");
                //    DB.EseguiSQL("update pcR set tin_IsMaster = pcL.tin_IsMaster, dtt_Modifica = pcL.dtt_Modifica, nva_IP=pcL.nva_IP  from [" + nva_IpRemoto + "].dbo.tb_PC pcR join tb_Pc pcL on pcR.nva_Nome=pcL.nva_Nome where pcL.dtt_Modifica > pcR.dtt_Modifica ");
                //    lbStatoConnessioneRemota.Text="OK";
                //}catch(Exception ex)
                //{
                //    lbStatoConnessioneRemota.Text = "Errore";
                //}


                ////lbTimer.Text = "sp_TrovaNuoviFileSorgenti";
                //ListaFilesDaCopiare =  sp_TrovaNuoviFileSorgenti();

                //if (IsOnline)
                //{
                //    //lbTimer.Text = "exec sp_SyncDB_AggiornaDBLocaleConNuoviRecord";
                //    DB.EseguiSQL("exec sp_SyncDB_AggiornaDBLocaleConNuoviRecord");
                //    //lbTimer.Text = "AvviaCopiaFileTraPC";
                //    AvviaCopiaFileTraPC(ListaFilesDaCopiare);
                //}

                //if ((!IsOnline) || (IsMaster))
                //{
                //    //lbTimer.Text = "exec sp_GetHashComplessivoTutteLeSceneDaFileSorgenti";
                //    DB.EseguiSQL("exec sp_GetHashComplessivoTutteLeSceneDaFileSorgenti");
                //}

                ////lbTimer.Text = "generazione scene";
                //AvviaGenerazioneScene();

                //if (IsOnline)
                //{
                //    //lbTimer.Text = "TrovaTuttiIFileDaAcquisireInSpecificaCartella scene";
                //    ListaFilesDaCopiare = TrovaTuttiIFileDaAcquisireInSpecificaCartella("Scene");
                //    //lbTimer.Text = "AvviaCopiaFileTraPC scene";
                //    AvviaCopiaFileTraPC(ListaFilesDaCopiare);

                //    //lbTimer.Text = "TrovaTuttiIFileDaAcquisireInSpecificaCartella gif250";
                //    ListaFilesDaCopiare = TrovaTuttiIFileDaAcquisireInSpecificaCartella("gif250");
                //    //lbTimer.Text = "AvviaCopiaFileTraPC gif250";
                //    AvviaCopiaFileTraPC(ListaFilesDaCopiare);
                //}
                */


                if (cb_auto.Checked)
                {
                    Avvia();
                }


                t.Enabled = true;
                RiavviaTimer();
            }

        }





        private void Avvia()
        {
            IntervalloControlloConnessione= IntervalloControlloConnessione -1;
            if (IntervalloControlloConnessione < 0)
            {
                IntervalloControlloConnessione = DurataIntervalloControlloConnessione;
                ControlliPreliminari();
            }

            IntervalloControlloPriorita = IntervalloControlloPriorita - 1;
            if (IntervalloControlloPriorita < 0)
            {
                IntervalloControlloPriorita = DurataIntervalloControlloPriorita;
                ImpostaPriorita();
            }
         
            DataTable operazioni = DB.GetDataFromSQL("sp_SyncGetAzioneDaFare " + Convert.ToInt16(IsOnline).ToString() + "," + Convert.ToInt16(IsMaster).ToString());

            if (operazioni.Rows.Count > 0)
            {
                if (operazioni.Rows[0][0].ToString().ToLower().StartsWith("sp_"))
                {
                    ChiamaProcedura(operazioni.Rows[0][0].ToString());
                }else{
                    ChiamaFunzione(operazioni.Rows[0][0].ToString());
                }
            }
            dgStorico.Rows.Clear();
            DataTable Storico = DB.GetDataFromSQL("sp_SyncGetUltimeOperazioni");
            foreach (DataRow r in Storico.Rows)
            {
                dgStorico.Rows.Add(r["nva_DataInizio"], r["nva_DataFine"], r["nva_Descrizione"], r["nva_Esito"], r["bit_Errore"]);
            }

        }


        private void ControllaConsistenzaDir()
        {
            if (!Directory.Exists(nva_PathCompletaBaseDirLocale + @"\Scene"))
                Directory.CreateDirectory(nva_PathCompletaBaseDirLocale + @"\Scene");

            if (!Directory.Exists(nva_PathCompletaBaseDirLocale + @"\Gif250"))
                Directory.CreateDirectory(nva_PathCompletaBaseDirLocale + @"\Gif250");
        }



        private void ChiamaFunzione(string NomeOperazione)
        {
            switch (NomeOperazione)
            {
            case "TrovaECopiaNuoviFileSorgenti":
                Task.Run(() => TrovaECopiaNuoviFileSorgenti());
                break;
            case "TrovaTuttiIFileDaAcquisireInSpecificaCartella_Scene":
                Task.Run(() => TrovaTuttiIFileDaAcquisireInSpecificaCartella_Scene());
            break;
            case "TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250":
                Task.Run(() => TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250());
            break;
            }
        }

        private void TrovaECopiaNuoviFileSorgenti()
        {
            int op = AggiornaStatoSyncAvvio("TrovaECopiaNuoviFileSorgenti");
            try
            {
                Dictionary<String, DateTime> obj = new Dictionary<String, DateTime>();
                DataTable dt = DB.GetDataFromSQL("exec sp_TrovaNuoviFileSorgenti "+ op.ToString());
                foreach (DataRow r in dt.Rows)
                {
                    obj.Add("sorgenti/" + r["nva_NomeFile"].ToString(), Convert.ToDateTime(r["dtt_UltimaScritturaSulFile"]));
                }
                AvviaCopiaFileTraPC(obj);
                AggiornaStatoSyncTermine("TrovaECopiaNuoviFileSorgenti", "OK");
            }catch (Exception e){
                AggiornaStatoSyncTermine("TrovaECopiaNuoviFileSorgenti", e.Message,true);
            }
        }






        private void TrovaTuttiIFileDaAcquisireInSpecificaCartella_Scene()
        {
            AggiornaStatoSyncAvvio("TrovaTuttiIFileDaAcquisireInSpecificaCartella_Scene");
            try { 
                Dictionary<String, DateTime> obj = TrovaTuttiIFileDaAcquisireInSpecificaCartella("Scene");
                dg_Copie.Rows.Add(DateTime.Now.ToString("yyyyMMddHHmmssfff"), "(300)Trovo i file in cartella scene: ");

                AvviaCopiaFileTraPC(obj);
                AggiornaStatoSyncTermine("TrovaTuttiIFileDaAcquisireInSpecificaCartella_Scene", "OK");
            }catch (Exception e) {
                AggiornaStatoSyncTermine("TrovaTuttiIFileDaAcquisireInSpecificaCartella_Scene", e.Message, true);
                dg_Copie.Rows.Add(DateTime.Now.ToString("yyyyMMddHHmmssfff"), "(305): "+e.Message.ToString());

            }
        }





        private void TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250()
        {

            AggiornaStatoSyncAvvio("TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250");
            try
            {
                Dictionary<String, DateTime> obj = TrovaTuttiIFileDaAcquisireInSpecificaCartella("gif250");
                AvviaCopiaFileTraPC(obj);
                AggiornaStatoSyncTermine("TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250", "OK");
            }
            catch (Exception e)
            {
                AggiornaStatoSyncTermine("TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250", e.Message, true);
            }


        }

        private void ChiamaProcedura(String NomeOperazione)
        {

            Task.Run(() => ChiamaProceduraIndicandoInizioEFine(NomeOperazione) );


        }


        private void ChiamaProceduraIndicandoInizioEFine(String NomeOperazione)
        {
            int op = AggiornaStatoSyncAvvio(NomeOperazione);
            DB.EseguiSQL(NomeOperazione+' '+ op.ToString());
           AggiornaStatoSyncTermine(NomeOperazione, "OK");

        }



        //public async void AvviaGenerazioneScene()
        //{
        //  int rimanenti = 0;

        // //   while (rimanenti > 0)
        ////    {
        //        rimanenti = Convert.ToInt32(DB.GetDataFromSQL("sp_SperimentoGenerazioneComposizioneScena_v11").Rows[0][0]);


        //        //lbTimer.Text = "rimanenti " + rimanenti.ToString();
        //   // }


        //}

        private int AggiornaStatoSyncAvvio(string Operazione)
        {

            DataTable dt = DB.GetDataFromSQL("sp_SyncSettaAvvioFineoperazione  '" + Operazione + "'");

            return Convert.ToInt32(dt.Rows[0][0]);

        }
        private void AggiornaStatoSyncTermine(string Operazione,string Esito, Boolean Errore = false)
        {
            if (Errore)
            {
                DB.EseguiSQL("sp_SyncSettaAvvioFineoperazione  '" + Operazione + "','" + Esito.Replace("'", "") + "',1");

            }
            else
            {
                DB.EseguiSQL("sp_SyncSettaAvvioFineoperazione  '" + Operazione + "','" + Esito.Replace("'", "") + "'");

            }

        }

        private Boolean InviaPing(string ip)
        {
            string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes(data);

            int timeout = 10000;
            Ping pingSender = new Ping();

            PingOptions options = new PingOptions(64, true);

            PingReply reply = pingSender.Send(ip, timeout, buffer, options);

            if (reply.Status == IPStatus.Success)
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        private void ControlliPreliminari()
        {
            IsOnline = false;
            IsMaster = false;
            try { 

                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
                //lbTimer.Text = "drPcLocale";
                DataRow drPcLocale = DB.GetDataFromSQL("select isnull(tin_IsMaster,0) tin_IsMaster, nva_IP , id,nva_Nome, dtt_Modifica from tb_Pc where Id= " + StaticStartConfig.int_IdentificazionePC).Rows[0];

                //lbTimer.Text = "drPcRemoto";
                DataRow drPcRemoto = DB.GetDataFromSQL("select  isnull(tin_IsMaster,0) tin_IsMaster, nva_IP , id,nva_Nome , dtt_Modifica from tb_Pc where Id <> " + StaticStartConfig.int_IdentificazionePC).Rows[0];

                nva_NomePcRemoto = drPcRemoto["nva_Nome"].ToString();
                nva_NomePcLocale = drPcLocale["nva_Nome"].ToString();

                nva_IpRemoto = drPcRemoto["nva_IP"].ToString();
                nva_IpLocale = drPcLocale["nva_IP"].ToString();

                IsMaster = Convert.ToBoolean(drPcLocale["tin_IsMaster"]);

                String nva_IpLocaleReale = localIPs[(localIPs.Count() - 1)].ToString();

                if ((nva_IpLocaleReale != nva_IpLocale)&&(!nva_IpLocaleReale.Contains("127.0.0.1")))
                {
                   //// MessageBox.Show("Ip effettivo diverso da quello archiviato! (effettivo: " + nva_IpLocaleReale + " - archiviato: " + nva_IpLocale + ")");
           
            
            
                }

                //  public System.Net.NetworkInformation.PingReply

                if (!InviaPing(nva_IpRemoto))
                {
                   // pbStatusLink.Image = Properties.Resources._002_broken_link;
                    DB.EseguiSQL("update tb_PC set tin_IsOnline = 0, dtt_UltimoControlloOnline = getdate() where nva_IP='" + nva_IpLocale + "'");
                    pbStatusLink.BackColor = Color.Red;
                }
                else
                {

  
                    try
                    {
                        //lbTimer.Text = "proc146";
                        DB.EseguiSQL("update pcR set tin_IsMaster = pcL.tin_IsMaster, dtt_Modifica = pcL.dtt_Modifica, nva_IP=pcL.nva_IP  from [" + nva_IpRemoto + "].[NgmStudioDevelop].dbo.tb_PC pcR join tb_Pc pcL on pcR.nva_Nome=pcL.nva_Nome where pcL.dtt_Modifica > pcR.dtt_Modifica;" +
                                "update pcL set tin_IsMaster = pcR.tin_IsMaster, dtt_Modifica = pcR.dtt_Modifica, nva_IP=pcR.nva_IP  from tb_PC pcL join [" + nva_IpRemoto + "].[NgmStudioDevelop].dbo.tb_Pc pcR on pcR.nva_Nome=pcL.nva_Nome where pcR.dtt_Modifica > pcL.dtt_Modifica;");
                        //lbTimer.Text = "proc150";
                        DB.EseguiSQL("update tb_PC set tin_IsOnline = 1, dtt_UltimoControlloOnline = getdate() where nva_IP='" + nva_IpLocale + "'");

                        //lbStatoConnessioneRemota.Text = "OK";

                        IsOnline = true;
                        pbStatusLink.Image = Properties.Resources._001_link;

                        pbStatusLink.BackColor = PictureBox.DefaultBackColor;
                    }
                    catch (Exception ex)
                    {
                        pbStatusLink.Image = Properties.Resources._002_broken_link;

                        DB.EseguiSQL("update tb_PC set tin_IsOnline = 0, dtt_UltimoControlloOnline = getdate() where nva_IP='" + nva_IpLocale + "'");

                        pbStatusLink.BackColor = Color.Red;
                    }
                }

                lbDataOraControlloConnessione.Text = DateTime.Now.ToString();

                gbLocale.Text = "QUESTO PC: " + nva_NomePcLocale;
                gbRemoto.Text = "PC REMOTO: " + nva_NomePcRemoto;


                lbIpPcRemoto.Text = nva_IpRemoto;
                lbIpPcLocale.Text = nva_IpLocale;


                if (((Convert.ToInt32(drPcLocale["tin_IsMaster"]) + Convert.ToInt32(drPcRemoto["tin_IsMaster"])) > 1) || ((Convert.ToInt32(drPcLocale["tin_IsMaster"]) + Convert.ToInt32(drPcRemoto["tin_IsMaster"])) == 0))
                {
                    lbTipoPcLocale.Text = "Errore!";
                    lbTipoPcRemoto.Text = "Errore!";
                }
                else
                {
                    if (IsMaster)
                    {
                        lbTipoPcLocale.Text = "Master";
                        lbTipoPcRemoto.Text = "Backup";
                    }
                    else
                    {
                        lbTipoPcLocale.Text = "Backup";
                        lbTipoPcRemoto.Text = "Master";
                    }
                }

                dgFileMancanti.Rows.Clear();
                DataTable FileMancanti = DB.GetDataFromSQL("exec sp_GetListaFileMancanti");
                foreach (DataRow r in FileMancanti.Rows)
                {
                    dgFileMancanti.Rows.Add(r["nva_NomeFile"]);
                }
            }catch (Exception e){
               // MessageBox.Show("(569)" + e.Message.ToString());
            }

        }




        public async void AvviaCopiaFileTraPC(Dictionary<String, DateTime> lf)
        {
            try {
                dg_Copie.Rows.Add(DateTime.Now.ToString("yyyyMMddHHmmssfff"), "Avvio copia");
                //progressBar1.Value = 0;
                await CopyFiles(lf, nva_PathCompletaBaseDirRemota, nva_PathCompletaBaseDirLocale);
            }catch (Exception e){
               // MessageBox.Show("(569)" + e.Message.ToString());
            }
        }


        public void ImpostaPriorita()
        {
            Process[] processlist = Process.GetProcesses();
            try{
                foreach (Process theprocess in processlist){
                    if (theprocess.ProcessName.Contains("chrom")){
                        if (theprocess.PriorityClass != System.Diagnostics.ProcessPriorityClass.High){
                            theprocess.PriorityClass = System.Diagnostics.ProcessPriorityClass.High;
                        }
                    }
                }
            }catch (Exception e){
               // MessageBox.Show("(569)" + e.Message.ToString());
            }
        }




        //void AvanzaProgressBar(int prog,string s)
        //{




        //    if(s!=""){

        //        dgView.Rows.Add( DateTime.Now.ToString("yyyyMMddHHmmssfff"), s );
        //        dgView.Sort(DataOra, ListSortDirection.Descending);
        //        if(dgView.Rows.Count>1000)
        //            dgView.Rows.RemoveAt(dgView.Rows.Count-1);




        //    }



        //    lbOperazioneSvolta.Text = "copio: " + s;
        //    progressBar1.Value = prog;

        //    if (prog == 100)
        //    {


        //      //Task.Run(() =>
        //      //  {
        //      //      AggiungiLog("exec sp_ControllaFileAcquisisciHashFileTutteLeScene");
        //      //      DB.EseguiSQL("exec sp_ControllaFileAcquisisciHashFileTutteLeScene");
        //      //      AggiungiLog("RiavviaTimer()");
        //      //      RiavviaTimer();
        //      //  });


        //       // progressBar1.Value = 0;
        //    //    
        //    }
        //}





        void RiavviaTimer(){
            //lbOperazioneSvolta.Text = "-";
            ContatoreSecondi = DurataCountdownSecondi;
            t.Enabled=true;
            t.Start();
        }



        //public async void TrovaTuttiIFileDaAcquisire()
        //{



        //    ListaFilesDaCopiare.Clear();

        //    SincronizzaFileSorgenti();




        //   // TrovaTuttiIFileDaAcquisireInSpecificaCartella("Scene");
        //  //  TrovaTuttiIFileDaAcquisireInSpecificaCartella("gif250");
        //    //TrovaTuttiIFileDaAcquisireInSpecificaCartella("Sorgenti");

        //}






            /*

            private void TrovaTuttiIFileDaAcquisireInSpecificaCartella(String StrCartella)
            {
                String Tabella;
                if (IsPcAttivo)
                {
                    Tabella = "tb_LogFileSync";
                }
                else
                {
                    Tabella = "[" + nva_IpRemoto + "].[NgmStudioDevelop].dbo.tb_LogFileSync";
                }


                Int64 i = 0;
                DirectoryInfo DirInfoCartellaLocale = new DirectoryInfo(Path.Combine(nva_PathCompletaBaseDirLocale, StrCartella));
                DirectoryInfo DirInfoCartellaRemota = new DirectoryInfo(Path.Combine(nva_PathCompletaBaseDirRemota, StrCartella));


                foreach (FileInfo FileInfoRemoto in DirInfoCartellaRemota.GetFiles())
                {
                    i = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssfff")) + 1;

                    String HashFileRemoto = BytesToString(GetHashSha256(FileInfoRemoto.FullName));


                    DataTable dtFile = DB.GetDataFromSQL("select * from " + Tabella + " where nva_PathFile like '" + StrCartella + "/" + FileInfoRemoto.Name + "' ");

                    if (dtFile.Rows.Count == 0)
                    {
                        DB.EseguiSQL("insert into " + Tabella + " (nva_PathFile,nva_HashPC1,nva_HashPC2,)");

                    }

                    if (!FileInfoRemoto.Name.ToLower().Contains("_tmp.mp4"))
                    {
                        if (File.Exists(Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name)))
                        {
                            string StringaFileLocale = Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name);
                            FileInfo FileInfoLocale = new FileInfo(StringaFileLocale);
                            String HashFileLocale = BytesToString(GetHashSha256(FileInfoLocale.FullName));
                            if(HashFileRemoto != HashFileLocale)
                            {
                                dgControlli.Rows.Add(i, FileInfoRemoto.FullName, FileInfoLocale.FullName, UltimaModificaRemoto, UltimaModificaLocale);
                                dgControlli.Rows[dgControlli.Rows.Count - 1].DefaultCellStyle.BackColor = Color.Red;
                                ListaFilesDaCopiare.Add(FileInfoRemoto.FullName, FileInfoLocale.FullName);
                            }

                        }
                        else
                        {
                            dgControlli.Rows.Add(i, FileInfoRemoto.FullName, Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name), UltimaModificaRemoto, "File non presente localmente");
                            dgControlli.Rows[dgControlli.Rows.Count - 1].DefaultCellStyle.BackColor = Color.Red;

                            ListaFilesDaCopiare.Add(FileInfoRemoto.FullName, Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name));
                        }
                    }



                    dgControlli.Sort(DataOraIncrementale, ListSortDirection.Descending);
                    if (dgControlli.Rows.Count > 20)
                        dgControlli.Rows.RemoveAt(dgControlli.Rows.Count - 1);


                }
            }

        */










        private Dictionary<String, DateTime> TrovaTuttiIFileDaAcquisireInSpecificaCartella(String StrCartella)
        {
           Dictionary<String, DateTime> lf = new Dictionary<String, DateTime>();
            try{
              


                Int64 i = 0;

                DirectoryInfo DirInfoCartellaLocale = new DirectoryInfo(Path.Combine(nva_PathCompletaBaseDirLocale, StrCartella));
                DirectoryInfo DirInfoCartellaRemota = new DirectoryInfo(Path.Combine(nva_PathCompletaBaseDirRemota, StrCartella));

                // // MessageBox.Show(DirInfoCartellaLocale.FullName);

                foreach (FileInfo FileInfoRemoto in DirInfoCartellaRemota.GetFiles())
                {
                    i = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssfff")) + 1;
                    DateTime CreazioneRemoto = FileInfoRemoto.CreationTime;
                    DateTime UltimaModificaRemoto = FileInfoRemoto.LastWriteTime;

                    if (!FileInfoRemoto.Name.ToLower().Contains("_tmp.mp4")){

                        if (File.Exists(Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name))){
                            string StringaFileLocale = Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name);

                            FileInfo FileInfoLocale = new FileInfo(StringaFileLocale);

                            DateTime UltimaModificaLocale = FileInfoLocale.LastWriteTime;

                            try
                            {
                                if (UltimaModificaRemoto > UltimaModificaLocale){

                                    lf.Add(Path.Combine(StrCartella + "/" + FileInfoRemoto.Name), UltimaModificaRemoto);
                                    dg_Copie.Rows.Add(DateTime.Now.ToString("yyyyMMddHHmmssfff"), "(769)Aggiungo: " + Path.Combine(StrCartella + "/" + FileInfoRemoto.Name));
                                }
                            }catch (Exception ex){

                               // MessageBox.Show(ex.Message);
                            }
                        }else{
                            lf.Add(Path.Combine(StrCartella + "/" + FileInfoRemoto.Name), UltimaModificaRemoto);
                            dg_Copie.Rows.Add(DateTime.Now.ToString("yyyyMMddHHmmssfff"), "(781)Aggiungo: " + Path.Combine(StrCartella + "/" + FileInfoRemoto.Name));
                        }
                    }
                }
            }catch(Exception e){
               // MessageBox.Show("791:" + e.Message.ToString());
            }

            return lf;
        }




        //void AggiungiLog(String Log)
        //{
        //    dgLog.Rows.Add(DateTime.Now.ToString("yyyyMMddHHmmssfff"),Log);

        //    dgLog.Sort(DataOraLog, ListSortDirection.Descending);

        //    if (dgLog.Rows.Count > 1000)
        //        dgLog.Rows.RemoveAt(dgLog.Rows.Count - 1);
        //}


        //private static SHA256 Sha256 = SHA256.Create();


        //private static byte[] GetHashSha256(string filename)
        //{
        //    using (FileStream stream = File.OpenRead(filename))
        //    {
        //        return Sha256.ComputeHash(stream);
        //    }
        //}

        //public static string BytesToString(byte[] bytes)
        //{
        //    string result = "";
        //    foreach (byte b in bytes) result += b.ToString("x2");
        //    return result;
        //}

        private void btn_PcAttivo_Click(object sender, EventArgs e)
        {
            try{
                DB.EseguiSQL("exec [sp_ImpostaQuestoPCComeMaster]");
                //lbStatoConnessioneRemota.Text = "OK";
            }catch (Exception ex){
               // MessageBox.Show("832:" + ex.Message.ToString());
            }
        }





        public async Task CopyFiles(Dictionary<string, DateTime> files, String nva_PathCompletaBaseDirRemota, string nva_PathCompletaBaseDirLocale)
        {

            try
            {
                for (int x = 0; x < files.Count; x++)
                {

                    try
                    {

                        var item = files.ElementAt(x);
                        //lbTimer.Text = "File avvio copia " +item.Key  ;

                        String from = nva_PathCompletaBaseDirRemota + item.Key;
                        String to = nva_PathCompletaBaseDirLocale + item.Key;

                        DateTime DataOraModificaFinale = item.Value;

                        using (var outStream = new FileStream(to, FileMode.Create, FileAccess.Write, FileShare.Read))
                        {
                            using (var inStream = new FileStream(from, FileMode.Open, FileAccess.Read, FileShare.Read))
                            {
                                await inStream.CopyToAsync(outStream);
                            }

                        }

                        File.SetLastWriteTime(to, DataOraModificaFinale);

                        double d = (Convert.ToDouble((x + 1)) / Convert.ToDouble(files.Count)) * 100;
                        dg_Copie.Rows.Add(DateTime.Now.ToString("yyyyMMddHHmmssfff"), "OK: " + nva_PathCompletaBaseDirRemota + item.Key);
                        // GestioneLog.

                        //progressCallback(Convert.ToInt32(Math.Round(d, 0)), to.ToString());

                    }
                    catch (Exception e)
                    {

                        dg_Copie.Rows.Add(DateTime.Now.ToString("yyyyMMddHHmmssfff"), "(891) Errore: " + e.Message.ToString());

                       // MessageBox.Show("891:" + e.Message.ToString());
                        //lbTimer.Text=("548:" + e.Message.ToString());

                    }
                }
            }catch (Exception e){
                // MessageBox.Show("908:" + e.Message.ToString());

                dg_Copie.Rows.Add(DateTime.Now.ToString("yyyyMMddHHmmssfff"), "(864) Errore: " + e.Message.ToString());

            }

            //progressCallback(100, "");

        }


        private void btn_avanzaManuale_Click(object sender, EventArgs e)
        {
            Avvia();
        }




        private void btn_SFolder_Click(object sender, EventArgs e)
        {

            if (ofolder_OpenFolderDialog.ShowDialog() == DialogResult.OK)
            {

            }

        }




        private void btn_SFile_Click(object sender, EventArgs e)
        {

            ofile_OpenFileDialog.Filter = "ZIP|*.zip";
            if (ofile_OpenFileDialog.ShowDialog() == DialogResult.OK)
            {

                //ZipFile.ExtractToDirectory(ofile_OpenFileDialog.FileName, extractPath);


            }

        }













    }

}



    //public static class Copier
    //{
    //    public static async Task CopyFiles(Dictionary<string, DateTime> files, String nva_PathCompletaBaseDirRemota,string nva_PathCompletaBaseDirLocale, Action<int,string> progressCallback)
    //    {



    //        for (int x = 0; x < files.Count; x++)
    //        {

    //            try
    //            {

              

    //            var item = files.ElementAt(x);
    //            String from = nva_PathCompletaBaseDirRemota+ item.Key ;
    //            String to = nva_PathCompletaBaseDirLocale + item.Key;


    //            DateTime DataOraModificaFinale = item.Value;



    //            using (var outStream = new FileStream(to, FileMode.Create, FileAccess.Write, FileShare.Read))
    //            {
    //                using (var inStream = new FileStream(from, FileMode.Open, FileAccess.Read, FileShare.Read))
    //                {
    //                    await inStream.CopyToAsync(outStream);
                        
    //                }
                    
    //            }

    //          File.SetLastWriteTime(to, DataOraModificaFinale);

    //        double d = (Convert.ToDouble((x + 1)) / Convert.ToDouble( files.Count)) * 100;
    //        progressCallback(Convert.ToInt32( Math.Round(d,0)), to.ToString());

    //            }
    //            catch (Exception e)
    //            {
    //                AggiungiLog("548:" + e.Message.ToString());

    //            }
    //        }
    //        progressCallback(100,"");



    //    }
    //}








