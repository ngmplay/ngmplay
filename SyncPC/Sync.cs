﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SyncPC.Utility;


namespace SyncPC
{
    public partial class Sync : Form
    {

        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenu contextMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.MenuItem menuItem4;



        //String nva_PathBaseDirRemota;
        //String nva_PathBaseDirLocale;
        String nva_NomeDirectoryStadio;
        String nva_IpRemoto;
        String nva_IpLocale;
        String nva_NomePcLocale;
        String nva_NomePcRemoto;

        String nva_PathCompletaBaseDirRemotaVideo;
        String nva_PathCompletaBaseDirLocaleVideo;


        Boolean IsOnline = false;
        Boolean IsMaster = false;
        Dictionary<String, DateTime> ListaFilesDaCopiare = new Dictionary<String, DateTime>();
        Timer t = new Timer();
        int DurataCountdownSecondi = 1;
        int ContatoreSecondi = 1;
        DataTable dt = new DataTable();
        Boolean IsPcAttivo = false;
        int DurataIntervalloControlloConnessione = 50;
        int DurataIntervalloControlloPriorita = 10;
        int IntervalloControlloConnessione = 0;
        int IntervalloControlloPriorita = 0;

        int IntervalloControlloRisultati = 15;
        int ContatoreControlloRisultati = 0;

        int IntervalloPuliziaDB = 30;
        int ContatorePuliziaDB = 8;


        DateTime dttPingEseguito = DateTime.MinValue;
        Boolean risultatoPing = false;


        public Sync()
        {
            try
            {
                Log.AggiungiLog("InitializeComponent", "Sync-Sync");
                InitializeComponent();
                
                this.ShowInTaskbar = false;
                this.Opacity = 0;

                SincronizzaOrologio();

                Log.AggiungiLog("ParametriDaFile", "Sync-Sync");
                ParametriDaFile.GetParametri();
                Log.AggiungiLog("ParametriDaDB", "Sync-Sync");
                ParametriDaDB.GetParametri();



                Log.AggiungiLog("Update Storico operazioni", "Sync-Sync");
                DB.EseguiSQL("update tb_SyncStoricoOperazioni set dtt_Fine = getdate(), bit_errore =1 , nva_Esito = nva_Esito + ' nuovo riavvio' where dtt_Fine is null ");

                Log.AggiungiLog("Avvio processo", "Sync-Sync");
                ImpostaTaskBarProcess();
                LetturaConfig();
                ControlloDirectory.ControllaConsistenzaDir(nva_PathCompletaBaseDirLocaleVideo);
                t.Interval = 1000;
                t.Tick += T_Tick;

                RiavviaTimer();
            }
            catch ( Exception e)
            {

                Log.AggiungiErrore("catch 100: " + e.Message.ToString(), "Sync-Sync");


            }

            

        }




        private void SincronizzaOrologio()
        {
            /*

            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = "orologio.bat";
            p.StartInfo.Verb = "runas";
            p.Start();
            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();

            Log.AggiungiLog(output, "SincronizzaOrologio");
            */
            // File.Delete(batFileName);


        }



        private void T_Tick(object sender, EventArgs e)
        {

          
            ContatoreSecondi = ContatoreSecondi - 1;
            if (ContatoreSecondi == 0)
            {

                t.Enabled = false;
                Avvia();
                t.Enabled = true;
                RiavviaTimer();
            }



            ContatorePuliziaDB = ContatorePuliziaDB - 1;
            if (ContatorePuliziaDB <= 0)
            {
                ContatorePuliziaDB = IntervalloPuliziaDB;
                try
                {
                    DB.EseguiSQL("exec sp_Pulisci_DatiToUI");
                    DB.EseguiSQL("exec sp_Pulisci_DatiToPlayer");
                }
                catch (Exception ee)
                {
                    Log.AggiungiErrore(ee.Message, "SyncPC.Sync.T_Tick");
                }
            }




            if (fg.IsYes(ParametriDaDB.Parametro_nva_LiveScoreEnable)) 
            { 
                ContatoreControlloRisultati = ContatoreControlloRisultati - 1;
                if (ContatoreControlloRisultati <= 0)
                {
                    ContatoreControlloRisultati = IntervalloControlloRisultati;
                    if (dttPingEseguito < (DateTime.Now.AddMinutes(-5)))
                    {
                        risultatoPing = EseguiPing.PingGoogle();
                        Log.AggiungiLog("Eseguo Ping", "SyncPC.Sync.T_Tick");
                    }
                    if (risultatoPing){
                        try
                        {
                            ServiceGetDati.ArchiviaInDBRisultati();
                        }
                        catch (Exception ee)
                        {
                            Log.AggiungiErrore(ee.Message, "SyncPC.Sync.T_Tick");
                        }
                    }
                    else
                    {
                        Log.AggiungiLog("Ping senza successo", "SyncPC.Sync.T_Tick");
                    }
                }
            }



   

        }
        private void Avvia()
        {

            try
            {

            
            IntervalloControlloConnessione = IntervalloControlloConnessione - 1;
            if (IntervalloControlloConnessione < 0)
            {
                IntervalloControlloConnessione = DurataIntervalloControlloConnessione;
                ControlliPreliminari();
            }

            IntervalloControlloPriorita = IntervalloControlloPriorita - 1;
            if (IntervalloControlloPriorita < 0)
            {
                IntervalloControlloPriorita = DurataIntervalloControlloPriorita;
                //ImpostaPriorita();
            }

            DataTable operazioni = DB.GetDataFromSQL("sp_SyncGetAzioneDaFare " + Convert.ToInt16(IsOnline).ToString() + "," + Convert.ToInt16(IsMaster).ToString());


            if (operazioni.Rows.Count > 0)
            {
                String Procedura = operazioni.Rows[0][0].ToString();

                Log.AggiungiLog("Avvio procedura "+ Procedura, "Sync-Avvia");

                if (Procedura.ToLower().StartsWith("sp_"))
                {
                    ChiamaProcedura(Procedura);
                }
                else
                {
                    ChiamaFunzione(Procedura);
                }
            }
            else
            {
                
                DataTable operazioneInCorso = DB.GetDataFromSQL("sp_SyncGetAzioneInCorso " + Convert.ToInt16(IsOnline).ToString() + "," + Convert.ToInt16(IsMaster).ToString());
                
                if (operazioneInCorso.Rows.Count > 0)
                {
                    Log.AggiungiLog("In corso: " + operazioneInCorso.Rows[0][0], "Sync-Avvia");
                }
                else
                {
                    Log.AggiungiLog("Bloccato", "Sync-Avvia");
                }
                   
            }


            }
            catch (Exception e)
            {

                Log.AggiungiErrore(e.Message, "Sync-Avvia");


            }
            /*  
            DataTable Storico = DB.GetDataFromSQL("sp_SyncGetUltimeOperazioni");
            foreach (DataRow r in Storico.Rows)
            {
                Log.AggiungiLog("Processo inizio:'"+ r["nva_DataInizio"]+"' fine:'"+r["nva_DataFine"]+"' descrizione:" + r["nva_Descrizione"]+" eisto:"+ r["nva_Esito"]);
            }
            */
        }
        public async void AvviaCopiaFileTraPC(Dictionary<String, DateTime> lf)
        {
            try
            {
                Log.AggiungiLog("Copio file da: " + nva_PathCompletaBaseDirRemotaVideo + " a: "+ nva_PathCompletaBaseDirLocaleVideo, "Sync-AvviaCopiaFileTraPC");

                
                await CopyFiles(lf, nva_PathCompletaBaseDirRemotaVideo, nva_PathCompletaBaseDirLocaleVideo);
            }
            catch (Exception e)
            {
                Log.AggiungiErrore(e.Message, "Sync-AvviaCopiaFileTraPC");
            }
        }
        private void TrovaECopiaNuoviFileSorgenti()
        {
            Log.AggiungiLog("Avvio", "Sync-TrovaECopiaNuoviFileSorgenti");
            int op = AggiornaStatoSyncAvvio("TrovaECopiaNuoviFileSorgenti");
            try
            {
                Log.AggiungiLog("inizio", "Sync-TrovaECopiaNuoviFileSorgenti");

                Dictionary<String, DateTime> obj = new Dictionary<String, DateTime>();
                DataTable dt = DB.GetDataFromSQL("exec sp_TrovaNuoviFileSorgenti " + op.ToString());
                foreach (DataRow r in dt.Rows)
                {
                    Log.AggiungiLog("Trovato nuovo file sorgente: "+ r["nva_NomeFile"].ToString(), "Sync-TrovaECopiaNuoviFileSorgenti");
                    String fileNameKey = "sorgenti/" + r["nva_NomeFile"].ToString();
                    if (obj.ContainsKey(fileNameKey))
                    {
                        DateTime fileDate = obj[fileNameKey];
                        if (fileDate < Convert.ToDateTime(r["dtt_UltimaScritturaSulFile"]))
                            obj[fileNameKey] = Convert.ToDateTime(r["dtt_UltimaScritturaSulFile"]);
                    }
                    else
                        obj.Add(fileNameKey, Convert.ToDateTime(r["dtt_UltimaScritturaSulFile"]));
                }

                //copio anche i files del maxischermo





                AvviaCopiaFileTraPC(obj);
                AggiornaStatoSyncTermine("TrovaECopiaNuoviFileSorgenti", "OK");
            }
            catch (Exception e)
            {
                AggiornaStatoSyncTermine("TrovaECopiaNuoviFileSorgenti", e.Message, true);
                Log.AggiungiErrore(e.Message, "Sync-TrovaECopiaNuoviFileSorgenti");
            }
        }
        private void ChiamaFunzione(string NomeOperazione)
        {
            try
            {
                switch (NomeOperazione)
                {
                    case "TrovaECopiaNuoviFileSorgenti":
                        Task.Run(() => TrovaECopiaNuoviFileSorgenti());
                        break;
                    case "TrovaTuttiIFileDaAcquisireInSpecificaCartella_Scene":
                        Task.Run(() => TrovaTuttiIFileDaAcquisireInSpecificaCartella_Scene());
                        break;
                    case "TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250":
                        Task.Run(() => TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250());
                        break;
                }
            }
            catch (Exception e)
            {
                Log.AggiungiErrore(e.Message, "Sync-ChiamaFunzione");
            }
        }
        void RiavviaTimer()
        {
            ContatoreSecondi = DurataCountdownSecondi;
            t.Enabled = true;
            t.Start();
        }
        private void ChiamaProcedura(String NomeOperazione)
        {
            Task.Run(() => ChiamaProceduraIndicandoInizioEFine(NomeOperazione));
        }
        private void ChiamaProceduraIndicandoInizioEFine(String NomeOperazione)
        {
            int op = AggiornaStatoSyncAvvio(NomeOperazione);
            DB.EseguiSQL(NomeOperazione + ' ' + op.ToString());
            AggiornaStatoSyncTermine(NomeOperazione, "OK");
        }
        private int AggiornaStatoSyncAvvio(string Operazione)
        {
            DataTable dt = DB.GetDataFromSQL("sp_SyncSettaAvvioFineoperazione  '" + Operazione + "'");
            return Convert.ToInt32(dt.Rows[0][0]);
        }
        private void AggiornaStatoSyncTermine(string Operazione, string Esito, Boolean Errore = false)
        {
            if (Errore)
            {
                Log.AggiungiLog("ERRORE Operazione:" + Operazione + " Esito:" + Esito, "Sync-AggiornaStatoSyncTermine");
                DB.EseguiSQL("sp_SyncSettaAvvioFineoperazione  '" + Operazione + "','" + Esito.Replace("'", "") + "',1");
            }
            else
            {
                Log.AggiungiLog("Operazione:" + Operazione + " Esito:" + Esito, "Sync-AggiornaStatoSyncTermine");
                DB.EseguiSQL("sp_SyncSettaAvvioFineoperazione  '" + Operazione + "','" + Esito.Replace("'", "") + "'");
            }
        }
        private void LetturaConfig()
        {


            if(ParametriDaFile.int_IdentificazionePC == 0)
            {
                Log.AggiungiLog("ParametriDaFile.int_IdentificazionePC == 0", "Sync-LetturaConfig");
                this.Close();
            }

            if (ParametriDaFile.ID_int_Stadio==0)
            {
                Log.AggiungiLog("ParametriDaFile.ID_int_Stadi == 0", "Sync-LetturaConfig");
                this.Close();
            }

            DataTable dtStadio = DB.GetDataFromSQL("select * from tb_Stadi where Id= " + ParametriDaFile.ID_int_Stadio);
            DataRow drStadio;

            if (dtStadio.Rows.Count == 0)
            {
                Log.AggiungiLog(@"nessuno stadio con l'id contenuto in c:\parametriPC.txt", "Sync-LetturaConfig");
                this.Close();
            }

            drStadio = dtStadio.Rows[0];
            nva_NomeDirectoryStadio = drStadio["nva_NomeDirectory"].ToString();

            DataRow drPcLocale = DB.GetDataFromSQL("select * from tb_Pc where Id= " + ParametriDaFile.int_IdentificazionePC).Rows[0];
            DataRow drPcRemoto = DB.GetDataFromSQL("select * from tb_Pc where Id <> " + ParametriDaFile.int_IdentificazionePC).Rows[0];
            
            if (Convert.ToInt32(drPcLocale["tin_IsMaster"]) == 1)
            {
                Log.AggiungiLog("PC MASTER", "Sync-LetturaConfig");
                IsPcAttivo = true;
            }


            nva_IpRemoto = drPcRemoto["nva_IP"].ToString();
          //  nva_PathBaseDirLocale = @"C:\NGMPlay\publish\wwwroot";
           // nva_PathBaseDirRemota = @"\\" + nva_IpRemoto;

            nva_PathCompletaBaseDirLocaleVideo = @"C:\NGMPlay\publish\wwwroot\video\" + nva_NomeDirectoryStadio + @"\";
            nva_PathCompletaBaseDirRemotaVideo = @"\\" + nva_IpRemoto + @"\wwwroot\video\" + nva_NomeDirectoryStadio + @"\";





          //  Log.AggiungiLog("nva_IpRemoto: "+ nva_IpRemoto, "Sync-LetturaConfig");
          //  Log.AggiungiLog("nva_PathBaseDirLocale: " + nva_PathBaseDirLocale, "Sync-LetturaConfig");
         //   Log.AggiungiLog("nva_PathBaseDirRemota: " + nva_PathBaseDirRemota, "Sync-LetturaConfig");
         //   Log.AggiungiLog("nva_PathCompletaBaseDirLocale: " + nva_PathCompletaBaseDirLocaleVideo, "Sync-LetturaConfig");
       //     Log.AggiungiLog("nva_PathCompletaBaseDirRemota: " + nva_PathBaseDirRemota, "Sync-LetturaConfig");

            IsPcAttivo = true;

            Utility.ControlloDirectory.ControllaConsistenzaDir(nva_PathCompletaBaseDirLocaleVideo);



        }
        private void ImpostaTaskBarProcess()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenu1 = new System.Windows.Forms.ContextMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            // Initialize contextMenu1
            this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] { this.menuItem1, this.menuItem2, this.menuItem3, this.menuItem4, });





            this.menuItem3.Index = 1;
            this.menuItem3.Text = "Chiudi";
            this.menuItem3.Click += new System.EventHandler(this.Chiudi_Click);


            this.menuItem4.Index = 2;
            this.menuItem4.Text = "Riavvia";
            this.menuItem4.Click += new System.EventHandler(this.Riavvia_Click);



            // Set up how the form should be displayed.
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Text = "NGM Studio Background Worker Utility";

            // Create the NotifyIcon.
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);

            // The Icon property sets the icon that will appear
            // in the systray for this application.

            Bitmap b = new Bitmap(Properties.Resources.adv);


            notifyIcon1.Icon = Icon.FromHandle(b.GetHicon());

            // The ContextMenu property sets the menu that will
            // appear when the systray icon is right clicked.
            notifyIcon1.ContextMenu = this.contextMenu1;

            // The Text property sets the text that will be displayed,
            // in a tooltip, when the mouse hovers over the systray icon.
            notifyIcon1.Text = "Form1 (NotifyIcon example)";
            notifyIcon1.Visible = true;

            // Handle the DoubleClick event to activate the form.
            notifyIcon1.Click += new System.EventHandler(this.notifyIcon1_DoubleClick);



        }
        private void AggiornaStato(string Stato)
        {
            this.menuItem2.Index = 0;
            this.menuItem2.Enabled = false;
            this.menuItem2.Text = Stato;
        }
        private void Riavvia_Click(object Sender, EventArgs e)
        {
            Log.AggiungiLog("Lancio riavvio", "Sync-Riavvia_Click");
            Process.Start("RestartApp.exe");
        }
        private void Chiudi_Click(object Sender, EventArgs e)
        {
            Log.AggiungiLog("Lancio chiusura", "Sync-Chiudi_Click");
            this.Close();
        }
        private void notifyIcon1_DoubleClick(object Sender, EventArgs e)
        {
            // Show the form when the user double clicks on the notify icon.
            Log.AggiungiLog("Lancio attivazione finestra", "Sync-notifyIcon1_DoubleClick");
            // Set the WindowState to normal if the form is minimized.
            if (this.WindowState == FormWindowState.Minimized)
                this.WindowState = FormWindowState.Normal;

            // Activate the form.
            this.Activate();
        }
        private void ControlliPreliminari()
        {
            IsOnline = false;
            IsMaster = false;
            try
            {
                Log.AggiungiLog("Controlli preliminari", "Sync-ControlliPreliminari");
                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
                Log.AggiungiLog("Identifico il PC come "+ ParametriDaFile.int_IdentificazionePC.ToString(), "Sync-ControlliPreliminari");
                DataRow drPcLocale = DB.GetDataFromSQL("select isnull(tin_IsMaster,0) tin_IsMaster, nva_IP , id,nva_Nome, dtt_Modifica from tb_Pc where Id= " + ParametriDaFile.int_IdentificazionePC).Rows[0];
                Log.AggiungiLog("377", "Sync-ControlliPreliminari");
                DataRow drPcRemoto = DB.GetDataFromSQL("select  isnull(tin_IsMaster,0) tin_IsMaster, nva_IP , id,nva_Nome , dtt_Modifica from tb_Pc where Id <> " + ParametriDaFile.int_IdentificazionePC).Rows[0];
             

                nva_NomePcRemoto = drPcRemoto["nva_Nome"].ToString();
                nva_NomePcLocale = drPcLocale["nva_Nome"].ToString();

                nva_IpRemoto = drPcRemoto["nva_IP"].ToString();
                nva_IpLocale = drPcLocale["nva_IP"].ToString();

                IsMaster = Convert.ToBoolean(drPcLocale["tin_IsMaster"]);

                String nva_IpLocaleReale = localIPs[(localIPs.Count() - 1)].ToString();

                if ((nva_IpLocaleReale != nva_IpLocale) && (!nva_IpLocaleReale.Contains("127.0.0.1")))
                {
                    Log.AggiungiLog("Errore IP effettivo diverso da IP archiviato in DB - Effettivo: "+ nva_IpLocaleReale+ " Archiviato in DB: "+ nva_IpLocaleReale, "Sync-ControlliPreliminari");
                }

                if (!EseguiPing.PingIP(nva_IpRemoto))
                {
            
                    DB.EseguiSQL("update tb_PC set tin_IsOnline = 0, dtt_UltimoControlloOnline = getdate() where nva_IP='" + nva_IpLocale + "'");
                    DB.EseguiSQL("update tb_PC set tin_IsOnline = 0 ");

                    Log.AggiungiLog("Errore PC remoto non correttamente connesso", "Sync-ControlliPreliminari");

                }
                else
                {


                    try
                    {
                        DB.EseguiSQL("update pcR set tin_IsMaster = pcL.tin_IsMaster, dtt_Modifica = pcL.dtt_Modifica, nva_IP=pcL.nva_IP  from [" + nva_IpRemoto + "].[NgmStudioDevelop].dbo.tb_PC pcR join tb_Pc pcL on pcR.nva_Nome=pcL.nva_Nome where pcL.dtt_Modifica > pcR.dtt_Modifica;" +
                                "update pcL set tin_IsMaster = pcR.tin_IsMaster, dtt_Modifica = pcR.dtt_Modifica, nva_IP=pcR.nva_IP  from tb_PC pcL join [" + nva_IpRemoto + "].[NgmStudioDevelop].dbo.tb_Pc pcR on pcR.nva_Nome=pcL.nva_Nome where pcR.dtt_Modifica > pcL.dtt_Modifica;");

                        DB.EseguiSQL("update tb_PC set tin_IsOnline = 1, dtt_UltimoControlloOnline = getdate() where nva_IP='" + nva_IpLocale + "'");
                        DB.EseguiSQL("update tb_PC set tin_IsOnline = 1 ");
                        IsOnline = true;

                        Log.AggiungiLog("Connessione con DB remoto ("+ nva_IpRemoto + ") effettuata correttamente", "Sync-ControlliPreliminari");
                    }
                    catch (Exception ex)
                    {
    
                        DB.EseguiSQL("update tb_PC set tin_IsOnline = 0, dtt_UltimoControlloOnline = getdate() where nva_IP='" + nva_IpLocale + "'");

                        Log.AggiungiLog("Errore Connessione con DB remoto (" + nva_IpRemoto + ") ", "Sync-ControlliPreliminari");
                    }
                }

                Log.AggiungiLog("QUESTO PC: " + nva_NomePcLocale+ " - PC REMOTO: " + nva_NomePcRemoto, "Sync-ControlliPreliminari");

                if (((Convert.ToInt32(drPcLocale["tin_IsMaster"]) + Convert.ToInt32(drPcRemoto["tin_IsMaster"])) > 1) || ((Convert.ToInt32(drPcLocale["tin_IsMaster"]) + Convert.ToInt32(drPcRemoto["tin_IsMaster"])) == 0))
                {
                    Log.AggiungiLog("Errore entrambi i pc sono configurati come master o backup", "Sync-ControlliPreliminari");
                }
                else
                {
                    if (IsMaster)
                    {
                        Log.AggiungiLog("PC locale MASTER - PC remoto BACKUP", "Sync-ControlliPreliminari");
                    }
                    else{
                        Log.AggiungiLog("PC locale BACKUP - PC remoto MASTER", "Sync-ControlliPreliminari");
                    }
                }


                DataTable FileMancanti = DB.GetDataFromSQL("exec sp_GetListaFileMancanti");
                foreach (DataRow r in FileMancanti.Rows)
                {
                    Log.AggiungiLog("ERRORE File mancante! "+r["nva_NomeFile"], "Sync-ControlliPreliminari");
                }
            }
            catch (Exception e)
            {
                Log.AggiungiLog("Catch 379: " + e.Message.ToString(), "Sync-ControlliPreliminari");
            }
        }
        private void TrovaTuttiIFileDaAcquisireInSpecificaCartella_Scene()
        {
            Log.AggiungiLog("Avvio", "Sync-TrovaTuttiIFileDaAcquisireInSpecificaCartella_Scene");
            AggiornaStatoSyncAvvio("TrovaTuttiIFileDaAcquisireInSpecificaCartella_Scene");
            try
            {
                Dictionary<String, DateTime> obj = TrovaTuttiIFileDaAcquisireInSpecificaCartella("Scene");
                
                AvviaCopiaFileTraPC(obj);
                AggiornaStatoSyncTermine("TrovaTuttiIFileDaAcquisireInSpecificaCartella_Scene", "OK");
            }
            catch (Exception e)
            {
                AggiornaStatoSyncTermine("TrovaTuttiIFileDaAcquisireInSpecificaCartella_Scene", e.Message, true);
                
            }
        }
        private void TrovaTuttiIFileDaAcquisireInSpecificaCartella_Sorgenti()
        {
            Log.AggiungiLog("Avvio", "Sync-TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250");
            AggiornaStatoSyncAvvio("TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250");
            try
            {
                Dictionary<String, DateTime> obj = TrovaTuttiIFileDaAcquisireInSpecificaCartella("gif250");
                AvviaCopiaFileTraPC(obj);
                AggiornaStatoSyncTermine("TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250", "OK");
            }
            catch (Exception e)
            {
                AggiornaStatoSyncTermine("TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250", e.Message, true);
            }


        }
        private void TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250()
        {
            Log.AggiungiLog("Avvio", "Sync-TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250");
            AggiornaStatoSyncAvvio("TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250");
            try
            {
                Dictionary<String, DateTime> obj = TrovaTuttiIFileDaAcquisireInSpecificaCartella("gif250");
                AvviaCopiaFileTraPC(obj);
                AggiornaStatoSyncTermine("TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250", "OK");
            }
            catch (Exception e)
            {
                AggiornaStatoSyncTermine("TrovaTuttiIFileDaAcquisireInSpecificaCartella_gif250", e.Message, true);
            }


        }
        private Dictionary<String, DateTime> TrovaTuttiIFileDaAcquisireInSpecificaCartella(String StrCartella)
        {
            Dictionary<String, DateTime> lf = new Dictionary<String, DateTime>();
            try
            {



                Int64 i = 0;

                DirectoryInfo DirInfoCartellaLocale = new DirectoryInfo(Path.Combine(nva_PathCompletaBaseDirLocaleVideo, StrCartella));
                DirectoryInfo DirInfoCartellaRemota = new DirectoryInfo(Path.Combine(nva_PathCompletaBaseDirRemotaVideo, StrCartella));
                Log.AggiungiLog("Locale:"+DirInfoCartellaLocale, "TrovaTuttiIFileDaAcquisireInSpecificaCartella");
                Log.AggiungiLog("Remota:" + DirInfoCartellaRemota, "TrovaTuttiIFileDaAcquisireInSpecificaCartella");
                // // MessageBox.Show(DirInfoCartellaLocale.FullName);

                foreach (FileInfo FileInfoRemoto in DirInfoCartellaRemota.GetFiles())
                {
                    i = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssfff")) + 1;
                    DateTime CreazioneRemoto = FileInfoRemoto.CreationTime;
                    DateTime UltimaModificaRemoto = FileInfoRemoto.LastWriteTime;

                    if (!FileInfoRemoto.Name.ToLower().Contains("_tmp.mp4"))
                    {
                        Log.AggiungiLog(FileInfoRemoto.Name.ToLower(), "TrovaTuttiIFileDaAcquisireInSpecificaCartella");
                        if (File.Exists(Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name)))
                        {
                            string StringaFileLocale = Path.Combine(DirInfoCartellaLocale.FullName, FileInfoRemoto.Name);
                            Log.AggiungiLog("localmente esistente:"+FileInfoRemoto.Name.ToLower(), "TrovaTuttiIFileDaAcquisireInSpecificaCartella");
                            FileInfo FileInfoLocale = new FileInfo(StringaFileLocale);

                            DateTime UltimaModificaLocale = FileInfoLocale.LastWriteTime;

                            try
                            {
                                if (UltimaModificaRemoto > UltimaModificaLocale)
                                {
                                    Log.AggiungiLog("da copiare - modifica recente: " + FileInfoRemoto.Name.ToLower(), "TrovaTuttiIFileDaAcquisireInSpecificaCartella");
                                    lf.Add(Path.Combine(StrCartella + "/" + FileInfoRemoto.Name), UltimaModificaRemoto);
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.AggiungiErrore(ex.Message, "TrovaTuttiIFileDaAcquisireInSpecificaCartella");
                                // MessageBox.Show(ex.Message);
                            }
                        }
                        else
                        {
                            Log.AggiungiLog("da copiare - inesistente: " + FileInfoRemoto.Name.ToLower(), "TrovaTuttiIFileDaAcquisireInSpecificaCartella");
                            lf.Add(Path.Combine(StrCartella + "/" + FileInfoRemoto.Name), UltimaModificaRemoto);
                            
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // MessageBox.Show("791:" + e.Message.ToString());
            }

            return lf;
        }
        public async Task CopyFiles(Dictionary<string, DateTime> files, String nva_PathCompletaBaseDirRemota, string nva_PathCompletaBaseDirLocale)
        {

            try
            {
                for (int x = 0; x < files.Count; x++)
                {

                    try
                    {

                        var item = files.ElementAt(x);
                        //lbTimer.Text = "File avvio copia " +item.Key  ;

                        String from = nva_PathCompletaBaseDirRemota + item.Key;
                        String to = nva_PathCompletaBaseDirLocale + "temp-"+item.Key;

                        DateTime DataOraModificaFinale = item.Value;

                        using (var outStream = new FileStream(to, FileMode.Create, FileAccess.Write, FileShare.Read))
                        {
                            using (var inStream = new FileStream(from, FileMode.Open, FileAccess.Read, FileShare.Read))
                            {
                                await inStream.CopyToAsync(outStream);
                            }
                        }
                        //Se la copia è andata bene, sostituisci il file esistente con quello nuovo
                        File.Delete(nva_PathCompletaBaseDirLocale + item.Key);
                        File.Move(nva_PathCompletaBaseDirLocale + "temp-" + item.Key, nva_PathCompletaBaseDirLocale + item.Key);

                        File.SetLastWriteTime(to, DataOraModificaFinale);

                        double d = (Convert.ToDouble((x + 1)) / Convert.ToDouble(files.Count)) * 100;
             
                        Log.AggiungiLog("Copia ok " + nva_PathCompletaBaseDirRemota + item.Key, "Sync-CopyFiles");

                    }
                    catch (Exception e)
                    {

                        Log.AggiungiLog("(891) Errore copia: " + e.Message.ToString(), "Sync-CopyFiles");


                    }
                }
            }
            catch (Exception e)
            {
                // MessageBox.Show("908:" + e.Message.ToString());

                //dg_Copie.Rows.Add(DateTime.Now.ToString("yyyyMMddHHmmssfff"), "(864) Errore: " + e.Message.ToString());

            }

            //progressCallback(100, "");

        }
        private void button1_Click(object sender, EventArgs e)
        {
            Avvia();
        }
    }
}
