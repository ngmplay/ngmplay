﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SyncPC
{
    public partial class Form1 : Form
    {

        static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        public Form1()
        {
            InitializeComponent();


            myTimer.Tick += new EventHandler(TimerEventProcessor);


            myTimer.Interval = 10000;
            myTimer.Start();


        }


        private void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {

           dg_aggiornamenti.DataSource =  DB.GetDataFromSQL("exec sp_SyncDB_AggiornaDBLocaleConNuoviRecord ").DefaultView;


        }




    }
}
