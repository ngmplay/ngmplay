﻿namespace SyncPC
{
    partial class SincronizzoFileTraPc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabOperazioniAutomatiche = new System.Windows.Forms.TabPage();
            this.btn_avanzaManuale = new System.Windows.Forms.Button();
            this.cb_auto = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dgFileMancanti = new System.Windows.Forms.DataGridView();
            this.NomeFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.dgStorico = new System.Windows.Forms.DataGridView();
            this.nva_DataInizio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nva_DataFine = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nva_DescrizioneCompleta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nva_Esito = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bit_Errore = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.pbStatusLink = new System.Windows.Forms.PictureBox();
            this.lbDataOraControlloConnessione = new System.Windows.Forms.Label();
            this.gbRemoto = new System.Windows.Forms.GroupBox();
            this.lbIpPcRemoto = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbTipoPcRemoto = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.gbLocale = new System.Windows.Forms.GroupBox();
            this.lbIpPcLocale = new System.Windows.Forms.Label();
            this.lbTipoPcLocale = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControlPrincipale = new System.Windows.Forms.TabControl();
            this.tabImportazioneManuale = new System.Windows.Forms.TabPage();
            this.btn_Import = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_SFile = new System.Windows.Forms.Button();
            this.tb_File = new System.Windows.Forms.TextBox();
            this.dg_EsitoImportazione = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_SFolder = new System.Windows.Forms.Button();
            this.tb_Folder = new System.Windows.Forms.TextBox();
            this.ofile_OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.ofolder_OpenFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.label8 = new System.Windows.Forms.Label();
            this.dg_Copie = new System.Windows.Forms.DataGridView();
            this.DataOra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabOperazioniAutomatiche.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFileMancanti)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgStorico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatusLink)).BeginInit();
            this.gbRemoto.SuspendLayout();
            this.gbLocale.SuspendLayout();
            this.tabControlPrincipale.SuspendLayout();
            this.tabImportazioneManuale.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_EsitoImportazione)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_Copie)).BeginInit();
            this.SuspendLayout();
            // 
            // tabOperazioniAutomatiche
            // 
            this.tabOperazioniAutomatiche.Controls.Add(this.label8);
            this.tabOperazioniAutomatiche.Controls.Add(this.dg_Copie);
            this.tabOperazioniAutomatiche.Controls.Add(this.btn_avanzaManuale);
            this.tabOperazioniAutomatiche.Controls.Add(this.cb_auto);
            this.tabOperazioniAutomatiche.Controls.Add(this.label5);
            this.tabOperazioniAutomatiche.Controls.Add(this.dgFileMancanti);
            this.tabOperazioniAutomatiche.Controls.Add(this.label2);
            this.tabOperazioniAutomatiche.Controls.Add(this.dgStorico);
            this.tabOperazioniAutomatiche.Location = new System.Drawing.Point(4, 22);
            this.tabOperazioniAutomatiche.Name = "tabOperazioniAutomatiche";
            this.tabOperazioniAutomatiche.Padding = new System.Windows.Forms.Padding(3);
            this.tabOperazioniAutomatiche.Size = new System.Drawing.Size(1170, 561);
            this.tabOperazioniAutomatiche.TabIndex = 0;
            this.tabOperazioniAutomatiche.Text = "Operazioni automatiche";
            this.tabOperazioniAutomatiche.UseVisualStyleBackColor = true;
            // 
            // btn_avanzaManuale
            // 
            this.btn_avanzaManuale.Location = new System.Drawing.Point(1017, 17);
            this.btn_avanzaManuale.Name = "btn_avanzaManuale";
            this.btn_avanzaManuale.Size = new System.Drawing.Size(75, 23);
            this.btn_avanzaManuale.TabIndex = 16;
            this.btn_avanzaManuale.Text = "avanza";
            this.btn_avanzaManuale.UseVisualStyleBackColor = true;
            this.btn_avanzaManuale.Click += new System.EventHandler(this.btn_avanzaManuale_Click);
            // 
            // cb_auto
            // 
            this.cb_auto.AutoSize = true;
            this.cb_auto.Location = new System.Drawing.Point(851, 21);
            this.cb_auto.Name = "cb_auto";
            this.cb_auto.Size = new System.Drawing.Size(114, 17);
            this.cb_auto.TabIndex = 15;
            this.cb_auto.Text = "avanzamento auto";
            this.cb_auto.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(73, 275);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(267, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "File mancanti che impediscono la generazione di scene";
            // 
            // dgFileMancanti
            // 
            this.dgFileMancanti.AllowUserToAddRows = false;
            this.dgFileMancanti.AllowUserToDeleteRows = false;
            this.dgFileMancanti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFileMancanti.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NomeFile});
            this.dgFileMancanti.Location = new System.Drawing.Point(76, 291);
            this.dgFileMancanti.MultiSelect = false;
            this.dgFileMancanti.Name = "dgFileMancanti";
            this.dgFileMancanti.ReadOnly = true;
            this.dgFileMancanti.RowHeadersVisible = false;
            this.dgFileMancanti.ShowCellErrors = false;
            this.dgFileMancanti.ShowCellToolTips = false;
            this.dgFileMancanti.ShowEditingIcon = false;
            this.dgFileMancanti.ShowRowErrors = false;
            this.dgFileMancanti.Size = new System.Drawing.Size(1016, 105);
            this.dgFileMancanti.TabIndex = 13;
            // 
            // NomeFile
            // 
            this.NomeFile.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NomeFile.HeaderText = "Nome file";
            this.NomeFile.Name = "NomeFile";
            this.NomeFile.ReadOnly = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(73, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(280, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Ultime operazioni di sincronizzazione e generazione scene";
            // 
            // dgStorico
            // 
            this.dgStorico.AllowUserToAddRows = false;
            this.dgStorico.AllowUserToDeleteRows = false;
            this.dgStorico.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgStorico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgStorico.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nva_DataInizio,
            this.nva_DataFine,
            this.nva_DescrizioneCompleta,
            this.nva_Esito,
            this.bit_Errore});
            this.dgStorico.Location = new System.Drawing.Point(76, 55);
            this.dgStorico.MultiSelect = false;
            this.dgStorico.Name = "dgStorico";
            this.dgStorico.ReadOnly = true;
            this.dgStorico.RowHeadersVisible = false;
            this.dgStorico.ShowCellErrors = false;
            this.dgStorico.ShowCellToolTips = false;
            this.dgStorico.ShowEditingIcon = false;
            this.dgStorico.ShowRowErrors = false;
            this.dgStorico.Size = new System.Drawing.Size(1016, 205);
            this.dgStorico.TabIndex = 11;
            // 
            // nva_DataInizio
            // 
            this.nva_DataInizio.FillWeight = 150F;
            this.nva_DataInizio.HeaderText = "Data Inizio Operazione";
            this.nva_DataInizio.Name = "nva_DataInizio";
            this.nva_DataInizio.ReadOnly = true;
            this.nva_DataInizio.Width = 150;
            // 
            // nva_DataFine
            // 
            this.nva_DataFine.FillWeight = 150F;
            this.nva_DataFine.HeaderText = "Data Fine Operazione";
            this.nva_DataFine.Name = "nva_DataFine";
            this.nva_DataFine.ReadOnly = true;
            this.nva_DataFine.Width = 150;
            // 
            // nva_DescrizioneCompleta
            // 
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.nva_DescrizioneCompleta.DefaultCellStyle = dataGridViewCellStyle1;
            this.nva_DescrizioneCompleta.FillWeight = 185F;
            this.nva_DescrizioneCompleta.HeaderText = "Operazione svolta";
            this.nva_DescrizioneCompleta.Name = "nva_DescrizioneCompleta";
            this.nva_DescrizioneCompleta.ReadOnly = true;
            this.nva_DescrizioneCompleta.Width = 185;
            // 
            // nva_Esito
            // 
            this.nva_Esito.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.nva_Esito.DefaultCellStyle = dataGridViewCellStyle2;
            this.nva_Esito.FillWeight = 300F;
            this.nva_Esito.HeaderText = "Esito";
            this.nva_Esito.Name = "nva_Esito";
            this.nva_Esito.ReadOnly = true;
            // 
            // bit_Errore
            // 
            this.bit_Errore.FillWeight = 50F;
            this.bit_Errore.HeaderText = "Errore";
            this.bit_Errore.Name = "bit_Errore";
            this.bit_Errore.ReadOnly = true;
            this.bit_Errore.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.bit_Errore.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.bit_Errore.Width = 50;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(422, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Ultimo test:";
            // 
            // pbStatusLink
            // 
            this.pbStatusLink.Location = new System.Drawing.Point(503, 4);
            this.pbStatusLink.Name = "pbStatusLink";
            this.pbStatusLink.Size = new System.Drawing.Size(64, 64);
            this.pbStatusLink.TabIndex = 13;
            this.pbStatusLink.TabStop = false;
            // 
            // lbDataOraControlloConnessione
            // 
            this.lbDataOraControlloConnessione.AutoSize = true;
            this.lbDataOraControlloConnessione.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDataOraControlloConnessione.Location = new System.Drawing.Point(517, 73);
            this.lbDataOraControlloConnessione.Name = "lbDataOraControlloConnessione";
            this.lbDataOraControlloConnessione.Size = new System.Drawing.Size(155, 20);
            this.lbDataOraControlloConnessione.TabIndex = 7;
            this.lbDataOraControlloConnessione.Text = "12/12/2020 12:36:40";
            // 
            // gbRemoto
            // 
            this.gbRemoto.Controls.Add(this.lbIpPcRemoto);
            this.gbRemoto.Controls.Add(this.label7);
            this.gbRemoto.Controls.Add(this.lbTipoPcRemoto);
            this.gbRemoto.Controls.Add(this.label4);
            this.gbRemoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbRemoto.Location = new System.Drawing.Point(735, 12);
            this.gbRemoto.Name = "gbRemoto";
            this.gbRemoto.Size = new System.Drawing.Size(373, 64);
            this.gbRemoto.TabIndex = 9;
            this.gbRemoto.TabStop = false;
            this.gbRemoto.Text = "PC remoto";
            // 
            // lbIpPcRemoto
            // 
            this.lbIpPcRemoto.AutoSize = true;
            this.lbIpPcRemoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbIpPcRemoto.Location = new System.Drawing.Point(234, 32);
            this.lbIpPcRemoto.Name = "lbIpPcRemoto";
            this.lbIpPcRemoto.Size = new System.Drawing.Size(129, 20);
            this.lbIpPcRemoto.TabIndex = 4;
            this.lbIpPcRemoto.Text = "000.000.000.000";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(208, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 20);
            this.label7.TabIndex = 3;
            this.label7.Text = "IP:";
            // 
            // lbTipoPcRemoto
            // 
            this.lbTipoPcRemoto.AutoSize = true;
            this.lbTipoPcRemoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTipoPcRemoto.Location = new System.Drawing.Point(99, 32);
            this.lbTipoPcRemoto.Name = "lbTipoPcRemoto";
            this.lbTipoPcRemoto.Size = new System.Drawing.Size(63, 20);
            this.lbTipoPcRemoto.TabIndex = 3;
            this.lbTipoPcRemoto.Text = "Backup";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "Tipologia:";
            // 
            // gbLocale
            // 
            this.gbLocale.Controls.Add(this.lbIpPcLocale);
            this.gbLocale.Controls.Add(this.lbTipoPcLocale);
            this.gbLocale.Controls.Add(this.label3);
            this.gbLocale.Controls.Add(this.label1);
            this.gbLocale.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbLocale.Location = new System.Drawing.Point(12, 12);
            this.gbLocale.Name = "gbLocale";
            this.gbLocale.Size = new System.Drawing.Size(376, 64);
            this.gbLocale.TabIndex = 8;
            this.gbLocale.TabStop = false;
            this.gbLocale.Text = "Questo PC";
            // 
            // lbIpPcLocale
            // 
            this.lbIpPcLocale.AutoSize = true;
            this.lbIpPcLocale.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbIpPcLocale.Location = new System.Drawing.Point(243, 30);
            this.lbIpPcLocale.Name = "lbIpPcLocale";
            this.lbIpPcLocale.Size = new System.Drawing.Size(129, 20);
            this.lbIpPcLocale.TabIndex = 3;
            this.lbIpPcLocale.Text = "000.000.000.000";
            // 
            // lbTipoPcLocale
            // 
            this.lbTipoPcLocale.AutoSize = true;
            this.lbTipoPcLocale.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTipoPcLocale.Location = new System.Drawing.Point(103, 30);
            this.lbTipoPcLocale.Name = "lbTipoPcLocale";
            this.lbTipoPcLocale.Size = new System.Drawing.Size(49, 20);
            this.lbTipoPcLocale.TabIndex = 2;
            this.lbTipoPcLocale.Text = "Attivo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(217, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "IP:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tipologia:";
            // 
            // tabControlPrincipale
            // 
            this.tabControlPrincipale.Controls.Add(this.tabOperazioniAutomatiche);
            this.tabControlPrincipale.Controls.Add(this.tabImportazioneManuale);
            this.tabControlPrincipale.Location = new System.Drawing.Point(12, 93);
            this.tabControlPrincipale.Name = "tabControlPrincipale";
            this.tabControlPrincipale.SelectedIndex = 0;
            this.tabControlPrincipale.Size = new System.Drawing.Size(1178, 587);
            this.tabControlPrincipale.TabIndex = 17;
            // 
            // tabImportazioneManuale
            // 
            this.tabImportazioneManuale.Controls.Add(this.btn_Import);
            this.tabImportazioneManuale.Controls.Add(this.groupBox2);
            this.tabImportazioneManuale.Controls.Add(this.dg_EsitoImportazione);
            this.tabImportazioneManuale.Controls.Add(this.groupBox1);
            this.tabImportazioneManuale.Location = new System.Drawing.Point(4, 22);
            this.tabImportazioneManuale.Name = "tabImportazioneManuale";
            this.tabImportazioneManuale.Size = new System.Drawing.Size(1170, 561);
            this.tabImportazioneManuale.TabIndex = 1;
            this.tabImportazioneManuale.Text = "Importazione manuale playlist";
            this.tabImportazioneManuale.UseVisualStyleBackColor = true;
            // 
            // btn_Import
            // 
            this.btn_Import.Location = new System.Drawing.Point(806, 54);
            this.btn_Import.Name = "btn_Import";
            this.btn_Import.Size = new System.Drawing.Size(346, 23);
            this.btn_Import.TabIndex = 2;
            this.btn_Import.Text = "Importa";
            this.btn_Import.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_SFile);
            this.groupBox2.Controls.Add(this.tb_File);
            this.groupBox2.Location = new System.Drawing.Point(410, 29);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(373, 67);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Seleziona il file ZIP della playlist";
            // 
            // btn_SFile
            // 
            this.btn_SFile.Location = new System.Drawing.Point(276, 25);
            this.btn_SFile.Name = "btn_SFile";
            this.btn_SFile.Size = new System.Drawing.Size(75, 23);
            this.btn_SFile.TabIndex = 0;
            this.btn_SFile.Text = "Seleziona";
            this.btn_SFile.UseVisualStyleBackColor = true;
            this.btn_SFile.Click += new System.EventHandler(this.btn_SFile_Click);
            // 
            // tb_File
            // 
            this.tb_File.Location = new System.Drawing.Point(19, 27);
            this.tb_File.Name = "tb_File";
            this.tb_File.Size = new System.Drawing.Size(251, 20);
            this.tb_File.TabIndex = 1;
            // 
            // dg_EsitoImportazione
            // 
            this.dg_EsitoImportazione.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_EsitoImportazione.Location = new System.Drawing.Point(21, 134);
            this.dg_EsitoImportazione.Name = "dg_EsitoImportazione";
            this.dg_EsitoImportazione.Size = new System.Drawing.Size(1131, 408);
            this.dg_EsitoImportazione.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_SFolder);
            this.groupBox1.Controls.Add(this.tb_Folder);
            this.groupBox1.Location = new System.Drawing.Point(21, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(373, 67);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seleziona la cartella della playlist";
            // 
            // btn_SFolder
            // 
            this.btn_SFolder.Location = new System.Drawing.Point(276, 25);
            this.btn_SFolder.Name = "btn_SFolder";
            this.btn_SFolder.Size = new System.Drawing.Size(75, 23);
            this.btn_SFolder.TabIndex = 0;
            this.btn_SFolder.Text = "Seleziona";
            this.btn_SFolder.UseVisualStyleBackColor = true;
            this.btn_SFolder.Click += new System.EventHandler(this.btn_SFolder_Click);
            // 
            // tb_Folder
            // 
            this.tb_Folder.Location = new System.Drawing.Point(19, 27);
            this.tb_Folder.Name = "tb_Folder";
            this.tb_Folder.Size = new System.Drawing.Size(251, 20);
            this.tb_Folder.TabIndex = 1;
            // 
            // ofile_OpenFileDialog
            // 
            this.ofile_OpenFileDialog.FileName = "-";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(73, 404);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "File copiati";
            // 
            // dg_Copie
            // 
            this.dg_Copie.AllowUserToAddRows = false;
            this.dg_Copie.AllowUserToDeleteRows = false;
            this.dg_Copie.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_Copie.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DataOra,
            this.dataGridViewTextBoxColumn1});
            this.dg_Copie.Location = new System.Drawing.Point(76, 420);
            this.dg_Copie.MultiSelect = false;
            this.dg_Copie.Name = "dg_Copie";
            this.dg_Copie.ReadOnly = true;
            this.dg_Copie.RowHeadersVisible = false;
            this.dg_Copie.ShowCellErrors = false;
            this.dg_Copie.ShowCellToolTips = false;
            this.dg_Copie.ShowEditingIcon = false;
            this.dg_Copie.ShowRowErrors = false;
            this.dg_Copie.Size = new System.Drawing.Size(1016, 105);
            this.dg_Copie.TabIndex = 17;
            // 
            // DataOra
            // 
            this.DataOra.HeaderText = "Istante operazione";
            this.DataOra.Name = "DataOra";
            this.DataOra.ReadOnly = true;
            this.DataOra.Width = 200;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Nome file";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // SincronizzoFileTraPc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1202, 692);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tabControlPrincipale);
            this.Controls.Add(this.pbStatusLink);
            this.Controls.Add(this.gbRemoto);
            this.Controls.Add(this.gbLocale);
            this.Controls.Add(this.lbDataOraControlloConnessione);
            this.Name = "SincronizzoFileTraPc";
            this.Text = "Utility Supervisione";
            this.tabOperazioniAutomatiche.ResumeLayout(false);
            this.tabOperazioniAutomatiche.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFileMancanti)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgStorico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatusLink)).EndInit();
            this.gbRemoto.ResumeLayout(false);
            this.gbRemoto.PerformLayout();
            this.gbLocale.ResumeLayout(false);
            this.gbLocale.PerformLayout();
            this.tabControlPrincipale.ResumeLayout(false);
            this.tabImportazioneManuale.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_EsitoImportazione)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_Copie)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage tabOperazioniAutomatiche;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_avanzaManuale;
        private System.Windows.Forms.CheckBox cb_auto;
        private System.Windows.Forms.PictureBox pbStatusLink;
        private System.Windows.Forms.Label lbDataOraControlloConnessione;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgFileMancanti;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomeFile;
        private System.Windows.Forms.GroupBox gbRemoto;
        private System.Windows.Forms.Label lbIpPcRemoto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbTipoPcRemoto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox gbLocale;
        private System.Windows.Forms.Label lbIpPcLocale;
        private System.Windows.Forms.Label lbTipoPcLocale;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgStorico;
        private System.Windows.Forms.DataGridViewTextBoxColumn nva_DataInizio;
        private System.Windows.Forms.DataGridViewTextBoxColumn nva_DataFine;
        private System.Windows.Forms.DataGridViewTextBoxColumn nva_DescrizioneCompleta;
        private System.Windows.Forms.DataGridViewTextBoxColumn nva_Esito;
        private System.Windows.Forms.DataGridViewCheckBoxColumn bit_Errore;
        private System.Windows.Forms.TabControl tabControlPrincipale;
        private System.Windows.Forms.TabPage tabImportazioneManuale;
        private System.Windows.Forms.DataGridView dg_EsitoImportazione;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_SFolder;
        private System.Windows.Forms.TextBox tb_Folder;
        private System.Windows.Forms.OpenFileDialog ofile_OpenFileDialog;
        private System.Windows.Forms.Button btn_Import;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_SFile;
        private System.Windows.Forms.TextBox tb_File;
        private System.Windows.Forms.FolderBrowserDialog ofolder_OpenFolderDialog;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dg_Copie;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataOra;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    }
}