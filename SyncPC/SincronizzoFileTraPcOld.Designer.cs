﻿namespace SyncPC
{
    partial class SincronizzoFileTraPc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lbOperazioneSvolta = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbTimer = new System.Windows.Forms.Label();
            this.dgView = new System.Windows.Forms.DataGridView();
            this.DataOra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomeFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgControlli = new System.Windows.Forms.DataGridView();
            this.DataOraIncrementale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomeFileLocale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataModifcaLocale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_PcAttivo = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbTipoPcLocale = new System.Windows.Forms.Label();
            this.lbTipoPcRemoto = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbIpPcLocale = new System.Windows.Forms.Label();
            this.lbIpPcRemoto = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbStatoConnessioneRemota = new System.Windows.Forms.Label();
            this.lbDataOraControlloConnessione = new System.Windows.Forms.Label();
            this.dgLog = new System.Windows.Forms.DataGridView();
            this.DataOraLog = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgControlli)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgLog)).BeginInit();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 317);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(1146, 23);
            this.progressBar1.Step = 1;
            this.progressBar1.TabIndex = 0;
            // 
            // lbOperazioneSvolta
            // 
            this.lbOperazioneSvolta.AutoSize = true;
            this.lbOperazioneSvolta.Location = new System.Drawing.Point(18, 133);
            this.lbOperazioneSvolta.Name = "lbOperazioneSvolta";
            this.lbOperazioneSvolta.Size = new System.Drawing.Size(979, 13);
            this.lbOperazioneSvolta.TabIndex = 1;
            this.lbOperazioneSvolta.Text = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
    "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
    "a";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Prossimo controllo:";
            // 
            // lbTimer
            // 
            this.lbTimer.AutoSize = true;
            this.lbTimer.Location = new System.Drawing.Point(119, 109);
            this.lbTimer.Name = "lbTimer";
            this.lbTimer.Size = new System.Drawing.Size(33, 13);
            this.lbTimer.TabIndex = 3;
            this.lbTimer.Text = "Timer";
            // 
            // dgView
            // 
            this.dgView.AllowUserToAddRows = false;
            this.dgView.AllowUserToDeleteRows = false;
            this.dgView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DataOra,
            this.NomeFile});
            this.dgView.Location = new System.Drawing.Point(12, 346);
            this.dgView.MultiSelect = false;
            this.dgView.Name = "dgView";
            this.dgView.ReadOnly = true;
            this.dgView.RowHeadersVisible = false;
            this.dgView.ShowCellErrors = false;
            this.dgView.ShowCellToolTips = false;
            this.dgView.ShowEditingIcon = false;
            this.dgView.ShowRowErrors = false;
            this.dgView.Size = new System.Drawing.Size(1146, 145);
            this.dgView.TabIndex = 4;
            // 
            // DataOra
            // 
            this.DataOra.HeaderText = "Data";
            this.DataOra.Name = "DataOra";
            this.DataOra.ReadOnly = true;
            this.DataOra.Width = 220;
            // 
            // NomeFile
            // 
            this.NomeFile.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NomeFile.HeaderText = "NomeFile";
            this.NomeFile.Name = "NomeFile";
            this.NomeFile.ReadOnly = true;
            // 
            // dgControlli
            // 
            this.dgControlli.AllowUserToAddRows = false;
            this.dgControlli.AllowUserToDeleteRows = false;
            this.dgControlli.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgControlli.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DataOraIncrementale,
            this.dataGridViewTextBoxColumn2,
            this.NomeFileLocale,
            this.dataGridViewTextBoxColumn1,
            this.DataModifcaLocale});
            this.dgControlli.Location = new System.Drawing.Point(12, 153);
            this.dgControlli.MultiSelect = false;
            this.dgControlli.Name = "dgControlli";
            this.dgControlli.ReadOnly = true;
            this.dgControlli.RowHeadersVisible = false;
            this.dgControlli.ShowCellErrors = false;
            this.dgControlli.ShowCellToolTips = false;
            this.dgControlli.ShowEditingIcon = false;
            this.dgControlli.ShowRowErrors = false;
            this.dgControlli.Size = new System.Drawing.Size(1146, 153);
            this.dgControlli.TabIndex = 6;
            // 
            // DataOraIncrementale
            // 
            this.DataOraIncrementale.HeaderText = "DataOraIncrementale";
            this.DataOraIncrementale.Name = "DataOraIncrementale";
            this.DataOraIncrementale.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "NomeFileRemoto";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // NomeFileLocale
            // 
            this.NomeFileLocale.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NomeFileLocale.HeaderText = "NomeFileLocale";
            this.NomeFileLocale.Name = "NomeFileLocale";
            this.NomeFileLocale.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "DataModifcaRemoto";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 180;
            // 
            // DataModifcaLocale
            // 
            this.DataModifcaLocale.HeaderText = "DataModifcaLocale";
            this.DataModifcaLocale.Name = "DataModifcaLocale";
            this.DataModifcaLocale.ReadOnly = true;
            this.DataModifcaLocale.Width = 180;
            // 
            // btn_PcAttivo
            // 
            this.btn_PcAttivo.Location = new System.Drawing.Point(21, 32);
            this.btn_PcAttivo.Name = "btn_PcAttivo";
            this.btn_PcAttivo.Size = new System.Drawing.Size(253, 43);
            this.btn_PcAttivo.TabIndex = 7;
            this.btn_PcAttivo.Text = "Imposta questo PC come Attivo";
            this.btn_PcAttivo.UseVisualStyleBackColor = true;
            this.btn_PcAttivo.Click += new System.EventHandler(this.btn_PcAttivo_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbDataOraControlloConnessione);
            this.groupBox1.Controls.Add(this.lbStatoConnessioneRemota);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lbIpPcLocale);
            this.groupBox1.Controls.Add(this.lbTipoPcLocale);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(347, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(426, 94);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Questo PC";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbIpPcRemoto);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.lbTipoPcRemoto);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(779, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(221, 94);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "PC remoto";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tipologia:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "IP:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Tipologia:";
            // 
            // lbTipoPcLocale
            // 
            this.lbTipoPcLocale.AutoSize = true;
            this.lbTipoPcLocale.Location = new System.Drawing.Point(66, 20);
            this.lbTipoPcLocale.Name = "lbTipoPcLocale";
            this.lbTipoPcLocale.Size = new System.Drawing.Size(34, 13);
            this.lbTipoPcLocale.TabIndex = 2;
            this.lbTipoPcLocale.Text = "Attivo";
            // 
            // lbTipoPcRemoto
            // 
            this.lbTipoPcRemoto.AutoSize = true;
            this.lbTipoPcRemoto.Location = new System.Drawing.Point(75, 20);
            this.lbTipoPcRemoto.Name = "lbTipoPcRemoto";
            this.lbTipoPcRemoto.Size = new System.Drawing.Size(44, 13);
            this.lbTipoPcRemoto.TabIndex = 3;
            this.lbTipoPcRemoto.Text = "Backup";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 45);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "IP:";
            // 
            // lbIpPcLocale
            // 
            this.lbIpPcLocale.AutoSize = true;
            this.lbIpPcLocale.Location = new System.Drawing.Point(33, 45);
            this.lbIpPcLocale.Name = "lbIpPcLocale";
            this.lbIpPcLocale.Size = new System.Drawing.Size(40, 13);
            this.lbIpPcLocale.TabIndex = 3;
            this.lbIpPcLocale.Text = "0.0.0.0";
            // 
            // lbIpPcRemoto
            // 
            this.lbIpPcRemoto.AutoSize = true;
            this.lbIpPcRemoto.Location = new System.Drawing.Point(42, 45);
            this.lbIpPcRemoto.Name = "lbIpPcRemoto";
            this.lbIpPcRemoto.Size = new System.Drawing.Size(40, 13);
            this.lbIpPcRemoto.TabIndex = 4;
            this.lbIpPcRemoto.Text = "0.0.0.0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(169, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Connessione Db verso PC remoto:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(244, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Ultimo test:";
            // 
            // lbStatoConnessioneRemota
            // 
            this.lbStatoConnessioneRemota.AutoSize = true;
            this.lbStatoConnessioneRemota.Location = new System.Drawing.Point(179, 70);
            this.lbStatoConnessioneRemota.Name = "lbStatoConnessioneRemota";
            this.lbStatoConnessioneRemota.Size = new System.Drawing.Size(22, 13);
            this.lbStatoConnessioneRemota.TabIndex = 6;
            this.lbStatoConnessioneRemota.Text = "OK";
            // 
            // lbDataOraControlloConnessione
            // 
            this.lbDataOraControlloConnessione.AutoSize = true;
            this.lbDataOraControlloConnessione.Location = new System.Drawing.Point(305, 70);
            this.lbDataOraControlloConnessione.Name = "lbDataOraControlloConnessione";
            this.lbDataOraControlloConnessione.Size = new System.Drawing.Size(110, 13);
            this.lbDataOraControlloConnessione.TabIndex = 7;
            this.lbDataOraControlloConnessione.Text = "12/12/2020 12:36:40";
            // 
            // dgLog
            // 
            this.dgLog.AllowUserToAddRows = false;
            this.dgLog.AllowUserToDeleteRows = false;
            this.dgLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DataOraLog,
            this.dataGridViewTextBoxColumn4});
            this.dgLog.Location = new System.Drawing.Point(12, 497);
            this.dgLog.MultiSelect = false;
            this.dgLog.Name = "dgLog";
            this.dgLog.ReadOnly = true;
            this.dgLog.RowHeadersVisible = false;
            this.dgLog.ShowCellErrors = false;
            this.dgLog.ShowCellToolTips = false;
            this.dgLog.ShowEditingIcon = false;
            this.dgLog.ShowRowErrors = false;
            this.dgLog.Size = new System.Drawing.Size(1146, 145);
            this.dgLog.TabIndex = 10;
            // 
            // DataOraLog
            // 
            this.DataOraLog.HeaderText = "DataOraLog";
            this.DataOraLog.Name = "DataOraLog";
            this.DataOraLog.ReadOnly = true;
            this.DataOraLog.Width = 220;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.HeaderText = "Log";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // SincronizzoFileTraPc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1170, 667);
            this.Controls.Add(this.dgLog);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_PcAttivo);
            this.Controls.Add(this.dgControlli);
            this.Controls.Add(this.dgView);
            this.Controls.Add(this.lbTimer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbOperazioneSvolta);
            this.Controls.Add(this.progressBar1);
            this.Name = "SincronizzoFileTraPc";
            this.Text = "Utility Supervisione";
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgControlli)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgLog)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lbOperazioneSvolta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbTimer;
        private System.Windows.Forms.DataGridView dgView;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataOra;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomeFile;
        private System.Windows.Forms.DataGridView dgControlli;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataOraIncrementale;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomeFileLocale;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataModifcaLocale;
        private System.Windows.Forms.Button btn_PcAttivo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbIpPcLocale;
        private System.Windows.Forms.Label lbTipoPcLocale;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lbIpPcRemoto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbTipoPcRemoto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbDataOraControlloConnessione;
        private System.Windows.Forms.Label lbStatoConnessioneRemota;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataOraLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
    }
}