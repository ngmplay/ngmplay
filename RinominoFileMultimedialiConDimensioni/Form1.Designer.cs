﻿namespace RinominoFileMultimedialiConDimensioni
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Rinonima = new System.Windows.Forms.Button();
            this.tb_CartellaSelezionata = new System.Windows.Forms.TextBox();
            this.btn_Seleziona = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_Rinonima
            // 
            this.btn_Rinonima.Location = new System.Drawing.Point(198, 164);
            this.btn_Rinonima.Name = "btn_Rinonima";
            this.btn_Rinonima.Size = new System.Drawing.Size(108, 23);
            this.btn_Rinonima.TabIndex = 0;
            this.btn_Rinonima.Text = "Rinomina files";
            this.btn_Rinonima.UseVisualStyleBackColor = true;
            this.btn_Rinonima.Click += new System.EventHandler(this.btn_Rinonima_Click);
            // 
            // tb_CartellaSelezionata
            // 
            this.tb_CartellaSelezionata.Location = new System.Drawing.Point(42, 84);
            this.tb_CartellaSelezionata.Name = "tb_CartellaSelezionata";
            this.tb_CartellaSelezionata.Size = new System.Drawing.Size(353, 20);
            this.tb_CartellaSelezionata.TabIndex = 1;
            // 
            // btn_Seleziona
            // 
            this.btn_Seleziona.Location = new System.Drawing.Point(401, 82);
            this.btn_Seleziona.Name = "btn_Seleziona";
            this.btn_Seleziona.Size = new System.Drawing.Size(107, 23);
            this.btn_Seleziona.TabIndex = 2;
            this.btn_Seleziona.Text = "Seleziona cartella";
            this.btn_Seleziona.UseVisualStyleBackColor = true;
            this.btn_Seleziona.Click += new System.EventHandler(this.btn_Seleziona_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 260);
            this.Controls.Add(this.btn_Seleziona);
            this.Controls.Add(this.tb_CartellaSelezionata);
            this.Controls.Add(this.btn_Rinonima);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Rinonima;
        private System.Windows.Forms.TextBox tb_CartellaSelezionata;
        private System.Windows.Forms.Button btn_Seleziona;
    }
}

