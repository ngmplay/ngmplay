﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Alturos.VideoInfo;


namespace RinominoFileMultimedialiConDimensioni
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            tb_CartellaSelezionata.Text = @"\\192.168.50.211\Public\Public\LED\PLAYLIST 2016-2017\trans\mau\EMPOLI - ASCOLI\SPOT\Tabellone";
        }

        string NomeSubDir = "rinominati";


        private void btn_Seleziona_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    tb_CartellaSelezionata.Text =  fbd.SelectedPath;
                }
            }
        }

        //C:\Users\mpetrini\Documents\FIORENTINA - FIORENTINA P. 29-08\SPOT\Led
        private static Regex regex2 = new Regex("^[0-9]{2,}$", RegexOptions.Compiled);
        private static Regex regex = new Regex("^[0-9]{1}$", RegexOptions.Compiled);
        private void btn_Rinonima_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tb_CartellaSelezionata.Text))
            {


                string[] files = Directory.GetFiles(tb_CartellaSelezionata.Text);
                if (Directory.Exists(tb_CartellaSelezionata.Text + @"\" + NomeSubDir + @"\")) 
                {
                    Directory.Delete(tb_CartellaSelezionata.Text + @"\" + NomeSubDir + @"\",true);
                }
             
                    Directory.CreateDirectory(tb_CartellaSelezionata.Text + @"\" + NomeSubDir + @"\");

                foreach (String f in files)
                {
                    try
                    {
                        FileInfo fi = new FileInfo(f);

                        VideoAnalyzer videoAnalyzer = new VideoAnalyzer("ffprobe/ffprobe.exe");
              
                        var analyzeResult = videoAnalyzer.GetVideoInfo(f);
                      
                        var videoStream = analyzeResult.VideoInfo.Streams.Single(o => o.CodecType == "video");
                        var width = videoStream.Width;
                        var height = videoStream.Height;
                        var durata = (Convert.ToUInt16(videoStream.Duration) * 1000).ToString();
                        String NomeFile = fi.Name.ToUpper();

                        if (NomeFile.EndsWith(".MP4"))
                        {
                           //
                            
                          //  NomeFile = NomeFile.Substring(0, NomeFile.Length - 4);



                         //   NomeFile = NomeFile.Replace("&", "_");
                          // NomeFile = NomeFile.Replace(" ", "_");
                          // NomeFile = NomeFile.Replace("__", "_");
                          //  NomeFile = NomeFile.Replace("__", "_");

                            //NomeFile = NomeFile.Replace("_2100", "");
                            //NomeFile = NomeFile.Replace("_1800", "");
                            string NuovoNome = "";
                            int eee = 1;
                            /*
                            for (int i = 9999; i < 10003; i++)
                            {
                                for (int q = 10; q < 2500; q++)
                                {
                                    for (int s = 89; s < 91; s++)
                                    {
                                        String ss = NomeFile.ToLower().Replace("_" + i.ToString() + "_" + q.ToString() + "_" + s.ToString() + ".mp4", "");
                                        if (NomeFile.ToLower() != ss)
                                        {
                                            NomeFile = ss;
                                            break;
                                        }
                                       
                                    }
                                }
                            }//_10000_2400_90.MP4



                for (int s = 100; s < 2500; s++)
                {
                    String ss = NomeFile.ToLower().Replace("_"+ s.ToString() + ".mp4", "");
                    if (NomeFile.ToLower() != ss)
                    {
                        NomeFile = ss;
                        break;
                    }

                }
  

                */






                            //NuovoNome = NomeFile.ToUpper();

                            NomeFile = NomeFile.ToUpper().Replace(".MP4", "");

                            NuovoNome = NomeFile.ToUpper()+"_";

                            //int contatore = 0;

                            /*
                            foreach (String parte in NomeFile.Split('_'))
                            {
                                if (!regex2.IsMatch(parte) || (contatore == 0))
                                {
                                    if (regex.IsMatch(parte))
                                    {
                                        NuovoNome = NuovoNome.Trim('_');
                                        // NuovoNome += parte + "_";
                                    }
                                    NuovoNome += parte + "_";

                                }

                                contatore += 1;
                            }
                            */



                               NuovoNome += durata.ToString() + "_" + width.ToString() + "_" + height.ToString() + ".MP4";


                            try
                            {

                                File.Copy(f, fi.DirectoryName + @"\" + NomeSubDir + @"\" + NuovoNome);
                            }
                            catch (Exception ee)
                            {


                            }
                            Console.WriteLine(NomeFile);
                            Console.WriteLine(fi.DirectoryName + @"\" + NuovoNome);
                            Console.WriteLine(NuovoNome);
                            Console.WriteLine("--------------------------------------------------------------");
                        }

                    }
                    catch (Exception eee)
                    {

                    }
                }
            }
        }
    
        private void button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tb_CartellaSelezionata.Text))
            {
                string[] files = Directory.GetFiles(tb_CartellaSelezionata.Text);
                foreach (String f in files)
                {

                    FileInfo fi = new FileInfo(f);

                    var videoAnalyzer = new VideoAnalyzer("ffprobe/ffprobe.exe");
                    var analyzeResult = videoAnalyzer.GetVideoInfo(f);
                    var videoStream = analyzeResult.VideoInfo.Streams.Single(o => o.CodecType == "video");
                    var width = videoStream.Width;
                    var height = videoStream.Height;
                    var durata = videoStream.Duration;
                    String NomeFile = fi.Name.ToUpper();


                   

                    if (NomeFile.EndsWith(".MP4"))
                    {
                        string NuovoNome = NomeFile.Substring(0, NomeFile.Length - 4);

                       // string NuovoNome = "F"+NomeFile.Replace("-","T")+".MP4";

                            File.Move(f, fi.DirectoryName + @"\" + NuovoNome);
     




                        Console.WriteLine(NomeFile);
                        Console.WriteLine(fi.DirectoryName + @"\" + NuovoNome);
                        Console.WriteLine(NuovoNome);
                        Console.WriteLine("--------------------------------------------------------------");
                    }


                }
            }
        }
    }
}
