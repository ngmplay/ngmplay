﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace VideoCompositionCreator
{
    class Program
    {
        static void Main(string[] args)
        {

            List<string> paths = new List<string>();



           
            paths.Add("video/unicalce_1760.mp4");
            paths.Add("video/unicalce_1760.mp4");
            paths.Add("video/CENTRO_SERVICE_10sec_1260_90.mp4");
            paths.Add("video/INT_1920.mp4");

            MergeHstackCropAndVstack(paths, 900, 90, 1, "ciao", 1600, "prova");

            Console.WriteLine("Hello World!");
        }





public static string MergeHstackCropAndVstack(List<string> paths, int width, int height, int type, string period, int maxWidth, string pathOutput)
{
    string str2 = string.Empty;
    string str3 = string.Empty;
    string str = string.Empty;
    string crop = string.Empty;
    string split = string.Empty;
    string totalCrop = string.Empty;
    int mod = 0;
    double differenceBetweenLed = 0;
    int initialMaxWidth = 0;
    int widthLed = 0;
    string newPath = System.IO.Path.Combine(pathOutput, "COMPOSITION_" + type + "_" + period + "_CROPVSTACK_COMPLETED.mp4");

    if (Utility.IsExistsFile(newPath))
        Utility.DeleteFile(newPath);

    switch (height)
    {
        case 48:
            differenceBetweenLed = (double)maxWidth / 64;
            widthLed = 64;
            break;
        case 72:
            differenceBetweenLed = (double)maxWidth / 96;
            widthLed = 96;
            break;
        case 90:
            differenceBetweenLed = (double)maxWidth / 120;
            widthLed = 120;
            break;
        case 64:
            differenceBetweenLed = (double)maxWidth / 192;
            widthLed = 192;
            break;
        default:
            differenceBetweenLed = 0;
            widthLed = 0;
            break;
    }

    if ((differenceBetweenLed % 1) != 0)
    {
        var ledforRow = (int)differenceBetweenLed;
        initialMaxWidth = maxWidth;
        maxWidth = ledforRow * widthLed;
    }

    //if (width >= maxWidth)
    //    mod = width % maxWidth;
    //else
    //    mod = maxWidth - width;

    if (width < initialMaxWidth)
        mod = initialMaxWidth - (width % initialMaxWidth);
    else
        mod = maxWidth - (width % maxWidth);

    if (mod == maxWidth)
        mod = 0;

    if (paths.Count == 1 && mod == 0)
    {
        Utility.MoveAndRenameFile(paths[0], pathOutput, "COMPOSITION_" + type + "_" + period + "_CROPVSTACK_COMPLETED");
        return newPath;
    }

    if (paths.Count > 0)
    {
        foreach (var item in paths)
            str2 += " -i " + item;

        for (int i = 0; i < paths.Count; i++)
            str3 += "[" + i + ":v]";


        if (mod > 0)
        {
            var count = paths.Count + 1;

            str += " -filter_complex color=black:" + mod + "x" + height + ":d=1[c];";
            str += str3 + "[c]hstack=inputs=" + count;
        }
        else
        {
            str += " -filter_complex ";
            str += str3 += "hstack=inputs=" + paths.Count;
        }

        if (width <= maxWidth)
        {
            str += " " + newPath;
        }
        else
        {
            str += "[tot];";

            var loopCrop = (mod + width) / maxWidth;

            var startCrop = 0;

            if (loopCrop > 0)
            {

                split = "[tot]split=" + loopCrop;

                for (int i = 0; i < loopCrop; i++)
                {
                    split += "[tot" + i + "]";

                    crop += "[tot" + i + "]crop=" + maxWidth + ":" + height + ":" + startCrop + ":0[crop" + i + "];";
                    startCrop += maxWidth;
                    totalCrop += "[crop" + i + "]";
                }

                str += split + ";";

                str += crop;

                totalCrop += "vstack=inputs=" + loopCrop;

                if (initialMaxWidth != 0)
                {
                    var differenceWidth = initialMaxWidth - maxWidth;

                            if (differenceWidth > 0)
                            {
                                str = str.Insert(17, "color=black:" + differenceWidth + "x" + (height * loopCrop) + ":d=1[z];");
                                totalCrop += "[final];[final][z]hstack";
                            }
                            else
                                Console.WriteLine("Error Method MergeHstackCropAndVstack: Il numero di pannelli led nella prima riga deve essere maggiore della seconda riga.");
                }

                totalCrop += " " + newPath;

                str += totalCrop;
            }
        }

        str2 += str;

    }
    else
        return string.Empty;

    ExecuteFFMPEG(str2);
    return newPath;
}











        public static void ExecuteFFMPEG(string strParam)
        {
            try
            {
                Process ffmpeg = new Process();
                ProcessStartInfo ffmpeg_StartInfo = new System.Diagnostics.ProcessStartInfo(Directory.GetCurrentDirectory() + @"\ffmpeg.exe", strParam);
                ffmpeg_StartInfo.UseShellExecute = false;
                ffmpeg_StartInfo.RedirectStandardOutput = true;
                ffmpeg.StartInfo = ffmpeg_StartInfo;
                //ffmpeg_StartInfo.CreateNoWindow = true;
                ffmpeg.Start();
                ffmpeg.WaitForExit();

                ffmpeg.Close();
                ffmpeg.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("errroore");
            }
        }






    }
}