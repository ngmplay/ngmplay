﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;

namespace VideoCompositionCreator
{
    public static class Utility
    {
        public static string SerializeObject<T>(this T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        public static void Serialize<T>(string path, T obj)
        {
            var serializer = new XmlSerializer(typeof(T));

            using (var stream = new GZipStream(File.Create(path),
                                               CompressionMode.Compress))
            {
                serializer.Serialize(stream, obj);
            }
        }

        public static T Deserialize<T>(string path, object obj)
        {
            var serializer = new XmlSerializer(typeof(T));

            using (var stream = new GZipStream(File.OpenRead(path),
                                               CompressionMode.Decompress))
            {
                return (T)serializer.Deserialize(stream);
            }
        }

        public static T Deserialize<T>(this string toDeserialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            StringReader textReader = new StringReader(toDeserialize);
            return (T)xmlSerializer.Deserialize(textReader);
        }


        public static bool CheckInternetConnection()
        {
            try
            {
                //using (var client = new WebClient())
                //{
                //    using (var stream = client.OpenRead("http://www.google.com"))
                //    {
                //        return true;
                //    }
                //}

                Ping myPing = new Ping();
                String host = "google.com";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                return (reply.Status == IPStatus.Success);
            }
            catch
            {
                return false;
            }
        }

        public static string RemoveNonDigits(this string value)
        {
            return Regex.Replace(value, "[^0-9]", string.Empty);
        }

        public static int ToIntOrZero(this string toConvert)
        {
            try
            {
                if (toConvert == null || toConvert.Trim() == string.Empty) return 0;
                return int.Parse(toConvert);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public class EnumerableComparer<T> : IComparer<IEnumerable<T>>
        {
            /// <summary>
            /// Create a sequence comparer using the default comparer for T.
            /// </summary>
            public EnumerableComparer()
            {
                comp = Comparer<T>.Default;
            }

            /// <summary>
            /// Create a sequence comparer, using the specified item comparer
            /// for T.
            /// </summary>
            /// <param name="comparer">Comparer for comparing each pair of
            /// items from the sequences.</param>
            public EnumerableComparer(IComparer<T> comparer)
            {
                comp = comparer;
            }

            /// <summary>
            /// Object used for comparing each element.
            /// </summary>
            private IComparer<T> comp;


            /// <summary>
            /// Compare two sequences of T.
            /// </summary>
            /// <param name="x">First sequence.</param>
            /// <param name="y">Second sequence.</param>
            public int Compare(IEnumerable<T> x, IEnumerable<T> y)
            {
                using (IEnumerator<T> leftIt = x.GetEnumerator())
                using (IEnumerator<T> rightIt = y.GetEnumerator())
                {
                    while (true)
                    {
                        bool left = leftIt.MoveNext();
                        bool right = rightIt.MoveNext();

                        if (!(left || right)) return 0;

                        if (!left) return -1;
                        if (!right) return 1;

                        int itemResult = comp.Compare(leftIt.Current, rightIt.Current);
                        if (itemResult != 0) return itemResult;
                    }
                }
            }
        }
        //TODO: Chiedere: Non riesco a trovare il metodo "MemberwiseClone", per cosa viene usata?
        public static T Clone<T>(this T obj)
        {
            var inst = obj.GetType().GetMethod("MemberwiseClone", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);

            return (T)inst.Invoke(obj, null);
        }

        public static Boolean IsExistsFile(string path)
        {
            return File.Exists(path);
        }

        public static Boolean IsExistsDirectory(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            return di.Exists;
        }

        public static void MoveAndRenameFile(string pathFileInput, string pathOutput, string nameFile)
        {
            if (IsExistsFile(pathFileInput))
            {
                var extension = System.IO.Path.GetExtension(pathFileInput);

                File.Move(pathFileInput, System.IO.Path.Combine(pathOutput, nameFile + extension));
            }
        }

        public static void DeleteFile(string path)
        {
            if (File.Exists(path))
                File.Delete(path);
        }

        public static void DeleteAllFile(string path)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(path);

            if (di.Exists)
            {
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
            }
            else
            {
                Directory.CreateDirectory(path);
            }
        }
    }
}
