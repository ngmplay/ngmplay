﻿using System;
using System.IO;
using System.Security.Cryptography;


namespace hash
{
    class Program
    {
        static void Main(string[] args)
        {

            if (args[0].ToString() == "")
            {
                Console.WriteLine("<file da valutare non indicato>");
            }
            else
            { 
                if (File.Exists(args[0].ToString()))
                {
                    Console.WriteLine(BytesToString(GetHashSha256(args[0].ToString())));
                }
                else
                {
                   Console.WriteLine("<file "+args[0].ToString()+" non trovato>");
                }
            }

        }


        private static SHA256 Sha256 = SHA256.Create();

  
        private static byte[] GetHashSha256(string filename)
        {
            using (FileStream stream = File.OpenRead(filename))
            {
                return Sha256.ComputeHash(stream);
            }
        }

        public static string BytesToString(byte[] bytes)
        {
            string result = "";
            foreach (byte b in bytes) result += b.ToString("x2");
            return result;
        }
    }
}
