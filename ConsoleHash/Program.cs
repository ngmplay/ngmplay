﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleHash
{
    class Program
    {
        static void Main(string[] args)
        {

            if (args[0].ToString() == "")
            {
                Console.WriteLine("<no input file>");
            }
            else
            {
                String NomeFile = args[0].ToString().Replace("\"", "");

                if (File.Exists(NomeFile))
                {
                    Log.AggiungiLog("Esistente: |"+ NomeFile + "|", "ConsoleHash");

                    Console.WriteLine(BytesToString(GetHashSha256(NomeFile)));

                    FileInfo fi = new FileInfo(NomeFile);

                    DateTime dt = fi.LastWriteTime;

                    Console.WriteLine(dt.Day.ToString().PadLeft(2, '0') +'-'+ dt.Month.ToString().PadLeft(2, '0') +'-'+ dt.Year.ToString().PadLeft(4, '0')    + ' '  + dt.Hour.ToString().PadLeft(2, '0') +':' + dt.Minute.ToString().PadLeft(2, '0')+':'  + dt.Second.ToString().PadLeft(2, '0'));
                }
                else
                {
                    Log.AggiungiLog("Inesistente: |" + args[0].ToString() + "|", "ConsoleHash");
                    Console.WriteLine("<file not found>");

                    Console.WriteLine("01-01-1900 00:00:00");

                }
            }

        }


        private static SHA256 Sha256 = SHA256.Create();


        private static byte[] GetHashSha256(string filename)
        {
            using (FileStream stream = File.OpenRead(filename))
            {
                return Sha256.ComputeHash(stream);
            }
        }

        public static string BytesToString(byte[] bytes)
        {
            string result = "";
            foreach (byte b in bytes) result += b.ToString("x2");
            return result;
        }
    }
}
