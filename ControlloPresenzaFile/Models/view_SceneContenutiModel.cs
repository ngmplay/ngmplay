﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class view_SceneContenutiLedModel
    {

      
        public String nva_NomeStadio { get; set; }
        public String nva_NomePeriodo { get; set; }
        public String nva_NomeBox { get; set; }
        public String nva_PahtFile { get; set; }
        public String nva_Sponsor { get; set; }
    //    public TimeSpan time_inizio  { get; set; }
        public int int_MarginL { get; set; }
        public int int_MarginT { get; set; }
        public int int_H { get; set; }
        public int int_W { get; set; }
        public int fk_int_Periodo { get; set; }
        public int fk_int_Stadio { get; set; }
     //   public int fk_int_Periodo { get; set; }
        
        public int fk_int_TipoElemento { get; set; }
        public int fk_int_Scena { get; set; }
        public int int_Ordine { get; set; }

        public string Tabella { get; set; } = "view_SceneContenutiLed";

    }
}
