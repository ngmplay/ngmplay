﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_TipoElemento : ControlloPresenzaFile.Utility.ExtendModel
    {
        public tb_TipoElemento(int ID = 0)
        {
            if (ID > 0)
                this.Popola(ID);
        }



        public int ID {get;set; }
        public string nva_Nome { get; set; }

     

    }
}
