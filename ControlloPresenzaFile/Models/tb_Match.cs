﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_Match : ControlloPresenzaFile.Utility.ExtendModel
    {


        public tb_Match(int ID = 0)
        {
            if (ID > 0)
                this.Popola(ID);
        }

        public int IsSelected { get; set; }

        public int Id { get; set; }
        int _fk_int_Stadio;
        public int fk_int_Stadio {
            get {
                return _fk_int_Stadio;
            }
            set {
                _fk_int_Stadio = value;
                Stadio = new tb_Stadi();
                Stadio.Popola(value);
            }
        }

        public string nva_Titolo { get;set; }

        int _fk_int_SquadraCasa;
        public int fk_int_SquadraCasa
        {
            get
            {
                return _fk_int_SquadraCasa;
            }
            set
            {
                _fk_int_SquadraCasa = value;
                SquadraCasa = new tb_Squadre();
                SquadraCasa.Popola(value);
            }
        }


        /*versione PEGGIORATIVA delle prestazioni*/
        int _fk_int_SquadraOspite;
        public int fk_int_SquadraOspite
        {
            get
            {
                return _fk_int_SquadraOspite;
            }
            set
            {
                _fk_int_SquadraOspite = value;
                SquadraOspite = new tb_Squadre();
                SquadraOspite.Popola(value);
            }
        }





        public tb_Stadi Stadio { get; set; }
        public tb_Squadre SquadraCasa { get; set; }
        public tb_Squadre SquadraOspite { get; set; }





        // public string Tabella { get; set; } = "tb_Scene";

    }
}
