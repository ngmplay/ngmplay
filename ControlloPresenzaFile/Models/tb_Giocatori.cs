﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_Giocatori: ControlloPresenzaFile.Utility.ExtendModel
    {

        public tb_Giocatori(){}

        public tb_Giocatori(int ID = 0)
        {
            if (ID > 0)
                this.Popola(ID);
        }


        public int Id { get; set; }
        public int fk_int_Squadra { get; set; }
        public string nva_Nome { get;set; }
        public string nva_Cognome { get; set; }
        public string nva_Numero { get; set; }
        public DateTime? dtt_DataNascita { get; set; } = null;
        public string nva_Nazionalità { get; set; }
        public string nva_Ruolo { get; set; }




    }
}
