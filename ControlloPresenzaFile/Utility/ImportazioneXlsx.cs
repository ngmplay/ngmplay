﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using System.IO;
using System.Data;
using Microsoft.Win32;
using System.Collections;


namespace ControlloPresenzaFile.Utility
{
    public class ImportazioneXlsx
    {

        public DataTable dtable;
        private string _PathFile;
        private int RigaConIntestazioni = 3;



        public ImportazioneXlsx(string PathFile)
        {
            _PathFile = PathFile;
            dtable = XlsToDatatable();

        }



        public string CreasqlDataTable(string NomeTabella)
        {

          return CreateTABLE(NomeTabella, dtable);

        }


       







        private string FindFirtsNoEmptyCell(ExcelWorksheet workSheet, int colonna)
        {
            for (int r = RigaConIntestazioni; r <= 100; r++)
            {
                string s = workSheet.Cells[r, colonna].Text;
                if (s.Length > 0)
                    return s;
            }
            return "";
        }







        private DataTable CreateDataTableForData(ExcelWorksheet workSheet)
        {
            DataTable dt = new DataTable();
            var end = workSheet.Dimension.End;
            try
            {
                dt.Columns.Add(new DataColumn { ColumnName = "Periodo" });
                dt.Columns.Add(new DataColumn { ColumnName = "Progressivo" });
                dt.Columns.Add(new DataColumn { ColumnName = "Durata" });


                for (int col = 4; col <= end.Column; col++)
                {
                    string NomeColonna = workSheet.Cells[RigaConIntestazioni, col].Text.Trim().Replace(" ", "_");
                    int.TryParse(workSheet.Cells[(RigaConIntestazioni + 1), col].Text.Trim(), out int Size);
                    DataColumn d = new DataColumn();
                    d.ColumnName = NomeColonna;
                    d.ExtendedProperties["size"] = Size;
                    dt.Columns.Add(d);
                }
            }
            catch (Exception ex)
            {
               Console.WriteLine("Controllare che le celle nella prima riga non siano vuote o abbiano nomi univoci");
            }
            return dt;
        }



        /*
        private void SalvaDatatableToXls(DataTable dt)
        {
            FileInfo file = new FileInfo(filePath);
            using (ExcelPackage package = new ExcelPackage(file))
            {
                int r = 2;
                foreach (DataRow dr in dt.Rows)
                {
                    int c = 3;
                    foreach (DataColumn col in dt.Columns)
                    {
                        object _act = workSheet.Cells[r, c].Value;
                        object _new = dr[col];
                        Console.WriteLine("----------------------------------");
                        Console.WriteLine("act:");
                        Console.WriteLine(_act);
                        Console.WriteLine("new:");
                        Console.WriteLine(_new);
                        Console.WriteLine("----------------------------------");
                        if (_act != _new)
                        {
                            workSheet.Cells[r, c].Value = _new;
                        }
                        c++;
                    }
                    r++;
                }
                package.Save();
            }
        }
        */



        private DataTable XlsToDatatable()
        {
            FileInfo file = new FileInfo(_PathFile);
            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
                var start = workSheet.Dimension.Start;
                var end = workSheet.Dimension.End;
                DataTable dt = CreateDataTableForData(workSheet);
                for (int row = (RigaConIntestazioni + 1); row <= end.Row; row++)
                {

                    int c = 1;
                    DataRow dr = dt.NewRow();
                    foreach (DataColumn col in dt.Columns)
                    {
                        object o = null;

                       // switch (col.ColumnName)
                        //{
                            //case "Periodo":
                                //if ((workSheet.Cells[row, 1].Text != "") && (workSheet.Cells[row, 2].Text == ""))
                                //{
                        o = workSheet.Cells[row, c].Text;
                                //}
                                //else if (dt.Rows.Count > 1)
                                //{
                                //    o = dt.Rows[dt.Rows.Count - 1][col];
                                //}

                            //    break;
                            //case "Progressivo":
                            //    o = workSheet.Cells[row, c].Value;
                            //    break;

                            //default:
                            //    o = workSheet.Cells[row, c].Value;

                            //    break;
                     //   }


                        dr[col] = o;

                        c++;

                    }
                    dt.Rows.Add(dr);
                }


                DataTable dt2 = new DataTable();
                dt2 = dt.Copy();

                return dt2;
            }
        }



        public static string CreateTABLE(string tableName, DataTable table)
        {
            string sqlsc;
            sqlsc = "CREATE TABLE " + tableName + "(";
            for (int i = 0; i < table.Columns.Count; i++)
            {
                sqlsc += "\n [" + table.Columns[i].ColumnName + "] ";
                string columnType = table.Columns[i].DataType.ToString();
                switch (columnType)
                {
                    case "System.Int32":
                        sqlsc += " int ";
                        break;
                    case "System.Int64":
                        sqlsc += " bigint ";
                        break;
                    case "System.Int16":
                        sqlsc += " smallint";
                        break;
                    case "System.Byte":
                        sqlsc += " tinyint";
                        break;
                    case "System.Decimal":
                        sqlsc += " decimal ";
                        break;
                    case "System.DateTime":
                        sqlsc += " datetime ";
                        break;
                    case "System.String":
                    default:
                        sqlsc += string.Format(" nvarchar({0}) ", table.Columns[i].MaxLength == -1 ? "max" : table.Columns[i].MaxLength.ToString());
                        break;
                }
                if (table.Columns[i].AutoIncrement)
                    sqlsc += " IDENTITY(" + table.Columns[i].AutoIncrementSeed.ToString() + "," + table.Columns[i].AutoIncrementStep.ToString() + ") ";
                if (!table.Columns[i].AllowDBNull)
                    sqlsc += " NOT NULL ";
                sqlsc += ",";
            }
            return sqlsc.Substring(0, sqlsc.Length - 1) + "\n)";
        }


    }



}



