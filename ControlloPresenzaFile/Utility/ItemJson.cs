﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
//using System.Linq.Dynamic.Core;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Utility
{
    public class ItemJson
    {

        public String ItemsToJson(IQueryable items, List<String> columnNames, String sort, String order, Int32 limit, Int32 offset)
        {
            try
            {
                // where clause is set, count total records
                Int32 count = items.Count();

                // Skip requires sorting, so make sure there is always sorting
                String sortExpression = "";

                if (sort != null && sort.Length > 0)
                    sortExpression += String.Format("{0} {1}", sort, order);

                // show all records if limit is not set
                if (limit == 0)
                    limit = count;

                // Prepare json structure
                var result = new
                {
                    total = count,
                    rows = items.OrderBy(sortExpression).Skip(offset).Take(limit).Select("new (" + String.Join(",", columnNames) + ")")
                };

                return JsonConvert.SerializeObject(result, Formatting.None, new JsonSerializerSettings()
                { MetadataPropertyHandling = MetadataPropertyHandling.Ignore });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
