﻿using RenderMachine.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ControlloPresenzaFile
{
    public static class ii
    {

        public static string AcquisisciConfigurazioneStadio(int fk_int_Stadio, string xmlString)
        {
            try
            {

                //tb_Stadi stadio = new tb_Stadi();
                //stadio.Popola(fk_int_Stadio);
                //stadio.tin_ConfigurazioneOK = 0;
                //stadio.DB_Update();
               
   

                //DB.EseguiSQL("update tb_DisposizioneLed set dtt_Cancellazione = getdate() where fk_int_Stadio = " + fk_int_Stadio.ToString());
                var xml = XDocument.Parse(xmlString);







                DataTable dt = DB.GetDataFromSQL("select * from tb_layer");
                foreach (DataRow r in dt.Rows)
                {


                    XElement elemento = (from xml2 in xml.Root.Descendants(r["nva_NomeTagXML"].ToString()) select xml2).FirstOrDefault();
                    if (elemento != null)
                    {


                      

                    var query = from c in elemento.Descendants("segmento")
                                select new
                                {
                                    height = c.Attribute("h").Value,
                                    width = c.Attribute("w").Value,
                                    name = c.Attribute("name").Value,
                                    position = c.Attribute("position").Value
                                };


                        foreach (var c in query)
                        {

                            Console.WriteLine(c.position);

                           // Console.WriteLine(c.height);
                            tb_DisposizioneLed dlm = new tb_DisposizioneLed();
                            dlm.int_H = Convert.ToInt32(c.height);
                            dlm.int_W = Convert.ToInt32(c.width);
                            dlm.fk_int_Stadio = fk_int_Stadio;
                            dlm.fk_int_Layer = Convert.ToInt32(r["ID"]); // int.Parse(c.layer);
                            dlm.int_Ordine = Convert.ToInt32(c.position);
                            dlm.nva_Nome = c.name;
                          //  DB.DB_InsertInto(dlm);

                        }

                   //     DB.EseguiSQL("Exec sp_import_01_AggiornaDisposizioneLed @fk_int_Stadio = " + fk_int_Stadio.ToString() + ", @fk_int_Layer =  " + r["ID"].ToString());

                    }

                }





                //var queryLED = from c in xml.Root.Descendants("positionLed") select new { positionX = c.Element("positionX").Value, positionY = c.Element("positionY").Value, width = c.Element("width").Value, height = c.Element("height").Value };
                //var queryMaxischermo = from c in xml.Root.Descendants("positionLedWall") select new { positionX = c.Element("positionX").Value, positionY = c.Element("positionY").Value };
                //Console.WriteLine(queryLED.FirstOrDefault().width.ToString() + " " + queryLED.FirstOrDefault().height.ToString());







                //stadio.int_W = int.Parse(queryLED.FirstOrDefault().width);
                //stadio.int_H = int.Parse(queryLED.FirstOrDefault().height);
                //stadio.int_X = int.Parse(queryLED.FirstOrDefault().positionX);
                //stadio.int_Y = int.Parse(queryLED.FirstOrDefault().positionY);
                //stadio.int_XMaxischermo = int.Parse(queryMaxischermo.FirstOrDefault().positionX);
                //stadio.int_YMaxischermo = int.Parse(queryMaxischermo.FirstOrDefault().positionY);
                //stadio.int_WMaxischermo = 0;
                //stadio.int_HMaxischermo = 0;
                //stadio.tin_ConfigurazioneOK = 1;
                //stadio.DB_Update();



                return "";
            }
            catch (Exception ex)
            {

                return ex.ToString();
            }

        }
    }
}
