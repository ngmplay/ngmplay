﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Timers;


namespace ImpostaPriorità
{
    public partial class Form1 : Form

    {


        static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        static int alarmCounter = 1;
        static bool exitFlag = false;


        public Form1()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(0, 0);

            myTimer.Tick += new EventHandler(TimerEventProcessor);

     
            myTimer.Interval = 60000;
            myTimer.Start();



            //Process me = Process.GetCurrentProcess();
            //foreach (Process p in Process.GetProcesses())
            //{

            //    if (p.Id != me.Id
            //            && !p.ProcessName.ToLower().StartsWith("winlogon")
            //            && !p.ProcessName.ToLower().StartsWith("system idle process")
            //            && !p.ProcessName.ToLower().StartsWith("taskmgr")
            //            && !p.ProcessName.ToLower().StartsWith("spoolsv")
            //            && !p.ProcessName.ToLower().StartsWith("csrss")
            //            && !p.ProcessName.ToLower().StartsWith("smss")
            //            && !p.ProcessName.ToLower().StartsWith("svchost")
            //            && !p.ProcessName.ToLower().StartsWith("services")
            //            && !p.ProcessName.ToLower().StartsWith("lsass")
            //            && !p.ProcessName.ToLower().StartsWith("dotnet")
            //            && !p.ProcessName.ToLower().StartsWith("chrome")
            //        )
            //    {
            //        if (p.MainWindowHandle != IntPtr.Zero)
            //        {
            //            p.Kill();
            //        }
            //    }


            //}
        }

        // This is the method to run when the timer is raised.
        private  void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {

            Imposta();
           // myTimer.Start();
           // myTimer.Enabled = true;

          
        }

        public void Imposta()
        {
           





            Process[] processlist = Process.GetProcesses();
            textBox1.Clear();
            foreach (Process theprocess in processlist)
            {
                if (theprocess.ProcessName.Contains("chrom"))
                {
                     if (theprocess.PriorityClass != System.Diagnostics.ProcessPriorityClass.High)
                    {
                        theprocess.PriorityClass = System.Diagnostics.ProcessPriorityClass.High;
                        textBox1.AppendText("imposto priorità realtime al processo " + theprocess.ProcessName + " ID:  " + theprocess.Id+ System.Environment.NewLine);
                    }
                }
            }
        }
    }
}
