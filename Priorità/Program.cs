﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


namespace Priorità
{
    class Program
    {



            static void Main(string[] args)
            {

            Process[] processlist = Process.GetProcesses();

            foreach (Process theprocess in processlist)
            {
                if (theprocess.ProcessName.Contains("chrom"))
                {
                    // Console.WriteLine("Process: {0} ID: {1} Priorità {2}", theprocess.ProcessName, theprocess.Id, theprocess.BasePriority);

                    Console.WriteLine("imposto priorità realtime ai processi");
                    if(theprocess.PriorityClass != System.Diagnostics.ProcessPriorityClass.RealTime) { 
                        theprocess.PriorityClass = System.Diagnostics.ProcessPriorityClass.RealTime;
                        Console.WriteLine("imposto priorità realtime al processo {0} ID: {1} ", theprocess.ProcessName, theprocess.Id);

                    }
                }
            }

        }

        
    }
}

