﻿
function PopolaFormCrudGiocatore(id) {
    $.ajax({
        url: "Crud/GetGiocatore",
        type: "get",
        data: { 'id': id },
        success: function (data) {
            data = JSON.parse(data)[0];
            Object.keys(data).forEach(function (k) {
                console.log(k + ' - ' + data[k]);
                $("#form_giocatori_" + k).val(data[k]);
            });
            var date = new Date();
            let antiCache = date.getTime();
            $("#PreviewImmagineGiocatore").attr("src", "\assetsSquadre\\" + data["nva_Directory"] + "\\Players\\" + data["nva_Numero"] + ".png?" + antiCache);
        }
    });
}




$('body').on('click', '.btn-salvaGiocatore', function () {
    let argomento = $(this).closest('table').data("argomento")
    var formData = new FormData();
    formData.append('ImmagineGiocatore', $('#form_giocatori_ImmagineGiocatore')[0].files[0]);
    formData.append('id', "50");
    formData.append('nva_Nome', $('#form_giocatori_nva_Nome').val());
    formData.append('nva_Cognome', $('#form_giocatori_nva_Cognome').val());
    formData.append('fk_int_Squadra', $('#form_giocatori_fk_int_Squadra').val());
    formData.append('nva_Numero', $('#form_giocatori_nva_Numero').val());

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "crud/SalvaGiocatore",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data);
            data = JSON.parse(data)
            PopolaFormCrudGiocatore(data.id)
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
    });

})