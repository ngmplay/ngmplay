﻿var codaAnimazioneEventiMaxischermo = [];
var codaAnimazioneEventiLed = [];
var TimerEvento;
var DatiEseguiti = [];
var SettingMatch = {} //configurazione del match: data, ora, etc...
var SettingSquadra = {} //configurazione squadra: nome, etc...
var RosaSquadra = {} //lista dei giocatori
var objEvento; //evento da archiviare ed in alcuni casi riprodurre su maxischermo o bordocampo
var fk_int_RiproduzioneSelezionata;
//var Messaggi = [];

var int_MinutoPartita = 1; //minutaggio partita, variabile globale da incrementare da parte del player
var settings = {};
var scena = {}
var SettingGlobalVars = []
var ObjPlaylist = {}

var arrTimeStampInizioPeriodo = {}
var timeStampAct = 0
var arrTimeStampFinePeriodo = {}
var fk_int_PeriodoAttivo = 0;
var offsetOrologio = 0;
var offsetSpot = 0;
var sessioneDispositivo;

var ArrDecodificaSquadre = {};

var timerScambioDati;
var periodoScambioDati = 1000;

var PosizioMememoriaScaler = 0

var ArrTipoEventiMatch;

var ArrEventiPartita = [];
ArrEventiPartita[1] = "Goal";
ArrEventiPartita[2] = "Ammonizione";
ArrEventiPartita[3] = "Espulsione";
ArrEventiPartita[4] = "Sostituzione";
ArrEventiPartita[5] = "Auto-Goal";
ArrEventiPartita[6] = "Presentazione giocatore";
var StrJsonSetupSchermataUI = "";

var dt = new Date();
var TimeAvvioSchermata =  dt.getTime();
var TimerAggiornamentoLog;

var ritardoTraClientEServer = 0;

var IntervalloSecondiCaricamentoRisultatiLive = 5
var ContatoreSecondiCaricamentoRisultatiLive = 0;

$.fn.removeClassStartingWith = function (filter) {
    $(this).removeClass(function (index, className) {
        return (className.match(new RegExp("\\S*" + filter + "\\S*", 'g')) || []).join(' ')
    });
    return this;
};

class clsPosizioneGiocatore {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

class clsEventoMatch {
    constructor(fk_int_Giocatore, fk_int_GiocatoreSecondario, fk_int_Squadra, fk_int_TipoEventoMatch, int_MinutoPartita = 0) {
        
        this.fk_int_Giocatore = fk_int_Giocatore;
        this.fk_int_GiocatoreSecondario = fk_int_GiocatoreSecondario;
        this.fk_int_Squadra = fk_int_Squadra;
        this.fk_int_TipoEventoMatch = fk_int_TipoEventoMatch;
        this.int_MinutoPartita = int_MinutoPartita;
        this.tin_Led = true;
        this.tin_Maxischermo = true ;
        this.nva_Note = "";
        this.nva_TipoEsecuzione = "";
        this.ObjRisultatoService = undefined; 

    }
}



class ObjRisultatoService {
    constructor(fk_int_SquadraCasa, fk_int_SquadraOspiti, int_GoalCasa, int_GoalOspiti) {
        this.fk_int_SquadraCasa = fk_int_SquadraCasa;
        this.fk_int_SquadraOspiti = fk_int_SquadraOspiti;
        this.int_GoalCasa = int_GoalCasa;
        this.int_GoalOspiti = int_GoalOspiti;
    }
}





class clsFileDownload {
    constructor(Contatore, NomeFile, DataFile) {
        this.Contatore = Contatore;
        this.NomeFile = NomeFile;
        this.DataFile = DataFile;
    }
}


class clsMatch {
    constructor(id,fk_int_Stadio, dtt_DataOra , nva_Titolo, fk_int_SquadraCasa, fk_int_SquadraOspite, IsSelected) {

        this.id=id
        this.fk_int_Stadio = fk_int_Stadio
        this.dtt_DataOra = dtt_DataOra
        this.nva_Titolo = nva_Titolo
        this.fk_int_SquadraCasa = fk_int_SquadraCasa
        this.fk_int_SquadraOspite = fk_int_SquadraOspite
        this.IsSelected = IsSelected

    }
}








function PopolaSettingMatch() {


    $.ajax({

        url: "/Ajax/sp_GetSelectedMatch",
        async: false

    }).done(function (data) {
        if (data == "") {

            alert("attenzione! non riesco a popolare le info per il match con id ");
        } else {

       
        //////console.log("PopolaSettingMatch data:" + data)
        SettingMatch = JSON.parse(data)
        console.log("SettingMatch:")
        console.log(SettingMatch)

        fk_int_RiproduzioneSelezionata = SettingMatch.fk_int_Riproduzione;

            $.ajax({

                url: "/Ajax/sp_GetSquadreCategoria",
                async: false

            }).done(function (data) {
                $.each(JSON.parse(data), function (ii, contenuto) {
                    PopolaSettingSquadra(contenuto.id);
                   // console.log(contenuto);
                });
            });
        

        /*
        PopolaSettingSquadra(SettingMatch.fk_int_SquadraCasa);
        PopolaSettingSquadra(SettingMatch.fk_int_SquadraOspite);
        */





        PopolaRosaSquadra(SettingMatch.fk_int_SquadraCasa);
        PopolaRosaSquadra(SettingMatch.fk_int_SquadraOspite);





        sp_SendToPlayerDecodificaSquadre();
        }
    });

}


function TraduciMinutiSecondiSet() {
    return (parseInt($(".SetMinuti").val()) * 60) + parseInt($(".SetSecondi").val());
}

function IsYes(valore) {

    if (valore == true) { return true }
    valore = valore.toLowerCase();
    if (valore == "1") { return true }
    if (valore == "si") { return true }
    if (valore == "yes") { return true }
    if (valore == "true") { return true }
    //console.log("return false")
    return false;
}

function AvviaVideoDaZero(DivVideo) {

    setTimeout(function () {
        DivVideo.each(function () {
            $(this).get(0).pause();
            $(this).get(0).currentTime = 0;
            $(this).get(0).play();
        });
    }, 500);

}




function sp_GetParametriIniziali() {
    return $.ajax({
        url: "/Ajax/sp_GetParametriIniziali"
    }).done(function (data) {

        ArrParametriGenerali = JSON.parse(data);
        //console.log("ArrParametriGenerali");
        //console.log(ArrParametriGenerali);
    });
}

function sp_GetTipoEventiMatch() {
    return $.ajax({
        url: "/Ajax/sp_GetTipoEventiMatch"
    }).done(function (data) {

        ArrTipoEventiMatch = JSON.parse(data);

    });
}



function sp_SendToPlayerDecodificaSquadre() {
    return $.ajax({
        url: "/Ajax/sp_SendToPlayerDecodificaSquadre"
    }).done(function (data) {
        //nodata
    });
}





function ControllaSetting(obj) {
    if (obj.ID > 0) {
        return true
    }
    alert("prima di proseguire devi selezionare un match")
    return false
}


function PopolaObjPlaylist() {
 
    $.ajax({
        url: "/Ajax/sp_GetSelectedPlaylist",
        async: false
    }).done(function (data) {

        ObjPlaylist = JSON.parse(data);

    });
}


function getAllKeyWithProperty(arr, IdDaTrovare) {
    let a = [];
    for (item in arr) {
        if (arr[item].fk_int_SceneEventiSpeciali == IdDaTrovare) {
            a.push(item);
        }
    }
    return a;
}

function Adesso() {
    let date = new Date();
    let y = parseInt(date.getTime()) + parseInt(ritardoTraClientEServer) 
    return y ; //va solo su client
}






function PopolaSettingSquadra(fk_int_Squadra) {
    //////console.log("fk_int_Squadra: " + fk_int_Squadra);

    $.ajax({
        url: "/Ajax/GetSettingSquadra?fk_int_Squadra=" + fk_int_Squadra,
        async: false
    }).done(function (data) {
        console.log(data);

        SettingSquadra[fk_int_Squadra] = JSON.parse(data);
        //console.log("PopolaSettingSquadra eseguito");
        //console.log(SettingSquadra[fk_int_Squadra]);
    });


}

function PopolaRosaSquadra(fk_int_Squadra) {
    //////console.log("PopolaRosaSquadra: " + fk_int_Squadra);
    return $.ajax({
        url: "/Ajax/GetRosaSquadra?fk_int_Squadra=" + fk_int_Squadra
    }).done(function (data) {
        RosaSquadra[fk_int_Squadra] = JSON.parse(data);

       //console.log("PopolaRosaSquadra eseguito");
      //console.log(RosaSquadra);
    });
}


function SetDeleted(ID, tb) {
    SettingGlobalVars[tb] = ID;
    //console.log("SetDeleted");
    //console.log(ID);
    return $.ajax({
        url: "/Ajax/SetDeleted",
        data: "ID=" + ID + "&tb=" + tb,
        async: false
    });
}


function SetSelected(ID, tb) {
    SettingGlobalVars[tb] = ID;
    //////console.log(tb);

    return $.ajax({
        url: "/Ajax/SetSelected",
        data: "ID=" + ID + "&tb=" + tb,
        async: false
    });
}

function GetSelected(tb) {
    return $.ajax({
        url: "/Ajax/GetSelected",
        data: "tb=" + tb,
        async: false
    }).done(function (ID) {
        SettingGlobalVars[tb] = ID;
        return ID
    });
}






function numProps(obj) {
    var c = 0;
    for (var key in obj) {
        if (obj.hasOwnProperty(key))++c;
    }
    return c;
}


function IsUndefined(obj) {

    if (obj == undefined) {
        return true;
    } else {
        return false;
    }

}


function CostruisciMessaggio(dato, Messaggi = []) {

    var Messaggio = {}
    Messaggio.nva_Dato = encodeURIComponent(dato)
    Messaggi.push(Messaggio);

    return Messaggi;
}



function SetDatiToPlayer(Messaggi) {
  
    if (Messaggi.length > 0) {
        //////console.log("SetDatiToPlayer INVIO: " + JSON.stringify(Messaggi))
        $.ajax({
            dataType: 'text',
            contentType: 'application/json',
            type: 'POST',
            url: "/Ajax/SetDatiToPlayer",
            data: JSON.stringify(Messaggi)
           // async: 0
        }).done(function (data) {
            Messaggi = [];
           // ////console.log("svuoto i messaggi");
        })
    } else {
        //////console.log("nessun messaggio da inviare")
    }

}


function SetDatiToUI(Messaggi) {

    if (Messaggi.length > 0) {
        ////console.log("SetDatiToUI INVIO: " + JSON.stringify(Messaggi))
        $.ajax({
            dataType: 'text',
            contentType: 'application/json',
            type: 'POST',
            url: "/Ajax/SetDatiToUI",
            data: JSON.stringify(Messaggi)
        }).done(function (data) {

            Messaggi = [];

        })

    } else {

        //////console.log("nessun messaggio da inviare")

    }

}



    function GetDatiToUi() {


        $.ajax({
            url: "/Ajax/GetDatiToUi"
        }).done(function (data) {

            data = JSON.parse(data);
            if (data.length > 0) {
                $.each(data, function (ii, contenuto) {
                    //////console.log("attenzione!")
                   // //console.log(contenuto)
                    try {

                        if (!DatiEseguiti.includes(contenuto.ID)) {
                            let g = decodeURIComponent(contenuto.nva_Dato);
                            //console.log("GetDatiToUi eseguo: " + g)
                            DatiEseguiti.push(contenuto.ID);
                            eval(g)
                        }
                    }
                    catch (err) {
                        ////console.log("ATTENZIONE!!! ERRORE EVAL")
                        ////console.log(err)
                        ////console.log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
                    }
                    /*
                    $.ajax({ url: "/Ajax/SetExecutedFromPlayerToUi?ID=" + contenuto.ID }).done(function (conferma) {
                        if (conferma != "ok") {
                            alert("problema trasporto dati " + contenuto.ID)
                        }
                    })
                    */
                });
            }

        });

    }



function GetDatiToPlayer() {


    $.ajax({
        url: "/Ajax/GetDatiToPlayer"
    }).done(function (data) {

        data = JSON.parse(data);
        if (data.length > 0) {
            $.each(data, function (ii, contenuto) {

                try {

                    if (!DatiEseguiti.includes(contenuto.ID)) {
                        let g = decodeURIComponent(contenuto.nva_Dato);
                       //console.log("GetDatiToPlayer eseguo: " + g)
                        DatiEseguiti.push(contenuto.ID);
                        eval(g)
                        //delete  g;
                    }

                }
                catch (err) {
                    ////console.log("ATTENZIONE!!! ERRORE EVAL")
                    ////console.log(err)
                    ////console.log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
                }
                /*
                $.ajax({ url: "/Ajax/SetExecutedFromUiToPlayer?ID=" + contenuto.ID }).done(function (conferma) {
                    if (conferma != "ok") {
                        alert("problema trasporto dati " + contenuto.ID)
                    }
                })
                */
            });
        }

    });








}



//funzione temporanea per il controllo degli array (archiviazione nel db, comprensione problematiche di timing)
function DebugVariabili(Messaggio, NomeVariabile, Variabile) {
    let ooo = {};
    ooo.nva_Messaggio = Messaggio;
    ooo.nva_NomeArray = NomeVariabile;
    ooo.nva_Contenuto = JSON.stringify(Variabile);
    ooo.nva_NumeroElementi = Variabile.length;


    $.ajax({
        dataType: 'text',
        type: 'POST',
        url: "/Ajax/ArchiviaRecordDebug",
        data: ooo
    })

    //delete  ooo;

}



function ScriviLog(Messaggio) {

 
    console.error(Messaggio);
    let msg = encodeURIComponent(Messaggio)
    $.ajax({
        dataType: 'text',
        type: 'POST',
        url: "/Ajax/ScriviLog",
        data: { "msg": msg}
    })
    //delete  msg;


}

function pad(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}


function RiduciPeriodoScambioDati() {

    periodoScambioDati = 250;

    setTimeout(AumentaPeriodoScambioDati, 10000);

}

function AumentaPeriodoScambioDati() {

    periodoScambioDati = 1000;

}

function OrdinaElementiPerTagOrdine(a, b) {
    return ($(b).data('ordine')) < ($(a).data('ordine')) ? 1 : -1;
}

function AccodaDebugOnScreen(messaggio) {
    $("#DebugScaler").html(messaggio + "<BR/>" + $("#DebugScaler").html());
}

function CambiaMemoriaScaler(pin, ForzaInvio = false) {
    //console.log("541 CambiaMemoriaScaler(pin:" + pin + ")");
    //console.log("CambiaMemoriaScaler");
    AccodaDebugOnScreen("pin:"+pin);

    if (IsYes(ArrParametriGenerali.nva_EseguiChiamateScaler) == true) {
        //console.log("eseguo chiamata scaler");
        //console.log("545 PosizioMememoriaScaler:(" + PosizioMememoriaScaler + ")");
        if ((PosizioMememoriaScaler != pin) || (ForzaInvio)) {
            PosizioMememoriaScaler = pin
            //console.log("548 PosizioMememoriaScaler:(" + PosizioMememoriaScaler+")");
            $.ajax({ url: "/Ajax/SimulaPressioneTastoScaler?pin=" + pin }).done(function (data) {
                //console.log("PosizioMememoriaScaler:("+PosizioMememoriaScaler+") data:("+data+")")
                if (PosizioMememoriaScaler != data) {
                    //console.log("PosizioMememoriaScaler != data");

                    PosizioMememoriaScaler = 0;
                }
                //console.log("556 PosizioMememoriaScaler:(" + PosizioMememoriaScaler+")");
            });
        }
    }
            
}

function ArchiviaLog(nva_Messaggio,tin_Tipo=0) {

    try {
        if (IsYes(ArrParametriGenerali.nva_ArchiviaLogJavascript) == true) {
            $.ajax({ url: "/Ajax/ArchiviaLog", data: { "nva_Messaggio": nva_Messaggio, "tin_Tipo": tin_Tipo } });
        }
    } catch (messaggio) {

        $.ajax({ url: "/Ajax/ArchiviaLog", data: { "nva_Messaggio": messaggio, "tin_Tipo": 1 } });
    }

}

function GetEstensioneFile(filename) {
    //console.log("GetEstensioneFile");
//console.log(filename);
    //console.log("split");
    //console.log(filename.split('.'));
    //console.log("pop");
    //console.log(filename.split('.').pop());
    //console.log("toLowerCase");
    //console.log(filename.split('.').pop().toLowerCase());

    return filename.split('.').pop().toLowerCase();
}

function GetNomeFileSenzaEstensione(filename) {

    return filename.split('.').slice(0, -1).join('.').toUpperCase();
}

function json2array(json) {
    var result = [];
    var keys = Object.keys(json);
    keys.forEach(function (key) {
        result.push(json[key]);
    });
    return result;
}

function FormattaTimestampToggmmAAAHHmmss(ts) {
    ts = new Date(ts);
    let hours = ts.getHours();
    let minutes = ts.getMinutes();
    let seconds = ts.getSeconds();
    return hours + ':' + ("0" + minutes).substr(-2) + ':' + ("0" + seconds).substr(-2);
}


function FormattaTimestampToTempoPartita(diff) {

    let hours = Math.floor(diff / (1000 * 60 * 60));
    diff -= hours * (1000 * 60 * 60);

    let mins = Math.floor(diff / (1000 * 60));
    diff -= mins * (1000 * 60);

    let seconds = Math.floor(diff / (1000));
    diff -= seconds * (1000);

    return ("0" + mins).substr(-2) + ':' + ("0" + seconds).substr(-2);
}

(function (window) {
    window.htmlentities = {
		/**
		 * Converts a string to its html characters completely.
		 *
		 * @param {String} str String with unescaped HTML characters
		 **/
        encode: function (str) {
            var buf = [];

            for (var i = str.length - 1; i >= 0; i--) {
                buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
            }

            return buf.join('');
        },
		/**
		 * Converts an html characterSet into its original character.
		 *
		 * @param {String} str htmlSet entities
		 **/
        decode: function (str) {
            return str.replace(/&#(\d+);/g, function (match, dec) {
                return String.fromCharCode(dec);
            });
        }
    };
})(window);