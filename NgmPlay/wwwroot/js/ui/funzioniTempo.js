﻿
function tick() {
    //console.log("tick!");
    $.ajax({
        url: "/Ajax/sp_GetStatoAct",
    }).done(function (data) {
        if (data != "") {
            data = JSON.parse(data);
            data = data[0];
            IstanteInizioAvvioRiproduzione = data.int_StartPlayng;
            fk_int_PeriodoAttivo = data.id;
            $(".StatoAct").html(data.nva_Nome);
            int_MinutoPartita = Math.ceil((Adesso() - IstanteInizioAvvioRiproduzione) / 60000)

        }
    });

    if (fk_int_PeriodoAttivo > 0) {
        //let date = new Date();
        //let tsa = date.getTime() 

        let tsa = Adesso();

        let secondSpan = Math.floor((tsa - IstanteInizioAvvioRiproduzione) / 1000);
        let minuti = Math.floor(secondSpan / 60);
        let secondi = secondSpan - (minuti * 60);
        $("#TempoTrascorso").val(pad(minuti, 2) + ":" + pad(secondi, 2));
    } else {
        $("#TempoTrascorso").val("00:00");
    }

    //LimitiTempo();

}





function OgniSecondo() {
    TreSecondi = TreSecondi - 1;
    if (TreSecondi == 0) {
        OgniTreSecondi();
        TreSecondi = 3;
    }

    tick();
}

function OgniSecondo_Scaletta() {
    TreSecondi = TreSecondi - 1;
    if (TreSecondi == 0) {
        OgniTreSecondi_Scaletta();
        TreSecondi = 3;
    }

    tick();
}


function OgniTreSecondi() {
    sp_SetupSchermataUI();

    if (($("#ModalImportPlaylist").data('bs.modal') || {})._isShown) {
        PopolaModalPlaylist();
    }

    var periodoOraFissaAvviato = AvviaPeriodiOraFissa();
    if (!periodoOraFissaAvviato)
        AvviaPeriodiOraRelativa();
}

function OgniTreSecondi_Scaletta() {
    sp_SetupSchermataScaletta();
}

function sp_SetupSchermataScaletta() {
    var adesso = Adesso();
    $("#ListaSceneMaxischermo1T li").each(function (i, item) {
        if (item.dataset.dataorainizio > -1 && item.dataset.dataorainizio < adesso) {
            $(this).removeClass("table-warning");
            $(this).addClass("table-danger");
        }
    });
    $("#ListaSceneMaxischermo2T li").each(function (i, item) {
        if (item.dataset.dataorainizio > -1 && item.dataset.dataorainizio < adesso) {
            $(this).removeClass("table-warning");
            $(this).addClass("table-danger");
        }
    });
    $("#ListaSceneLed1T li").each(function (i, item) {
        if (item.dataset.dataorainizio > -1 && item.dataset.dataorainizio < adesso) {
            $(this).removeClass("table-warning");
            $(this).addClass("table-danger");
        }
    });
    $("#ListaSceneLed2T li").each(function (i, item) {
        if (item.dataset.dataorainizio > -1 && item.dataset.dataorainizio < adesso) {
            $(this).removeClass("table-warning");
            $(this).addClass("table-danger");
        }
    });
}


function sp_SetupSchermataUI() {
   // console.log("sp_SetupSchermataUI");
    $.ajax({
        url: "/Ajax/sp_SetupSchermataUI",
    }).done(function (data) {
        ////////console.log(data);
        if (data != StrJsonSetupSchermataUI) {
            StrJsonSetupSchermataUI = data;
            data = JSON.parse(data);
            $.each(data, function (i, item) {
                ////console.log(item);//{Destinazione: "Led", Iper: 2, Loaded: 0}
                $(".idPeriodo_" + item.Iper).prop("disabled", false);

                if ((item.LoadedLed > 0) && (item.LoadedMaxischermo > 0)) {
                    $(".idPeriodo_" + item.Iper).removeClass("btn-danger")
                    $(".idPeriodo_" + item.Iper).removeClass("btn-warning")
                    $(".idPeriodo_" + item.Iper).removeClass("btn-dark")
                    $(".idPeriodo_" + item.Iper).addClass("btn-success")
                } else {
                    $(".idPeriodo_" + item.Iper).addClass("btn-danger")
                    $(".idPeriodo_" + item.Iper).removeClass("btn-warning")
                    $(".idPeriodo_" + item.Iper).removeClass("btn-dark")
                    $(".idPeriodo_" + item.Iper).removeClass("btn-success")
                }
            })
        }
    })
}

function AvviaPeriodiOraFissa() {
    var avviato = false;
    /*
     * Se c'è un periodo a un'ora fissa (es. un prepartita che deve iniziare a una certa ora)
     * lo acquisisce dal DB (annullandogli l'avvio automatico) e lo avvia con la funzione 
     * AvviaPeriodoBottone
     */
    $.ajax({
        url: "/Ajax/AcquisisciPeriodiOraFissa",
    }).done(function (data) {
        if (data != undefined) {
            data = JSON.parse(data);
            $.each(data, function (i, item) {
                AvviaPeriodoBottone(item.fk_int_Periodo);
                avviato = true;
            })
        }
    })
    return avviato;
}


function AvviaPeriodiOraRelativa() {
    var avviato = false;
    /*
     * Se c'è un periodo a un'ora relativa (es. un evento da mostrare al 15' del secondo tempo...)
     * lo acquisisce dal DB (annullandogli l'avvio automatico) e lo avvia con la funzione 
     * AvviaPeriodoBottone
     */
    let tsa = Adesso();
    $.ajax({
        url: "/Ajax/AcquisisciPeriodiOraRelativa",
        data: {adesso: tsa}
    }).done(function (data) {
        if (data != undefined) {
            data = JSON.parse(data);
            $.each(data, function (i, item) {
                AvviaPeriodoBottone(item.fk_int_Periodo);
                avviato = true;
            })
        }
    })
    return avviato;
}
