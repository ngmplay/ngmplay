﻿
$("#ModalImportPlaylist").on('hidden.bs.modal', function () {
    clearTimeout(TimerAggiornamentoLog);
});
var contatoreScaricati = 0;
var tuttiDaScaricare = 0;

function AggiornaLogImportazione() {
    
    $.ajax({
        url: "/Ajax/GetLogImportazione"
    }).done(function (data) {
        console.log(data);
        data = JSON.parse(data);
        $(".LogElaborazionePlaylist ul").empty();
        $(data).each(function (k, o) {
            $(".LogElaborazionePlaylist ul").append("<li>" + o.nva_Log +"</li>");
        });
        PopolaModalPlaylist();
    });
}


$(".btn-ControllaAggiornamenti").click(function () {
    clearTimeout(TimerAggiornamentoLog);
    $(".progressDownload").css("width", "0%");
    $(".LogElaborazionePlaylist ul").empty();

    $.ajax({
        url: "/Ajax/ControllaAggiornamenti"
    }).done(function (data) {
        data = JSON.parse(data);
        console.log(data);
        if (data.length == 0) {
            alert("nessun file da aggiornare!");
        } else {
            contatoreScaricati = 0;
            tuttiDaScaricare = data.length;
            let domanda = confirm("sono presenti " + data.length + " files da aggiornare, vuoi proseguire con l'aggiornamento?");
            $(".progressDownload").css("width","0%");
            
            if (domanda == true) {
                
                $(".logImportazione").show();


                let NomeFile_xlsxPlaylist = "";
                let NomeFile_xlsxEventi = "";

                console.log("eccommi qui");

                $(data).each(function (k, o) {
                    if (o.NomeFile.toLowerCase().endsWith("playlist.xlsx")) {
                        TimerAggiornamentoLog = setInterval(AggiornaLogImportazione, 1000);
                        NomeFile_xlsxPlaylist = o.NomeFile;
                    }

                    if (o.NomeFile.toLowerCase().endsWith("eventi.xlsx")) {
                        TimerAggiornamentoLog = setInterval(AggiornaLogImportazione, 1000);
                        NomeFile_xlsxEventi = o.NomeFile;
                    }
                });



                $(data).each(function (k, o) {
                    $.ajax({
                        url: "/Ajax/DownloadFile",
                        data: o,
                        type: 'POST'
                    }).done(function (dd) {
                        if (dd != "ko") {
                            contatoreScaricati = contatoreScaricati + 1;
                            let w = (contatoreScaricati / tuttiDaScaricare) * 100;
                            $(".progressDownload").css("width", w + "%");
                        }
                    });
                });


                let l = JSON.stringify(data);

                let cp='1';
                if (NomeFile_xlsxPlaylist != "") {
                    cp='0';
                }

                let ce ='1';
                if (NomeFile_xlsxEventi != "") {
                    ce ='1';
                }


                $.ajax({
                    url: "/Ajax/InserisciAggiornamentoPlaylist",
                    data: { 'log': l, 'consistenzaPlaylist': cp, 'consistenzaEventi': ce },
                    type: 'POST'
                }).done(function (h) {
                    console.log("InserisciAggiornamentoPlaylist");
                    if (NomeFile_xlsxPlaylist != "") {
                        $.ajax({
                            url: "/Ajax/ImportaPlaylist",
                            type: 'POST'
                        }).done(function (hh) {
                            console.log("InserisciAggiornamentoPlaylist");
                        });
                    } else if (NomeFile_xlsxEventi != "") {

                        console.log("Aggiorno eventi");
                  
                        $.ajax({
                            url: "/Ajax/StepImportEventi",
                            type: 'POST'
                        }).done(function (hhh) {
                            console.log("InserisciAggiornamentoEventi");
                        });
                    }

                });






            

            }
        }
    });
});




$('.btn-SincronizzaDati').click(function () {
    var id = 0;
    $("#ModalDownloadAssets").modal("show");
    $('#ModalDownloadAssets').find(".Esito").empty();
    $('#ModalDownloadAssets').find(".Esito").append("<li>Inizio controllo categorie <div class='spinner-border spinner1' role='status'></div></li>");
    $.ajax({
        url: "/Ajax/SincronizzaCategorie"
    }).done(function (h) {

        $('.spinner1').hide();
        $('#ModalDownloadAssets').find(".Esito").append(h);
        $('#ModalDownloadAssets').find(".Esito").append("<li>Inizio controllo squadre <div class='spinner-border spinner2' role='status'></div></li>");
        $.ajax({
            url: "/Ajax/SincronizzaSquadre"
        }).done(function (hh) {
            $('.spinner2').hide();
            $('#ModalDownloadAssets').find(".Esito").append(hh);
            $('#ModalDownloadAssets').find(".Esito").append("<li>Inizio Controllo giocatori <div class='spinner-border spinner3' role='status'></div></li>");
            $.ajax({
                url: "/Ajax/SincronizzaGiocatori"
            }).done(function (hhh) {
                $('.spinner3').hide();
                $('#ModalDownloadAssets').find(".Esito").append(hhh);
                $('#ModalDownloadAssets').find(".Esito").append("<li>Attesa chiusura <div class='spinner-border' role='status'></div></li>");
                setTimeout(function () { $("#ModalDownloadAssets").modal("hide"); }, 2000);
            });           
        });           
    });   
    
});


$(".btn_Parametri").click(function () {
    GetParametriFromDatabase();
    $("#ModalParametri").modal("show");
});


$(".btn-SwitchPeriodi").click(function () {
    if ($(".btn-SwitchPeriodi").text().indexOf("SUPPLEMENTARI") >= 0) {
        $(".btn-SwitchPeriodi").html("T. REGOLAMENTARI")
        $(".periodi-regolari").hide();
        $(".periodi-supplementari").show();
    } else {
        $(".btn-SwitchPeriodi").html("SUPPLEMENTARI")
        $(".periodi-regolari").show();
        $(".periodi-supplementari").hide();
    }
});

$(".btn-SwitchPeriodi_FunzioniSpeciali").click(function () {
    if ($(".btn-SwitchPeriodi_FunzioniSpeciali").text().indexOf("SPECIALI") >= 0) {
        $(".btn-SwitchPeriodi_FunzioniSpeciali").html("CHIUDI F. SPEC.")
        $(".funzioni-speciali").show();
    } else {
        $(".btn-SwitchPeriodi_FunzioniSpeciali").html("FUNZIONI SPECIALI")
        $(".funzioni-speciali").hide();
    }
});


$(".btn_Luminosita").click(function () {
    GetValoriLuminositaFromDatabase();
    $("#ModalLuminosita").modal("show");
});

$(".btn-temporizzaPrepartita").click(function () {
    $("#ModalOrarioPrepartita").modal("show");
});

$(".btn-temporizzaEvento").click(function () {
    if ($(this).html().indexOf("AVVIO") > -1) {
        $(".InizioEventoOre").val(0);
        $(".InizioEventoMinuti").val(0);
        $(".InizioEventoIdPeriodo").html($(this).data("fk_int_periodo"));
    }
    else {
        $(".InizioEventoOre").val($(this).html().split(':')[0]);
        $(".InizioEventoMinuti").val($(this).html().split(':')[1]);
        $(".InizioEventoIdPeriodo").html($(this).data("fk_int_periodo"));
    }
    $("#ModalOrarioEvento").modal("show");
});

$(".btn-temporizzaEventoInContenitore").click(function () {
    if ($(this).html().indexOf("AVVIO") > -1) {
        $(".InizioEventoInContenitoreMinuti").val(0);
        $(".InizioEventoInContenitoreSecondi").val(0);
        $(".InizioEventoInContenitoreIdPeriodo").html($(this).data("fk_int_periodo"));
    }
    else {
        $(".InizioEventoInContenitoreMinuti").val($(this).html().split(':')[0]);
        $(".InizioEventoInContenitoreSecondi").val(($(this).html().split(':')[1]).split(' ')[0]);
        $(".InizioEventoInContenitoreIdPeriodo").html($(this).data("fk_int_periodo"));
        var codicePeriodoContenitore = ($(this).html().split(':')[1]).split(' ')[1];
        var valoreOpzionePeriodoContenitore = findValueInSelect("InizioEventoInContenitoreIdPeriodoContenitore", codicePeriodoContenitore);
        if (valoreOpzionePeriodoContenitore != null) {
            $("#InizioEventoInContenitoreIdPeriodoContenitore").val(valoreOpzionePeriodoContenitore);
        }
    }
    $("#ModalOrarioEventoInContenitore").modal("show");
});


$('body').on('click', '#ModalSceltaMatch .modifica', function () {
    PopolaMatch($(this).closest("tr").attr("id").replace("Match_",''))
})
$('body').on('click', '#ModalSceltaMatch .salva', function () {
    SalvaMatch($(this).closest("tr").attr("id").replace("Match_", ''))
})
$('body').on('click', '#ModalSceltaMatch .seleziona', function () {
    SelezionaMatch($(this).closest("tr").attr("id").replace("Match_", ''))
})
$('body').on('click', '#ModalSceltaMatch .elimina', function () {
    EliminaMatch($(this).closest("tr").attr("id").replace("Match_", ''))
})
$('body').on('click', '.nva_TipoEsecuzione', function () {
    $(this).closest("tr").find(".nva_TipoEsecuzione").removeClass("active");
    $(this).addClass("active");
})


$(".btn_ApriModalImportAssets").click(function () {

    $('#FileAssetsDaCaricare').click();

});

$('#FileAssetsDaCaricare').change(function () {
  
    var formData = new FormData();
    formData.append('FileAssetsDaCaricare', $('#FileAssetsDaCaricare')[0].files[0]);
    //formData.append('nva_NomePlaylist', $('#DescrizioneFile').val());



    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "/Ajax/RicevoFormAssets",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {

            $('#FileAssetsDaCaricare').val("")

            alert(data);
        },
        error: function (e) {

        }
    });
});



$('body').on('click', '.btn-LanciaEvento', function () {
    let adesso = Adesso();
    let objGS;
    if ($(this).data("evento") == 12) {
        objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraCasa, $(this).data("evento")) //var
        objGS.tin_Led = 0;
        objGS.tin_Maxischermo = 1;
        AvviaAnimazioneEvento(objGS);
    } else if ($(this).data("evento") == 13) {
        objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraCasa, $(this).data("evento")) //recupero
        objGS.tin_Led = 0;
        objGS.tin_Maxischermo = 1;
        objGS.nva_Note = $(".recupero.valore").val();
        AvviaAnimazioneEvento(objGS);
    } else if ($(this).data("evento") == 50) {
        objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraCasa, $(this).data("evento"))
        objGS.tin_Led = $(this).data("led");
        objGS.tin_Maxischermo = $(this).data("maxischermo");
        objGS.nva_Note = $(this).data("filename")
        objGS.nva_TipoEsecuzione = $(this).closest("tr").find(".active").data("tipoesecuzione");
        AvviaAnimazioneEvento(objGS);
    } else if ($(this).data("evento") == 15) {
        $.ajax({
            url: "/Ajax/sp_GestisciRiproduzioneMaxischermo",
            data: "fk_int_PeriodoDaLanciare=60&int_DataOraJavascript=" + adesso
        }).done(function (data) {
            console.log(data);
            console.log("avviato 60");
        });
    } else if ($(this).data("evento") == 16) {
        $.ajax({
            url: "/Ajax/sp_GestisciRiproduzioneMaxischermo",
            data: "fk_int_PeriodoDaLanciare=61&int_DataOraJavascript=" + adesso
        }).done(function (data) {
            console.log(data);
            console.log("avviato 61");
        });
    } else if ($(this).data("evento") == 17) {
        $.ajax({
            url: "/Ajax/sp_GestisciRiproduzioneMaxischermo",
            data: "fk_int_PeriodoDaLanciare=62&int_DataOraJavascript=" + adesso
        }).done(function (data) {
            console.log(data);
            console.log("avviato 62");
        });
    }

});





$(".btn_ApriModalSelezionePC").click(function () {
    ControlloSwitchSelezionePc("dummy");
    $("#ModalSelezionePC").modal("show");
});




$(".btn_SelezionePC").click(function () {
    ControlloSwitchSelezionePc($(this).attr("id"))
});




$(".btn-GestisciPeriodiPreload").click(function () {
    $("#ModalGestisciPeriodiPreload").modal("show");
    let Comando = CostruisciMessaggio('SendPeriodiPresenti()');

    SetDatiToPlayer(Comando);

});

$(".btn-AggiornaStatoPeriodi").click(function () {
    let Comando = CostruisciMessaggio('SendPeriodiPresenti()');
    SetDatiToPlayer(Comando);
});




$(".btnCaricaMaxischermo").click(function () {
    let Comando = CostruisciMessaggio('caricaScenaMaxischermo(\'{"id":5,"sid":2003,"rid":567,"msecd":5000,"msdi":1565016951199,"msdf":1565016956199,"start":0,"stop":0,"iper":1,"nid":6,"est":"c","tsc":0,"vel":100,"path":"/video/cremonese/maxi3.mp4"}\');');
    SetDatiToPlayer(Comando);
});


$(".btn-SchermataImpostazioni").click(function () {
    ArchiviaLog("premo .btn-SchermataImpostazioni", 3)
    $("#SchermataImpostazioni").show()
    $("#SchermataControlloRiproduzione").hide()
});


$(".btn-SchermataControlloRiproduzione").click(function () {

    openFullscreen()

    ArchiviaLog("premo .btn-SchermataControlloRiproduzione", 3)
    $("#SchermataControlloRiproduzione").show()
    $("#SchermataImpostazioni").hide()
});

$(".btn-Scaletta").click(function () {
    window.location.href = '/sorgentiVideo';

    ArchiviaLog("premo .btn-Scaletta", 3)
});



$(".zPiu").click(function () {
  //  openFullscreen()
   // $("body").css("zoom");
    //console.log(document.body.style.zoom);
   // let z = parseInt(document.body.style.zoom.replace("%", ""))+10;
    //console.log($("body").css("zoom"));
    //let z = $("body").css("zoom") + 0.1;
    //document.body.style.zoom = z+"%";
   // $("body").css("zoom", z)
   // $("body").scale(1)
    alert(window.screen.height)
});
$(".zMeno").click(function () {
    $("body").scale(0.5)
    //openFullscreen()
    //console.log(document.body.style.zoom);
    //let z = parseInt(document.body.style.zoom.replace("%", "")) - 10;
    //console.log($("body").css("zoom"));
    //let z = $("body").css("zoom") + 0.1;
    //document.body.style.zoom = z + "%";
});


function openFullscreen() {
    if (!document.fullscreenElement) {
        elem.requestFullscreen();
    }
}


$(".btn-evento").click(function () {
    let az = $(this).data("azione")
    ArchiviaLog("premo .btn-evento " + az, 3)
    $(".CheckboxAnimazioneModalCasaOspiti").hide();
    if (az == "angolo") {
        alert("angolo")
    } else {
        var titolo = "";
        switch (az) {
            case "ammonizione":
                titolo = "AMMONIZIONE: Seleziona la squadra che ha subito un'ammonizione"
                break;
            case "rosa":
                titolo = "ROSA: Seleziona la squadra di cui presentare rosa"
                break;


            case "espulsione":
                titolo = "ESPULSIONE: Seleziona la squadra che ha subito un'espulsione"
                break;
            case "goal":
                $(".CheckboxAnimazioneModalCasaOspiti").show();
                titolo = "GOAL: Seleziona la squadra a cui assegnare il goal"
                break;
            case "rigore":
                titolo = "RIGORE: Seleziona la squadra che calcia il rigore "
                break;
            case "sostituzione":
                titolo = "SOSTITUZIONE: Seleziona la squadra che sostituisce il giocatore "
                break;
            case "formazione_iniziale":
                titolo = "FORMAZIONE: Seleziona la squadra di cui impostare la formazione "
                break;
        }
        $("#ModalCasaOspiti").data("azione", az);
        $("#ModalCasaOspiti").find(".modal-title").html(titolo);
        $("#ModalCasaOspiti").modal("show");
    }
});



$(".btn-CasaOspiti").click(function () {
    let az = $("#ModalCasaOspiti").data("azione")
    let fk_int_squadra = $(this).data("fk_int_squadra")
    ArchiviaLog("premo .btn-CasaOspiti azione:" + az + " fk_int_squadra:" + fk_int_squadra, 3)
    $("#ModalCasaOspiti").modal("hide");
    switch (az) {
        case "rosa":
            RosaGenerico(fk_int_squadra);
            break;

        case "ammonizione":
            AmmonizioneGenerico(fk_int_squadra);
            break;
        case "espulsione":
            EspulsioneGenerico(fk_int_squadra);
            break;
        case "goal":
            if (((IsYes(ArrParametriGenerali.tin_FesteggiaGoalSquadraOspite)) || (fk_int_squadra == SettingMatch.fk_int_SquadraCasa)) && ($("#VisualizzaAnimazioneModalCasaOspiti").is(':checked'))) {
                objEvento = new clsEventoMatch(0, 0, fk_int_squadra, 1, int_MinutoPartita);
                AvviaAnimazioneEvento(objEvento);
            }
            PopUpSceltaGiocatoreGoalGenerico(fk_int_squadra, 0);
            break;
        case "rigore":
            RigoreGenerico(fk_int_squadra);
            break;
        case "sostituzione":
            SostituzioneGenerico(fk_int_squadra);
            break;
        case "formazione_iniziale":
            FormazioneGenerica(fk_int_squadra);
            break;
    }
});


$(".btn-applicaOffset").click(function () {
    ArchiviaLog("premo .btn-applicaOffset", 3)
    // document.querySelector('video').playbackRate = 3;
    let Comando = CostruisciMessaggio("ApplicaOffset(" + $(".offset.valore").val() + ");");
    SetDatiToPlayer(Comando);
});


$(".btn-applicaRecupero").click(function () {
    ArchiviaLog("premo .btn-applicaRecupero", 3)
    // document.querySelector('video').playbackRate = 3;
    let Comando = CostruisciMessaggio("ApplicaRecupero(" + $(".recupero.valore").val() + ");");
    SetDatiToPlayer(Comando);
    let adesso = Adesso();
    let objGS;
    objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraCasa, 13) //recupero
    objGS.tin_Led = 0;
    objGS.tin_Maxischermo = 1;
    objGS.nva_Note = $(".recupero.valore").val();
    AvviaAnimazioneEvento(objGS);
});


$(".btn-offsetTempo").click(function () {
    ArchiviaLog("premo .btn-offsetTempo " + $(this).data("tempo"), 3)
    // document.querySelector('video').playbackRate = 3;
    let Comando = CostruisciMessaggio('offsetTempo(' + $(this).data("tempo") + ');');
    SetDatiToPlayer(Comando);
});


$(".btn_NuovaRiproduzione").click(function () {
    // document.querySelector('video').playbackRate = 3;
    ArchiviaLog("premo .btn-btn_NuovaRiproduzione", 3)
    var domanda = confirm("Sei sicuro di voler generare una nuova riproduzione?");
    if (domanda === true) {
        ArchiviaLog("premo .btn-btn_NuovaRiproduzione alert true", 3)
        GeneraRiproduzione();
        let Comando = CostruisciMessaggio('location.reload();');
        SetDatiToPlayer(Comando);
        location.reload();
    }
});


$(".btn-applicaGuadagnoVelocita").click(function () {
    // document.querySelector('video').playbackRate = 3;
    let Comando = CostruisciMessaggio("ModificaVelocitaAvanzamento(" + $(".guadagnoVelocita.valore").val() + ");");
    SetDatiToPlayer(Comando);
});


$(".btn-preloadSceneSync").click(function () {
    //var date = new Date();
    //let adesso = date.getTime();
    let adesso = Adesso();

    ArchiviaLog("premo .btn-preloadSceneSync", 3)

    //////////console.log("sp_PreloadRiproduzionePeriodoLed @@fk_int_Periodo=" + fk_int_PeriodoAttivo + ", @@int_DataOraRichiestaAvvio=" + adesso + ", @@int_SkipToTime=" + (TraduciMinutiSecondiSet() * 1000));
    $(".btn-preloadSceneSync").hide();
    $(".btn-sync").show();

    $.ajax({
        url: "/Ajax/sp_PreloadRiproduzionePeriodoLed",
        data: "fk_int_Periodo=" + fk_int_PeriodoAttivo + "&int_DataOraRichiestaAvvio=" + adesso + "&int_SkipToTime=" + (TraduciMinutiSecondiSet() * 1000)
    }).done(function (data) {
        ////////console.log("sp_PreloadRiproduzionePeriodoLed");
        ////////console.log(data);
    });

    $.ajax({
        url: "/Ajax/sp_PreloadRiproduzionePeriodoMaxischermo",
        data: "fk_int_Periodo=" + fk_int_PeriodoAttivo + "&int_DataOraRichiestaAvvio=" + adesso + "&int_SkipToTime=" + (TraduciMinutiSecondiSet() * 1000)
    }).done(function (data) {
        ////////console.log("sp_PreloadRiproduzionePeriodoMaxischermo");
        ////////console.log(data);
    });

    RiduciPeriodoScambioDati();

});



$(".btn-sync").click(function () {
    ArchiviaLog("premo .btn-sync", 3)
    let Comando = ""
    $(".btn-preloadSceneSync").show();
    $(".btn-sync").hide();

    //let date = new Date();
    //let adesso = date.getTime();
    let adesso = Adesso();

    //TempoTrascorsoPeriodoInCorso = TraduciMinutiSecondiSet();
    IstanteInizioAvvioRiproduzione = adesso;

    //$("#GetTempoTrascorso").val(TempoTrascorsoPeriodoInCorso);
    //////////console.log("@@int_DataOraJavascript=" + adesso + ", @@int_SkipToMillesimoSecondoSpecificoPeriodo=" + (TraduciMinutiSecondiSet() * 1000));

    $.ajax({
        url: "/Ajax/sp_GestisciRiproduzioneLed",
        data: "int_DataOraJavascript=" + adesso + "&int_SkipToMillesimoSecondoSpecificoPeriodo=" + (TraduciMinutiSecondiSet() * 1000) + "&forzaTimeskip=1"
    }).done(function (data) {

        ////////////console.log(data);
    });

        ////////console.log("prima sp_GestisciRiproduzioneMaxischermo");
    $.ajax({
        url: "/Ajax/sp_GestisciRiproduzioneMaxischermo",
        data: "int_DataOraJavascript=" + adesso + "&int_SkipToMillesimoSecondoSpecificoPeriodo=" + (TraduciMinutiSecondiSet() * 1000)+"&forzaTimeskip=1"
    }).done(function (data) {

        ////////console.log("dentro sp_GestisciRiproduzioneMaxischermo");
    });

});







$(".btn-piu").click(function () {
    ArchiviaLog("premo .btn-piu", 3)
    let val = parseInt($(this).closest("tr").find(".slider").val()) + 1;
    $(this).closest("tr").find(".slider").val(val)
    $(this).closest("tr").find(".valore").val(val)
});

$(".btn-meno").click(function () {
    ArchiviaLog("premo .btn-meno", 3)

    let val = parseInt($(this).closest("tr").find(".slider").val()) - 1;
    $(this).closest("tr").find(".slider").val(val)
    $(this).closest("tr").find(".valore").val(val)
});


$(".btn-piu-recupero").click(function () {
    ArchiviaLog("premo .btn-piu-recupero", 3)
    let val = parseInt($(this).closest("tr").find(".valore").val()) + 1;
    $(this).closest("tr").find(".valore").val(val)
});

$(".btn-meno-recupero").click(function () {
    ArchiviaLog("premo .btn-meno-recupero", 3)
    let val = parseInt($(this).closest("tr").find(".valore").val()) - 1;
    if (val < 0) { val = 0; };
    $(this).closest("tr").find(".valore").val(val)
});
$(".btn-piu-minuti").click(function () {
    ArchiviaLog("premo .btn-piu-minuti", 3)

    let val = parseInt($(".SetMinuti").val()) + 1;

    if (val < 0) val = 0;
    $(".SetMinuti").val(val);
    MostraPreloadSync();
});

$(".btn-meno-minuti").click(function () {
    ArchiviaLog("premo .btn-meno-minuti", 3)

    let val = parseInt($(".SetMinuti").val()) - 1;
    if (val < 0) val = 0;
    $(".SetMinuti").val(val);
    MostraPreloadSync();
});
$(".btn-piu-secondi").click(function () {
    ArchiviaLog("premo .btn-piu-secondi", 3)
    let val = parseInt($(".SetSecondi").val()) + 1;
    if (val > 59) val = 0;
    if (val < 0) val = 59;
    $(".SetSecondi").val(val)
    MostraPreloadSync();
});

$(".btn-meno-secondi").click(function () {
    ArchiviaLog("premo .btn-meno-secondi", 3)
    let val = parseInt($(".SetSecondi").val()) - 1;
    if (val > 59) val = 0;
    if (val < 0) val = 59;
    $(".SetSecondi").val(val);
    MostraPreloadSync();
});



$(".SetSecondi").click(function () {
    ArchiviaLog("premo .SetSecondi", 3)
    MostraPreloadSync();
});

$(".SetMinuti").click(function () {
    ArchiviaLog("premo .SetMinuti", 3)
    MostraPreloadSync();
});


$(".SetSecondi").on('input change focus', function () {
    LimitiTempo();
});

$(".SetMinuti").on('input change focus', function () {
    LimitiTempo();
});

function LimitiTempo() {
    if ((fk_int_PeriodoAttivo == 10) || (fk_int_PeriodoAttivo == 20)) {
        if ($(".SetMinuti").val() > 44) {
            $(".SetMinuti").val(44)
        }
    }
    if ((fk_int_PeriodoAttivo == 30) || (fk_int_PeriodoAttivo == 40)) {
        if ($(".SetMinuti").val() > 14) {
            $(".SetMinuti").val(14)
        }
    }
    if ($(".SetSecondi").val()>59) {
        $(".SetSecondi").val(59)
    }
}

function ResetTempo() {
    $(".SetSecondi").val(0)
    $(".SetMinuti").val(0)
}





$(".btn-Maxischermo").click(function () {
    AzioneTastiMaxischermo($(this).data("oggetto"))
});


$(".btn-Maxischermo").dblclick(function () {
    let Comando = CostruisciMessaggio("codaAnimazioneEventiMaxischermo = [];");
    Comando = CostruisciMessaggio("RipulisciDivEventiMaxischermo();$('#EventiPartitaMaxischermo').css({ 'opacity': '0' });AnimazioneIngressoHomeMaxischermo();", Comando);
    SetDatiToPlayer(Comando);
});


/*settaggio slider*/
$(document).on('input', ".valore", function () {
    // //////////console.log($(this).val());
    // $(this).closest("tr").find(".valore").val($(this).val())
    $(this).closest("tr").find(".sliderTempo").val($(this).val())
});



$(document).on('input', ".sliderGuadagnoVelocita", function () {
    $(this).closest("tr").find(".valore").val($(this).val())
});




$(document).on('input', ".sliderTempo", function () {
    $(this).closest("tr").find(".valore").val($(this).val())
});

$(".sliderTempo").mouseup(function () {

    //////////console.log($(this).val());

});





$(".btn-ApriModalEventi").click(function () {
    ArchiviaLog("premo .btn-ApriModalEventi", 3)
   // alert("ciao");

    /*
    $(".tbodyEventi").empty();
    $.ajax({
        url: "/Ajax/GetEventiFromPath"
    }).done(function (data) {
        let dd = JSON.parse(data);
        //console.log(dd)
        let stringa
        let iii = 0

        $.each(dd, function (k, v) {
            iii = iii + 1;
            //console.log(v);

            stringa = '<tr><td>'

            stringa = stringa + '<div class="btn-group" role="group" >'
            stringa = stringa + '<button type="button" class="btn btn-lg btn-secondary nva_TipoEsecuzione active" data-tipoesecuzione="Loop">Loop</button>'
            stringa = stringa + '<button type="button" class="btn btn-lg btn-secondary nva_TipoEsecuzione" data-tipoesecuzione="StopUltimoFrame">Freeze</button>'
            stringa = stringa + '<button type="button" class="btn btn-lg btn-secondary nva_TipoEsecuzione" data-tipoesecuzione="TornaPlaylist">Playlist</button>'
            stringa = stringa + '</div>'

            stringa = stringa + '</td>';

            if (v.isLed == true) {
                stringa = stringa + '<td>' + StringaElemento(v, 1, 0) + '</td>'
            } else {
                stringa = stringa + '<td></td>'
            }
            if (v.isMaxischermo == true) {
                stringa = stringa + '<td>' + StringaElemento(v, 0, 1) + '</td>'
            } else {
                stringa = stringa + '<td></td>'
            }
            if ((v.isMaxischermo == true) && (v.isLed == true)) {
                stringa = stringa + '<td>' + StringaElemento(v, 1, 1) + '</td>'
            } else {
                stringa = stringa + '<td></td>'
            }
            stringa = stringa + '</tr>';

            $(".tbodyEventi").append(stringa);
        });
        $("#ModalEventi").modal("show");
    });
    */
    $("#ModalEventi").modal("show");

});


$(".btn_ApriModalPlaylist").click(function () {
    ArchiviaLog("premo .btn_ApriModalPlaylist", 3)
    PopolaModalPlaylist();
    $("#ModalImportPlaylist").modal("show");
});





$(".btn_lista_scene").click(function () {
    ArchiviaLog("premo .btn_lista_scene", 3)
    ////////////console.log("eccomi");
    window.location.href = "/scene/";
    //$("<a href='/Scene/'></a>").click();
});



$('body').on('click', '.btn_CancellaPlaylist', function () {
    ArchiviaLog("premo .btn_CancellaPlaylist", 3)
    var id = $(this).closest("tr").data("id");
    CancellaPlaylist(id);
});



$('#ModalImportPlaylist').on('click', '.btn-SelezionaPlaylist', function () {
    ArchiviaLog("premo .btn-SelezionaPlaylist", 3)
    SetSelected($(this).closest("tr").data("id"), "tb_Playlist");

    GeneraRiproduzione();

    let Comando = CostruisciMessaggio('location.reload();');
    SetDatiToPlayer(Comando);
    location.reload();

});


$('body').on('click', '.btn-RiproduciSceneSpecialiDentroPeriodo', function () {

    ArchiviaLog("premo .btn-RiproduciSceneSpecialiDentroPeriodo", 3)
    AvviaRiproduzioneScenaSpeciale($(this).data("fk_int_scenaled"), $(this).data("fk_int_scenamaxischermo"));

});




$('body').on('click', '.btn-UnloadPeriodo', function () {
    ArchiviaLog("premo .btn-UnloadPeriodo", 3)
    Comando = CostruisciMessaggio("UnloadPeriodo(" + $(this).data("fk_int_periodo") + ");");
    ////////console.log("UnloadPeriodo(" + $(this).data("fk_int_periodo") + ");");
    SetDatiToPlayer(Comando);

});


$('body').on('click', '.btn-LoadPeriodo', function () {
    ArchiviaLog("premo .btn-LoadPeriodo", 3)


    Comando = CostruisciMessaggio("PreloadPeriodo(" + $(this).data("fk_int_periodo") + ");");
    SetDatiToPlayer(Comando);
    ////////console.log("PreloadPeriodo(" + $(this).data("fk_int_periodo") + ");");

});







$('body').on('click', '.btn-periodi', function () {
    ResetTempo();

    ArchiviaLog("premo .btn-periodi", 3)
    let i = $(this).data("fk_int_periodo");
    //AvviaPeriodoBottone(i);

    console.log(i);
    let Comando = ""

    //if ($(this).hasClass("btn-success")) {
    //let date = new Date();

    let adesso = Adesso();
    fk_int_PeriodoAttivo = i;

    //TempoTrascorsoPeriodoInCorso = 0;
    IstanteInizioAvvioRiproduzione = adesso;

   // if ((i > 70) || (i < 60)) {


    $.ajax({
        url: "/Ajax/sp_GestisciRiproduzioneLed",
        data: "fk_int_PeriodoDaLanciare=" + i + "&int_DataOraJavascript=" + adesso
    }).done(function (data) {
        //////console.log('sp_GestisciRiproduzioneLed');
    });


    ////////console.log('sp_GestisciRiproduzioneMaxischermo');
    $.ajax({
        url: "/Ajax/sp_GestisciRiproduzioneMaxischermo",
        data: "fk_int_PeriodoDaLanciare=" + i + "&int_DataOraJavascript=" + adesso
    }).done(function (data) {
        //////////console.log('sp_GestisciRiproduzioneMaxischermo');
    });
   // }

/*
    let objGS
    switch (i) {
        case 61:
            objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraOspite, 10, 0)
            AvviaAnimazioneEvento(objGS);
            //console.log("avvio 61");
            break;
        case 62:
            objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraOspite, 11, 0)
            AvviaAnimazioneEvento(objGS);
            //console.log("avvio 62");
            break;
        case 63:
            objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraCasa, 14, 0)
            AvviaAnimazioneEvento(objGS);
            //console.log("avvio 62");
            break;
        case 66:
            objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraCasa, 10, 0)
            AvviaAnimazioneEvento(objGS);
            //console.log("avvio 66");
            break;
        case 65:
            objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraCasa, 11, 0)
            AvviaAnimazioneEvento(objGS);
            //console.log("avvio 65");
            break;
    }

*/
});





function AvviaPeriodoBottone(i) {
    ResetTempo();

    console.log(i);
    let Comando = ""

    let adesso = Adesso();
    fk_int_PeriodoAttivo = i;

    IstanteInizioAvvioRiproduzione = adesso;

    $.ajax({
        url: "/Ajax/sp_GestisciRiproduzioneLed",
        data: "fk_int_PeriodoDaLanciare=" + i + "&int_DataOraJavascript=" + adesso
    }).done(function (data) {
        //////console.log('sp_GestisciRiproduzioneLed');
    });


    ////////console.log('sp_GestisciRiproduzioneMaxischermo');
    $.ajax({
        url: "/Ajax/sp_GestisciRiproduzioneMaxischermo",
        data: "fk_int_PeriodoDaLanciare=" + i + "&int_DataOraJavascript=" + adesso
    }).done(function (data) {
        //////////console.log('sp_GestisciRiproduzioneMaxischermo');
    });
    // }

    /*
        let objGS
        switch (i) {
            case 61:
                objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraOspite, 10, 0)
                AvviaAnimazioneEvento(objGS);
                //console.log("avvio 61");
                break;
            case 62:
                objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraOspite, 11, 0)
                AvviaAnimazioneEvento(objGS);
                //console.log("avvio 62");
                break;
            case 63:
                objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraCasa, 14, 0)
                AvviaAnimazioneEvento(objGS);
                //console.log("avvio 62");
                break;
            case 66:
                objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraCasa, 10, 0)
                AvviaAnimazioneEvento(objGS);
                //console.log("avvio 66");
                break;
            case 65:
                objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraCasa, 11, 0)
                AvviaAnimazioneEvento(objGS);
                //console.log("avvio 65");
                break;
        }
    
    */
}





$(".btn-GenerazioneEventi").click(function () {
    $.ajax({
        url: "/Ajax/GenerazioneEventi?destinazione=maxischermo"
    }).done(function (data) {

    });
});


/*
$(".btn-FormazioneCasa").click(function () {
    ArchiviaLog("premo .btn-FormazioneCasa", 3)

    
});
*/
$(".btn-GoalCasa").click(function () {
    ArchiviaLog("premo .btn-GoalCasa", 3)
    PrimaAnimazioneGoalCasa();
    PopUpSceltaGiocatoreGoalGenerico(SettingMatch.fk_int_SquadraCasa, 0)
});

$(".btn-GoalOspiti").click(function () {
    ArchiviaLog("premo .btn-GoalOspiti", 3)
    PrimaAnimazioneGoalOspiti();
    PopUpSceltaGiocatoreGoalGenerico(SettingMatch.fk_int_SquadraOspite, 0)
});

$(".btn-Autogoal").click(function () {
    ArchiviaLog("premo .btn-Autogoal", 3)
    let idsquadraact = $("#ModalFormazione").data("fk_int_squadra")


    PopUpSceltaGiocatoreGoalGenerico(idsquadraact, 1)
});


$(".btn-PresentazioneFormazione").click(function () {
    ArchiviaLog("premo .btn-PresentazioneFormazione", 3)
    PresentazioneFormazione(SettingMatch.fk_int_SquadraCasa)

});


$(".btn-PresentazioneFormazioneOspiti").click(function () {
    ArchiviaLog("premo .btn-PresentazioneFormazioneOspiti", 3)
    PresentazioneFormazione(SettingMatch.fk_int_SquadraOspite)
});



$(".btn-pallaSuDischetto").click(function () {
    ArchiviaLog("premo .btn-pallaSuDischetto", 3)
    //quado clicco pallaSuDischetto avvio la scena qualora vi sia la necessità
    AvviaRiproduzioneScenaSpeciale($(".boxRigore").data("fk_int_scenaLed"), $(".boxRigore").data("fk_int_scenaMaxischermo"));

    $(".btn-pallaSuDischetto").hide();
    $(".btn-rigoreTrasformato").show();
    $(".btn-rigoreNonTrasformato").show();
});




$(".btn-rigoreTrasformato").click(function () {
    ArchiviaLog("premo .btn-rigoreTrasformato", 3)
    let IdGiocatoreSelezionato = $("#ModalFormazione").find(".bordoSelezione").data("fk_int_giocatore");
    let IdSquadraSelezionata = $("#ModalFormazione").data("fk_int_squadra");
    let objGoal = new clsEventoMatch(IdGiocatoreSelezionato, 0, IdSquadraSelezionata, 1, int_MinutoPartita)
    ArchiviaEventoSuDB(objGoal);

    if (SettingMatch.fk_int_SquadraCasa == IdSquadraSelezionata) {
        let objGG = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraCasa, 1, int_MinutoPartita)
        if ($("#VisualizzaAnimazioneGiocatore").is(':checked')) {
            AvviaAnimazioneEvento(objGG);
        }

        let objGS = new clsEventoMatch(IdGiocatoreSelezionato, 0, SettingMatch.fk_int_SquadraCasa, 1, int_MinutoPartita)
        if ($("#VisualizzaAnimazioneGiocatore").is(':checked')) {
            AvviaAnimazioneEvento(objGG);
        }
    }
    $("#ModalFormazione").modal("hide");
});



$(".btn-rigoreNonTrasformato").click(function () {
    ArchiviaLog("premo .btn-rigoreNonTrasformato", 3)

    objEvento.fk_int_TipoEventoMatch = 7
    ArchiviaEventoSuDB(objEvento)
    $("#ModalFormazione").modal("hide");
});



/**********inizio scelta match ***************/
$("#btn-SceltaMatch").click(function () {
    $("#ModalSceltaMatch").modal("show");
    // $("#ModalSceltaMatch").find("option").last().attr("selected", "selected");
});




$("#ModalSceltaMatch .btn-Salva").click(function () {
    //////////console.log("click su #ModalSceltaMatch .btn-Salva");
    let IdMatch = $("#SelectMatch").find("select").val()


    SetSelected(IdMatch, "tb_Match")


    GeneraRiproduzione();

    let Comando = CostruisciMessaggio('location.reload();');
    SetDatiToPlayer(Comando);
    location.reload();


    //PopolaSettingMatch();


    /*
    $("#navbar-titolo").html(SettingMatch.nva_Titolo);
    let Comando = CostruisciMessaggio("PopolaSettingMatch();");
    Comando = CostruisciMessaggio("InizializzaMatch();", Comando);
    SetDatiToPlayer(Comando);
    */




    $("#ModalSceltaMatch").modal("hide");

});
        /**********fine scelta match ***************/








$('#ModalFormazione').on('click', '.btn-SalvaFormazione', function () {
    ////////////console.log("sonon qui")
    var ele = $(this).closest(".modal");
    //alert(ele.data("fk_int_squadra"));
    var Formazione = JSON.stringify(AttribuisciProgressivoTitolari(ele))
    //var Formazione = JSON.stringify(GetFormazione())

    //console.log(Formazione)


    $('#ModalFormazione').find(".spinner-border").show();
    $.ajax({
        dataType: 'text',
        contentType: 'application/json',
        type: 'POST',
        url: "Ajax/SetFormazione",
        data: Formazione
    }).done(function (data) {

        CostruisciFormazione().done(function () {
            $("#ModalFormazione").find("ul").sortable({
                stop: function () {
                    $("#ModalFormazione").find(".btn-Salva").show();
                    AttribuisciProgressivoTitolariSortable($("#ModalFormazione"))
                    //EvidenziaElementoSortable();
                }
            });

            NascondiBottoniDentroFormazione();
            BottoniVisibiliDentroFormazione();

            $("#ModalFormazione").modal("show");

        });
    })

});




$('body').on('click', '#trTipoEvento img', function () {
    //////////console.log(el);
    ////////console.log($(this).closest("td").data("ideventomatch"))

    SetStato_fk_int_TipoEventoMatch($(this).closest("td").data("ideventomatch"), $("#trSquadraEventoMatch").data("id"));

});

$('body').on('click', '.tdPeriodo', function () {
    //console.log("evvai!");
    ////////console.log($(this).closest("td").data("ideventomatch"))

    SetStato_fk_int_Periodo($(this).data("idperiodo"));

});



$('body').on('click', '#trMinutoEvento .btn', function () {
    var v = 0;
    if ($(this).hasClass("btn-p10")) v = 10;
    if ($(this).hasClass("btn-p1")) v = 1;
    if ($(this).hasClass("btn-m1")) v = -1;
    if ($(this).hasClass("btn-m10")) v = -10;

    v = GetStato_int_MinutoPartita() + v;

    if (v > 150) {
        v = 150;
    }
    if (v < 0) {
        v = 0;
    }

    SetStato_int_MinutoPartita(v);

});



$('body').on('click', '.btn-GiocatorePrecedenteSuccessivoModificaEventoMatch', function () {


    if ($(this).closest('table').closest('tr').attr('id') == 'trGiocatorePrincipale') {
        if ($(this).hasClass("Precedente")) {
            SetStato_fk_int_Giocatore($(this).closest('table').find('.GiocatoreEventoMatchSelezionato').prev().find('img').data('idgiocatore'));
        } else {
            SetStato_fk_int_Giocatore($(this).closest('table').find('.GiocatoreEventoMatchSelezionato').next().find('img').data('idgiocatore'));
        }
    } else {
        if ($(this).hasClass("Precedente")) {
            SetStato_fk_int_GiocatoreSecondario($(this).closest('table').find('.GiocatoreEventoMatchSelezionato').prev().find('img').data('idgiocatore'));
        } else {
            SetStato_fk_int_GiocatoreSecondario($(this).closest('table').find('.GiocatoreEventoMatchSelezionato').next().find('img').data('idgiocatore'));
        }
    }

});



$('body').on('click', '.btn-eliminaEventoMatch', function () {
    var domanda = confirm("Sei sicuro di voler eliminare questo eveto?");
    if (domanda === true) {
        var _id = $("#ModalEditaEventiMatch").data("fk_int_eventoMatch");
        $.ajax({
            url: "/Ajax/sp_CancellaEventoMatch",
            type: 'get',
            data: { id: _id }
        }).done(function (data) {
            if (data == "ok") {
                PopolaEventiMatch();
                $("#ModalEditaEventiMatch").modal("hide");
            } else {
                alert("errore");
            }

        });

    }

});


$('body').on('click', '.btn-salvaEventoMatch', function () {

    var _id = $("#ModalEditaEventiMatch").data("fk_int_eventoMatch");
    var _fk_int_Giocatore = GetStato_fk_int_Giocatore();
    var _fk_int_GiocatoreSecondario = GetStato_fk_int_GiocatoreSecondario();
    var _fk_int_TipoEventoMatch = GetStato_fk_int_TipoEventoMatch();
    var _fk_int_Periodo = GetStato_fk_int_Periodo();
    var _int_MinutoPartita = GetStato_int_MinutoPartita();

    $("#ModalEditaEventiMatch").data("fk_int_eventoMatch");
    $.ajax({
        url: "/Ajax/sp_EditaEventoMatch",
        type: 'get',
        data: { id: _id, fk_int_Giocatore: _fk_int_Giocatore, fk_int_GiocatoreSecondario: _fk_int_GiocatoreSecondario, fk_int_TipoEventoMatch: _fk_int_TipoEventoMatch, int_MinutoPartita: _int_MinutoPartita, fk_int_Periodo: _fk_int_Periodo }

    }).done(function (data) {

        if (data == "ok") {
            PopolaEventiMatch();
            $("#ModalEditaEventiMatch").modal("hide");
        } else {
            alert("errore");
        }

    });

});


$('body').on('click', '.btn-cancellaEventoMatch', function () {
    var _id = $("#ModalEditaEventiMatch").data("fk_int_eventoMatch");
    $("#ModalEditaEventiMatch").data("fk_int_eventoMatch");
    $.ajax({
        url: "/Ajax/sp_cancellaEventoMatch",
        type: 'get',
        data: { id: _id }
    }).done(function (data) {
        if (data == "ok") {
            PopolaEventiMatch();
            $("#ModalEditaEventiMatch").modal("hide");
        } else {
            alert("errore");
        }
    });
});



$('body').on('click', '.btn_EditaEventoMatch', function () {

    $("#ModalEditaEventiMatch").data("fk_int_eventoMatch", $(this).closest("tr").data("fk_int_evento"));

    $.ajax({
        url: "/Ajax/sp_GetEventiMatch?id=" + $(this).closest("tr").data("fk_int_evento"),
    }).done(function (data) {
        data = JSON.parse(data);
        var item = data[0];

        $("#trSquadraEventoMatch").data("id", item.fk_int_Squadra);
        $("#trSquadraEventoMatch img").attr("src", "/assetsSquadre/" + (SettingSquadra[item.fk_int_Squadra]).nva_Directory + "/Grafica/logo.png");

        console.log("1153 ");
        console.log(item);

        if (item.fk_int_TipoEventoMatch == 5) {

            PopolaListaGiocatoriPerEventiMatch(GetSquadraAvversaria(item.fk_int_Squadra));

        } else {

            PopolaListaGiocatoriPerEventiMatch(item.fk_int_Squadra);

        }

        SetStato_fk_int_Giocatore(item.fk_int_Giocatore);

       SetStato_fk_int_Periodo(item.fk_int_Periodo);
       SetStato_fk_int_TipoEventoMatch(item.fk_int_TipoEventoMatch, item.fk_int_Squadra);
       SetStato_fk_int_Giocatore(item.fk_int_Giocatore);
        SetStato_fk_int_GiocatoreSecondario(item.fk_int_GiocatoreSecondario);

        SetStato_int_MinutoPartita(item.int_MinutoPartita);

        $("#ModalEditaEventiMatch").modal("show");
    });
});







//clicco sul tasto per avviare animazione
$('body').on('click', '.btn-RiproduciGoal', function () {
    if (RosaSquadra[objEvento.fk_int_Squadra][objEvento.fk_int_Giocatore] != undefined) {
        if ((objEvento.fk_int_Squadra == SettingMatch.fk_int_SquadraCasa)|| (IsYes(ArrParametriGenerali.tin_PresentazioneFormazioneSenzaVideo))) {
            if ($("#VisualizzaAnimazioneGiocatore").is(':checked')) {
                AvviaAnimazioneEvento(objEvento)
            }
        }
    }
    ArchiviaEventoSuDB(objEvento)
    $("#ModalFormazione").modal("hide");
});


//clicco sul tasto per avviare animazione
$('body').on('click', '.btn-RiproduciSostituzione', function () {
    if ($("#VisualizzaAnimazioneGiocatore").is(':checked')) {
        AvviaAnimazioneEvento(objEvento)
    }

    ArchiviaEventoSuDB(objEvento)
    $("#ModalFormazione").modal("hide");
});



//clicco sul tasto per avviare animazione
$('body').on('click', '.btn-PresentazioneEstesaCompatta', function () {
     
    if ($(this).data("tipoact") == 'estesa') {

        $(this).data("tipoact", "compatta");
        $(this).html("Pres. estesa");
        $(".singoloGiocatore").hide();
        $(".tdTitolariCasa").show();
        
    } else {

        $(this).data("tipoact", "estesa");
        $(this).html("Pres. compatta");
        $(".singoloGiocatore").show();
        $(".tdTitolariCasa").hide();

    }

});



$('body').on('click', '.btn-ResetPresentazione', function () {
    ResetPresentazione();
});



function ResetPresentazione() {

    $(".Speaker").html("");

    $(".trPresentazioneFormazione td").each(function () {
        $(this).removeClass("inCorso");
    });


    $(".btn-RiproduciPrecedente").hide();
    $(".RiproduciSuccessivo").html("Avvio");
    console.log("==0");
    // successivo = $(".btn_ElementoPresentazioneFormazione").first();
    $(".btn-RiproduciSuccessivo").show();
    $(".btn-RiproduciPrecedente").hide();
}

$('body').on('click', '.btn-RiproduciPrecedente', function () {
    fk_int_PeriodoAttivo = $(this).data("fk_int_periodo");

    if ($(".btn_ElementoPresentazioneFormazione.inCorso").length > 0) {
        console.log(">0");
        successivo = $(".btn_ElementoPresentazioneFormazione.inCorso").prevAll('td:visible:first ');
        $(".btn_ElementoPresentazioneFormazione.inCorso").removeClass("inCorso");
        $(".btn-RiproduciPrecedente").html("Precedente");
        $(".btn-RiproduciPrecedente").show();
    } else {
        $(".btn-RiproduciPrecedente").hide();
        $(".RiproduciSuccessivo").html("Avvio");
        console.log("==0");
        successivo = $(".btn_ElementoPresentazioneFormazione").first();
        console.log(successivo);
    }

    successivo.addClass("inCorso");

    if ($(".btn_ElementoPresentazioneFormazione.inCorso").data("elemento") == "SponsorFormazione") {
        console.log("mostro home");
        $(".btn-RiproduciPrecedente").hide();


    } else {
        $(".btn-RiproduciPrecedente").show();
        $(".btn-RiproduciSuccessivo").html("Successivo");
        RiproduciPresentazione();
    }

  


})

 //ArrParametriGenerali["nva_TestoPresentazioneFormazioneTitolari"]
function StringaListaGiocatoriTitolari(fk_int_Squadra, nva_MessaggioIniziale, ConIlNumero) {
   
    $.ajax({
        url: "/Ajax/sp_GetGiocatoriMatch?fk_int_Squadra=" + fk_int_Squadra
    }).done(function (data) {
        var Stringa = "<div><h2>" + nva_MessaggioIniziale+"</h2>";
        var lista = JSON.parse(data);
        $(lista).each(function (k, o) {
            if (o.int_Ordine < 12) {
                Stringa = Stringa + "<nobr><span style='font-size:40px;'>" + o.nva_Numero + " " + o.nva_Cognome + " <span class='badge badge-secondary'>(" + o.nva_Cognome + ") </span></span></nobr> ";
            }
        });
        Stringa = Stringa + "</div>";
        $(".Speaker").html(Stringa)

    })

}


//ArrParametriGenerali["nva_TestoPresentazioneFormazioneRiserve"] 
function StringaListaGiocatoriRiserve(fk_int_Squadra, nva_MessaggioIniziale) {

    $.ajax({
        url: "/Ajax/sp_GetGiocatoriMatch?fk_int_Squadra=" + fk_int_Squadra
    }).done(function (data) {
        var Stringa = "<div><h1>" + nva_MessaggioIniziale + "</h1>";
        var lista = JSON.parse(data);

        $(lista).each(function (k, o) {
            if (o.int_Ordine > 11 && o.int_Ordine != 0 && o.nva_Numero != '0') {
                Stringa = Stringa + "<nobr><span style='font-size:40px;'>" + o.nva_Numero + " " + o.nva_Cognome + " <span class='badge badge-secondary'>(" + o.nva_Cognome + ") </span></span></nobr> ";
            }
        });

        Stringa = Stringa + "</div>";
        $(".Speaker").html(Stringa)
    })

}







function RiproduciPresentazione() {
    let adesso = Adesso();
    let inCorso = "";
    let successivo = "";
    console.log("Adesso:" + adesso);

    $(".Speaker").html("");

    if (Number.isInteger($(".btn_ElementoPresentazioneFormazione.inCorso").data("elemento")) == true) {
        console.log("PRESENTAZIONE FORMAZIONE:giocatore da presentare");

        var incrementalePresentato = $(".btn_ElementoPresentazioneFormazione.inCorso").data("elemento");
        console.log(incrementalePresentato)
        console.log(ArrFormazioneCliccata)
        let objGS = new clsEventoMatch(ArrFormazioneCliccata[incrementalePresentato].ID, 0, (ArrFormazioneCliccata[incrementalePresentato]).fk_int_Squadra, 9, 0)
        AvviaAnimazioneEvento(objGS);
        $(".Speaker").html("<span style='font-size:50px'>" + ArrParametriGenerali["nva_TestoPresentazioneGiocatoreTitolareCasa"] + " " + (ArrFormazioneCliccata[incrementalePresentato]).nva_Numero + " " + (ArrFormazioneCliccata[incrementalePresentato]).nva_Nome + " " + (ArrFormazioneCliccata[incrementalePresentato]).nva_Cognome + "</span> <span class='badge badge-secondary'  style='font-size:50px'>" + (ArrFormazioneCliccata[incrementalePresentato]).nva_Pronuncia+"</span></span>");

        //$(".Speaker").html((ArrFormazioneCliccata[incrementalePresentato]).nva_Pronuncia);


    } else if ($(".btn_ElementoPresentazioneFormazione.inCorso").data("elemento") == "RiserveCasa") {
        console.log("PRESENTAZIONE FORMAZIONE:RiserveCasa");


       // console.log(ArrFormazioneCliccata);

        console.log("RiserveCasa");
        objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraCasa, 11, 0)
        AvviaAnimazioneEvento(objGS);

        StringaListaGiocatoriRiserve(SettingMatch.fk_int_SquadraCasa, ArrParametriGenerali["nva_TestoPresentazioneFormazioneRiserveCasa"]);

    } else if ($(".btn_ElementoPresentazioneFormazione.inCorso").data("elemento") == "RiserveOspiti") {
        console.log("PRESENTAZIONE FORMAZIONE:Riserve Ospiti");

        StringaListaGiocatoriRiserve(SettingMatch.fk_int_SquadraOspite, ArrParametriGenerali["nva_TestoPresentazioneFormazioneRiserveOspiti"]);


        console.log("RiserveOspiti");
        objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraOspite, 11, 0)
        AvviaAnimazioneEvento(objGS);
       
    } else if ($(".btn_ElementoPresentazioneFormazione.inCorso").data("elemento") == "TitolariCasa") {
        console.log("PRESENTAZIONE FORMAZIONE:Titolari Casa");

     
        console.log("TitolariCasa");
        objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraCasa, 10, 0)
        AvviaAnimazioneEvento(objGS);
        
    } else if ($(".btn_ElementoPresentazioneFormazione.inCorso").data("elemento") == "TitolariOspiti") {
        console.log("PRESENTAZIONE FORMAZIONE:Titolari Ospiti");
       // SetDatiToPlayer(CostruisciMessaggio("GestisciTemporizzazioneAnimazioniLed('home');$('.div_h72').empty();"));

        StringaListaGiocatoriTitolari(SettingMatch.fk_int_SquadraOspite, ArrParametriGenerali["nva_TestoPresentazioneFormazioneTitolariOspiti"], ArrParametriGenerali["nva_TestoPresentazioneGiocatoreTitolareOspiti"]);


        objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraOspite, 10, 0)
        AvviaAnimazioneEvento(objGS);
        
    } else if ($(".btn_ElementoPresentazioneFormazione.inCorso").data("elemento") == "SponsorFormazione") {

        $(".Speaker").html("<h1>" + ArrParametriGenerali["nva_TestoPresentazioneFormazioneSponsor"] + "</h1>" );


      


        console.log("PRESENTAZIONE FORMAZIONE:Sponsor Formazione");
        SetDatiToPlayer(CostruisciMessaggio("GestisciTemporizzazioneAnimazioniLed('home');$('.div_h72').empty();"));
       
        $.ajax({
            url: "/Ajax/sp_GestisciRiproduzioneLed",
            data: "fk_int_PeriodoDaLanciare=60&int_DataOraJavascript=" + adesso
        }).done(function (data) { });
       

        objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraOspite, 15, 0)
        AvviaAnimazioneEvento(objGS);
        
        /*
        $.ajax({
            url: "/Ajax/sp_GestisciRiproduzioneMaxischermo",
            data: "fk_int_PeriodoDaLanciare=60&int_DataOraJavascript=" + adesso
        }).done(function (data) { });
        */
        /*
        console.log("SponsorFormazione");
        */
    } else if ($(".btn_ElementoPresentazioneFormazione.inCorso").data("elemento") == "IntroCasa") {
        console.log("PRESENTAZIONE FORMAZIONE:Intro Casa");

       

        $(".Speaker").html("<h1>" +ArrParametriGenerali["nva_TestoPresentazioneFormazioneIntroduzione"]+"</h1>");

        SetDatiToPlayer(CostruisciMessaggio("GestisciTemporizzazioneAnimazioniLed('home');$('.div_h72').empty();"))
        //SetDatiToPlayer(CostruisciMessaggio("GestisciTemporizzazioneAnimazioniMaxischermo('home');"))
       /*
        $.ajax({
            url: "/Ajax/sp_GestisciRiproduzioneLed",
            data: "fk_int_PeriodoDaLanciare=63&int_DataOraJavascript=" + adesso
        }).done(function (data) { });
*/
       /*
        $.ajax({
            url: "/Ajax/sp_GestisciRiproduzioneMaxischermo",
            data: "fk_int_PeriodoDaLanciare=63&int_DataOraJavascript=" + adesso
        }).done(function (data) { });
       */
        
        objGS = new clsEventoMatch(0, 0, SettingMatch.fk_int_SquadraOspite, 14, 0)
        AvviaAnimazioneEvento(objGS);
        console.log("IntroCasa");
        
 		$.ajax({
            url: "/Ajax/sp_GestisciRiproduzioneLed",
            data: "fk_int_PeriodoDaLanciare=60&int_DataOraJavascript=" + adesso
        }).done(function (data) { });
		       
        
        
        
    }




}



//clicco sul tasto per avviare animazione
$('body').on('click', '.btn-RiproduciSuccessivo', function () {

    fk_int_PeriodoAttivo = $(this).data("fk_int_periodo");

    let adesso = Adesso();
    let inCorso = "";
    let successivo = "";

    if ($(".btn_ElementoPresentazioneFormazione.inCorso").length > 0) {
        if ($(".btn_ElementoPresentazioneFormazione.inCorso").data("elemento") == "RiserveCasa") {

            SetDatiToPlayer(CostruisciMessaggio("GestisciTemporizzazioneAnimazioniMaxischermo('home');GestisciTemporizzazioneAnimazioniLed('home');$('.div_h72').empty();"));

            $.ajax({
                url: "/Ajax/sp_GestisciRiproduzioneLed",
                data: "fk_int_PeriodoDaLanciare=63&int_DataOraJavascript=" + adesso
            }).done(function (data) { });

        }

        console.log(">0");
        successivo = $(".btn_ElementoPresentazioneFormazione.inCorso").nextAll('td:visible:first ');
        $(".btn_ElementoPresentazioneFormazione.inCorso").removeClass("inCorso");
        $(".btn-RiproduciPrecedente").html("Precedente");
        $(".btn-RiproduciPrecedente").show();
    } else {
        $(".btn-RiproduciPrecedente").hide();
        $(".btn-RiproduciSuccessivo").html("Avvio");
        console.log("==0");
        successivo = $(".btn_ElementoPresentazioneFormazione").first();
        console.log(successivo);
    }

    successivo.addClass("inCorso");

    if ($(".btn_ElementoPresentazioneFormazione.inCorso").data("elemento") == "RiserveCasa") {
        console.log("mostro home");
        $(".btn-RiproduciSuccessivo").html("Home");
    } else {
        $(".btn-RiproduciSuccessivo").html("Successivo");
    }

    RiproduciPresentazione();

});




$('body').on('click', '.btn-RiproduciPrecedente', function () {
    fk_int_PeriodoAttivo = $(this).data("fk_int_periodo");
    let adesso = Adesso();






});











//clicco sul tasto per avviare animazione
$('body').on('click', '.btn-RiproduciSuccessivo__old_non_in_uso', function () {
    fk_int_PeriodoAttivo = $(this).data("fk_int_periodo");

    console.log("sono qui");
    //let date = new Date();
    //let adesso = date.getTime();
    let adesso = Adesso();
    GiocatorePresentato = GiocatorePresentato + 1;
    //console.log('btn-RiproduciSuccessivo click');
    //console.log("GiocatorePresentato")
    //console.log(GiocatorePresentato)

    $(".eventoGiocatore").hide();



    if (ArrFormazioneCliccata[GiocatorePresentato] != undefined) {
        $(".giocatore_" + ArrFormazioneCliccata[GiocatorePresentato].ID).find(".eventoPresentazione").show();
    }



    if (GiocatorePresentato > 0) {
        $(".btn-RiproduciSuccessivo").text("Successivo");
        //console.log('GiocatorePresentato > 0');
    }



    if (GiocatorePresentato == parseInt(ArrParametriGenerali.int_NumeroGiocatoriDaPresentare)) {
        $(".btn-RiproduciSuccessivo").text("Presenta riserve casa");
        //console.log('(GiocatorePresentato == parseInt(ArrParametriGenerali.int_NumeroGiocatoriDaPresentare))');
    }




    if (GiocatorePresentato <= parseInt(ArrParametriGenerali.int_NumeroGiocatoriDaPresentare)) {



        if (IsYes(ArrParametriGenerali.tin_PresentazioneFormazioneSenzaVideo)) {

            console.log('tin_PresentazioneFormazioneSenzaVideo == 1');

            let objGS = new clsEventoMatch(ArrFormazioneCliccata[GiocatorePresentato].ID, 0, (ArrFormazioneCliccata[GiocatorePresentato]).fk_int_Squadra, 9, 0)
            //console.log(objGS);
            AvviaAnimazioneEvento(objGS);


        } else {

            console.log('tin_PresentazioneFormazioneSenzaVideo != 1');

       

            let IdPeriodoGiocatore = ArrAssociazioneGiocatorePeriodo[GiocatorePresentato][2];


            //console.log("fk_int_PeriodoDaLanciare=" + IdPeriodoGiocatore + "&int_DataOraJavascript=" + adesso);

            $.ajax({
                url: "/Ajax/sp_GestisciRiproduzioneLed",
                data: "fk_int_PeriodoDaLanciare=" + IdPeriodoGiocatore + "&int_DataOraJavascript=" + adesso
            }).done(function (data) {

            });

            $.ajax({
                url: "/Ajax/sp_GestisciRiproduzioneMaxischermo",
                data: "fk_int_PeriodoDaLanciare=" + IdPeriodoGiocatore + "&int_DataOraJavascript=" + adesso
            }).done(function (data) {


            });

        }

    }





    if (GiocatorePresentato == (parseInt(ArrParametriGenerali.int_NumeroGiocatoriDaPresentare) + 1)) {
        let objGS = new clsEventoMatch(0, 0, (ArrFormazioneCliccata[1]).fk_int_Squadra, 11, 0)
        AvviaAnimazioneEvento(objGS);
        console.log("AvviaAnimazioneEvento(objGS)");
        $(".btn-RiproduciSuccessivo").text("Torna alla home e chiudi finestra");

    }


    if (GiocatorePresentato == ((parseInt(ArrParametriGenerali.int_NumeroGiocatoriDaPresentare)) + 2)) {
        AzioneTastiMaxischermo('home')

        GiocatorePresentato = 0;
        console.log("AzioneTastiMaxischermo('home')");
        $("#ModalFormazione").modal("hide");
    }





});



//clicco su un giocatore
$('body').on('click', '.formazione', function () {
    //    alert("qui "+$("#ModalFormazione").data("azione"))
    let ParentDiv = $(this).closest(".modal");
    let ElementoDaEvidenziare = $(this).closest("li");
    let fk_int_SquadraCliccata = ParentDiv.data("fk_int_squadra");
    let fk_int_GiocatoreCliccato = $(this).closest("li").data("fk_int_giocatore");

    ////////////console.log("fk_int_SquadraCliccata = " + fk_int_SquadraCliccata);
    ////////////console.log("fk_int_GiocatoreCliccato = " + fk_int_GiocatoreCliccato);


    if ($("#ModalFormazione").data("azione") == "formazione") {
        ParentDiv.find(".eventoGiocatore").hide();
        if (ElementoDaEvidenziare.hasClass('bg-danger')) {
            ElementoDaEvidenziare.removeClass("bg-danger")
            ElementoDaEvidenziare.addClass("non-convocati")
            ElementoDaEvidenziare.removeClass("convocati")
        } else {
            ElementoDaEvidenziare.addClass("bg-danger")
            ElementoDaEvidenziare.removeClass("non-convocati")
            ElementoDaEvidenziare.addClass("convocati")

        }

        EvidenziaElemento(ElementoDaEvidenziare);

    }



    if ($("#ModalFormazione").data("azione") == "goal") {
        if (ParentDiv.find(".bordoSelezione").length > 1) {
            $(".eventoGiocatore").hide();
            ParentDiv.find("li").removeClass("bordoSelezione"); //pulisco tutti i giocatori
        } else if (ParentDiv.find(".bordoSelezione").length == 1) {
            if (fk_int_GiocatoreCliccato != ParentDiv.find(".bordoSelezione").data("fk_int_giocatore")) {
                //entro solo se ho cliccato un giocatore diverso
                let IdGiocatoreGoal = ParentDiv.find(".bordoSelezione").data("fk_int_giocatore")
                ElementoDaEvidenziare.find(".eventoAssist").show();
                ElementoDaEvidenziare.addClass("bordoSelezione");
                objEvento = new clsEventoMatch(IdGiocatoreGoal, fk_int_GiocatoreCliccato, fk_int_SquadraCliccata, 1, int_MinutoPartita)

            }
        } else {
            objEvento = new clsEventoMatch(fk_int_GiocatoreCliccato, 0, fk_int_SquadraCliccata, 1, int_MinutoPartita)

            ElementoDaEvidenziare.find(".eventoGoal").show();
            ElementoDaEvidenziare.addClass("bordoSelezione");
            //    ////////////console.log("goal");
        }
        //AnimaAzione("goal");
    }


    if ($("#ModalFormazione").data("azione") == "autogoal") {
        $(".eventoGiocatore").hide();
        ParentDiv.find("li").removeClass("bordoSelezione"); //pulisco tutti i giocatori
        objEvento = new clsEventoMatch(fk_int_GiocatoreCliccato, 0, fk_int_SquadraCliccata, 5, int_MinutoPartita)
        ElementoDaEvidenziare.find(".eventoGoal").show();
        ElementoDaEvidenziare.addClass("bordoSelezione");

    }



    if ($("#ModalFormazione").data("azione") == "rigore") {

        ParentDiv.find(".eventoGiocatore").hide();
        ParentDiv.find("li").removeClass("bordoSelezione");
        ElementoDaEvidenziare.addClass("bordoSelezione");

        let obj = new clsEventoMatch(fk_int_GiocatoreCliccato, 0, fk_int_SquadraCliccata, 2, int_MinutoPartita)
        ArchiviaEventoSuDB(obj)

        if (($(".boxRigore").data("fk_int_scenaLed") + $(".boxRigore").data("fk_int_scenaMaxischermo")) > 0) {
            $("#ModalFormazione").find(".btn-pallaSuDischetto").show();
            $("#ModalFormazione").find(".btn-rigoreTrasformato").hide();
            $("#ModalFormazione").find(".btn-rigoreNonTrasformato").hide();
        } else {
            $("#ModalFormazione").find(".btn-pallaSuDischetto").hide();
            $("#ModalFormazione").find(".btn-rigoreTrasformato").show();
            $("#ModalFormazione").find(".btn-rigoreNonTrasformato").show();
        }

    }

    /* presentazione rosa **/
    if ($("#ModalFormazione").data("azione") == "rosa") {


        ParentDiv.find(".eventoGiocatore").hide();
        ParentDiv.find("li").removeClass("bordoSelezione");
        ElementoDaEvidenziare.addClass("bordoSelezione");




        let objGS = new clsEventoMatch(fk_int_GiocatoreCliccato, 0, fk_int_SquadraCliccata, 9, 0)
        AvviaAnimazioneEvento(objGS);



       // $("#ModalFormazione").modal("hide");
    }


    if ($("#ModalFormazione").data("azione") == "ammonizione") {
        ParentDiv.find(".eventoGiocatore").hide();
        ParentDiv.find("li").removeClass("bordoSelezione");
        ElementoDaEvidenziare.addClass("bordoSelezione");
        let obj = new clsEventoMatch(fk_int_GiocatoreCliccato, 0, fk_int_SquadraCliccata, 2, int_MinutoPartita)
        ArchiviaEventoSuDB(obj)
        if ($("#VisualizzaAnimazioneGiocatore").is(':checked')) {
            AvviaAnimazioneEvento(obj);
        }
        $("#ModalFormazione").modal("hide");
    }



    if ($("#ModalFormazione").data("azione") == "espulsione") {
        ParentDiv.find(".eventoGiocatore").hide();
        ParentDiv.find("li").removeClass("bordoSelezione");
        ElementoDaEvidenziare.addClass("bordoSelezione");
        let obj = new clsEventoMatch(fk_int_GiocatoreCliccato, 0, fk_int_SquadraCliccata, 3, int_MinutoPartita)
        ArchiviaEventoSuDB(obj)
        if ($("#VisualizzaAnimazioneGiocatore").is(':checked')) {
            AvviaAnimazioneEvento(obj);
        }
        $("#ModalFormazione").modal("hide");
    }




    if ($("#ModalFormazione").data("azione") == "sostituzione") {
        $("#ModalFormazione").find(".btn-RiproduciSostituzione").hide();
        if (ParentDiv.find(".bordoSelezione").length > 1) {
            $(".eventoGiocatore").hide();
            ParentDiv.find("li").removeClass("bordoSelezione"); //pulisco tutti i giocatori
        } else if (ParentDiv.find(".bordoSelezione").length == 1) {
            if (fk_int_GiocatoreCliccato != ParentDiv.find(".bordoSelezione").data("fk_int_giocatore")) {
                //entro solo se ho cliccato un giocatore diverso
                let IdGiocatoreGoal = ParentDiv.find(".bordoSelezione").data("fk_int_giocatore")
                ElementoDaEvidenziare.find(".eventoEntra").show();
                ElementoDaEvidenziare.addClass("bordoSelezione");
                objEvento = new clsEventoMatch(IdGiocatoreGoal, fk_int_GiocatoreCliccato, fk_int_SquadraCliccata, 4, int_MinutoPartita)
                $("#ModalFormazione").find(".btn-RiproduciSostituzione").show();
            }
        } else {

            objEvento = new clsEventoMatch(fk_int_GiocatoreCliccato, 0, fk_int_SquadraCliccata, 4, int_MinutoPartita)
            ElementoDaEvidenziare.find(".eventoEsce").show();
            ElementoDaEvidenziare.addClass("bordoSelezione");

        }
    }






});


var sendLux=1;
var tmrLux;
$(document).on('input', '.slider.luminosita', function () {

    
    let valore = $(this).val()
    if ($(this).hasClass("Tutti")) {
        MuoviTuttiGliSliderLuminosita(valore);
    } else {
        $(this).closest("tr").find(".valoreLuminosita").html(valore + "%");
        ControllaSliderDaMuovereConMe($(this))
    }
    if (sendLux == 1) {
        sendLux = 0;
        clearTimeout(tmrLux);
        SendLuminositaToPlayer();
        ArchiviaValoriLuminositaInDatabase();     
        tmrLux = setTimeout(function () {sendLux = 1;}, 300);
    }

});

$(document).on('change', '.slider.luminosita', function () {
    sendLux = 1;
    SendLuminositaToPlayer();
    ArchiviaValoriLuminositaInDatabase();
});


function ControllaSliderDaMuovereConMe(Elemento) {
    let tt = Elemento.closest("tr").find(".slider.luminosita").data("settore")
    let valore = Elemento.closest("tr").find(".slider.luminosita").val()
    $('.singolosettore').each(function (i, obj) {
        if (($(obj).data("settore") != tt) && ($(obj).closest("tr").find(".spostaAncheMe").is(':checked'))) {
            $(obj).closest("tr").find(".slider.luminosita").val(valore);
            $(obj).closest("tr").find(".valoreLuminosita").html(valore + "%");
        }
    });
}





$(document).on('click', '.btn-LuminositaUp', function() {
    MuoviSliderLuminosita($(this))
    SendLuminositaToPlayer();
    ArchiviaValoriLuminositaInDatabase();

});
$(document).on('click', '.btn-LuminositaDown', function() {
    MuoviSliderLuminosita($(this)) 
    SendLuminositaToPlayer();
    ArchiviaValoriLuminositaInDatabase();
});
$(document).on('click', '.btn-LuminositaOffsetUp', function() {
    SetUpOffsetLuminosita($(this))
    MuoviTuttiGliSliderLuminosita($(".slider.luminosita.Tutti").val())
    SendLuminositaToPlayer();
    ArchiviaValoriLuminositaInDatabase();
});
$(document).on('click', '.btn-LuminositaOffsetDown', function() {
    SetUpOffsetLuminosita($(this))
    MuoviTuttiGliSliderLuminosita($(".slider.luminosita.Tutti").val())
    SendLuminositaToPlayer();
    ArchiviaValoriLuminositaInDatabase();
});


function SetUpOffsetLuminosita(btn) {

    let val = parseInt(btn.closest("tr").find(".valoreOffsetLuminosita").html().replace("%", ""));
    let upDown = -2
    if (btn.hasClass("btn-LuminositaOffsetUp")) {
        upDown = 2
    }   
    val = val + upDown;
    if (val > 50) val = 50;
    if (val < -50) val = -50;
    btn.closest("tr").find(".valoreOffsetLuminosita").html(val+"%")
}


function MuoviSliderLuminosita(btn) {
    let upDown = -1
    if (btn.hasClass("btn-LuminositaUp")) {
        upDown=1
    }
    let val = parseInt(btn.closest("tr").find(".slider.luminosita").val()) + parseInt(upDown)
    if (val > 100) val = 100;
    if (val < 10) val = 10;
    if (btn.hasClass("Tutti")) {
        MuoviTuttiGliSliderLuminosita(val);
    } else {
        btn.closest("tr").find(".slider.luminosita").val(val);
        btn.closest("tr").find(".valoreLuminosita").html(val + "%");
        ControllaSliderDaMuovereConMe(btn)
    }
}




function MuoviTuttiGliSliderLuminosita(valore) {
   // //console.log("MuoviTuttiGliSliderLuminosita " + valore)

    $('.singolosettore').each(function(i, obj) {
        let valOffset = parseInt($(obj).closest("tr").find(".valoreOffsetLuminosita").html().replace("%", ""))
        //console.log("cambio su id:"+$(obj).data("target"));
        let newValue = 0;
        
        if (valOffset != 0) {
            let fact = valOffset / 100
            let other = parseInt(valore) * fact
            let toint = Math.round(other)
            newValue = toint + parseInt(valore);
        } else {
            newValue = parseInt(valore);
        }

        if (newValue > 100) newValue = 100;
        if (newValue < 10) newValue = 10;

        //console.log("valoreOffsetLuminosita:" + valOffset);

        $('.luminosita_' + $(obj).data("target")).closest("tr").find(".valoreLuminosita").html(newValue+"%");
        $('.luminosita_' + $(obj).data("target")).closest("tr").find(".slider.luminosita").val(newValue);
    
    });
    $(".valoreLuminosita.Tutti").html(valore+"%");
    $(".slider.luminosita.Tutti").val(valore);
 
}


function SendLuminositaToPlayer() {
    let arrTotal = [];
    $('.singolosettore').each(function (i, obj) {
        let vop = (1 - ($(obj).val() / 100))
        let ot = vop.toFixed(2);
        arrTotal.push([$(obj).data("settore"), ot]);
    });

    let s = JSON.stringify(arrTotal);
    SetDatiToPlayer(CostruisciMessaggio("RL('" + s + "')"))
}





class tb_Luminosita {
    constructor(nva_Settore, int_valore, int_OffsetPercentualeVariazioneGlobale) {
        this.nva_Settore = nva_Settore;
        this.int_valore = int_valore;
        this.int_OffsetPercentualeVariazioneGlobale = int_OffsetPercentualeVariazioneGlobale
    }
}

$(document).on('change', '.slider.luminosita.Maxischermo', function () {
    ArchiviaValoriLuminositaMaxischermoInDatabase();
});
$(document).on('click', '.btn-LuminositaUp.Maxischermo', function () {
    MuoviSliderLuminosita($(this))
    ArchiviaValoriLuminositaMaxischermoInDatabase();
});
$(document).on('click', '.btn-LuminositaDown.Maxischermo', function () {
    MuoviSliderLuminosita($(this))
    ArchiviaValoriLuminositaMaxischermoInDatabase();
});

function ArchiviaValoriLuminositaMaxischermoInDatabase() {
    let arr = [];
    arr.push(new tb_Luminosita('Maxischermo', $(".slider.luminosita.Maxischermo").val(), 0));
    //console.log("funziono");
    $.ajax({
        dataType: 'text',
        contentType: 'application/json',
        type: 'POST',
        url: "Home/ArchiviaLuminosita",
        data: JSON.stringify(arr)
    }).done(function (data) {
        if ($("#Luminosita_-1")[0]) {
            $.ajax({
                url: "Home/GetStatusMaxischermoFromCard"
            }).done(function (data) {
                //console.log(data);
            });
        }
    })



}


function ArchiviaValoriLuminositaInDatabase() {
    let arr = [];
    console.log("qui");
    arr.push(new tb_Luminosita("Tutti", $(".slider.luminosita.Tutti").val(),0));
    $('.singolosettore').each(function (i, obj) {
        console.log("-----------------------------");
        console.log($(obj).data("settore"));
        console.log($(obj).val());
        console.log($(obj).closest("tr").find(".valoreOffsetLuminosita").html().replace("%", ""));
        arr.push(new tb_Luminosita($(obj).data("settore"), $(obj).val(), $(obj).closest("tr").find(".valoreOffsetLuminosita").html().replace("%", "")  ));
    });

    $.ajax({
        dataType: 'text',
        contentType: 'application/json',
        type: 'POST',
        url: "Home/ArchiviaLuminosita",
        data: JSON.stringify(arr)
    }).done(function(data) {

    })
}




function GetValoriLuminositaFromDatabase() {

    $.ajax({
        dataType: 'text',
        contentType: 'application/json',
        url: "Home/GetLuminosita"
    }).done(function (data) {
        data = JSON.parse(data);
        $.each(data, function (index, value) {
            $('#Luminosita_' + value.nva_Settore).find(".valoreOffsetLuminosita").html(value.int_OffsetPercentualeVariazioneGlobale + "%");
            $('#Luminosita_' + value.nva_Settore).find(".valoreLuminosita").html(value.int_Valore + "%");
            $('#Luminosita_' + value.nva_Settore).find(".slider.luminosita").val(value.int_Valore);
        });
    })

}







$("#stop").click(function () {
    //////////////console.log("invio al player");
});








$(".btn-Importa").click(function () {
    ArchiviaLog("premo .btn-Importa", 3)
    var formData = new FormData();
    formData.append('FileDaCaricare', $('#FileDaCaricare')[0].files[0]);
    formData.append('fk_int_match', SettingMatch.ID);
    formData.append('nva_NomePlaylist', $('#DescrizioneFile').val());

    //let logInterval = setInterval(function () { alert("Hello"); }, 3000);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "/Ajax/RicevoFormPlaylist",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {

            PopolaModalPlaylist();

            $('#FileDaCaricare').val("")
            $('#DescrizioneFile').val("")

            alert(data);
        },
        error: function (e) {

        }
    });


});





$('#ModalFormazione').on('dblclick', '.formazione', function () {


    let ParentDiv = $(this).closest(".modal");
    let ElementoDaEvidenziare = $(this).closest("li");
    let fk_int_SquadraCliccata = ParentDiv.data("fk_int_squadra");
    let fk_int_GiocatoreCliccato = $(this).closest("li").data("fk_int_giocatore");

    //console.log("fk_int_GiocatoreCliccato: " + fk_int_GiocatoreCliccato);
    if (($("#ModalFormazione").data('bs.modal') || {})._isShown) {
        ricordaModalAperta = $("#ModalFormazione").data("fk_int_squadra");
        $("#ModalFormazione").modal('hide');
    } else {
        ricordaModalAperta = 0;
    }
    $("#ModalCrudGiocatore").modal('show');
    PopolaFormCrudGiocatore(fk_int_GiocatoreCliccato);


});



$('body').on('click', '.btn-eliminaGiocatore', function () {

    var domanda = confirm("Sei sicuro di voler eliminare il giocatore?");
    if (domanda === true) {

        let id = $('#form_giocatori_id').val();
        $.ajax({
            url: "Crud/eliminaGiocatore",
            type: "get",
            data: { 'id': id },
            success: function (data) {
                //console.log(data)
                $("#ModalCrudGiocatore").modal('hide');
                if (ricordaModalAperta > 0) {
                    FormazioneGenerica(ricordaModalAperta)
                    ricordaModalAperta = 0;
                }
            }
        });
    }
})




$('body').on('click', '.btn-InserisciGiocatore', function () {
    $(".btn-eliminaGiocatore").hide();
    $('#form_giocatori_id').val("0");
    $('#form_giocatori_nva_Nome').val("");
    $('#form_giocatori_nva_Cognome').val("");
    $('#form_giocatori_nva_Numero').val("");
    $('#PreviewImmagineGiocatore').attr("src", "");

    if (($("#ModalFormazione").data('bs.modal') || {})._isShown) {
        ricordaModalAperta = $("#ModalFormazione").data("fk_int_squadra");
        $("#form_giocatori_fk_int_Squadra").val(ricordaModalAperta);
        $("#ModalFormazione").modal('hide');
    } else {
        ricordaModalAperta = 0;
    }

    $("#ModalCrudGiocatore").modal('show');

})



$('body').on('click', '.btn-rimuoviImmagineGiocatore', function () {
    let id = $('#form_giocatori_id').val();
    $.ajax({
        url: "Crud/rimuoviImmagineGiocatore",
        type: "get",
        data: { 'id': id },
        success: function (data) {
            //console.log(data)
            data = JSON.parse(data)[0];
            //var date = new Date();
            //let adesso = date.getTime();
            let adesso = Adesso();
            $("#PreviewImmagineGiocatore").attr("src", "\assetsSquadre\\" + data["nva_Directory"] + "\\Players\\" + data["nva_Numero"] + ".png?" + adesso);
        }
    });
});



$('body').on('click', '.btn-ChiudiModalCrudGiocatore', function () {
    $("#ModalCrudGiocatore").modal('hide');
    if (ricordaModalAperta > 0) {
        FormazioneGenerica(ricordaModalAperta)
        ricordaModalAperta = 0;
    }
});



//$('#myModal').on('hidden.bs.modal', function () {
//$('body').on('click', '.btn-salvaGiocatore', function () {
//    if (rememberModal == 1) {

//    } 
//})




$('body').on('click', '.btn-salvaGiocatore', function () {


    //console.log("salvo giocatore");
    let id = $('#form_giocatori_id').val();

    var formData = new FormData();
    formData.append('ImmagineGiocatore', $('#form_giocatori_ImmagineGiocatore')[0].files[0]);
    formData.append('id', id);
    formData.append('nva_Nome', $('#form_giocatori_nva_Nome').val());
    formData.append('nva_Cognome', $('#form_giocatori_nva_Cognome').val());
    formData.append('fk_int_Squadra', $('#form_giocatori_fk_int_Squadra').val());
    formData.append('nva_Numero', $('#form_giocatori_nva_Numero').val());
    //console.log(formData);
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "crud/SalvaGiocatore",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
            //console.log(data);
            data = JSON.parse(data)


            if (data.Errori.length == 0) {

                if (ricordaModalAperta > 0) {
                    $("#ModalCrudGiocatore").modal('hide');
                    FormazioneGenerica(ricordaModalAperta)
                    ricordaModalAperta = 0;
                } else {
                    PopolaFormCrudGiocatore(id)
                }


                if (($('#form_giocatori_fk_int_Squadra').val() == SettingMatch.fk_int_SquadraCasa) || ($('#form_giocatori_fk_int_Squadra').val() == SettingMatch.fk_int_SquadraCasa)) {

                    PopolaRosaSquadra($('#form_giocatori_fk_int_Squadra').val());
                    let Comando = CostruisciMessaggio('PopolaRosaSquadra(' + $('#form_giocatori_fk_int_Squadra').val() + ')');
                    SetDatiToPlayer(Comando);
                }


            } else {

                alert(data.Errori.join());
            }
        },
        error: function (e) {
            //console.log("ERROR : ", e);
        }
    });


})


$('body').on('click', '.btn-SalvaOraInizioPrepartita', function () {

    var ore = $(".InizioPrepartitaOre").val();
    var minuti = $(".InizioPrepartitaMinuti").val();
    $.ajax({
        url: "/Ajax/SalvaOraInizioPrepartita",
        type: 'get',
        data: { ora: ore + ":" + minuti }
    }).done(function (data) {
        if (data == "ok") {
            $("#ModalOrarioPrepartita").modal("hide");
            $(".btn-temporizzaPrepartita").html(ore + ":" + minuti);
        } else {
            alert("errore");
        }

    });
});

$('body').on('click', '.btn-CancellaOraInizioPrepartita', function () {
    $.ajax({
        url: "/Ajax/CancellaOraInizioPrepartita",
        type: 'get',
        data: { ora: "0:0" }
    }).done(function (data) {
        if (data == "ok") {
            $("#ModalOrarioPrepartita").modal("hide");
            $(".btn-temporizzaPrepartita").html("AVVIO AUT.");
            $(".InizioPrepartitaOre").val(0);
            $(".InizioPrepartitaMinuti").val(0);
        } else {
            alert("errore");
        }

    });
});


$('body').on('click', '.btn-SalvaOraInizioEvento', function () {

    var ore = $(".InizioEventoOre").val();
    var minuti = $(".InizioEventoMinuti").val();
    var idPeriodo = $(".InizioEventoIdPeriodo").html();
    $.ajax({
        url: "/Ajax/SalvaOraInizioEvento",
        type: 'get',
        data: { ora: ore + ":" + minuti, idPeriodo: idPeriodo }
    }).done(function (data) {
        if (data == "ok") {
            $("#ModalOrarioEvento").modal("hide");
            $(".avvioIdPeriodo_" + idPeriodo).html(ore + ":" + minuti);
            $(".avvioIdPeriodoInContenitore_" + idPeriodo).prop("disabled", true);
        } else {
            alert("errore");
        }

    });
});

$('body').on('click', '.btn-CancellaOraInizioEvento', function () {
    var idPeriodo = $(".InizioEventoIdPeriodo").html();
    $.ajax({
        url: "/Ajax/CancellaOraInizioEvento",
        type: 'get',
        data: { ora: "0:0", idPeriodo: idPeriodo }
    }).done(function (data) {
        if (data == "ok") {
            $("#ModalOrarioEvento").modal("hide");
            $(".avvioIdPeriodo_" + idPeriodo).html("AVVIO ORE");
            $(".avvioIdPeriodoInContenitore_" + idPeriodo).prop("disabled", false);
            $(".avvioIdPeriodo_" + idPeriodo).prop("disabled", false);
            $(".InizioEventoOre").val(0);
            $(".InizioEventoMinuti").val(0);
        } else {
            alert("errore");
        }

    });
});

$('body').on('click', '.btn-SalvaOraInizioEventoInContenitore', function () {

    var minuti = $(".InizioEventoInContenitoreMinuti").val();
    var secondi = $(".InizioEventoInContenitoreSecondi").val();
    var idPeriodo = $(".InizioEventoInContenitoreIdPeriodo").text();
    var idPeriodoContenitore = $(".InizioEventoInContenitoreIdPeriodoContenitore").val();
    var secondiTotali = (parseInt(minuti) * 60) + parseInt(secondi);
    $.ajax({
        url: "/Ajax/SalvaInizioEventoInContenitore",
        type: 'get',
        data: { secondi: secondiTotali, idPeriodo: idPeriodo, idPeriodoContenitore: idPeriodoContenitore }
    }).done(function (data) {
        if (data.indexOf("ok") > -1) {
            $("#ModalOrarioEventoInContenitore").modal("hide");
            if (data.length > 3) {
                var codicePeriodo = data.substring(3);
                $(".avvioIdPeriodoInContenitore_" + idPeriodo).html(pad(minuti, 2) + ":" + pad(secondi, 2) + " " + codicePeriodo);
                $(".avvioIdPeriodo_" + idPeriodo).prop("disabled", true);
            }
        } else {
            alert("errore");
        }

    });
});

$('body').on('click', '.btn-CancellaOraInizioEventoInContenitore', function () {
    var idPeriodo = $(".InizioEventoInContenitoreIdPeriodo").html();
    $.ajax({
        url: "/Ajax/CancellaOraInizioEvento",
        type: 'get',
        data: { ora: "0:0", idPeriodo: idPeriodo }
    }).done(function (data) {
        if (data == "ok") {
            $("#ModalOrarioEventoInContenitore").modal("hide");
            $(".avvioIdPeriodoInContenitore_" + idPeriodo).html("AVVIO IN PER.");
            $(".avvioIdPeriodoInContenitore_" + idPeriodo).prop("disabled", false);
            $(".avvioIdPeriodo_" + idPeriodo).prop("disabled", false);
            $(".InizioEventoInContenitoreMinuti").val(0);
            $(".InizioEventoInContenitoreSecondi").val(0);
        } else {
            alert("errore");
        }

    });
});

$('body').on('click', '.evento_TipoEsecuzione', function () {

    var nuovoTipoEsecuzione = $(this).data("tipoesecuzione"); 
    var id_Periodo = $(this).data("idperiodo"); 
    $(this).closest("td").find(".evento_TipoEsecuzione").removeClass("active");
    $(this).addClass("active");

    $.ajax({
        url: "/Ajax/SalvaTipoEsecuzioneEvento",
        type: 'get',
        data: { idPeriodo: id_Periodo, tipoEsecuzione: nuovoTipoEsecuzione }
    }).done(function (data) {
        if (data == "ok") {

        } else {
            alert("errore");
        }

    });
});

function findValueInSelect(nomeSelect, etichettaParziale) {
    var ret = null;
    Array.from(document.querySelector("#"+nomeSelect).options).forEach(function (option_element) {
        var option_text = option_element.text;
        var option_value = option_element.value;
        if (option_text.indexOf(etichettaParziale) > -1)
            ret = option_value;
    });
    return ret;
}