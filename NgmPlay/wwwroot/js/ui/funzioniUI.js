﻿/*riproduzione*/
function GeneraRiproduzione() {
    $.ajax({
        url: "/Ajax/sp_GeneraRiproduzione",
        async: false
    }).done(function (data) {

        data = JSON.parse(data);
        fk_int_Riproduzione = data[0]["id"];
    })
}





function GetUltimaRiproduzione() {
    $.ajax({
        url: "/Ajax/sp_GetUltimaRiproduzione",
        async: false
    }).done(function (data) {
        data = JSON.parse(data);
        fk_int_Riproduzione = data[0]["id"];
    })
}



function MostraPreloadSync() {
    $(".btn-preloadSceneSync").show();
    $(".btn-sync").hide();

}

function StringaElemento(elemento, isLed, isMaxischermo) {
    if (elemento.Tipo == 'mp4') {
        return ('<button type="button" class=" btn btn-danger btn-lg  btn-block btn-LanciaEvento" data-led=' + isLed + ' data-maxischermo=' + isMaxischermo + ' data-evento="50" data-filename="' + elemento.Nome + '"><i class="fa fa-film"></i>&nbsp;' + GetNomeFileSenzaEstensione(elemento.Nome) + '</button>');
    } else {
        return ('<button type="button" class=" btn btn-danger btn-lg  btn-block btn-LanciaEvento" data-led=' + isLed + ' data-maxischermo=' + isMaxischermo + ' data-evento="50" data-filename="' + elemento.Nome + '"><i class="fa fa-picture-o"></i>&nbsp;' + GetNomeFileSenzaEstensione(elemento.Nome) + '</button>');
    }
}

function AzioneTastiMaxischermo(oggetto) {
    $.ajax({
        url: "/Ajax/sp_AzioneTastiMaxischermo",
        data: "oggetto=" + oggetto
    }).done(function (data) {
        ////////console.log(data);
    });
}

function CostruisciGraficaDipendenteDaMatch() {
    //console.log(SettingMatch)
    //console.log("controllare sotto");
    //console.log(SettingSquadra[SettingMatch.fk_int_SquadraCasa])

    $(".LogoSquadraCasa").attr("src", "/assetsSquadre/" + (SettingSquadra[SettingMatch.fk_int_SquadraCasa]).nva_Directory + "/Grafica/logo.png?" + TimeAvvioSchermata)
    $(".LogoSquadraOspite").attr("src", "/assetsSquadre/" + (SettingSquadra[SettingMatch.fk_int_SquadraOspite]).nva_Directory + "/Grafica/logo.png?" + TimeAvvioSchermata)

    //$(".LogoSquadraOspite").attr("src", "/img/squadre/" + SettingMatch.fk_int_SquadraOspite + ".png")

    $(".btn-Casa").data("fk_int_squadra", SettingMatch.fk_int_SquadraCasa);
    $(".btn-Ospiti").data("fk_int_squadra", SettingMatch.fk_int_SquadraOspite);
}

function GetListaEventiSpecialiPeriodiMatch(data, fk_int_periodo) {

    data = JSON.parse(data);
    $(".PulsantiSceneSpeciali").empty();
    $(".boxRigore").hide();
    $(".boxRigore").data("fk_int_scenaLed", 0);
    $(".boxRigore").data("fk_int_scenaMaxischermo", 0);

    $.each(data, function (i, item) {

        if (item.nva_Nome.toLowerCase().includes("penalty") || item.nva_Nome.toLowerCase().includes("rigore")) {
            $(".boxRigore").show();
            $(".boxRigore").data("fk_int_scenaLed", item.fk_int_ScenaLed);
            $(".boxRigore").data("fk_int_scenaMaxischermo", item.fk_int_ScenaMaxischermo);
        } else {
            $(".PulsantiSceneSpeciali").append('<button type="button" class="btn btn-danger btn-lg btn-block btn-RiproduciSceneSpecialiDentroPeriodo"  data-fk_int_scenaled="' + item.fk_int_ScenaLed + '"   data-fk_int_scenamaxischermo="' + item.fk_int_ScenaMaxischermo + '" >' + item.nva_Nome + '</button>');
        }

    });

}

function AvviaRiproduzioneScenaSpeciale(fk_int_scenaLed, fk_int_scenaMaxischermo) {

    let adesso = Adesso() 

    if (fk_int_scenaLed > 0) {
        $.ajax({
            url: "/Ajax/sp_GestisciRiproduzioneLed",
            data: "fk_int_ScenaSpecificaPush=" + fk_int_scenaLed + "&int_DataOraJavascript=" + adesso
        }).done(function (data) {
            ////////console.log(data);
        });
    }

    if (fk_int_scenaMaxischermo > 0) {
        $.ajax({
            url: "/Ajax/sp_GestisciRiproduzioneMaxischermo",
            data: "fk_int_ScenaSpecificaPush=" + fk_int_scenaMaxischermo + "&int_DataOraJavascript=" + adesso
        }).done(function (data) {
            ////////console.log(data);
        });
    }
}

function GetSceneInPeriodi(Scenecariche) {

    $(".SuntoCaricamentoScene").each(function () {

        let gg = Scenecariche[$(this).data("fk_int_periodo")];

        if (!(gg === undefined)) {
            $(this).find(".NumeroSceneCaricatePeriodo").html(gg);
            $(this).addClass("SuntoCaricamentoSceneCarica")
            $(this).removeClass("SuntoCaricamentoSceneScarica")
        } else {
            $(this).removeClass("SuntoCaricamentoSceneCarica")
            $(this).addClass("SuntoCaricamentoSceneScarica")
        }

    });

}

function CancellaPlaylist(ID) {
    $.ajax({
        url: "/Ajax/CancellaPlaylist?ID=" + ID
    }).done(function (data) {
        PopolaModalPlaylist();
    });
}

function PopolaModalPlaylist() {
    var tbody = $(".tabellaPlaylist");

    if (ControllaSetting(SettingMatch)) {
        $.ajax({
            url: "/Ajax/GetPlaylist"
        }).done(function (data) {
            data = JSON.parse(data);
            tbody.empty();
            $.each(data, function (k, item) {
                tbody.append('<tr class="playlist' + ((item.IsSelected) ? ' bg-danger' : '') + '" data-id="' + item.ID + '"><td>' + item.nva_NomePlaylist + '</td><td>' + item.int_SceneGenerate + '/' + item.int_SceneTotali+'</td><td><b>' + item.nva_Provenienza + '</b></td><td>' + item.nva_DataCaricamento + '</td><td><img  src="img/consistenza_' + item.int_Consistenza + '.png"/></td><td><button type="button" class="btn btn-primary btn-SelezionaPlaylist ' + ((item.IsSelected) ? 'invisible' : '') +'">Seleziona</button><button type="button" class="btn btn-danger btn_CancellaPlaylist '+((item.IsSelected) ? 'invisible' : '')+'">Cancella</button></td></tr>');
            });
        });
    }
}

function FormazioneGenerica(idSquadra) {

    if (ControllaSetting(SettingMatch)) {
        $("#ModalFormazione").data("fk_int_squadra", idSquadra);
        $("#ModalFormazione").data("fk_int_match", SettingMatch.ID);
        $("#ModalFormazione").data("azione", "formazione");

        CostruisciFormazione().done(function () {
            $("#ModalFormazione").find("ul").sortable({
                stop: function () {
                    $("#ModalFormazione").find(".btn-Salva").show();
                    AttribuisciProgressivoTitolariSortable($("#ModalFormazione"))
                }
            });
            NascondiBottoniDentroFormazione();
            BottoniVisibiliDentroFormazione();
            $("#ModalFormazione").modal("show");
        });
    }
}

function BottoniVisibiliDentroFormazione() {
    $("#ModalFormazione").find(".DivVisualizzaAnimazioneGiocatore").hide();
    $("#ModalFormazione").find(".btn-InserisciGiocatore").show();
    $("#ModalFormazione").find(".btn-SalvaFormazione").show();
    $("#ModalFormazione").find(".icone-eventi").hide();
    $('#ModalFormazione').find(".spinner-border").hide();
    $("#ModalFormazione").find("h5").text("Componi la formazione - " + SettingSquadra[$("#ModalFormazione").data("fk_int_squadra")].nva_Nome);

}

function FormazioneGenerica_ok_commentata(idSquadra) {

    if (ControllaSetting(SettingMatch)) {
        $("#ModalFormazione").data("fk_int_squadra", idSquadra);
        $("#ModalFormazione").data("fk_int_match", SettingMatch.ID);
        $("#ModalFormazione").data("azione", "formazione");

        CostruisciFormazione().done(function () {

            NascondiBottoniDentroFormazione();

            $(".DivVisualizzaAnimazioneGiocatore").hide();
            $("#ModalFormazione").find(".btn-SalvaFormazione").show();
            $("#ModalFormazione").find(".icone-eventi").hide();
            $('#ModalFormazione').find(".spinner-border").hide();
            $("#ModalFormazione").find("h5").text("Componi la formazione - " + SettingSquadra[idSquadra].nva_Nome);
            $("#ModalFormazione").modal("show");
        });
    }
}






function PresentazioneFormazione(idSquadra) {
    if (ControllaSetting(SettingMatch)) {
        $("#ModalPresentazioneFormazione").data("fk_int_squadra", idSquadra);
        $("#ModalPresentazioneFormazione").data("fk_int_match", SettingMatch.ID);
        $("#ModalPresentazioneFormazione").data("azione", "presentazioneformazione");
        $(".trPresentazioneFormazione").empty();
   

        var elemento = $("#ModalPresentazioneFormazione")
        var id = elemento.data("fk_int_squadra")
        elemento.find(".btn-Salva").hide();
        var ul = elemento.find("ul")
        ul.empty();
        var s = ""
        var numeroGiocatoriMostrati = 100
        numeroGiocatoriMostrati = parseInt(ArrParametriGenerali.int_NumeroGiocatoriDaPresentare);
        s = SettingSquadra[id];


        var Str = '';
        Str += '<td class="btn_ElementoPresentazioneFormazione" data-elemento="SponsorFormazione" >';
        Str += '<div    class="wrapperimage">';
        Str += '<img    class="eventoGiocatore eventoPresentazione " src="/img/icon/ciak.png"/>';
        Str += '<div    class="img">';
        Str += '<img    class="img-responsive giocatorePresentazione"  alt=""  src="/img/icon/video.png" />';
        Str += '<span> </span>';
        Str += '</div>';
        Str += '<div    class="nomeGiocatore">';
        Str += 'Sponsor formazione';
        Str += '</div>';
        Str += '</div>';
        Str += '</td>';
        $(".trPresentazioneFormazione").append(Str);





        Str = '';
        Str += '<td class="btn_ElementoPresentazioneFormazione" data-elemento="TitolariOspiti" >';
        Str += '<div    class="wrapperimage">';
        Str += '<img    class="eventoGiocatore eventoPresentazione " src="/img/icon/ciak.png"/>';
        Str += '<div    class="img">';
        Str += '<img    class="img-responsive giocatorePresentazione"  alt=""  src="/img/icon/titolari.png" />';
        Str += '<span> </span>'
        Str += '</div>';
        Str += '<div    class="nomeGiocatore">';
        Str += 'Titolari ospiti'
        Str += '</div>';
        Str += '</div>';
        Str += '</td>';
        $(".trPresentazioneFormazione").append(Str);



        Str = '';
        Str += '<td class="btn_ElementoPresentazioneFormazione" data-elemento="RiserveOspiti" >';
        Str += '<div    class="wrapperimage">';
        Str += '<img    class="eventoGiocatore eventoPresentazione " src="/img/icon/ciak.png"/>';
        Str += '<div    class="img">';
        Str += '<img    class="img-responsive giocatorePresentazione"  alt=""  src="/img/icon/riserve.png" />';
        Str += '<span> </span>'
        Str += '</div>';
        Str += '<div    class="nomeGiocatore">';
        Str += 'Riserve ospiti'
        Str += '</div>';
        Str += '</div>';
        Str += '</td>';
        $(".trPresentazioneFormazione").append(Str);


        Str = '';
        Str += '<td class="btn_ElementoPresentazioneFormazione" data-elemento="IntroCasa" >';
        Str += '<div    class="wrapperimage">';
        Str += '<img    class="eventoGiocatore eventoPresentazione " src="/img/icon/ciak.png"/>';
        Str += '<div    class="img">';
        Str += '<img    class="img-responsive giocatorePresentazione"  alt=""  src="/img/icon/fireworks.png" />';
        Str += '<span> </span>'
        Str += '</div>';
        Str += '<div    class="nomeGiocatore">';
        Str += 'Intro casa'
        Str += '</div>';
        Str += '</div>';
        Str += '</td>';
        $(".trPresentazioneFormazione").append(Str);





        Str = '';
        Str += '<td class="btn_ElementoPresentazioneFormazione tdTitolariCasa" data-elemento="TitolariCasa" >';
        Str += '<div    class="wrapperimage">';
        Str += '<img    class="eventoGiocatore eventoPresentazione " src="/img/icon/ciak.png"/>';
        Str += '<div    class="img">';
        Str += '<img    class="img-responsive giocatorePresentazione"  alt=""  src="/img/icon/titolari.png" />';
        Str += '<span> </span>'
        Str += '</div>';
        Str += '<div    class="nomeGiocatore">';
        Str += 'Titolari casa'
        Str += '</div>';
        Str += '</div>';
        Str += '</td>';
        $(".trPresentazioneFormazione").append(Str);

        ArrFormazioneCliccata = {};


        $.ajax({
            url: "/Ajax/sp_GetGiocatoriMatch?fk_int_Squadra=" + idSquadra
        }).done(function (data) {

            data = JSON.parse(data);
            ArrFormazioneCliccata = {};
            $.each(data, function (i, item) {
                
                var Ordine = "";
                var classeCss = "";

                TimeAvvioSchermata = Adesso();
                item.nva_Directory = s.nva_Directory;

                if ((item.int_Ordine > 0) && (item.int_Ordine < (parseInt(numeroGiocatoriMostrati) + 1))) {

                    ArrFormazioneCliccata[item.int_Ordine] = item;
                    var Str = '';
                    Str += '<td class="formazione singoloGiocatore btn_ElementoPresentazioneFormazione "   data-elemento="' + item.int_Ordine + '"    >';
                    Str += '<div class="wrapperimage">';
                    Str += '<img class="eventoGiocatore eventoPresentazione " src="/img/icon/ciak.png"/>';
                    Str += '<div class="img_small">';
                    Str += '<img class="img-responsive giocatorePresentazione"  alt=""  src="/assetsSquadre/' + s.nva_Directory + '/Players/' + item.nva_Numero + '.png?' + TimeAvvioSchermata + '" />';
                    Str += '<span>' + item.nva_Numero + '</span>';
                    Str += '</div>';

                    Str += '<div class="nomeGiocatore">';
                    Str += item.nva_Cognome + ' ' + item.nva_Nome;
                    Str += '<p class="ordineGiocatore">' + Ordine + '</p>';
                    Str += '</div>';
                    Str += '</div>';
                    Str += '</td>';

                    $(".trPresentazioneFormazione").append(Str);

                } 

            
            });




            Str = '';
            Str += '<td class="btn_ElementoPresentazioneFormazione" data-elemento="RiserveCasa" >';
            Str += '<div class="wrapperimage">';
            Str += '<img class="eventoGiocatore eventoPresentazione " src="/img/icon/ciak.png"/>';
            Str += '<div class="img">';
            Str += '<img class="img-responsive giocatorePresentazione"  alt=""  src="/img/icon/riserve.png" />';
            Str += '<span> </span>'
            Str += '</div>';
            Str += '<div class="nomeGiocatore">';
            Str += 'Riserve casa'
            Str += '</div>';
            Str += '</div>';
            Str += '</td>';
            $(".trPresentazioneFormazione").append(Str);

            $(".btn-RiproduciSuccessivo").html("Avvio");
            $(".btn-RiproduciPrecedente").hide();


            elemento.find(".spinner-border").hide();

            $(".eventoGiocatore").hide();

            $(".btn-PresentazioneEstesaCompatta").data("tipoact", "estesa");
            $(".btn-PresentazioneEstesaCompatta").html("Pres. compatta");
            $(".singoloGiocatore").show();
            $(".tdTitolariCasa").hide();

        });




        $("#ModalPresentazioneFormazione").modal("show");

    }
}


function PresentazioneFormazione_new(idSquadra) {
    if (ControllaSetting(SettingMatch)) {
        $("#ModalFormazione").data("fk_int_squadra", idSquadra);
        $("#ModalFormazione").data("fk_int_match", SettingMatch.ID);
        $("#ModalFormazione").data("azione", "presentazioneformazione");

        if (IsYes(ArrParametriGenerali.tin_PresentazioneFormazioneSenzaVideo)) {

        } else {

            $.ajax({
                url: "/Ajax/sp_GetAssociazioneGiocatorePeriodo?fk_int_squadra=" + idSquadra
            }).done(function (data) {
                ArrAssociazioneGiocatorePeriodo = JSON.parse(data);
            });

            $.ajax({url: "/Ajax/sp_SendToPlayerPreloadScenePresentazioneGiocatori"}).done(function (data) {});

        }


        GiocatorePresentato = 0;
        CostruisciFormazione().done(function () {
            $("#ModalFormazione").find(".modal-title").html("Presentazione - " + SettingSquadra[idSquadra].nva_Nome);
            // $("#ModalFormazione").find("ul").sortable().sortable("disable");
            $('.non-convocati').hide();
            NascondiBottoniDentroFormazione();
            $("#ModalFormazione").find(".DivVisualizzaAnimazioneGiocatore").hide();
            $("#ModalFormazione").find(".pannelloRegiaPresentazioneFormazioni").show();
            $("#ModalFormazione .btn-Maxischermo").show();

            $("#ModalFormazione").find(".btn-RiproduciSuccessivo").text("Avvia presentazione primo giocatore");
            $("#ModalFormazione").find(".btn-RiproduciSuccessivo").show();
            //$("#ModalFormazione").find(".btn-RiproduciPrecedente").hide();
            $("#ModalFormazione").find(".icone-eventi").hide();
            $("#ModalFormazione").find("h5").text("Segui la presentazione dello speeker - " + SettingSquadra[idSquadra].nva_Nome);
            $('#ModalFormazione').find(".spinner-border").hide();
            $("#ModalFormazione").modal("show");

            $("#ModalFormazione .panchina").hide();

        });

    }
}

function PopUpSceltaGiocatoreGoalGenerico(idSquadra, autogoal) {
    if (ControllaSetting(SettingMatch)) {

        if (autogoal == 1) {
            $("#ModalFormazione").data("azione", "autogoal");
        } else {
            $("#ModalFormazione").data("azione", "goal");
        }

        $("#ModalFormazione").data("fk_int_squadra", idSquadra);
        //       $("#ModalFormazione").data("is_autogoal", autogoal);
        $("#ModalFormazione").data("fk_int_match", SettingMatch.ID);

        CostruisciFormazione().done(function () {
            $("#ModalFormazione").find(".modal-title").html("Seleziona esecutore goal - " + SettingSquadra[idSquadra].nva_Nome);
            //  $("#ModalFormazione").find("ul").sortable().sortable("disable");
            $('.non-convocati').hide();
            NascondiBottoniDentroFormazione();
            $("#ModalFormazione").find(".DivVisualizzaAnimazioneGiocatore").show();
            $("#ModalFormazione").find(".btn-RiproduciGoal").show();
            $("#ModalFormazione").find(".btn-Autogoal").show();
            $("#ModalFormazione").find(".icone-eventi").hide();

            $("#ModalFormazione").find("h5").text("Seleziona GOAL - " + SettingSquadra[idSquadra].nva_Nome);
            $('#ModalFormazione').find(".spinner-border").hide();
            $("#ModalFormazione").modal("show");
        });
    }
}

function InvertiSquadraPerAutogoal(Squadra) {
    if (Squadra == SettingMatch.fk_int_SquadraCasa) {
        SquadraAvversaria = SettingMatch.fk_int_SquadraOspite;
    } else {
        SquadraAvversaria = SettingMatch.fk_int_SquadraCasa;
    }
    return SquadraAvversaria
}




function RosaGenerico(idSquadra) {
    if (ControllaSetting(SettingMatch)) {
        $("#ModalFormazione").data("fk_int_squadra", idSquadra);
        $("#ModalFormazione").data("fk_int_match", SettingMatch.ID);
        $("#ModalFormazione").data("azione", "rosa");
        CostruisciFormazione().done(function () {
            $("#ModalFormazione").find(".modal-title").html("Seleziona da presentare - " + SettingSquadra[idSquadra].nva_Nome);
            // $("#ModalFormazione").find("ul").sortable().sortable("disable");
            $('.non-convocati').hide();

            NascondiBottoniDentroFormazione();
            $("#ModalFormazione").find(".DivVisualizzaAnimazioneGiocatore").show();
            // $("#ModalFormazione").find(".btn-RiproduciAmmonizione").show();
            $("#ModalFormazione").find(".icone-eventi").hide();
            $("#ModalFormazione").find("h5").text("Seleziona da presentare - " + SettingSquadra[idSquadra].nva_Nome);
            $('#ModalFormazione').find(".spinner-border").hide();
            $("#ModalFormazione").modal("show");
        });
    }
}




function AmmonizioneGenerico(idSquadra) {
    if (ControllaSetting(SettingMatch)) {
        $("#ModalFormazione").data("fk_int_squadra", idSquadra);
        $("#ModalFormazione").data("fk_int_match", SettingMatch.ID);
        $("#ModalFormazione").data("azione", "ammonizione");
        CostruisciFormazione().done(function () {
            $("#ModalFormazione").find(".modal-title").html("Seleziona ammonito - " + SettingSquadra[idSquadra].nva_Nome);
            // $("#ModalFormazione").find("ul").sortable().sortable("disable");
            $('.non-convocati').hide();

            NascondiBottoniDentroFormazione();
            $("#ModalFormazione").find(".DivVisualizzaAnimazioneGiocatore").show();
            // $("#ModalFormazione").find(".btn-RiproduciAmmonizione").show();
            $("#ModalFormazione").find(".icone-eventi").hide();
            $("#ModalFormazione").find("h5").text("Seleziona ammonito - " + SettingSquadra[idSquadra].nva_Nome);
            $('#ModalFormazione').find(".spinner-border").hide();
            $("#ModalFormazione").modal("show");
        });
    }
}

function EspulsioneGenerico(idSquadra) {
    if (ControllaSetting(SettingMatch)) {
        $("#ModalFormazione").data("fk_int_squadra", idSquadra);
        $("#ModalFormazione").data("fk_int_match", SettingMatch.ID);
        $("#ModalFormazione").data("azione", "espulsione");
        CostruisciFormazione().done(function () {
            $("#ModalFormazione").find(".modal-title").html("Seleziona espulso - " + SettingSquadra[idSquadra].nva_Nome);
            // $("#ModalFormazione").find("ul").sortable().sortable("disable");
            $('.non-convocati').hide();
            NascondiBottoniDentroFormazione();
            $("#ModalFormazione").find(".DivVisualizzaAnimazioneGiocatore").show();
            // $("#ModalFormazione").find(".btn-RiproduciAmmonizione").show();
            $("#ModalFormazione").find(".icone-eventi").hide();
            $("#ModalFormazione").find("h5").text("Seleziona espulso - " + SettingSquadra[idSquadra].nva_Nome);
            $('#ModalFormazione').find(".spinner-border").hide();
            $("#ModalFormazione").modal("show");
        });
    }
}



function SostituzioneGenerico(idSquadra) {
    if (ControllaSetting(SettingMatch)) {
        $("#ModalFormazione").data("fk_int_squadra", idSquadra);
        $("#ModalFormazione").data("fk_int_match", SettingMatch.ID);
        $("#ModalFormazione").data("azione", "sostituzione");
        CostruisciFormazione().done(function () {
            //$("#ModalFormazione").find(".modal-title").html("Seleziona il giocatore che esce e successivamente quello che entra - " + nomeSquadra);
            //$("#ModalFormazione").find("ul").sortable().sortable("disable");
            $('.non-convocati').hide();
            NascondiBottoniDentroFormazione();
            $("#ModalFormazione").find(".DivVisualizzaAnimazioneGiocatore").show();
            //$("#ModalFormazione").find(".btn-RiproduciSostituzione").show();
            $("#ModalFormazione").find(".icone-eventi").hide();
            $("#ModalFormazione").find("h5").text("Seleziona il giocatore che esce e successivamente quello che entra - " + SettingSquadra[idSquadra].nva_Nome);
            $('#ModalFormazione').find(".spinner-border").hide();
            $("#ModalFormazione").modal("show");
        });
    }
}

function RigoreGenerico(idSquadra) {
    if (ControllaSetting(SettingMatch)) {

        $("#ModalFormazione").data("fk_int_squadra", idSquadra);
        $("#ModalFormazione").data("fk_int_match", SettingMatch.ID);
        $("#ModalFormazione").data("azione", "rigore");
        CostruisciFormazione().done(function () {
            //   $("#ModalFormazione").find(".modal-title").html("Seleziona il giocatore che esce e successivamente quello che entra - " + nomeSquadra);
            //     $("#ModalFormazione").find("ul").sortable().sortable("disable");
            $('.non-convocati').hide();
            NascondiBottoniDentroFormazione();
            $("#ModalFormazione").find(".DivVisualizzaAnimazioneGiocatore").show();
            //  $("#ModalFormazione").find(".btn-RiproduciSostituzione").show();
            $("#ModalFormazione").find(".icone-eventi").hide();
            $("#ModalFormazione").find("h5").text("Seleziona il rigorista - " + SettingSquadra[idSquadra].nva_Nome);
            $('#ModalFormazione').find(".spinner-border").hide();
            $("#ModalFormazione").modal("show");
        });
    }
}

function PopolaMatch(idRowEdit = 0) {
    var date = new Date;



    let defaultObj = { id: 0, fk_int_SquadraCasa: 0, fk_int_SquadraOspite: 0, dd: date.getDate(), mm: (date.getMonth()+1), yyyy: date.getFullYear(), n: date.getMinutes(), hh: date.getHours()};
    $("#ModalSceltaMatch tbody").empty();
    $.ajax({
        url: "/Ajax/GetMatch"
    }).done(function (data) {
        data = JSON.parse(data);
        $.each(data, function (k, v) {
            if (idRowEdit == v.id) {
                $("#ModalSceltaMatch tbody").append(EditRowMatch(v))
                defaultObj = v;
            } else {
                $("#ModalSceltaMatch tbody").append(ViewRowMatch(v))
            }
        });
        
        if(idRowEdit == 0){
            $("#ModalSceltaMatch tbody").append(EditRowMatch(defaultObj))
        }
        //console.log("/Ajax/GetSquadre?fk_int_Stadio=" + ParametriPC_fk_int_Stadio);
        $.ajax({
            url: "/Ajax/GetSquadre?fk_int_Stadio=" + ParametriPC_fk_int_Stadio
        }).done(function (data) {
            data = JSON.parse(data);
            $.each(data, function (i, vv) {
                $("#SquadraCasa select").append("<option value='" + vv.ID + "'>" + vv.nva_Nome + "</option>")
            })
            $("#SquadraCasa select").val(defaultObj.fk_int_SquadraCasa);
        });
        $.ajax({
            url: "/Ajax/GetSquadre"
        }).done(function (data) {
            data = JSON.parse(data);
            $.each(data, function (i, vv) {
                $("#SquadraOspite select").append("<option value='" + vv.ID + "'>" + vv.nva_Nome + "</option>")
            })
            $("#SquadraOspite select").val(defaultObj.fk_int_SquadraOspite);
        });
        $('#DataMatch input').mask("99/99/9999");
        $('#OraMatch input').mask("99:99");
    });
}


function ViewRowMatch(v) {
    //console.log(v)
    return "<tr id='Match_" + v.id + "' class='" + (v.IsSelected == 1 ? 'bg-success' : '') + "'><th>" + v.nva_NomeSquadraCasa + "</th><th>" + v.nva_NomeSquadraOspite + "</th><th>" + pad(v.dd, 2) + "/" + pad(v.mm, 2) + "/" + v.yyyy + "</th><th>" + pad(v.hh, 2) + ":" + pad(v.n, 2) + "</th><th><div class='btn btn-warning modifica'>modfica</div>  <div class='btn btn-danger elimina " + (v.IsSelected == 1 ? "invisible" : "") +"'>elimina</div> <div class='btn btn-success seleziona " + (v.IsSelected == 1 ? "invisible" : "")+"'>seleziona</div></th></tr>"
}

function EditRowMatch(v) {
    return "<tr id='Match_" + v.id + "' class='" + (v.IsSelected == 1 ? 'bg-success' : '') + "'><th id='SquadraCasa'><select class='form-control'></select></th><th id='SquadraOspite'><select  class='form-control'></select></th><th id='DataMatch'><input   class='form-control' type='text' value='" + pad(v.dd, 2) + '/' + pad(v.mm, 2) + '/' + v.yyyy + "'  maxlength='10' size='10' /></th><th id='OraMatch'><input  class='form-control' type='text' value='" + pad(v.hh, 2) + ':' + pad(v.n, 2) +"' size='5'/></th><th><div class='btn btn-warning salva'>" + (v.id == 0 ? 'inserisci' : 'salva') + "</div></th></tr>"
}

function SelezionaMatch(id) {
    var domanda = confirm("Stai selezionando un match, sarà interrotta la riproduzione e ricaricato un nuovo layout, confermi?");
    if (domanda === true) {
        SetSelected(id, "tb_Match");

        GeneraRiproduzione();
        let Comando = CostruisciMessaggio('location.reload();');
        SetDatiToPlayer(Comando);
        location.reload();
    }
    /*
    $("#ModalSceltaMatch tr").removeClass("bg-success");
    $("#Match_" + id).addClass("bg-success");
    $("#ModalSceltaMatch .seleziona").removeClass("invisible");
    $("#Match_" + id + " .seleziona").addClass("invisible");
    $("#ModalSceltaMatch .elimina").removeClass("invisible");
    $("#Match_" + id + " .elimina").addClass("invisible");
    */

}

function EliminaMatch(id) {

    SetDeleted(id, "tb_Match");
    $("#Match_" + id).remove();
}

function SalvaMatch(id) {
    let valid = true;
    let idSquadraCasa = $("#SquadraCasa select").val();
    let idSquadraOspite = $("#SquadraOspite select").val();
    let ora = $("#OraMatch input").val();
    let data = $("#DataMatch input").val();
    $("#ModalSceltaMatch").find(".is-invalid").removeClass("is-invalid");
    if (ora.substring(0, 2) > 23 || ora.substring(3, 5) > 59 || ora.length != 5 || ora.substring(2, 3) != ":" ) {
        $("#OraMatch input").addClass("is-invalid");
        valid = false;
    }
    if (data.substring(0, 2) > 31 || data.substring(0, 2) == "00" || data.substring(3, 5) == "00" || data.substring(3, 5) > 12 || data.length != 10) {
        $("#DataMatch input").addClass("is-invalid");
        valid = false;
    }
    if (idSquadraCasa == undefined || idSquadraCasa == 0) {
        $("#SquadraCasa select").addClass("is-invalid");
        valid = false;
    }
    if (idSquadraOspite == undefined || idSquadraOspite == 0) {
        $("#SquadraOspite select").addClass("is-invalid");
        valid = false;
    }
    if (valid) {
        let dttOra = data+" "+ora
        let objMatch = new clsMatch(id, ParametriPC_fk_int_Stadio, dttOra, "mymatch", idSquadraCasa, idSquadraOspite, 0)
        console.log(objMatch);
        $.ajax({
            url: "/Ajax/InsertUpdateMatch",
            data: objMatch,
            type: 'POST'
        }).done(function (data) {

            PopolaMatch(0);

        });
    }
}



function PopolaEventiMatch() {
    if (ControllaSetting(SettingMatch)) {
        var tbody = $('.EventiMatch').find("tbody")
        tbody.empty();
        $.ajax({
            url: "/Ajax/sp_GetEventiMatch?id=0"
        }).done(function (data) {
            data = JSON.parse(data);
            $.each(data, function (i, item) {
                tbody.append('<tr data-fk_int_evento="' + item.ID + '"><td><img  height="60" src="/assetsSquadre/' + SettingSquadra[item.fk_int_Squadra].nva_Directory + '/Grafica/logo.png"/></td><td class="txtMinuto">' + item.nva_StringaMinuto + ' </td><td><img  height="60" src="img/icon/tabella_eventi_' + item.fk_int_TipoEventoMatch + '.png"/></td><td class="txtDescrizione">' + item.nva_DescrizioneEventoEstesa + '</td><td><button class="btn btn-outline-primary btn_EditaEventoMatch"><img width="30" src="img/icon/edit.png"/></button></td></tr>');
            });
        });
    }
}

function PopolaAzioniGiocatori(Div) {
    var tbody = Div.find("tbody")
    tbody.empty();
    $.ajax({
        url: "/Ajax/GetGiocatori?fk_int_Squadra=" + Div.data("fk_int_squadra") + "&fk_int_Match=" + Div.data("fk_int_match")
    }).done(function (data) {
        //////////////console.log(data)
        data = JSON.parse(data);
        $.each(data, function (i, item) {
            tbody.append('<tr class="azioni" data-fk_int_Giocatore="' + item.ID + '"><td><img class="img-thumbnail" width="60" src="assetsSquadre/' + item.nva_NomeSquadra + '/Players/' + item.nva_Numero + '.png"></td><td style="text-align:left"><b>' + item.nva_Numero + ' - ' + item.nva_Cognome + '<br>' + item.nva_Nome + '</b></td><td><img class="miniatura-tasti goal"  src="img/goal.png"/></td><td><img class="miniatura-tasti yellow"   src="img/yellow.png"/></td><td><img  class="miniatura-tasti red"  src="img/red.png"/></td><td><img  class="miniatura-tasti giocatore_entra"  src="img/up.png"/></td><td><img  class="miniatura-tasti giocatore_esce"  src="img/down.png"/></td></tr>');
        });
    });
}

function NascondiBottoniDentroFormazione() {
    $(".btn-SalvaFormazione").hide();
    $(".btn-RiproduciSostituzione").hide();
    $(".btn-RiproduciSuccessivo").hide();
    $("#ModalFormazione .btn-Maxischermo").hide();
    $(".btn-RiproduciEspulsione").hide();
    $(".btn-RiproduciAmmonizione").hide();
    $(".btn-RiproduciGoal").hide();
    $(".btn-Autogoal").hide();
    $(".btn-pallaSuDischetto").hide();
    $(".btn-rigoreTrasformato").hide();
    $(".btn-rigoreNonTrasformato").hide();
    $(".pannelloRegiaPresentazioneFormazioni").hide();
    $("#ModalFormazione .btn-InserisciGiocatore").hide();
}

function CostruisciFormazione() {

    var elemento = $("#ModalFormazione")
    var id = elemento.data("fk_int_squadra")
    elemento.find(".btn-Salva").hide();
    var ul = elemento.find("ul")
    ul.empty();
    var s = ""
    var numeroGiocatoriMostrati = 100
    //console.log("---3026")
    //console.log(parseInt(ArrParametriGenerali.int_NumeroGiocatoriDaPresentare))
    if (elemento.data("azione") == "autogoal") {
        s = SettingSquadra[InvertiSquadraPerAutogoal(id)]
    } else if (elemento.data("azione") == "presentazioneformazione") {
        //console.log("elemento.data(azione) == presentazioneformazione")


        numeroGiocatoriMostrati = parseInt(ArrParametriGenerali.int_NumeroGiocatoriDaPresentare);
        s = SettingSquadra[id]
    } else {


        s = SettingSquadra[id]
    }

    ////////console.log();

    return $.ajax({
        url: "/Ajax/sp_GetGiocatoriMatch?fk_int_Squadra=" + s.ID
    }).done(function (data) {
        //////console.log("-----------sp_GetGiocatoriMatch---------------------------------------");
        //////console.log(data)
        data = JSON.parse(data);
        ArrFormazioneCliccata = {};
        $.each(data, function (i, item) {


            var Ordine = "";
            var classeCss = "";

            ////////console.log("ArrFormazioneCliccata")
            if (item.int_Ordine > 0) {
                ArrFormazioneCliccata[item.int_Ordine] = item;
                classeCss = "bg-danger convocati";
                Ordine = item.int_Ordine
            } else {
                classeCss = "non-convocati";
                Ordine = "";
            }


            if ((item.int_Ordine > 0) && (item.int_Ordine < (parseInt(numeroGiocatoriMostrati) + 1))) {
                classeCss = classeCss + " titolare";
                //console.log(" titolare")
            } else if (item.int_Ordine > 0) {
                classeCss = classeCss + " panchina";
                //console.log(" panchina")
            }


            TimeAvvioSchermata = Adesso();

            item.nva_Directory = s.nva_Directory;
            var Str = '';
            ////////////console.log(item);
            Str += '<li     class="formazione giocatore_' + item.ID + ' ' + classeCss + '"    data-fk_int_Giocatore="' + item.ID + '"    >';
            Str += '<div    class="wrapperimage">';
            Str += '<img    class="eventoGiocatore eventoPresentazione " src="/img/icon/ciak.png"/>';
            Str += '<img    class="eventoGiocatore eventoGoal " src="/img/icon/goal.png"/>';
            Str += '<img    class="eventoGiocatore eventoAssist " src="/img/icon/assist.png"/>';
            Str += '<span   class="eventoGiocatore eventoEntra  "><b>ENTRA</b></span>';
            Str += '<span   class="eventoGiocatore eventoEsce  "><b>ESCE</b></span>';
            Str += '<div    class="img">';
            Str += '<img    class="img-responsive giocatore" alt="" src="/assetsSquadre/' + s.nva_Directory + '/Players/' + item.nva_Numero + '.png?' + TimeAvvioSchermata + '" />';
            Str += '<span>' + item.nva_Numero + '</span>'
            Str += '</div>';
            Str += '<div    class="icone-eventi"><i class="fa fa-square  fa-2x icon-ammonizione "></i><i class="fa fa-square  fa-2x icon-espulsione "></i><i class="fa fa-sign-out fa-2x icon-uscito"></i></i><i class="fa fa-sign-in fa-2x icon-entrato"></i></div>';
            Str += '<div    class="nomeGiocatore">';
            Str += item.nva_Cognome + ' ' + item.nva_Nome;
            Str += '<p class="ordineGiocatore">' + Ordine + '</p>';
            Str += '</div>';
            Str += '</div>';
            Str += '</li>';

            ul.append(Str);
            $(".eventoGiocatore").hide();
        });

    });

}

function GeneraStringaGiocatoriPerModificaEventoMatch(ArrGiocatoriObj) {
    console.log("GeneraStringaGiocatoriPerModificaEventoMatch");
    var Stringa = '<table width="100%"><tr><td width="30%"  align="right"><button class="btn btn-lg btn-primary btn-GiocatorePrecedenteSuccessivoModificaEventoMatch Precedente" >Precedente</button></td><td width="40%" align="center">';
    var ArrGiocatori = json2array(ArrGiocatoriObj)
    var ArrOrdinati = OrdinaArrayGiocatoriPernumero(ArrGiocatori);
    $.each(ArrOrdinati, function (i, item) {
        console.log(item.ID);
        Stringa = Stringa + '<span class="bottoneGiocatoreModificaEvento"><img class="img-thumbnail img-GiocatoreModificaEventoMatch" data-incrementalegiocatore="' + i + '" data-idgiocatore="' + item.ID + '" width="120" src="assetsSquadre/' + SettingSquadra[item.fk_int_Squadra].nva_Directory + '/Players/' + item.nva_Numero + '.png"><p><strong>' + item.nva_Numero + '</strong> ' + item.nva_Nome + ' ' + item.nva_Cognome + '</p></span>'
    });
    Stringa = Stringa + '</td><td width="30%"  align="left"><button class="btn btn-lg  btn-primary btn-GiocatorePrecedenteSuccessivoModificaEventoMatch Successivo" >Successivo</button></td></tr></table>';
    return Stringa;
}


function OrdinaArrayGiocatoriPernumero(list) {
    function compare(a, b) {
        a = parseInt(a["nva_Numero"]);
        b = parseInt(b["nva_Numero"]);
        return a - b;
    }
    return list.sort(compare);
}

function SetStato_fk_int_TipoEventoMatch(id, fk_int_Squadra) {

    console.log("864 SetStato_fk_int_TipoEventoMatch");

    $("#trTipoEvento").data("id", id);
    $(".tdTipoEvento").removeClass("td_TipoEventoMatchSelezionato");
    $("[data-ideventomatch=" + id + "]").addClass("td_TipoEventoMatchSelezionato");




    //console.log("879 giocatore: "+$("#trGiocatorePrincipale").data("id"));
    

    //SetStato_fk_int_Giocatore($("#trGiocatorePrincipale").data("id"));
    //SetStato_fk_int_GiocatoreSecondario($("#trGiocatoreSecondario").data("id"));

    $("#trGiocatoreSecondario").hide();

    console.log("886");

    switch (id) {
        case 5:
            $(".legendaGiocatorePrincipale").html("Realizzatore:");
            break;
        case 4:
            $("#trGiocatoreSecondario").show();
            $(".legendaGiocatorePrincipale").html("Entra:");
            break;
        case 3:
            $(".legendaGiocatorePrincipale").html("Espulso:");
            break;
        case 2:
            $(".legendaGiocatorePrincipale").html("Ammonito:");
            break;
        case 1:
            $(".legendaGiocatorePrincipale").html("Realizzatore:");
            break;
    }


}

function GetStato_fk_int_TipoEventoMatch() {

    return $("#trTipoEvento").data("id");
}

function GetStato_fk_int_Periodo() {

    return $("#trPeriodo").data("id");
}

function SetStato_fk_int_Squadra(id) {

    $("#trGiocatorePrincipale").find(".scelte .bottoneSquadrareModificaEvento").hide();
    $("#trGiocatorePrincipale").find("[data-idsquadra=" + id + "]").closest(".bottoneSquadraModificaEvento").show();

}

function SetStato_fk_int_Giocatore(id) {
    console.log("929 SetStato_fk_int_Giocatore:" + id);
  
    $("#trGiocatorePrincipale").data("id", id);
    console.log("932 :" + $("#trGiocatorePrincipale").data("id"));
    
    //$("#trGiocatorePrincipale").find(".scelte .bottoneGiocatoreModificaEvento").hide();
    $("#trGiocatorePrincipale").find(".scelte .bottoneGiocatoreModificaEvento").removeClass("GiocatoreEventoMatchSelezionato")
    $("#trGiocatorePrincipale").find(".scelte .bottoneGiocatoreModificaEvento").addClass("GiocatoreEventoMatchNonSelezionato")
    //  $("#trGiocatorePrincipale").find("[data-idgiocatore=" + id + "]").closest(".bottoneGiocatoreModificaEvento").show();

    if ($("#trGiocatorePrincipale").find("[data-idgiocatore=" + id + "]").length < 1) {
        ////////console.log("nessuna selezione");
        // $("#trGiocatorePrincipale td:first-child").closest(".bottoneGiocatoreModificaEvento").show();
        $("#trGiocatorePrincipale span:first-child").closest(".bottoneGiocatoreModificaEvento").removeClass("GiocatoreEventoMatchNonSelezionato")
        $("#trGiocatorePrincipale span:first-child").closest(".bottoneGiocatoreModificaEvento").addClass("GiocatoreEventoMatchSelezionato")
    } else {
        //$("#trGiocatorePrincipale").find("[data-idgiocatore=" + id + "]").closest(".bottoneGiocatoreModificaEvento").show();
        $("#trGiocatorePrincipale").find("[data-idgiocatore=" + id + "]").closest(".bottoneGiocatoreModificaEvento").removeClass("GiocatoreEventoMatchNonSelezionato")
        $("#trGiocatorePrincipale").find("[data-idgiocatore=" + id + "]").closest(".bottoneGiocatoreModificaEvento").addClass("GiocatoreEventoMatchSelezionato")

    }

  
}

function SetStato_fk_int_Periodo(id) {
    $("#trPeriodo").data("id", id);
    $(".tdPeriodo").removeClass("td_PeriodoSelezionato");
    $("[data-idperiodo=" + id + "]").addClass("td_PeriodoSelezionato");


}

function GetStato_fk_int_Giocatore() {
    return $("#trGiocatorePrincipale").data("id")
}

function SetStato_fk_int_GiocatoreSecondario(id) {
    $("#trGiocatoreSecondario").data("id", id)
    // $("#trGiocatoreSecondario").find(".scelte .bottoneGiocatoreModificaEvento").hide();
    $("#trGiocatoreSecondario").find(".scelte .bottoneGiocatoreModificaEvento").removeClass("GiocatoreEventoMatchSelezionato")
    $("#trGiocatoreSecondario").find(".scelte .bottoneGiocatoreModificaEvento").addClass("GiocatoreEventoMatchNonSelezionato")
    if ($("#trGiocatoreSecondario").find("[data-idgiocatore=" + id + "]").length < 1) {
        //$("#trGiocatoreSecondario td:first-child").closest(".bottoneGiocatoreModificaEvento").show();
        ////////console.log("nessuna selezione");
        $("#trGiocatoreSecondario span:first-child").closest(".bottoneGiocatoreModificaEvento").removeClass("GiocatoreEventoMatchNonSelezionato")
        $("#trGiocatoreSecondario span:first-child").closest(".bottoneGiocatoreModificaEvento").addClass("GiocatoreEventoMatchSelezionato")
    } else {
        // $("#trGiocatoreSecondario").find("[data-idgiocatore=" + id + "]").closest(".bottoneGiocatoreModificaEvento").show();
        $("#trGiocatoreSecondario").find("[data-idgiocatore=" + id + "]").closest(".bottoneGiocatoreModificaEvento").removeClass("GiocatoreEventoMatchNonSelezionato")
        $("#trGiocatoreSecondario").find("[data-idgiocatore=" + id + "]").closest(".bottoneGiocatoreModificaEvento").addClass("GiocatoreEventoMatchSelezionato")
    }
}

function GetStato_fk_int_GiocatoreSecondario() {
    return $("#trGiocatoreSecondario").data("id")
}

function SetStato_int_MinutoPartita(minuto) {
    $("#int_MinutoEventoMatch").html(minuto + "° minuto");
}

function GetStato_int_MinutoPartita() {
    return parseInt($("#int_MinutoEventoMatch").html().replace("° minuto", ""));
}

function PopolaListaGiocatoriPerEventiMatch(fk_int_Squadra) {
    ////////console.log("fk_int_Squadra");
    ////////console.log(fk_int_Squadra)

    var ArrGiocatoriObj = RosaSquadra[fk_int_Squadra]
    ////////console.log("ArrGiocatoriObj");
    ////////console.log(ArrGiocatoriObj)

    $("#trGiocatorePrincipale").find(".scelte").html(GeneraStringaGiocatoriPerModificaEventoMatch(ArrGiocatoriObj));
    $("#trGiocatoreSecondario").find(".scelte").html(GeneraStringaGiocatoriPerModificaEventoMatch(ArrGiocatoriObj));
}

function GetSquadraAvversaria(fk_int_Squadra) {
    ////////console.log("---------SettingMatch---------------------------");
    ////////console.log(SettingMatch);
    ////////console.log(SettingMatch["fk_int_SquadraCasa"]);
    if (SettingMatch["fk_int_SquadraCasa"] == fk_int_Squadra) {

        return SettingMatch["fk_int_SquadraOspite"];
    } else {
        return SettingMatch["fk_int_SquadraCasa"];
    }

}

function SortByClickOrder(a, b) {
    var a = parseInt($(a).find(".ordineGiocatore").text());
    var b = parseInt($(b).find(".ordineGiocatore").text());
    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
}

function EvidenziaElemento(Elemento) {
    var _Formazione = []
    $("#ModalFormazione").find('.non-convocati').each(function (i, td) {
        $(td).closest("li").find(".ordineGiocatore").text("");
    });
    if (Elemento.hasClass("convocati")) {
        Elemento.find(".ordineGiocatore").text(1000);
    }
    let r = 1;

    $("#ModalFormazione").find('.convocati').sort(SortByClickOrder).each(function (i, td) {
        var Titolare = {}
        Titolare.ID = 0
        Titolare.int_Ordine = r;
        Titolare.fk_int_Giocatore = $(td).data("fk_int_giocatore");
        Titolare.fk_int_Match = $("#ModalFormazione").closest(".modal").data("fk_int_match");
      
        _Formazione.push(Titolare);
        $(td).find(".ordineGiocatore").text(r);
        r = r + 1;
    });
    return _Formazione
}

function EvidenziaElementoSortable(Elemento) {
    var _Formazione = []
    $("#ModalFormazione").find('.non-convocati').each(function (i, td) {
        $(td).closest("li").find(".ordineGiocatore").text("");
    });
    if (Elemento.hasClass("convocati")) {
        Elemento.find(".ordineGiocatore").text(1000);
    }
    let r = 1;

    $("#ModalFormazione").find('.convocati').each(function (i, td) {
        var Titolare = {}
        Titolare.ID = 0
        Titolare.int_Ordine = r;
        Titolare.fk_int_Giocatore = $(td).data("fk_int_giocatore");
        Titolare.fk_int_Match = $("#ModalFormazione").closest(".modal").data("fk_int_match");

        _Formazione.push(Titolare);
        $(td).find(".ordineGiocatore").text(r);
        r = r + 1;
    });
    return _Formazione
}






function GetFormazione() {
    var _Formazione = [];
    let r = 1;
    $("#ModalFormazione").find('li').each(function (i, td) {
        var Titolare = {}
        Titolare.ID = 0
        if ($(td).hasClass("convocati")) {
            Titolare.int_Ordine = $(td).find(".ordineGiocatore").text();
        } else {
            Titolare.int_Ordine = 0;
        }
        Titolare.fk_int_Giocatore = $(td).data("fk_int_giocatore");
        Titolare.fk_int_Match = $("#ModalFormazione").closest(".modal").data("fk_int_match");
        _Formazione.push(Titolare);
        r = r + 1;
    });
    // //////////console.log(_Formazione);
    return _Formazione
}





function AttribuisciProgressivoTitolari(Elemento) {
    var _Formazione = []
    var q = 0
    var r = 0;
    let last = 0;
    Elemento.find('.ordineGiocatore').each(function (i, td) {
        if ($(td).text() > last) {
            last = $(td).text();

        }
    })


    IncrementaleGiocatore = last + 1;

    Elemento.find('.ordineGiocatore').each(function (i, td) {
        var Titolare = {}
        Titolare.ID = 0

        Titolare.int_Ordine = (($(td).text()=='') ? 0 : parseInt($(td).text())) ;

        Titolare.fk_int_Giocatore = $(td).closest("li").data("fk_int_giocatore");
        Titolare.fk_int_Match = Elemento.closest(".modal").data("fk_int_match");
        _Formazione.push(Titolare);
        console.log("AttribuisciProgressivoTitolari");
        console.log(Titolare);
    })

    return _Formazione
}







function AttribuisciProgressivoTitolariSortable(Elemento) {
    var _Formazione = []
    var q = 0
    var r = 0;
    Elemento.find('.ordineGiocatore').each(function (i, td) {
        if ($(td).closest("li").hasClass("bg-danger")) {
            q = q + 1;
            $(td).text(q);
            r = q;
        } else {
            $(td).text("");
            r = 0;
        }


        var Titolare = {}
        Titolare.ID = 0
        Titolare.int_Ordine = r
        Titolare.fk_int_Giocatore = $(td).closest("li").data("fk_int_giocatore");
        Titolare.fk_int_Match = Elemento.closest(".modal").data("fk_int_match");
        
        _Formazione.push(Titolare);
        console.log("AttribuisciProgressivoTitolariSortable");
        console.log(Titolare);
    })
    console.log(_Formazione);
    return _Formazione
}

function ArchiviaEventoSuDB(Evt) {
    //////////console.log(" ArchiviaEventoSuDB su DB");
    //////////console.log(int_MinutoPartita)
    Evt.int_MinutoPartita = int_MinutoPartita;
    $.ajax({
        url: "/Ajax/ArchiviaEvento",
        data: Evt,
        type: 'POST'

    }).done(function (data) {
        //console.log(data);

        PopolaEventiMatch();

    });

}

function AvviaAnimazioneEvento(evt) {
    //////////console.log("AvviaAnimazioneEvento");
    //////////console.log(evt);
    //////////console.log("------------------------------------------");
    //var date = new Date();
    //let adesso = date.getTime();
    let adesso = Adesso();
    let durata = 10000;

    
    if (evt.fk_int_TipoEventoMatch == 4) durata = 15000;
    $.ajax({
        url: "/Ajax/sp_GestisciRiproduzioneMaxischermo",
        data: "int_MillisecondiDaRecuperareSuHome=" + durata + "&int_DataOraJavascript=" + adesso
    }).done(function (data) {});
   


    let Comando = CostruisciMessaggio("Anima(" + JSON.stringify(evt) + ");");
    SetDatiToPlayer(Comando);


}





function CaricaScene() {
    $.ajax({
        url: "/AcquisizioneSceneLed/tb_sceneLed?fk_int_Periodo=2"
    }).done(function (data) {
        data = JSON.parse(data);
        var ArrSunto = Array;
        $.each(data, function (i, scena) {
            $.ajax({
                url: "/AcquisizioneSceneLed/SuntoContenutiScena?fk_int_Scena=" + scena.ID
                , async: false
            }).done(function (data_contenuti) {
                var s = "";
                data_contenuti = JSON.parse(data_contenuti);
                $.each(data_contenuti, function (ii, contenuto) {
                    s = s + " " + contenuto.nva_NomeFile
                });
                $("#ListaScene").append('<li class="ui-state-default" id="ui_scena_' + scena.ID + '"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><b>' + scena.ID + ' - ' + scena.int_Durata + ' msec</b>' + s + '</li>');
            });
        });
    });
}

function AggiornaPersistenceSetting() {
    setCookie("persistence-setting", JSON.stringify(settings))
}

function setCookie(key, value) {
    var act = getCookie(key);
    if (act) {
        value = act + value;
    }
    //  ////////////console.log(btoa(value));
    document.cookie = "cookie-msg-" + key + "=" + btoa(value) + "; path=/";
}

function cleanCookie(key) {

    document.cookie = "cookie-msg-" + key + "=; path=/";
}

function getCookie(key) {
    var cname = "cookie-msg-" + key + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(cname) == 0) {

            return atob(c.substring(cname.length, c.length));
        }
    }
    return null;
}

function MessaggioPopUp(messaggio) {

    alert(messaggio);

}

function ControlloSwitchSelezionePc(id) {

    $(".btn_SelezionePC").removeClass("btn_SelezionePCSelezionato");
    $(".btn_SelezionePC").addClass("disabled");
    $.ajax({
        url: "/Ajax/InviaSelezionePC?soggetto=" + id
    }).done(function (data) {
        ////////console.log(data)

        $(".btn_SelezionePC").attr("disabled", false);
        $(".btn_SelezionePC").removeClass("disabled");
        $.each(ArrSelezionePC, function (index, contenuto) {
            $("#" + contenuto.replace("pX", "p1")).removeClass("btn_SelezionePCSelezionato");
            $("#" + contenuto.replace("pX", "p0")).removeClass("btn_SelezionePCSelezionato");
            $("#" + contenuto.replace("pX", "p2")).removeClass("btn_SelezionePCSelezionato");

            if (data.includes(contenuto.replace("pX", "p1") + ":0") && data.includes(contenuto.replace("pX", "p2") + ":0")) {
                $("#" + contenuto.replace("pX", "p0")).addClass("btn_SelezionePCSelezionato");
            } else if (data.includes(contenuto.replace("pX", "p1") + ":1")) {
                $("#" + contenuto.replace("pX", "p1")).addClass("btn_SelezionePCSelezionato");
            } else if (data.includes(contenuto.replace("pX", "p2") + ":1")) {
                $("#" + contenuto.replace("pX", "p2")).addClass("btn_SelezionePCSelezionato");
            }

        });

    }).fail(function (data) {



    });


}

function openFullscreen() {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }
}

function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    }
}



function SettaRitardoClient() {
    var date = new Date();
    let adesso = date.getTime();

    $.ajax({
        url: "/Ajax/sp_SettaRitardoClient",
        data: { 'now': adesso },
    }).done(function (data) {
        console.log('ritardo client: ' + data + ' millisec');
       // console.log(data);
        ritardoTraClientEServer = data;
        //ritardoTraClientEServer = 0;
    })
}



function PopolaFormCrudGiocatore(id) {


    //console.log("chiamo PopolaFormCrudGiocatore con id = " + id)
    $.ajax({
        url: "Crud/GetGiocatore",
        type: "get",
        data: { 'id': id },
        success: function (data) {
            data = JSON.parse(data)[0];
            Object.keys(data).forEach(function (k) {
                //console.log(k + ' - ' + data[k]);
                $("#form_giocatori_" + k).val(data[k]);
            });
           // var date = new Date();
           // let adesso = date.getTime();

            let adesso = Adesso();
            $("#PreviewImmagineGiocatore").attr("src", "\assetsSquadre\\" + data["nva_Directory"] + "\\Players\\" + data["nva_Numero"] + ".png?" + adesso);
            $(".btn-eliminaGiocatore").show();
        }
    });


}
