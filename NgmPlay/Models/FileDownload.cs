﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class FileDownload
    {
        public FileDownload() { }

        public String Contatore { get; set; }
        public String NomeFile { get; set; }
        public String DataFile { get; set; }

    }
}
