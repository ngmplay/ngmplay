﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_Match : NgmPlay.Utility.ExtendModel
    {

        public tb_Match() { }


        public tb_Match(int ID = 0)
        {
            if (ID > 0)
                this.Popola(ID);
        }




        public int IsSelected { get; set; }
        public DateTime dtt_DataOra { get; set; }
        public int Id { get; set; }
        public int fk_int_Stadio { get; set; }
        public string nva_Titolo { get;set; }
        public int fk_int_SquadraCasa { get; set; }
        public int fk_int_SquadraOspite { get; set; }
        public int tin_TempiSupplementari { get; set; }



        public tb_Stadi Stadio { 
            get
            {
                if (fk_int_Stadio > 0)return new tb_Stadi(fk_int_Stadio);
                
                return null;
            }
        }
        public tb_Squadre SquadraCasa
        {
            get
            {
                if (fk_int_SquadraCasa > 0) return new tb_Squadre(fk_int_SquadraCasa);
                return null;
            }
        }
        public tb_Squadre SquadraOspite {
            get
            {
                if (fk_int_SquadraOspite > 0) return new tb_Squadre(fk_int_SquadraOspite);
                return null;
            }
        }


    }
}
