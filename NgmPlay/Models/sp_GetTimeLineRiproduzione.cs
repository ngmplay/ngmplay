﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class sp_GetTimeLineRiproduzione
    {


        public int ID { get; set; } = 0;
        //public int int_Progressivo { get; set; } = 0;
        public int int_NextId { get; set; } = 0;
        //public int int_NextProgressivo { get; set; } = 0;
        public int fk_int_Riproduzione { get; set; } = 0;
        public int int_PrimaScena { get; set; } = 0;
        public int fk_int_Scena { get; set; } = 0;
        public int int_IniziaDa { get; set; } = 0;
        public int int_StopA { get; set; } = 0;
        public int fk_int_Periodo { get; set; } = 0;
        public int fk_int_TipoScena { get; set; } = 0;
        public int int_Velocità { get; set; } = 0;
        public Int64 int_DataOraInizio { get; set; } = 0;
        public Int64 int_DataOraFine { get; set; } = 0;
        public int int_Durata { get; set; } = 0;
        /*
			(ROW_NUMBER() OVER(ORDER BY  per.int_Ordine, s.int_ordine ASC) -1) AS int_Progressivo
			,s.ID 
			,s.int_durata
			,s.int_iniziaDa
			,s.int_stopA
			,s.fk_int_Periodo
			,per.nva_Codice as nva_CodicePeriodo
			,per.tin_isLoop
			,isnull(ts.ID ,0) fk_int_TipoScena
			,NULL as int_NextID
			,NULL as int_NextProgressivo
         */

    }
}

