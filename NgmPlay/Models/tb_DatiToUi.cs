﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_DatiToUi
    {
 

        public int ID { get; set; }
        public String nva_Dato { get; set; }
        public DateTime? dtt_Scrittura { get; set; } = DateTime.Now;
        public DateTime? dtt_Lettura { get; set; } = null;



    }
}
