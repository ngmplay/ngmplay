﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_Squadre : NgmPlay.Utility.ExtendModel
    {

        public tb_Squadre(){}

        public tb_Squadre(int ID = 0)
        {
            if (ID > 0)
                this.Popola(ID);
        }


        public int ID { get; set; }

        public string nva_ColoreTestoAnimazione { get; set; }

        public int fk_int_StadioDefault { get; set; }
        public string nva_Directory { get; set; }
        public string nva_Nome { get; set; }




    }
}
