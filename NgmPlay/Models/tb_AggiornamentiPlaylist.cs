﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_AggiornamentiPlaylist : NgmPlay.Utility.ExtendModel
    {


        public int ID { get; set; }
        public int fk_int_Match { get; set; }
        public DateTime dtt_DataOraUltimoAggiornamento { get; set; }
        public int tin_Consistenza { get; set; }


    }
}
