﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_Scena_Extended : tb_Scena
    {
 
        public int Id_int_Prev
        {

            get {
                DataTable dt = DB.GetDataFromSQL("select top(1) ID from tb_scene where  fk_int_Periodo= " + fk_int_Periodo  + " and fk_int_Stadio = " + fk_int_Stadio + " and int_ordine < "+ int_Ordine + " order by int_ordine desc");
                if(dt.Rows.Count > 0)
                {
                    return Convert.ToInt32 (dt.Rows[0][0]);
                }
                else
                {
                    return 0;

                }

            }


        }


        public int Id_int_Next
        {

            get
            {
                DataTable dt = DB.GetDataFromSQL("select top(1) ID from tb_scene where  fk_int_Periodo= " + fk_int_Periodo + " and fk_int_Stadio = " + fk_int_Stadio + " and int_ordine > " + int_Ordine + " order by int_ordine");
                if (dt.Rows.Count > 0)
                {
                    return Convert.ToInt32(dt.Rows[0][0]);

                }
                else
                {
                    return 0;

                }

            }


        }


    }
}
