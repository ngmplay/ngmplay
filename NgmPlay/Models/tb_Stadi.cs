﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_Stadi : NgmPlay.Utility.ExtendModel
    {

        public tb_Stadi(int ID = 0)
        {
            if (ID > 0)
                this.Popola(ID);
        }

        public int ID {get;set; }
        public int tin_ConfigurazioneOK { get; set; }
        public string nva_Nome { get; set; }
        public string nva_Città { get; set; }
        public string nva_Squadra { get; set; }
        public string nva_Capienza { get; set; }
        public string nva_NomeDirectory { get; set; }
        public int int_X {get;set; }
        public int int_Y {get;set; }
        public int int_W {get;set; }
        public int int_H {get;set; }
        public int int_XMaxischermo {get;set; }
        public int int_YMaxischermo {get;set; }
        public int int_WMaxischermo {get;set; }
        public int int_HMaxischermo {get;set; }
        public string nva_SendingCardMaxischermo { get; set; } 
        public string nva_URLScaricoDati { get; set; } 

    }
}
