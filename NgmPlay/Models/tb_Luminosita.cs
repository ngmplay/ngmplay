﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_Luminosita : NgmPlay.Utility.ExtendModel
    {
        public tb_Luminosita() { }

        public tb_Luminosita(int ID = 0)
        {
            if (ID > 0) 
                this.Popola(ID);
        }

        public int Id { get; set; }
       // public int fk_int_DisposizioneLed { get; set; }
        public String nva_Settore { get; set; }

        public int int_Valore { get; set; }
        public int int_OffsetPercentualeVariazioneGlobale { get; set; }



    }
}
