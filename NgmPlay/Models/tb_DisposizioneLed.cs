﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_DisposizioneLed : NgmPlay.Utility.ExtendModel
    {


        public tb_DisposizioneLed() {}

        public tb_DisposizioneLed(int ID = 0)
        {
            if (ID > 0)
                this.Popola(ID);
        }

        public int Id { get; set; }
        public int fk_int_Stadio { get; set; }
      //  public int fk_int_TipoElemento { get; set; }
        public int fk_int_Layer { get; set; }
        public string nva_Nome { get; set; }
        public int int_Ordine { get; set; }
        public int int_W { get; set; }
        public int int_H { get; set; }
        public int int_incrW { get; set; }
        public int int_MarginL { get; set; }
        public int int_MarginT { get; set; }
    //    public string nva_GruppoLuminosita { get; set; }

        public tb_Stadi tb_Stadi
        {
            get
            {
                return new tb_Stadi(this.fk_int_Stadio);
            }
        }

        /*versione con miglioramento delle prestazioni*/
        //public tb_TipoElemento tb_TipoElemento
        //{
        //    get
        //    {
        //        return new tb_TipoElemento(this.fk_int_TipoElemento);
        //    }
        //}





    }
}
