﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class scene_riprodotte_compresse_maxischermo : NgmPlay.Utility.ExtendModel
    {


        //void scene_riprodotte_compresse()
        //{

        //}

        //{"id":11424,"sid":2100,"msecd":12000,"msdi":1562064358705,"msdf":1562064370705,"start":0,"stop":0,"pro":0,"iper":3,"nid":11425,"npr":102,"psc":0}

        public int id { get; set; } = 0;
        public int sid { get; set; } = 0;
        public int rid { get; set; } = 0;
        public int msecd { get; set; } = 0;
        public Int64 msdi { get; set; } = 0;
        public Int64 msdf { get; set; } = 0;
        public int start { get; set; } = 0;
        public int stop { get; set; } = 0;
        //public int pro { get; set; } = 0;
        public int iper { get; set; } = 0;
        public int nid { get; set; } = 0;
        //public int npr { get; set; } = 0;
        public string est { get; set; } = "";
        //public int usc { get; set; } = 0;
        public int tsc { get; set; } = 0;
        public int vel { get; set; } = 0;

        public string pf { get; set; } = "";
    }
}
