﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NgmStudio.Model
{
    public class Score : INotifyPropertyChanged
    {
        public string IdMatch { get; set; }
        
        private string _date;
        public string Date {
            get { return _date; }
            set
            {
                if (value != _date)
                {
                    _date = value;
                    OnPropertyChanged("Date");
                }
            }
        }

        private string _status;
        public string Status {
            get { return _status; }
            set
            {
                if (value != _status)
                {
                    _status = value;
                    OnPropertyChanged("Status");
                }
            }
        }

        private string _time;
        public string Time
        {
            get { return _time; }
            set
            {
                if (value != _time)
                {
                    _time = value;
                    OnPropertyChanged("Time");
                }
            }
        }

        public string IdTeamHome { get; set; }
        public string IdTeamVisitor { get; set; }

        private string _nameTeamHome;
        public string NameTeamHome
        {
            get { return _nameTeamHome; }
            set
            {
                if (value != _nameTeamHome)
                {
                    _nameTeamHome = value;
                    OnPropertyChanged("NameTeamHome");
                }
            }
        }

        private string _nameTeamVisitor;
        public string NameTeamVisitor
        {
            get { return _nameTeamVisitor; }
            set
            {
                if (value != _nameTeamVisitor)
                {
                    _nameTeamVisitor = value;
                    OnPropertyChanged("NameTeamVisitor");
                }
            }
        }

        private string _scoreTeamHome;
        public string ScoreTeamHome
        {
            get { return _scoreTeamHome; }
            set
            {
                if (value != _scoreTeamHome)
                {
                    _scoreTeamHome = value;
                    OnPropertyChanged("ScoreTeamHome");
                }
            }
        }

        private string _scoreTeamVisitor;
        public string ScoreTeamVisitor
        {
            get { return _scoreTeamVisitor; }
            set
            {
                if (value != _scoreTeamVisitor)
                {
                    _scoreTeamVisitor = value;
                    OnPropertyChanged("ScoreTeamVisitor");
                }
            }
        }

        #region INotifyPropertyChanged implementation
        // Basically, the UI thread subscribes to this event and update the binding if the received Property Name correspond to the Binding Path element
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
