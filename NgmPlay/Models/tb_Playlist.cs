﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_Playlist : NgmPlay.Utility.ExtendModel
    {


       public tb_Playlist( int ID = 0)
        {
            if(ID>0)
             this.Popola(ID);
        }
        public int IsSelected { get; set; }
        public int tin_ElaboraScene { get; set; }
        public int ID { get; set; }
        public int fk_int_Stadio { get; set; }
        public int fk_int_ProvenienzaPlaylist { get; set; }
        public string nva_NomePlaylist { get; set; }
        public string nva_TableNamePlaylist { get; set; }

        public string nva_TableNameEventi { get; set; }
        public DateTime dtt_DataCaricamento { get; set; }
        public int tin_Consistenza { get; set; }



    //  public string Tabella { get; } = "tb_DisposizioneLed";

    }
}
