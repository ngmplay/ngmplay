﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_SceneRiproduzioniLed  : NgmPlay.Utility.ExtendModel
    {

        public int ID {get;set; } = 0;
        //public int int_Progressivo { get; set; } = 0;
        public int int_NextID { get; set; } = 0;
        //public int int_NextProgressivo { get; set; } = 0;
        public int fk_int_Riproduzione { get; set; } = 0;
        public string chr_Estremo { get; set; } = "";
        //public int int_UltimaScena { get; set; } = 0;
        public int fk_int_Scena { get; set; } = 0;
        public int int_IniziaDa { get; set; } = 0;
        public int int_StopA { get; set; } = 0;
        public int fk_int_Periodo { get; set; } = 0;
        public int fk_int_SceneEventiSpeciali { get; set; } = 0;
        public int int_Velocità { get; set; } = 0;
        public Int64 int_DataOraInizio { get; set; } = 0;
        public Int64 int_DataOraFine { get; set; } = 0;
        public int int_Durata { get; set; } = 0;


    }
}

