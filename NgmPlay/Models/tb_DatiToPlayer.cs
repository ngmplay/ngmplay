﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_DatiToPlayer
    {
        private DateTime? _dtt_Scrittura = null;

        public int ID { get; set; }

        public String nva_Dato { get; set; }

        public DateTime? dtt_Scrittura { get; set; } = DateTime.Now;

        public DateTime? dtt_LetturaPC1 { get; set; } = null;
        public DateTime? dtt_LetturaPC2 { get; set; } = null;
    }
}
