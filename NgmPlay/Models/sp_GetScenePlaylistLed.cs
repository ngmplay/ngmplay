﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class sp_GetScenePlaylistLed
    {



        public int int_Progressivo { get; set; }
        public int ID {get;set; }
        public int int_Durata {get;set; }
        public int int_IniziaDa { get;set; }
        public int int_StopA { get;set; }
        public int fk_int_SceneEventiSpeciali { get; set; }


    }
}

