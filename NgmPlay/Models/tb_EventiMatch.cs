﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_EventiMatch : NgmPlay.Utility.ExtendModel
    {

        public tb_EventiMatch(){}

        public tb_EventiMatch(int ID = 0)
        {
            if (ID > 0)
                this.Popola(ID);
        }

        public int ID { get; set; } = 0;
        public int fk_int_Match { get; set; }
        public int fk_int_Periodo { get; set; }
        public int fk_int_Riproduzione { get; set; }
        public int fk_int_Giocatore { get; set; }
        public int fk_int_GiocatoreSecondario { get; set; }
        public int fk_int_Squadra { get; set; }
        public int fk_int_TipoEventoMatch { get; set; }
        public int int_MinutoPartita { get; set; }
        public int tin_Maxischermo { get; set; }
        public int tin_Led { get; set; }
        public String nva_Note { get; set; }
        public String nva_TipoEsecuzione { get; set; }

    }
}
