﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_Periodi
    {

        public int ID {get;set; }

        public int int_NextID { get; set; }
        public string nva_Nome { get; set; }
        public string nva_Codice { get; set; }
        public int int_Ordine { get; set; }
        public int tin_isLoop { get; set; }
        public int tin_PermettiUnload { get; set; }
        // public string Tabella { get; set;         public string nva_Nome { get; set; }} = "tb_Scene";

    }
}
