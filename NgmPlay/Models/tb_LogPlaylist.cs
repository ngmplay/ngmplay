﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_LogPlaylist : NgmPlay.Utility.ExtendModel
    {

        public tb_LogPlaylist()
        {

        }

        public tb_LogPlaylist(int ID = 0)
        {
            if (ID > 0)
                this.Popola(ID);
        }

        public tb_LogPlaylist(int fk_int_Playlist, String nva_Log)
        {
            this.fk_int_Playlist = fk_int_Playlist;
            this.nva_Log = nva_Log;
            DB_InsertInto();
        }

        public int ID { get; set; }
        public int fk_int_Playlist { get; set; }
        //public DateTime? dtt_Inizio { get; set; }
        //public DateTime? dtt_Fine { get; set; }
        public string nva_Log { get; set; }



    }
}
