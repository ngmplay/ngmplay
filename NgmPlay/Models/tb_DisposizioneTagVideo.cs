﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_DisposizioneTagVideo : NgmPlay.Utility.ExtendModel
    {
        public tb_DisposizioneTagVideo(){}

        public tb_DisposizioneTagVideo(int ID = 0)
        {
            if (ID > 0)
                this.Popola(ID);
            
        }

        public  int Id { get; set; }
        public int fk_int_DisposizioneLed { get; set; }
        public int int_W { get; set; }
        public int int_H { get; set; }
        public int int_MarginL  { get; set; }
        public int int_MarginT { get; set; }

    //    string Tabella = "tb_DisposizioneTagVideo";

        public tb_DisposizioneLed tb_DisposizioneLed
        {
            get{
               return new tb_DisposizioneLed(this.fk_int_DisposizioneLed);
            }
        }    



    }
}
