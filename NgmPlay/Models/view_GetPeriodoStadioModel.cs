﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class view_GetPeriodoStadioModel
    {


        public int ID { get; set; }
        public string nva_NomePeriodo { get; set; }
        public string nva_NomeStadio { get; set; }
        public int fk_int_Stadio { get; set; }
        public int fk_int_Periodo { get; set; }


    // public string Tabella { get; set; } = "tb_Scene";

}
}
