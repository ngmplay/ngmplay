﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenderMachine.Models
{
    public class tb_ImportFileConfigurator : NgmPlay.Utility.ExtendModel
    {


        public int ID { get; set; }
        public int fk_int_Stadio { get; set; }
        public string nva_Contenuto { get; set; }
        public int tin_Valido { get; set; }
        public DateTime dtt_Caricamento { get; set; } = DateTime.Now;

    }
}
