﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NgmPlay.Utility
{
    public class ValueOperations
    {
        public static int DbToInteger(object o)
        {
            int ret = 0;
            if (o != null && o != DBNull.Value)
                ret = Convert.ToInt32(o);
            return ret;
        }

        public static long DbToLong(object o)
        {
            long ret = 0;
            if (o != null && o != DBNull.Value)
                ret = Convert.ToInt64(o);
            return ret;
        }
    }
}
