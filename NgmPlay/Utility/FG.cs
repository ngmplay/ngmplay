﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NgmPlay.Utility
{
    public static class fg
    {

        public static string PulisciStringa(string Stringa)
        {

            Stringa = Stringa.Replace("  ", " ").Replace("  ", " ").Replace("  ", " ").ToLower().Trim().Replace(" ","_");
            Regex rgx = new Regex("[^a-z0-9_]");
            Stringa = rgx.Replace(Stringa, "");
            return Stringa;
        }


        public static Boolean IsYes(string Stringa)
        {
            Stringa = Stringa.ToLower();
            if (Stringa == "si") return true;
            if (Stringa == "yes") return true;
            if (Stringa == "ok") return true;
            if (Stringa == "1") return true;
            return false;
        }
        public static Boolean IsYes(int i)
        {
            if (i > 0) return true;
            return false;
        }

        public static void DeleteDirectory(string target_dir)
        {
            string[] files = Directory.GetFiles(target_dir);
            string[] dirs = Directory.GetDirectories(target_dir);

            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            foreach (string dir in dirs)
            {
                DeleteDirectory(dir);
            }

            Directory.Delete(target_dir, false);
        }


        public static Boolean Exist(string url)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "HEAD";

            bool exists;
            try
            {
                request.GetResponse();
                exists = true;
            }
            catch
            {
                exists = false;
            }

            return exists;
        }

        public static Boolean CopyImageFromWeb(string url, string LocalFile)
        {
            try
            {
                byte[] data;
                using (WebClient client = new WebClient())
                {
                    data = client.DownloadData(url);
                }
                if (data != null)
                {
                    try { File.Delete(LocalFile); } catch (Exception e) { }
                    File.WriteAllBytes(LocalFile, data);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }


        }





    }
}
