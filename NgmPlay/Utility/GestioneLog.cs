﻿using RenderMachine.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NgmPlay.Utility
{
    public static class GestioneLog
    {

        public static void ArchiviaMessaggioLog(string Stringa,string Titolo)
        {
            tb_Log l = new tb_Log();
            l.tin_Tipo = 0;
            l.nva_titolo = Titolo;
            l.nva_messaggio = Stringa;

            DB.DB_InsertInto(l);


        }

        public static void ArchiviaErroreLog(string Stringa, string Titolo)
        {
            tb_Log l = new tb_Log();
            l.tin_Tipo = 1;
            l.nva_titolo = Titolo;
            l.nva_messaggio = "EXE - "+Stringa;

            DB.DB_InsertInto(l);


        }




    }
}
