﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using NgmPlay.Utility;

static class DB 
{
     //const string  CsDefault  = @"Data Source=192.168.50.243\SQLEXPRESS;Initial Catalog=NgmStudioDevelop;Persist Security Info=True;User ID=sa; Password=SqlServer2016";
    //const string  CsDefault  = @"Data Source=ngm-010\SQLEXPRESS;Initial Catalog=NgmStudioDevelop;Persist Security Info=True;User ID=sa; Password=SqlServer2016";
   const string  CsDefault  = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=NgmStudioDevelop;Persist Security Info=True;User ID=sa; Password=dbcampo";

    public static List<string> CampiNonControllati { get; } = new List<string>(new string[] { "dtt_Cancellazione", "dtt_Modifica", "nva_UserId" });


    public static Boolean TableExists(String nometabella,String cs = CsDefault)
    {
        try
        {
            using (SqlConnection cn = new SqlConnection(cs))
            using (SqlCommand cmd = new SqlCommand("select top(1) from "+ nometabella, cn))
            {
            
            }
            return true;
        }
        catch (Exception ex)
        {

            return false;

        }
    }



    private static object ConvertiDBNullInNothing(object Valore)
    {
        if (Valore == DBNull.Value)
            return null;
        else
            return Valore;
    }

    private static object ConvertiNothinginDBnull(object Valore)
    {
        if (Valore == null)
            return DBNull.Value;
        else if (Valore.GetType() == typeof(DateTime) | Valore.GetType() == typeof(DateTime))
        {
            if ((DateTime)Valore == DateTime.MinValue)
                return DBNull.Value;
        }

        return Valore;
    }


    private static string ConvertiInStringaRobusto(object Valore)
    {
        if (Valore == null)
        {
            Valore = "";
        }
        else if (Valore.GetType() == typeof(DateTime))
        {
            if ((DateTime)Valore == DateTime.MinValue)
                Valore = "";
        }
        try
        {
            return Valore.ToString();
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private static object PopolaOggetto(object Oggetto, DataRow Dr)
    { 
        List<System.Reflection.PropertyInfo> ListaPropertys = new List<System.Reflection.PropertyInfo>();
        ListaPropertys.AddRange(Oggetto.GetType().GetProperties());
        foreach (DataColumn c in Dr.Table.Columns)
        {
            if (ListaPropertys.Exists(x => x.Name.ToLower() == c.ColumnName.ToLower()))
            {
                SetProprietàOggetto(Oggetto, ListaPropertys.FirstOrDefault(x => x.Name.ToLower() == c.ColumnName.ToLower()).Name , ConvertiDBNullInNothing(Dr[c.ColumnName]));

            } else if (!CampiNonControllati.Contains(c.ColumnName, StringComparer.CurrentCultureIgnoreCase))
            {
                System.Diagnostics.Debug.WriteLine("Attenzione, l'oggetto tipo '" + (Oggetto.GetType()).ToString() + "' non possiede il parametro '" + c.ColumnName + "' pertanto non sarà valorizzato.");
            }      
        }
        return Oggetto;
    }



    public static List<object> PopolaListaOggetti(Type oggetto, String Query)
    {
        return PopolaListaOggetti(oggetto,GetDataFromSQL(Query));
    }




    public static List<object> PopolaListaOggetti(Type oggetto, DataTable Tabella)
    {


        List<object> l= new List<object>();



        foreach (DataRow r in Tabella.Rows)
        {
            object istanza = Activator.CreateInstance(oggetto);
            
            l.Add(PopolaOggetto(istanza, r));
        }


        return l;
        
    }



    public static string GetPaginatedSQL(int startRow, int numberOfRows, string sql, string orderingClause)
    {
        if (String.IsNullOrEmpty(orderingClause))
            throw new ArgumentNullException("orderingClause");


        if (numberOfRows <= 0)
        {
            return String.Format("{0} {1}", sql, orderingClause);
        }
        String partialSQL = sql.Remove(0, "SELECT ".Length);

        return String.Format(
            "SELECT * FROM ( SELECT ROW_NUMBER() OVER ({0}) AS rn, {1} ) AS SUB WHERE rn > {2} AND rn <= {3}",
            orderingClause,
            partialSQL,
            startRow.ToString(),
            (startRow + numberOfRows).ToString()
        );
    }



    public static object PopolaSingoloOggetto(object oggetto, DataTable Tabella)
    {
        if (Tabella.Rows.Count == 0)
            System.Diagnostics.Debug.WriteLine("Attenzione, il record con il quale si tenta di popolare l'oggetto tipo '" + (oggetto.GetType()).ToString() + "' è vuoto");
        else if (Tabella.Rows.Count > 1)
            System.Diagnostics.Debug.WriteLine("Attenzione, ci sono molteplici recordi con cui si tenta di popolare l'oggetto tipo '" + (oggetto.GetType()).ToString() + "'. Verifica la creazione della tabella");
        else
            return PopolaOggetto(oggetto, Tabella.Rows[0]);
        return null;
    }

    public static List<System.Reflection.PropertyInfo> GetProprietàDaOggetto(object Oggetto)
    {
        List<System.Reflection.PropertyInfo> ListaPropertys = new List<System.Reflection.PropertyInfo>();
        ListaPropertys.AddRange(Oggetto.GetType().GetProperties());
        return ListaPropertys;
    }

    public static int GetFirstFreeID(String NomeTabella)
    {
       DataTable dt= GetDataFromSQL("select max(id)+1 from "+ NomeTabella);
        if (dt.Rows.Count > 0) return Convert.ToInt32(dt.Rows[0][0]);
        return 1;

    }

    public static int EseguiSQL(string SQL, string cs = CsDefault)
    {
        int ret = 0;
        try
        {
            using (SqlConnection cn = new SqlConnection(cs))
            {
                using (SqlCommand cmd = new SqlCommand(SQL, cn))
                {
                    cmd.CommandTimeout = 0;
                    cn.Open();
                    ret = cmd.ExecuteNonQuery();
                }
            }            
        }
        catch (Exception ex)
        {
            GestioneLog.ArchiviaErroreLog("Exception SQL:(" + SQL + ") metodo classe DB - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
        }
        return ret;
    }


    public static void InserisciDataTableNelDB(DataTable dt , string NomeTabella ,string cs = CsDefault)
    {
        using (var bulkCopy = new SqlBulkCopy(cs, SqlBulkCopyOptions.KeepIdentity))
        {
           foreach (DataColumn col in dt.Columns)
            {
                bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
            }

            bulkCopy.BulkCopyTimeout = 600;
            bulkCopy.DestinationTableName = NomeTabella;
            bulkCopy.WriteToServer(dt);
        }

    }


    public static DataSet GetDataSetFromSQL(string Sql, string cs= CsDefault )
    {

        DataSet ds = new DataSet();
        try
        {
            SqlConnection conn = new SqlConnection(cs);
        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = conn.CreateCommand();
        cmd.CommandText = Sql;
        da.SelectCommand = cmd;
        

  
        da.Fill(ds);
           
        }
        catch (Exception ex)
        {

        }

        return ds;
        
    }



    public static DataTable GetDataFromSQL(string Sql, string cs= CsDefault )
    {
        DataTable dt = new DataTable();

        try
        {
            SqlConnection sqlConnection = new SqlConnection(cs);
            SqlDataAdapter da = new SqlDataAdapter(Sql, sqlConnection);
            da.Fill(dt);
        }catch(Exception ex)
        {
            GestioneLog.ArchiviaErroreLog("Exception SQL:("+ Sql + ") classe db - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
            throw new Exception(ex.Message);
          
        }

        return dt;
    }





    private static string GetTableName(object Oggetto ) {

        return Oggetto.GetType().ToString().Split('.').Last().Replace("[", "").Replace("]", "");

    }


    private static Boolean CheckIfIDIsIdentity(String NomeTabella)
    {
        DataTable dt = GetDataFromSQL("select COLUMN_NAME, TABLE_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME like '"+ NomeTabella + "' and COLUMN_NAME like 'ID' and  COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1 order by TABLE_NAME");
        if (dt.Rows.Count > 0) return true;
        return false;
        

    }






    public static int DB_InsertInto(object Oggetto, string cs = CsDefault)
    {

        string StringaSql="";
        try
        {
            List<PropertyInfo> lp = new List<PropertyInfo>();
            lp.AddRange(GetProprietàDaOggetto(Oggetto));

            SqlConnection SqlConn = new SqlConnection(CsDefault);

            SqlCommand SqlCmd = new SqlCommand();
            SqlCmd.Connection = SqlConn;

            string ListCampi = "";
            string InsertValori = "";



            string Tabella = GetTableName(Oggetto);  //.GetProperty("Tabella").GetValue(Oggetto, null).ToString();
            int IdInserito = 0;
            string Pk = "ID";
            if (!CheckIfIDIsIdentity(Tabella))
            {
                IdInserito = 0;
                Pk = "NOIDENTITY";
            }

            foreach (DataColumn colonna in GetDataFromSQL("select  top(1) * from " + Tabella, cs).Columns)
            {
                try
                {
                    if (lp.Exists(x => x.Name.ToLower() == colonna.ColumnName.ToLower()) & (colonna.ColumnName.ToString().ToLower() != Pk.ToLower()) )
                    {
                        ListCampi = ListCampi + colonna.ColumnName + ",";
                        InsertValori = InsertValori + "@" + colonna.ColumnName + ",";

            
                    
                        var valore = GetProprietàOggetto(Oggetto ,lp.FirstOrDefault(x => x.Name.ToLower() == colonna.ColumnName.ToLower()).Name);
                        if (colonna.ColumnName.ToLower() == "id") IdInserito = Convert.ToInt32(valore);



                        SqlCmd.Parameters.AddWithValue("@" + colonna.ColumnName, ConvertiNothinginDBnull(valore));
                    }
                }
                catch (Exception ex)
                {
                    GestioneLog.ArchiviaErroreLog("Exception classe: db - metodo: DB_InsertInto - costrutto:foreach - colonna: " + colonna.ColumnName + " " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                    throw new Exception(ex.Message);
                }
            }

            ListCampi = ListCampi + "nva_UserId,dtt_Cancellazione,dtt_Modifica";
            InsertValori = InsertValori + "@nva_UserId,@dtt_Cancellazione,getdate()";
            SqlCmd.Parameters.AddWithValue("@nva_UserId", Environment.MachineName );
            SqlCmd.Parameters.AddWithValue("@dtt_Cancellazione", DBNull.Value );

            if(Pk == "NOIDENTITY") {
                StringaSql = "Insert into " + GetTableName(Oggetto) + "(" + ListCampi + ") values (" + InsertValori + "); SELECT " + IdInserito.ToString() ;
            }
            else { 
                StringaSql = "Insert into " + GetTableName(Oggetto)+ "(" + ListCampi + ") values (" + InsertValori + "); SELECT @@identity;";
            }
            System.Diagnostics.Debug.WriteLine("-----------------------------------------------");
            System.Diagnostics.Debug.WriteLine(StringaSql);
            System.Diagnostics.Debug.WriteLine("-----------------------------------------------");


            SqlCmd.CommandText = StringaSql;
            SqlCmd.CommandType = CommandType.Text;
            SqlConn.Open();

            var o = SqlCmd.ExecuteScalar();

            int identity = System.Convert.ToInt32(o);






            
            Oggetto.GetType().GetProperty(Pk).SetValue(Oggetto, identity, null);

            SqlConn.Close();
            return identity;
        }
        catch (Exception ex)
        {

            GestioneLog.ArchiviaErroreLog("Exception classe: db - metodo: DB_InsertInto: " + ex.Message+" "+ StringaSql, System.Reflection.MethodInfo.GetCurrentMethod().ToString());

            return 0;
        }
    }




    private static string GetPrimaryKey(string NomeTabella, string cns = CsDefault)
    {
        try { 
        string s = @"SELECT column_name FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND    TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME AND  KU.table_name='" + NomeTabella + @"'";
        DataTable dt = GetDataFromSQL(s, cns);

        return dt.Rows[0][0].ToString();
    }
        catch (Exception ex)
        {

            GestioneLog.ArchiviaErroreLog("Exception classe: db - metodo: GetPrimaryKey: " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
            throw new Exception(ex.Message);
   
          
        }
    }


    public static int DB_Delete(object Oggetto, string cs = CsDefault)
    {

        try
        {
            List<PropertyInfo> lp = new List<PropertyInfo>();   
            lp.AddRange(GetProprietàDaOggetto(Oggetto));

   

        DB_ArchiviaRecordStorico("DELETE", Oggetto);


        SqlConnection SqlConn = new SqlConnection(CsDefault);

        SqlCommand SqlCmd = new SqlCommand();
        SqlCmd.Connection = SqlConn;

        string ListCampiValori = "";

        string Pk = "ID";

        SqlCmd.CommandText = "update " + GetTableName(Oggetto) + " set dtt_Cancellazione = getdate() where " + Pk + " = " + GetProprietàOggetto(Oggetto,Pk);
        SqlCmd.CommandType = CommandType.Text;
        SqlConn.Open();
        int i = System.Convert.ToInt32(SqlCmd.ExecuteScalar());
        SqlConn.Close();

        return i;

        }
        catch (Exception ex)
        {

            GestioneLog.ArchiviaErroreLog("Exception classe: db - metodo: DB_Delete: " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
            throw new Exception(ex.Message);


        }
    }


    private static void SetProprietàOggetto(object obj,string nomeproprietà,object valore)
    {
        try { 
        obj.GetType().GetProperty(nomeproprietà).SetValue(obj, valore, null);

    }
        catch (Exception ex)
        {

            GestioneLog.ArchiviaErroreLog("Exception classe: db - metodo: SetProprietàOggetto: " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
            throw new Exception(ex.Message);


}
    }

    private static object GetProprietàOggetto(object obj, string nomeproprietà)
    {

        try
        {
            nomeproprietà = nomeproprietà.Split(" ").LastOrDefault();



            var t = obj.GetType();
            var tt = t.GetProperty(nomeproprietà);
            var ttt = tt.GetValue(obj, null);
            return ttt;

        }
        catch(Exception ex)
        {
            GestioneLog.ArchiviaErroreLog("Exception classe: db - metodo: GetProprietàOggetto: " + ex.Message, "GetProprietàOggetto");

            return null;
        }


    }









    public static int DB_Update(object Oggetto, string cs = CsDefault)
    {
        string query = "";
        try
        {
            List<PropertyInfo> lp = new List<PropertyInfo>();
            lp.AddRange(GetProprietàDaOggetto(Oggetto));

           


            DB_ArchiviaRecordStorico("UPDATE", Oggetto);

            SqlConnection SqlConn = new SqlConnection(CsDefault);

            SqlCommand SqlCmd = new SqlCommand();
            SqlCmd.Connection = SqlConn;

            string ListCampiValori = "";

            ListCampiValori = ListCampiValori + "dtt_Modifica = getdate() ,";

            string Pk = "ID";

            foreach (PropertyInfo l in lp)
            {
                if (l.Name.ToLower() == Pk.ToLower()) Pk = l.Name;
            }

            foreach (DataColumn colonna in GetDataFromSQL("select  top(1) * from " + GetTableName(Oggetto), cs).Columns)
            {
                if (lp.Exists(x => x.Name.ToLower() == colonna.ColumnName.ToLower()) & colonna.ColumnName.ToLower().ToString() != Pk.ToLower().ToString())
                {
                    ListCampiValori = ListCampiValori + colonna.ColumnName + " = @" + colonna.ColumnName + ",";

                    object valore = ConvertiNothinginDBnull(GetProprietàOggetto(Oggetto, lp.FirstOrDefault(x => x.Name.ToLower() == colonna.ColumnName.ToLower()).ToString()));

                    try
                    {
                        System.Diagnostics.Debug.WriteLine(colonna.ColumnName + " -> " + valore.ToString());
                    }
                    catch (Exception ex)
                    {
                        GestioneLog.ArchiviaErroreLog("Exception classe: db - metodo: DB_Update: Colonna "+ colonna.ColumnName + " -> NON CONVERTIBILE " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                    }


                    SqlCmd.Parameters.AddWithValue("@" + colonna.ColumnName, valore);
                }
            }



            query = "update " + GetTableName(Oggetto) + " set " + ListCampiValori.Substring(0, ListCampiValori.Length - 1) + " where " + Pk + " = " + GetProprietàOggetto(Oggetto, Pk);
            SqlCmd.CommandText = query;
            SqlCmd.CommandType = CommandType.Text;
            SqlConn.Open();


            object result = SqlCmd.ExecuteScalar();
            result = (result == DBNull.Value) ? null : result;
            int i = Convert.ToInt32(result);

            SqlConn.Close();
    

            return System.Convert.ToInt32(i);

        }
        catch (Exception ex)
        {
            GestioneLog.ArchiviaErroreLog("Exception classe: db - metodo: DB_Update: " + ex.Message+" query: "+ query, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
            throw new Exception(ex.Message);
 

        }
    }


    private static void DB_ArchiviaRecordStorico(string Azione, object Oggetto)
    {

        string StingaSql = "";

        try
        {
            DB_CreaTbStorico(Oggetto);

            List<PropertyInfo> lp = new List<PropertyInfo>();
            lp.AddRange(GetProprietàDaOggetto(Oggetto));
            string Pk = "ID"; 

             StingaSql = " insert into  " + GetTableName(Oggetto) + "__LOG select *,getdate() as dtt_COMANDO,'" + Azione + "' as nva_COMANDO ,'" + Environment.UserName + "' as nva_UserId_COMANDO  from " + GetTableName(Oggetto) + " where " + Pk + " = " + GetProprietàOggetto(Oggetto,"ID").ToString();



            EseguiSQL(StingaSql, CsDefault);
        }


        catch (Exception ex)
        {

            GestioneLog.ArchiviaMessaggioLog("impossibile archiviare il record di log:"+ex.Message+ " StingaSql:"+ StingaSql, "DB_ArchiviaRecordStorico");


        }
    }



    public static bool IsValidDate(object _date)
    {
        try
        {
            DateTime date = (DateTime)_date;
            if(date > Convert.ToDateTime("01/01/1800") & date < Convert.ToDateTime("01/01/2080"))
                return true;
        }
        catch (Exception ex)
        {

        }
        return false;
    }


    private static void DB_CreaTbStorico(object Oggetto)
    {



        if (!ControllaEsistenzaTabellaLog(Oggetto))
        {
          GestioneLog.ArchiviaMessaggioLog("La tabella di log " + GetTableName(Oggetto) + " non esiste ", "DB_CreaTbStorico");
            if (CreaNuovaTabellaLog(Oggetto))
                GestioneLog.ArchiviaMessaggioLog("ho correttamente creato la tabella di log " + GetTableName(Oggetto) + "  ", "DB_CreaTbStorico");
            else
                GestioneLog.ArchiviaMessaggioLog("non sono riuscito a creare la tabella di log " + GetTableName(Oggetto) + "  ", "DB_CreaTbStorico");
        }
    }




    private static Boolean  ControllaEsistenzaTabellaLog(object Oggetto)
    {
        try
        {
            EseguiSQL("select top(1) * from " + GetTableName(Oggetto) + "__LOG", CsDefault);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }



    private static bool CreaNuovaTabellaLog(object Oggetto)
    {
        try
        {
            string Pk = "ID";
            EseguiSQL("Select top(1) 100000000 as " + Pk + "_new ,* ,getdate() As dtt_COMANDO,REPLICATE('',255)  as nva_COMANDO,REPLICATE('',255)  as nva_UserId_COMANDO into " + GetTableName(Oggetto) + "__LOG from " + GetTableName(Oggetto) + " where 1=0 ", CsDefault);

            EseguiSQL("ALTER TABLE " + GetTableName(Oggetto) + "__LOG DROP COLUMN " + Pk, CsDefault);
            EseguiSQL("sp_rename '" + GetTableName(Oggetto) + "__LOG." + Pk + "_new', '" + Pk + "' ,'COLUMN' ", CsDefault);

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }






}
