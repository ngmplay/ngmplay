﻿using RenderMachine.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace NgmPlay
{
    public static class ImportazioneConfig
    {

        public static string AcquisisciConfigurazioneStadio(int fk_int_Stadio, string xmlString)
        {
            try
            {

                DB.EseguiSQL("insert into tb_Log (nva_Messaggio, dtt_Modifica) values ('inizio AcquisisciConfigurazioneStadio()',getdate())");


                tb_Stadi stadio = new tb_Stadi();
                stadio.Popola(fk_int_Stadio);
                stadio.tin_ConfigurazioneOK = 0;
                stadio.DB_Update();

                DB.EseguiSQL("insert into tb_Log (nva_Messaggio, dtt_Modifica) values ('ripulisco tb_DisposizioneLed per stadio id:" + fk_int_Stadio.ToString() + "',getdate())");

                DB.EseguiSQL("update tb_DisposizioneLed set dtt_Cancellazione = getdate() where fk_int_Stadio = " + fk_int_Stadio.ToString());
                var xml = XDocument.Parse(xmlString);


                DataTable dt = DB.GetDataFromSQL("select * from tb_layer");
                foreach (DataRow r in dt.Rows)
                {
                    DB.EseguiSQL("insert into tb_Log (nva_Messaggio, dtt_Modifica) values ('inizio elaborazione layer "+ r["nva_NomeTagXML"].ToString() + "',getdate())");


                    XElement elemento = (from xml2 in xml.Root.Descendants(r["nva_NomeTagXML"].ToString()) select xml2).FirstOrDefault();
                    if (elemento != null)
                    {

                        var query = from c in elemento.Elements()  select new
                                    {
                                        height = c.Attribute("h").Value,
                                        width = c.Attribute("w").Value,
                                        name = c.Attribute("name").Value //, //layer = c.Element("layer").Value,
                                    };


                        int p = 0;
                        foreach (var c in query)
                        {
                            //Console.WriteLine(c.layer);
                            p++;

                            //Console.WriteLine(c.height);
                            tb_DisposizioneLed dlm = new tb_DisposizioneLed();
                            dlm.int_H = Convert.ToInt32(c.height);
                            dlm.int_W = Convert.ToInt32(c.width);
                            dlm.fk_int_Stadio = fk_int_Stadio;

                            dlm.fk_int_Layer = Convert.ToInt32(r["ID"]); // int.Parse(c.layer);
                            dlm.int_Ordine = Convert.ToInt32(p);
                            dlm.nva_Nome = c.name;
                            DB.DB_InsertInto(dlm);

                        }

                        DB.EseguiSQL("insert into tb_Log (nva_Messaggio, dtt_Modifica) values ('lancio sp_import_01_AggiornaDisposizioneLed @fk_int_Stadio = " + fk_int_Stadio.ToString() + ", @fk_int_Layer =  " + r["ID"].ToString() + "',getdate())");
                        DB.EseguiSQL("Exec sp_import_01_AggiornaDisposizioneLed @fk_int_Stadio = " + fk_int_Stadio.ToString() + ", @fk_int_Layer =  " + r["ID"].ToString());

                    }

                }



                var queryLED = from c in xml.Root.Descendants("positionLed") select new { positionX = c.Element("positionX").Value, positionY = c.Element("positionY").Value, width = c.Element("width").Value, height = c.Element("height").Value };
                var queryMaxischermo = from c in xml.Root.Descendants("positionLedWall") select new { positionX = c.Element("positionX").Value, positionY = c.Element("positionY").Value, width = c.Element("width").Value, height = c.Element("height").Value };
                Console.WriteLine(queryLED.FirstOrDefault().width.ToString() + " " + queryLED.FirstOrDefault().height.ToString());



                stadio.int_W = int.Parse(queryLED.FirstOrDefault().width);
                stadio.int_H = int.Parse(queryLED.FirstOrDefault().height);
                stadio.int_WMaxischermo = int.Parse(queryMaxischermo.FirstOrDefault().width);
                stadio.int_HMaxischermo = int.Parse(queryMaxischermo.FirstOrDefault().height);
                stadio.int_X = int.Parse(queryLED.FirstOrDefault().positionX);
                stadio.int_Y = int.Parse(queryLED.FirstOrDefault().positionY);
                stadio.int_XMaxischermo = int.Parse(queryMaxischermo.FirstOrDefault().positionX);
                stadio.int_YMaxischermo = int.Parse(queryMaxischermo.FirstOrDefault().positionY);

                stadio.tin_ConfigurazioneOK = 1;
                stadio.DB_Update();


                DB.EseguiSQL("insert into tb_Log (nva_Messaggio, dtt_Modifica) values ('Metodo AcquisisciConfigurazioneStadio terminato senza Exception',getdate())");


                return "";

            }catch (Exception ex){

                DB.EseguiSQL("insert into tb_Log (nva_Messaggio, dtt_Modifica) values ('Exception AcquisisciConfigurazioneStadio: " + ex.ToString().Replace("'","") + "',getdate())");



                return ex.ToString();
            }

        }
    }
}
