﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NgmPlay.Costanti
{
    public static  class TipoEventi
    {
        public const int Goal = 1;
        public const int Ammonizione = 2;
        public const int Espulsione = 3;
        public const int Sostituzione = 4;
        public const int Auto_Goal = 5;
        public const int Rigore_trasformato = 6;
        public const int Rigore_sbagliato = 7;
        public const int Corner_alto = 8;
        public const int Presentazione_giocatre = 9;

    }
}
