﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NgmPlay.Classi
{
    public class EsitoAggiornamento
    {
        public int NumeroAggiornati;
        public int NumeroInseriti;
        public int NumeroTotali;

        public  EsitoAggiornamento(int NumeroTotali, int NumeroInseriti, int NumeroAggiornati)
        {
            this.NumeroInseriti = NumeroInseriti;
            this.NumeroAggiornati = NumeroAggiornati;
            this.NumeroTotali = NumeroTotali;

        }

        public string Riepilogo(String tb)
        {
            String String="<li>"+ tb+ " -> ";
            String += "Inseriti:" + NumeroInseriti.ToString();
            String += " - ";
            String += "Aggiornati:" + NumeroAggiornati.ToString();
            String += " - ";
            String += "Totali:" + NumeroTotali.ToString();
            String += "</li>";
            return String;

        }



    }
}
