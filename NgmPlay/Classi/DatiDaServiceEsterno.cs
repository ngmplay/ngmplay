﻿using NgmStudio.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace NgmPlay.Classi
{
    public class DatiDaServiceEsterno
    {
        Boolean Loader = false;

        //System.Windows.Threading.DispatcherTimer dispatcherTimerLiveScore;
        System.Threading.Timer timerLiveScore;
        private string idVisitorTeam;
        private List<string> Status;
        private bool enableAudioLiveScore { get; set; }

        //private readonly BackgroundWorker worker = new BackgroundWorker();
        public DatiDaServiceEsterno()
        {


            timerLiveScore = new System.Threading.Timer(dispatcherTimerLiveScore_Tick, null, 1 , Convert.ToInt32( StaticStartConfig.Parametro_nva_LiveScorePolling));
            //timerLiveScore = new System.Threading.Timer(dispatcherTimerLiveScore_Tick, null, 1, setting.ledWall.settingLiveScore.Polling);
            timerLiveScore.Change(-1, -1);


            enableAudioLiveScore = false;

            Status = new List<string>();

            if (!string.IsNullOrWhiteSpace(StaticStartConfig.Parametro_nva_LiveScoreTypeOfStatus))
                Status = StaticStartConfig.Parametro_nva_LiveScoreTypeOfStatus.Split(';').ToList();
        }

        private void btnLoadMatch_Click()
        {
            if (Utility.InternetConnection.Check())
            {
                Loader = true;
                //txbMsgError.Text = "";

                List<Score> results = new List<Score>();

                string urlFixture = string.Empty;

                if (!string.IsNullOrWhiteSpace(StaticStartConfig.Parametro_nva_LiveScoreSerieA))
                    urlFixture = StaticStartConfig.Parametro_nva_LiveScoreUrlServer +
                        StaticStartConfig.Parametro_nva_LiveScoreUserGuid +
                        StaticStartConfig.Parametro_nva_LiveScoreUrlFixtureSerieA;
                else
                    urlFixture = StaticStartConfig.Parametro_nva_LiveScoreUrlServer +
                        StaticStartConfig.Parametro_nva_LiveScoreUserGuid +
                        StaticStartConfig.Parametro_nva_LiveScoreUrlFixtureSerieB;

                try
                {

                    Task.Factory.StartNew(() =>
                    {
                        XElement xelement = XElement.Load(urlFixture);
                        var matchs = GetMatchs(xelement);
                        if (matchs != null)
                        {
                            foreach (var match in matchs)
                            {
                                Score score = new Score();
                                score.IdMatch = match.Attribute("static_id").Value;
                                score.Date = match.Attribute("date").Value;
                                score.Status = match.Attribute("status").Value;
                                score.Time = match.Attribute("time").Value;
                                score.IdTeamHome = match.Element("localteam").Attribute("id").Value;
                                score.NameTeamHome = match.Element("localteam").Attribute("name").Value;
                                score.ScoreTeamHome = string.IsNullOrWhiteSpace(match.Element("localteam").Attribute("score").Value) ? "0" : match.Element("localteam").Attribute("score").Value;
                                score.IdTeamVisitor = match.Element("visitorteam").Attribute("id").Value;
                                score.NameTeamVisitor = match.Element("visitorteam").Attribute("name").Value;
                                score.ScoreTeamVisitor = string.IsNullOrWhiteSpace(match.Element("visitorteam").Attribute("score").Value) ? "0" : match.Element("visitorteam").Attribute("score").Value;
                                results.Add(score);
                            }

                            if (results.Count > 0)
                            {
                                if (!string.IsNullOrWhiteSpace(StaticStartConfig.Parametro_nva_LiveScoreIdTeam))
                                {
                                    var teamHome = results.Where(x => x.IdTeamHome.Equals(StaticStartConfig.Parametro_nva_LiveScoreIdTeam)).FirstOrDefault();

                                    if (teamHome != null)
                                    {
                                        var timeHome = teamHome.Time;
                                        //var StatusHome = teamHome.Status;
                                        var dateHome = teamHome.Date;


                                        idVisitorTeam = teamHome.IdTeamVisitor;

                                        if (!string.IsNullOrWhiteSpace(timeHome) && !string.IsNullOrWhiteSpace(dateHome))
                                        {
                                            var datetim = dateHome + " " + timeHome;

                                            var dat = DateTime.ParseExact(datetim, "dd.MM.yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                                            foreach (var item in results)
                                            {
                                                if (!Status.Any(x => x.Equals(item.Status)))
                                                {
                                                    if (!string.IsNullOrWhiteSpace(item.Time) && !string.IsNullOrWhiteSpace(item.Date))
                                                    {
                                                        var d = DateTime.ParseExact(item.Date + " " + item.Time, "dd.MM.yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                                                        if (d > dat)
                                                        {

                                                            if (item.Date == dateHome && item.Time.Length == 5)
                                                            {
                                                                d = d.ToLocalTime();
                                                                item.ScoreTeamHome = d.Hour.ToString() + ":";
                                                                item.ScoreTeamVisitor = d.Minute.ToString();
                                                            }
                                                            else
                                                            {
                                                                if (item.Date.Length > 4)
                                                                {
                                                                    item.ScoreTeamHome = item.Date.Substring(0, 2) + "/";
                                                                    item.ScoreTeamVisitor = item.Date.Substring(3, 2);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    })
                    .ContinueWith((t) =>
                    {
                        if (results.Count > 0)
                          //  dgScore.ItemsSource = results;

                        //Insert Tab H2H (testa a testa)
                  //      MainWindow parentWindow = Application.Current.MainWindow as MainWindow;
                   //     if (parentWindow != null)
                    //        parentWindow.InsertTabH2H(idVisitorTeam);


                        Loader = false;
                    }, System.Threading.CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());

                }
                catch (Exception ex)
                {
                  //  txbMsgError.Text = "Errore nel caricamento (LiveScore): " + ex;
                    StopLiveScore();
                }
            }
            else
            {
               // txbMsgError.Text = "Verificare la connessione internet.";
                StopLiveScore();
            }
        }


        private List<XElement> GetMatchs(XElement tournament)
        {
            string dateToday = DateTime.Today.ToString("dd.MM.yyyy");

            var weeks = tournament.Descendants("week").ToList();

            foreach (var we in weeks)
            {
                var matchs = we.Descendants("match").ToList();
                foreach (var match in matchs)
                {
                    if (match.HasAttributes && match.FirstAttribute.Value.Equals(dateToday))
                        return matchs;
                }
            }
            return null;
        }

        private void dispatcherTimerLiveScore_Tick(object state)
        {

            string idCategory = string.Empty;

            if (!string.IsNullOrWhiteSpace(StaticStartConfig.Parametro_nva_LiveScoreSerieA))
                idCategory = StaticStartConfig.Parametro_nva_LiveScoreIdSerieA;
            else
                idCategory = StaticStartConfig.Parametro_nva_LiveScoreIdSerieB;

            if (Utility.InternetConnection.Check())
            {
                WriteMessageError("");
                /*
                List<Score> results = dgScore.ItemsSource as List<Score>;

                
                try
                {
                    if (results != null && results.Count > 0)
                    {
                        var urlLiveScore = StaticStartConfig.Parametro_nva_LiveScoreUrlServer +
                        StaticStartConfig.Parametro_nva_LiveScoreUserGuid +
                        StaticStartConfig.Parametro_nva_LiveScoreUrlLiveScore;
                        XElement xelement = XElement.Load(urlLiveScore);

                        var allCategory = xelement.Descendants("category").ToList();

                        var category = (from cat in allCategory
                                        where cat.Attribute("id").Value.Equals(idCategory)
                                        select cat).FirstOrDefault();

                        var matchs = category.Descendants("match").ToList();

                        if (matchs != null)
                        {
                            foreach (var match in matchs)
                            {
                                var matchId = match.Attribute("static_id").Value;

                                var scoreHome = match.Element("localteam").Attribute("goals").Value;
                                var scoreVisitor = match.Element("visitorteam").Attribute("goals").Value;
                                var status = match.Attribute("status").Value;

                                if (!string.IsNullOrWhiteSpace(status) && !Status.Any(x => x.Equals(status)))
                                {
                                    if (scoreHome != "?")
                                    {
                                        var rowScore = results.FirstOrDefault(x => x.IdMatch.Equals(matchId));
                                        if (rowScore != null)
                                        {
                                            if (!rowScore.ScoreTeamHome.Equals(scoreHome) || !rowScore.ScoreTeamVisitor.Equals(scoreVisitor))
                                            {
                                                rowScore.ScoreTeamHome = scoreHome;
                                                rowScore.ScoreTeamVisitor = scoreVisitor;

                                                if (!string.IsNullOrWhiteSpace(StaticStartConfig.Parametro_nva_LiveScoreIdTeam))
                                                {
                                                    if (!rowScore.IdTeamHome.Equals(StaticStartConfig.Parametro_nva_LiveScoreIdTeam))
                                                    {
                                                        string newResult = "   " + rowScore.NameTeamHome + "  " + rowScore.NameTeamVisitor + "  " + rowScore.ScoreTeamHome + " : " + rowScore.ScoreTeamVisitor + "   ";

                                                        ShowLiveScore(newResult);

                                                        break;
                                                    }
                                                }
                                                else
                                                    WriteMessageError("Assenza IdLocalTeam in configurazione.");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    WriteMessageError("Errore aggiornamento risultati (LiveScore): " + ex);
                    //txbMsgError.Text = "Errore aggiornamento risultati (LiveScore): " + ex;
                    //StopLiveScore();
                }
                */


            }
            else
            {
                WriteMessageError("Verificare la connessione internet.");
                //txbMsgError.Text = "Verificare la connessione internet.";
                //StopLiveScore();
            }
        }

        private void WriteMessageError(string message)
        {
            /*
            Dispatcher.Invoke(() =>
            {
                //code to update ui
                this.txbMsgError.Text = message;
            });

            */
        }

        private void StopLiveScore()
        {
            //dispatcherTimerLiveScore.Stop();
            timerLiveScore.Change(-1, -1);
          //  btnAudioLiveScore.IsChecked = false;
           // btnLiveScore.IsChecked = false;
            enableAudioLiveScore = false;
            Loader = false;
        }

        private void btnStartLiveScore_Click()
        {
           // if ((sender as ToggleButton).IsChecked == true)
                timerLiveScore.Change(0, Convert.ToInt32( StaticStartConfig.Parametro_nva_LiveScorePolling));
           // else
             //   timerLiveScore.Change(-1, -1);
        }

        private void btnViewMatch_Click()
        {
            /*
            if ((sender as ToggleButton).IsChecked == true)
            {
                var listResults = dgScore.ItemsSource as List<Score>;

                GridResults allResults = new GridResults(listResults, settings);
                allResults.HorizontalAlignment = HorizontalAlignment.Left;

                LedWallRandomEventsPopUp ledWallRandomEvent = new LedWallRandomEventsPopUp();
                ledWallRandomEvent.Name = "AllResults";
                ledWallRandomEvent.Height = settings.ledWall.body.height;
                ledWallRandomEvent.Width = settings.ledWall.body.width;

                ledWallRandomEvent.Top = settings.positionLedWall.positionY;
                ledWallRandomEvent.Left = settings.positionLedWall.positionX;

                ledWallRandomEvent.stkRandomEvent.Children.Add(allResults);
                ledWallRandomEvent.Show();

            }
            else
            {
                var wi = Application.Current.Windows.OfType<LedWallRandomEventsPopUp>().FirstOrDefault(w => w.Name.Equals("AllResults"));
                if (wi != null)
                    wi.Close();
            }
            */
        }

        private void btnEnableAudioLiveScore_Click()
        {
            /*
            if ((sender as ToggleButton).IsChecked == true)
                enableAudioLiveScore = true;
            else
                enableAudioLiveScore = false;
            */
        }

        private void btnTestLiveScore_Click()
        {
            string newResult = "   MILAN   JUVENTUS   2 - 2  ";

            ShowLiveScore(newResult);
        }

        public void ShowLiveScore(string newResult)
        {

            /*
            new Thread(() =>
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    MarqueeTextControl textControl = new MarqueeTextControl(settings.ledWall.settingLiveScore, settings.ledWall.body.width);
                    textControl.MarqueeContent = newResult;
                    textControl.MarqueeTimeInSeconds = StaticStartConfig.Parametro_nva_LiveScoreTimeInSeconds;
                    var vb = textControl.canvasLiveScore.Background as VisualBrush;
                    var media = vb.Visual as MediaElement;
                    if (media != null && media.Source != null)
                    {
                        media.Visibility = Visibility.Visible;
                        media.Play();
                    }
                    textControl.StartMarqueeing(MarqueeType.RightToLeft);


                    LedWallRandomEventsPopUp ledWallRandomEvent = new LedWallRandomEventsPopUp();

                    ledWallRandomEvent.Height = StaticStartConfig.Parametro_nva_LiveScoreHeight;
                    ledWallRandomEvent.Width = settings.ledWall.body.width;

                    ledWallRandomEvent.Top = StaticStartConfig.Parametro_nva_LiveScorePositionY;
                    ledWallRandomEvent.Left = StaticStartConfig.Parametro_nva_LiveScorePositionX;

                    ledWallRandomEvent.stkRandomEvent.Children.Add(textControl);
                    ledWallRandomEvent.Close(StaticStartConfig.Parametro_nva_LiveScoreTimeInSeconds);

                    ledWallRandomEvent.Show();


                    if (System.IO.File.Exists(StaticStartConfig.Parametro_nva_LiveScoreUrlSoundLiveScore) && enableAudioLiveScore)
                    {
                        System.Media.SoundPlayer player = new System.Media.SoundPlayer();
                        player.SoundLocation = StaticStartConfig.Parametro_nva_LiveScoreUrlSoundLiveScore;
                        player.Play();
                    }
                }));
            }).Start();
            */

            //Task.Factory.StartNew(() =>
            //{
            //    MarqueeTextControl textControl = new MarqueeTextControl(settings.ledWall.settingLiveScore, settings.ledWall.body.width);
            //    textControl.MarqueeContent = newResult;
            //    textControl.MarqueeTimeInSeconds = StaticStartConfig.Parametro_nva_LiveScoreTimeInSeconds;
            //    var vb = textControl.canvasLiveScore.Background as VisualBrush;
            //    var media = vb.Visual as MediaElement;
            //    if (media != null && media.Source != null)
            //    {
            //        media.Visibility = Visibility.Visible;
            //        media.Play();
            //    }
            //    textControl.StartMarqueeing(MarqueeType.RightToLeft);


            //    LedWallRandomEventsPopUp ledWallRandomEvent = new LedWallRandomEventsPopUp();

            //    ledWallRandomEvent.Height = StaticStartConfig.Parametro_nva_LiveScoreHeight;
            //    ledWallRandomEvent.Width = settings.ledWall.body.width;

            //    ledWallRandomEvent.Top = StaticStartConfig.Parametro_nva_LiveScorePositionY;
            //    ledWallRandomEvent.Left = StaticStartConfig.Parametro_nva_LiveScorePositionX;

            //    ledWallRandomEvent.stkRandomEvent.Children.Add(textControl);
            //    ledWallRandomEvent.Close(StaticStartConfig.Parametro_nva_LiveScoreTimeInSeconds);

            //    ledWallRandomEvent.Show();


            //    if (System.IO.File.Exists(StaticStartConfig.Parametro_nva_LiveScoreUrlSoundLiveScore) && enableAudioLiveScore)
            //    {
            //        System.Media.SoundPlayer player = new System.Media.SoundPlayer();
            //        player.SoundLocation = StaticStartConfig.Parametro_nva_LiveScoreUrlSoundLiveScore;
            //        player.Play();
            //    }

            //})
            //.ContinueWith((t) =>
            //{

            //}, System.Threading.CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());

        }

    }
}
