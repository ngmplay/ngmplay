﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NgmPlay.Classi;

namespace NgmPlay
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            StaticStartConfig.PopolaCostantiDaDb();
            int counter = 0;
            string line;

            // Read the file and display it line by line.  
            System.IO.StreamReader file =  new System.IO.StreamReader(Configuration["FileParametriPC"]);
            while ((line = file.ReadLine()) != null)
            {

                line = line.ToString().ToLower().Replace(" ", "").Replace(";", "");
                if ((line.Length > 0)&&(line.Contains("=")))
                {
                 
                    string[] chiaveval = line.Split("=");
                    string chiave = chiaveval[0].ToLower();
                    string val = chiaveval[1];

                    if (chiave == "identificazionepc")
                    {
                        StaticStartConfig.int_IdentificazionePC = Convert.ToInt32(val);
                    }
                    if (chiave == "idstadio")
                    {
                        StaticStartConfig.ID_int_Stadio = Convert.ToInt32(val);
                    }


                    Console.WriteLine(line);

                }

            }

            file.Close();
 




        
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            //services.Configure<MvcOptions>(options =>
            //{
            //    options.Filters.Add(new CorsAuthorizationFilterFactory("AllowSpecificOrigin"));
            //});
            //services.AddCors(options =>
            //{
            //    options.AddPolicy("AllowSpecificOrigin",
            //        builder => builder.WithOrigins("http://localhost:5000").AllowAnyHeader().AllowAnyMethod());
            //});


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // services.AddMvc();
            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue; // In case of multipart
            });



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {



            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            //app.UseCors("AllowSpecificOrigin");

         //   app.UseCors(b => b.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());

            app.UseStaticFiles();
            app.UseCookiePolicy();
  
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });


        }




    }
}
