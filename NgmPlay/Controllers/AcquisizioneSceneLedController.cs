﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NgmPlay.Classi;
using NgmPlay.Utility;
using RenderMachine.Models;
namespace RenderMachine.Controllers
{
    public class AcquisizioneSceneLedController : Controller
    {
        //public String GetScenaSecondoSpecifico()
        //{
        //    string int_Secondo = Request.Query["int_Secondo"].ToString();
        //    string fk_int_Playlist = Request.Query["fk_int_Playlist"].ToString();
        //    string fk_int_Periodo = Request.Query["fk_int_Periodo"].ToString();
        //    DataTable dt = DB.GetDataFromSQL("exec sp_GetScenaSpecificoSecondoLed @fk_int_Periodo= " + fk_int_Periodo + ", @fk_int_Playlist =" + fk_int_Playlist + " , @time_Start = " + int_Secondo.ToString());
        //    if (dt.Rows.Count > 0)
        //    {
        //        DataRow r = dt.Rows[0];
        //        var obj = new { Chiave = r[0], ID = r[1], AvanzaDi = r[2], incrementaleSecondi = r[3] };
        //        return JsonConvert.SerializeObject(obj);
        //    }
        //    else
        //    {
        //        return "null";
        //    }
        //}
        public String GetTastiScenePlaylist()
        {
            try
            {
                string FK = Request.Query["fk_int_Playlist"].ToString();
                if (FK == "")
                {
                    FK = "1";
                }
                return JsonConvert.SerializeObject(DB.GetDataFromSQL("select * from tb_SceneEventiSpecialiLed where fk_int_Playlist = " + FK));
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
        }
        [HttpPost]
        public string OrdinaScene()
        {
            try
            {
                string nva_Ordine = HttpContext.Request.Form["ordine"];
                return DB.GetDataFromSQL("sp_AggiornaOrdinamentoTestSimone '" + nva_Ordine + "'").Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
        }
        public String SuntoContenutiScena()
        {
            try
            {
                string FK = Request.Query["fk_int_Scena"].ToString();
                if (FK == "")
                {
                    FK = "1";
                }
                return JsonConvert.SerializeObject(DB.GetDataFromSQL("select distinct nva_NomeFile from [tb_contenutiLed] where not nva_NomeFile is null and fk_int_Scena = " + FK));
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
        }
        public String tb_contenuti()
        {
            try
            {
                string FK = Request.Query["fk_int_Scena"].ToString();
                if (FK == "")
                {
                    FK = "1";
                }
                return JsonConvert.SerializeObject(DB.GetDataFromSQL("select * from [tb_contenutiLed] where  fk_int_Scena = " + FK));
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
        }
        /*
    try
    {
 * */
        public String tb_sceneLed()
        {
            try
            {
                string fk_int_Periodo = Request.Query["fk_int_Periodo"].ToString();
                if (fk_int_Periodo == "")
                {
                    fk_int_Periodo = "1";
                }
                string fk_int_Playlist = Request.Query["fk_int_Playlist"].ToString();
                if (fk_int_Playlist == "")
                {
                    fk_int_Playlist = "1";
                }
                return JsonConvert.SerializeObject(DB.GetDataFromSQL("select  ROW_NUMBER() OVER(ORDER BY int_Ordine ASC) AS int_Incrementale ,* from [tb_SceneLed] where fk_int_Playlist = " + fk_int_Playlist + " and  fk_int_Periodo = " + fk_int_Periodo + " order by int_Ordine"));
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
        }
        /*fine aggiunta*/
        [HttpPost]
        public string inserisciScena([FromBody] scene_riprodotte_compresse_led sc)
        {
            try
            {
                tb_SceneRiproduzioniLed sr = new tb_SceneRiproduzioniLed();
                //s.ID = f.id;
                sr.fk_int_Riproduzione = sc.rid;
                sr.fk_int_Scena = sc.sid;
                sr.int_Durata = sc.msecd;  //durata in millisecondi a velocità normale della scena caricata nella tabella tb_scena
                sr.int_DataOraInizio = sc.msdi;
                sr.int_DataOraFine = sc.msdf;
                sr.int_IniziaDa = sc.start;
                sr.int_StopA = sc.stop;
                //  sr.int_Progressivo = sc.pro;
                sr.fk_int_Periodo = sc.iper;
                sr.fk_int_SceneEventiSpeciali = sc.tsc;
                sr.int_NextID = sc.nid;
                //  sr.int_NextProgressivo = sc.npr;
                //  sr.int_PrimaScena = sc.psc;
                sr.chr_Estremo = sc.est;
                sc.id = DB.DB_InsertInto(sr);
                string json = JsonConvert.SerializeObject(sc);
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
        }
        [HttpPost]
        public string salvaScene([FromBody] List<scene_riprodotte_compresse_led> lsc)
        {
            try
            {
                foreach (scene_riprodotte_compresse_led sc in lsc)
                {
                    tb_SceneRiproduzioniLed sr = new tb_SceneRiproduzioniLed();
                    sr.Popola(sc.id);
                    //s.ID = f.id;
                    //s.fk_int_Riproduzione = f.rid;
                    //s.fk_int_Scena = f.sid;
                    sr.int_Durata = sc.msecd;
                    sr.int_DataOraInizio = sc.msdi;
                    sr.int_DataOraFine = sc.msdf;
                    sr.int_IniziaDa = sc.start;
                    sr.int_StopA = sc.stop;
                    sr.int_Velocità = sc.vel;
                    //  sr.int_Progressivo = sc.pro;
                    //s.fk_int_Periodo = f.iper;
                    sr.int_NextID = sc.nid;
                    //s.int_NextProgressivo = f.npr;
                    //sr.int_PrimaScena = sc.psc
                    sr.chr_Estremo = sc.est;
                    sr.DB_Update();
                }
                return "OK";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        [HttpPost]
        public string salvaScena([FromBody] scene_riprodotte_compresse_led f)
        {
            try
            {
                tb_SceneRiproduzioniLed s = new tb_SceneRiproduzioniLed();
                s.Popola(f.id);
                //s.ID = f.id;
                //s.fk_int_Riproduzione = f.rid;
                //s.fk_int_Scena = f.sid;
                s.int_Durata = f.msecd;
                s.int_DataOraInizio = f.msdi;
                s.int_DataOraFine = f.msdf;
                s.int_IniziaDa = f.start;
                s.int_StopA = f.stop;
                //s.int_Progressivo = f.pro;
                //s.fk_int_Periodo = f.iper;
                s.int_NextID = f.nid;
                //s.int_NextProgressivo = f.npr;
                s.int_Velocità = f.vel;
                //              s.int_PrimaScena = f.psc;
                //                s.int_UltimaScena = f.usc;
                s.chr_Estremo = f.est;
                s.DB_Update();
                return "OK";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        [HttpPost]
        public string salvaArrayTimelineMatch([FromBody] List<scene_riprodotte_compresse_led> prova)
        {
            try
            {
                return prova.Count().ToString();
                //        foreach (scene_riprodotte_compresse f in prova)
                //        {
                //            tb_SceneRiproduzioni s = new tb_SceneRiproduzioni();
                //            s.ID =  f.id ;
                //            s.fk_int_Scena = f.sid ;
                //            s.int_Durata = f.msecd ;
                //            s.int_DataOraInizio =  f.msdi ;
                //            s.int_DataOraFine =  f.msdf ;
                //            s.int_IniziaDa =  f.start ;
                //            s.int_StopA = f.stop ;
                //            s.int_Progressivo=   f.pro ;
                //          //  s.fk_int_SceneEventiSpeciali = f.tsc ;
                //            s.fk_int_Periodo =   f.iper  ;
                //            s.int_NextId =  f.nid ;
                //            s.int_PrimaScena = f.psc ;
                //s.DB_Update();
                //        }
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        [HttpPost]
        public string salvaArrayTimelineMatch_old()
        {
            try
            {
                List<scene_riprodotte_compresse_led> src = JsonConvert.DeserializeObject<List<scene_riprodotte_compresse_led>>(HttpContext.Request.Form["dati"]);
                foreach (scene_riprodotte_compresse_led f in src)
                {
                    Console.WriteLine(f.id);
                    //DB.EseguiSQL("delete from tb_Formazione where fk_int_Match = " + f.fk_int_Match + " and  fk_int_Giocatore = " + f.fk_int_Giocatore);
                    //if (f.int_Ordine > 0)
                    //DB.DB_InsertInto(f);
                }
                return "OK";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        //acquisisco le scene
        public String Index()
        {
            try
            {
                string fk = Request.Query["fk_int_ScenaRiproduzione"].ToString();
                string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GetContenutiSceneRiproduzioniLed  @fk_int_ScenaRiproduzione = " + fk));
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        //acquisisco le scene
        public String GetContenutiSceneLed()
        {
            try
            {
                string fk = Request.Query["fk_int_Scena"].ToString();
                string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GetContenutiSceneLed  @fk_int_Scena = " + fk));
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        //acquisisco le scene
        public String GetFileScenaLed()
        {
            try
            {
                string fk = Request.Query["fk_int_Scena"].ToString();
                return DB.GetDataFromSQL("select top(1) isnull(fk_int_FileScenaLed,0) from tb_sceneLed where id = " + fk).Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        public String sp_GetObjScenaLed()
        {
            try
            {
                string fk = Request.Query["fk_int_Scena"].ToString();
                return DB.GetDataFromSQL("sp_GetObjScenaLed " + fk).Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        //prendo tutte le scene di una riproduzione
        public String sp_GeneraRiproduzionePeriodo()
        {
            try
            {
                string fk_int_Riproduzione = Request.Query["fk_int_Riproduzione"].ToString();
                string fk_int_Periodo = Request.Query["fk_int_Periodo"].ToString();
                List<object> spl = DB.PopolaListaOggetti(typeof(tb_SceneRiproduzioniLed), "exec sp_GeneraRiproduzionePeriodoLed @fk_int_Riproduzione = " + fk_int_Riproduzione + ", @fk_int_Periodo = " + fk_int_Periodo);
                List<scene_riprodotte_compresse_led> splbis = new List<scene_riprodotte_compresse_led>();
                foreach (tb_SceneRiproduzioniLed riproduzione in spl)
                {
                    scene_riprodotte_compresse_led s = new scene_riprodotte_compresse_led();
                    s.id = riproduzione.ID;
                    s.nid = riproduzione.int_NextID;
                    s.rid = riproduzione.fk_int_Riproduzione;
                    s.sid = riproduzione.fk_int_Scena;
                    s.msecd = riproduzione.int_Durata;
                    s.msdi = riproduzione.int_DataOraInizio;
                    s.msdf = riproduzione.int_DataOraFine;
                    s.start = riproduzione.int_IniziaDa;
                    s.stop = riproduzione.int_StopA;
                    s.tsc = riproduzione.fk_int_SceneEventiSpeciali;
                    s.iper = riproduzione.fk_int_Periodo;
                    // s.psc = riproduzione.int_PrimaScena;
                    // s.usc = riproduzione.int_UltimaScena;
                    s.vel = riproduzione.int_Velocità;
                    s.est = riproduzione.chr_Estremo;
                    splbis.Add(s);
                }
                return JsonConvert.SerializeObject(splbis);
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        //prendo tutte le scene di una riproduzione
        public String sp_GetTimeLineRiproduzione()
        {
            try
            {
                string fk_int_Riproduzione = Request.Query["fk_int_Riproduzione"].ToString();
                List<object> spl = DB.PopolaListaOggetti(typeof(tb_SceneRiproduzioniLed), "exec sp_GetTimelineRiproduzioneLed @fk_int_Riproduzione = " + fk_int_Riproduzione);
                List<scene_riprodotte_compresse_led> splbis = new List<scene_riprodotte_compresse_led>();
                foreach (tb_SceneRiproduzioniLed riproduzione in spl)
                {
                    scene_riprodotte_compresse_led s = new scene_riprodotte_compresse_led();
                    s.id = riproduzione.ID;
                    s.nid = riproduzione.int_NextID;
                    s.rid = riproduzione.fk_int_Riproduzione;
                    s.sid = riproduzione.fk_int_Scena;
                    s.msecd = riproduzione.int_Durata;
                    s.msdi = riproduzione.int_DataOraInizio;
                    s.msdf = riproduzione.int_DataOraFine;
                    s.start = riproduzione.int_IniziaDa;
                    s.stop = riproduzione.int_StopA;
                    s.tsc = riproduzione.fk_int_SceneEventiSpeciali;
                    s.iper = riproduzione.fk_int_Periodo;
                    // s.psc = riproduzione.int_PrimaScena;
                    // s.usc = riproduzione.int_UltimaScena;
                    s.vel = riproduzione.int_Velocità;
                    s.est = riproduzione.chr_Estremo;
                    splbis.Add(s);
                }
                return JsonConvert.SerializeObject(splbis);
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        //prendo tutte le scene di un determinato periodo di riproduzione
        public String sp_GetTimelineRiproduzionePeriodo()
        {
            try
            {
                string fk_int_Riproduzione = Request.Query["fk_int_Riproduzione"].ToString();
                string fk_int_Periodo = Request.Query["fk_int_Periodo"].ToString();
                List<object> spl = DB.PopolaListaOggetti(typeof(tb_SceneRiproduzioniLed), "exec sp_GetTimelineRiproduzionePeriodo @fk_int_Riproduzione = " + fk_int_Riproduzione + ", @fk_int_Periodo = " + fk_int_Periodo);
                List<scene_riprodotte_compresse_led> splbis = new List<scene_riprodotte_compresse_led>();
                foreach (tb_SceneRiproduzioniLed riproduzione in spl)
                {
                    scene_riprodotte_compresse_led s = new scene_riprodotte_compresse_led();
                    s.id = riproduzione.ID;
                    s.nid = riproduzione.int_NextID;
                    s.rid = riproduzione.fk_int_Riproduzione;
                    s.sid = riproduzione.fk_int_Scena;
                    s.msecd = riproduzione.int_Durata;
                    s.msdi = riproduzione.int_DataOraInizio;
                    s.msdf = riproduzione.int_DataOraFine;
                    s.start = riproduzione.int_IniziaDa;
                    s.stop = riproduzione.int_StopA;
                    s.tsc = riproduzione.fk_int_SceneEventiSpeciali;
                    s.iper = riproduzione.fk_int_Periodo;
                    // s.psc = riproduzione.int_PrimaScena;
                    // s.usc = riproduzione.int_UltimaScena;
                    s.vel = riproduzione.int_Velocità;
                    s.est = riproduzione.chr_Estremo;
                    splbis.Add(s);
                }
                return JsonConvert.SerializeObject(splbis);
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        //prendo tutte le scene
        public String GetSceneRiproduzione()
        {
            try
            {
                string fk = Request.Query["fk_int_Riproduzione"].ToString();
                List<object> spl = DB.PopolaListaOggetti(typeof(tb_SceneRiproduzioniLed), "exec sp_GetTimeLineRiproduzione @fk_int_Riproduzione = " + fk);
                List<scene_riprodotte_compresse_led> splbis = new List<scene_riprodotte_compresse_led>();
                foreach (tb_SceneRiproduzioniLed riproduzione in spl)
                {
                    scene_riprodotte_compresse_led s = new scene_riprodotte_compresse_led();
                    s.id = riproduzione.ID;
                    s.nid = riproduzione.int_NextID;
                    s.rid = riproduzione.fk_int_Riproduzione;
                    s.sid = riproduzione.fk_int_Scena;
                    s.msecd = riproduzione.int_Durata;
                    s.msdi = riproduzione.int_DataOraInizio;
                    s.msdf = riproduzione.int_DataOraFine;
                    s.start = riproduzione.int_IniziaDa;
                    s.stop = riproduzione.int_StopA;
                    s.tsc = riproduzione.fk_int_SceneEventiSpeciali;
                    s.iper = riproduzione.fk_int_Periodo;
                    // s.psc = riproduzione.int_PrimaScena;
                    // s.usc = riproduzione.int_UltimaScena;
                    s.vel = riproduzione.int_Velocità;
                    s.est = riproduzione.chr_Estremo;
                    splbis.Add(s);
                }
                return JsonConvert.SerializeObject(splbis);
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
    }
}
