﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NgmPlay.Classi;
using NgmPlay.Utility;
using RenderMachine.Models;


namespace RenderMachine.Controllers
{
    public class AcquisizioneSceneMaxischermoController : Controller
    {





        public String sp_GetObjScenaMaxischermo()
        {
            try { 
            string fk = Request.Query["fk_int_Scena"].ToString();
            return DB.GetDataFromSQL("sp_GetObjScenaMaxischermo " + fk).Rows[0][0].ToString();
        }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
           
            }
}


        public String GetTastiScenePlaylist()
        {
            try { 
            string FK = Request.Query["fk_int_Playlist"].ToString();
            if (FK == "")
            {
                FK = "1";
            }
            return JsonConvert.SerializeObject(DB.GetDataFromSQL("select * from tb_SceneEventiSpecialiMaxischermo where fk_int_Playlist = " + FK));
        }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
           
            }
}




        [HttpPost]
        public string OrdinaScene()
        {
            try { 
            string nva_Ordine = HttpContext.Request.Form["ordine"];

            return DB.GetDataFromSQL("sp_AggiornaOrdinamento '" + nva_Ordine + "'").Rows[0][0].ToString();
        }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
           
            }
}




        public String SuntoContenutiScena()
        {
            try { 
            string FK = Request.Query["fk_int_Scena"].ToString();
            if (FK == "")
            {
                FK = "1";
            }


            return JsonConvert.SerializeObject(DB.GetDataFromSQL("select distinct nva_NomeFile from [tb_contenutiMaxischermo] where not nva_NomeFile is null and fk_int_Scena = " + FK));
        }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
           
            }
}




        public String tb_contenuti()
        {
            try { 
            string FK = Request.Query["fk_int_Scena"].ToString();
            if (FK == "")
            {
                FK = "1";
            }
            return JsonConvert.SerializeObject(DB.GetDataFromSQL("select * from [tb_contenutiMaxischermo] where  fk_int_Scena = " + FK));
        }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
           
            }
}



        public String tb_sceneMaxischermo()
        {
            try { 
            string fk_int_Periodo = Request.Query["fk_int_Periodo"].ToString();
            if (fk_int_Periodo == "")
            {
                fk_int_Periodo = "1";
            }
            string fk_int_Playlist = Request.Query["fk_int_Playlist"].ToString();
            if (fk_int_Playlist == "")
            {
                fk_int_Playlist = "1";
            }
            return JsonConvert.SerializeObject(DB.GetDataFromSQL("select  ROW_NUMBER() OVER(ORDER BY int_Ordine ASC) AS int_Incrementale ,* from [tb_SceneMaxischermo] where fk_int_Playlist = " + fk_int_Playlist + " and  fk_int_Periodo = " + fk_int_Periodo + " order by int_Ordine"));

        }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
           
            }
}


  



        //acquisisco le scene
        public String Index()
        {
            try { 
            string fk = Request.Query["fk_int_ScenaRiproduzione"].ToString();

            string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GetContenutiSceneRiproduzioniMaxischermo  @fk_int_ScenaRiproduzione = " + fk));
            return json;
        }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
           
            }
}



        //acquisisco le scene
        public String GetContenutiSceneMaxischermo()
        {
            try { 

            string fk = Request.Query["fk_int_Scena"].ToString();
            DataTable dt = DB.GetDataFromSQL("select nva_NomeFile from tb_SceneMaxischermo where id = " + fk);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString();
            }
            return "";
        }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception GetTastiScenePlaylist in AcquisizioneSceneLedController:" + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
           
            }

}











    }
}
