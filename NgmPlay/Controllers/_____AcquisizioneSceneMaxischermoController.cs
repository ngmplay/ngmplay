﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NgmPlay.Classi;
using NgmPlay.Utility;
using RenderMachine.Models;


namespace RenderMachine.Controllers
{
    public class AcquisizioneSceneMaxischermoController : Controller
    {

        public String GetPlaylist()
        {
            string FK = Request.Query["fk_int_Match"].ToString();
            if (FK == "")
            {
                FK = "1";
            }
            return JsonConvert.SerializeObject(DB.GetDataFromSQL("select p.*, CONVERT(varchar,dtt_DataCaricamento,103)+' '+ CONVERT(varchar,dtt_DataCaricamento,108)  nva_DataCaricamento, pp.nva_Provenienza  from [tb_PlaylistMaxischermo] p join tb_ProvenienzaPlaylist pp on pp.id= p.fk_int_ProvenienzaPlaylist where p.dtt_Cancellazione is null and  fk_int_Match = " + FK));
        }




        [HttpPost]
        public string inserisciScena([FromBody] scene_riprodotte_compresse_maxischermo sc)
        {
            tb_SceneRiproduzioniMaxischermo sr = new tb_SceneRiproduzioniMaxischermo();
            //s.ID = f.id;
            sr.fk_int_Riproduzione = sc.rid;
            sr.fk_int_Scena = sc.sid;
            sr.int_Durata = sc.msecd;
            sr.int_DataOraInizio =  sc.msdi;
            sr.int_DataOraFine = sc.msdf;
            sr.int_IniziaDa =  sc.start;
            sr.int_StopA =  sc.stop;
          //  sr.int_Progressivo = sc.pro;
            sr.fk_int_Periodo = sc.iper;
            sr.fk_int_TipoScena = sc.tsc;
            sr.int_NextID = sc.nid;
            //sr.int_NextProgressivo = sc.npr;
            //sr.int_PrimaScena = sc.psc;
            sr.chr_Estremo = sc.est;



            sc.id = DB.DB_InsertInto(sr);



            string json = JsonConvert.SerializeObject(sc);
            return json;
        }



        [HttpPost]
        public string salvaScene([FromBody] List<scene_riprodotte_compresse_maxischermo> lsc)
        {
            try
            {
                foreach (scene_riprodotte_compresse_maxischermo sc in lsc) { 
                    tb_SceneRiproduzioniMaxischermo sr = new tb_SceneRiproduzioniMaxischermo();
                    sr.Popola(sc.id);
                    //s.ID = f.id;
                    //s.fk_int_Riproduzione = f.rid;
                    //s.fk_int_Scena = f.sid;
                    sr.int_Durata = sc.msecd;
                    sr.int_DataOraInizio = sc.msdi;
                    sr.int_DataOraFine = sc.msdf;
                    sr.int_IniziaDa = sc.start;
                    sr.int_StopA = sc.stop;
                    sr.int_Velocità = sc.vel;
                    //  sr.int_Progressivo = sc.pro;
                    //s.fk_int_Periodo = f.iper;
                    sr.int_NextID = sc.nid;
                    //s.int_NextProgressivo = f.npr;
                    //sr.int_PrimaScena = sc.psc
                        sr.chr_Estremo = sc.est;
                    sr.DB_Update();
                }

            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return "OK";
        }


        [HttpPost]
        public string salvaScena([FromBody] scene_riprodotte_compresse_maxischermo f)
        {
            try
            {
                tb_SceneRiproduzioniMaxischermo s = new tb_SceneRiproduzioniMaxischermo();
                s.Popola(f.id);
                //s.ID = f.id;
                //s.fk_int_Riproduzione = f.rid;
                //s.fk_int_Scena = f.sid;
                s.int_Durata = f.msecd;
                s.int_DataOraInizio = f.msdi;
                s.int_DataOraFine = f.msdf;
                s.int_IniziaDa = f.start;
                s.int_StopA = f.stop;
                //s.int_Progressivo = f.pro;
                //s.fk_int_Periodo = f.iper;
                s.int_NextID = f.nid;
                //s.int_NextProgressivo = f.npr;
                s.int_Velocità = f.vel;
  //              s.int_PrimaScena = f.psc;
//                s.int_UltimaScena = f.usc;
                s.chr_Estremo = f.est;
                s.DB_Update(); 
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return "OK";
        }



        [HttpPost]
        public string salvaArrayTimelineMatch([FromBody] List<scene_riprodotte_compresse_maxischermo> prova)
        {

            try
            {

                return prova.Count().ToString();
        //        foreach (scene_riprodotte_compresse f in prova)
        //        {
        //            tb_SceneRiproduzioni s = new tb_SceneRiproduzioni();
                    
        //            s.ID =  f.id ;
        //            s.fk_int_Scena = f.sid ;
        //            s.int_Durata = f.msecd ;
        //            s.int_DataOraInizio =  f.msdi ;
        //            s.int_DataOraFine =  f.msdf ;
        //            s.int_IniziaDa =  f.start ;
        //            s.int_StopA = f.stop ;
        //            s.int_Progressivo=   f.pro ;
        //          //  s.fk_int_TipoScena = f.tsc ;
        //            s.fk_int_Periodo =   f.iper  ;
        //            s.int_NextId =  f.nid ;
        //            s.int_PrimaScena = f.psc ;


        //s.DB_Update();

        //        }
           }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return "OK";



        }




        [HttpPost]
        public string salvaArrayTimelineMatch_old()
        {

            try
            {
                List<scene_riprodotte_compresse_maxischermo> src =  JsonConvert.DeserializeObject <List<scene_riprodotte_compresse_maxischermo>>(HttpContext.Request.Form["dati"]);

                foreach (scene_riprodotte_compresse_maxischermo f in src)
                {

                    Console.WriteLine(f.id);

                    //DB.EseguiSQL("delete from tb_Formazione where fk_int_Match = " + f.fk_int_Match + " and  fk_int_Giocatore = " + f.fk_int_Giocatore);
                    //if (f.int_Ordine > 0)
                    //DB.DB_InsertInto(f);
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return "OK";


          
        }





        //acquisisco le scene
        public String Index()
        {
            string fk = Request.Query["fk_int_ScenaRiproduzione"].ToString();

            string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GetContenutiSceneRiproduzioniMaxischermo  @fk_int_ScenaRiproduzione = " + fk));
            return json;
        }








        //prendo tutte le scene di una riproduzione
        public String sp_GeneraRiproduzionePeriodo()
        {
            string fk_int_Riproduzione = Request.Query["fk_int_Riproduzione"].ToString();
            string fk_int_Periodo = Request.Query["fk_int_Periodo"].ToString();      

            List<object> spl = DB.PopolaListaOggetti(typeof(tb_SceneRiproduzioniMaxischermo), "exec sp_GeneraRiproduzionePeriodoMaxischermo @fk_int_Riproduzione = " + fk_int_Riproduzione +", @fk_int_Periodo = " + fk_int_Periodo);

            List <scene_riprodotte_compresse_maxischermo> splbis = new List<scene_riprodotte_compresse_maxischermo>();
            foreach (tb_SceneRiproduzioniMaxischermo riproduzione in spl)
            {
                scene_riprodotte_compresse_maxischermo s = new scene_riprodotte_compresse_maxischermo();
                s.id = riproduzione.ID;
                s.nid = riproduzione.int_NextID;
                s.rid = riproduzione.fk_int_Riproduzione;
                s.sid = riproduzione.fk_int_Scena;
                s.msecd = riproduzione.int_Durata;
                s.msdi = riproduzione.int_DataOraInizio;
                s.msdf = riproduzione.int_DataOraFine;
                s.start = riproduzione.int_IniziaDa;
                s.stop = riproduzione.int_StopA;
                s.tsc = riproduzione.fk_int_TipoScena;
                s.iper = riproduzione.fk_int_Periodo;
                // s.psc = riproduzione.int_PrimaScena;
                // s.usc = riproduzione.int_UltimaScena;
                s.vel = riproduzione.int_Velocità;
                s.est = riproduzione.chr_Estremo;
                splbis.Add(s);
            }
            return JsonConvert.SerializeObject(splbis);


        }













        //prendo tutte le scene di una riproduzione
        public String sp_GetTimeLineRiproduzioneMaxischermo()
        {
            string fk_int_Riproduzione = Request.Query["fk_int_Riproduzione"].ToString();
       

            List<object> spl = DB.PopolaListaOggetti(typeof(tb_SceneRiproduzioniMaxischermo), "exec sp_GetTimelineRiproduzione @fk_int_Riproduzione = " + fk_int_Riproduzione);

            List<scene_riprodotte_compresse_maxischermo> splbis = new List<scene_riprodotte_compresse_maxischermo>();
            foreach (tb_SceneRiproduzioniMaxischermo riproduzione in spl)
            {
                scene_riprodotte_compresse_maxischermo s = new scene_riprodotte_compresse_maxischermo();
                s.id = riproduzione.ID;
                s.nid = riproduzione.int_NextID;
                s.rid = riproduzione.fk_int_Riproduzione;
                s.sid = riproduzione.fk_int_Scena;
                s.msecd = riproduzione.int_Durata;
                s.msdi = riproduzione.int_DataOraInizio;
                s.msdf = riproduzione.int_DataOraFine;
                s.start = riproduzione.int_IniziaDa;
                s.stop = riproduzione.int_StopA;
                s.tsc = riproduzione.fk_int_TipoScena;
                s.iper = riproduzione.fk_int_Periodo;
                // s.psc = riproduzione.int_PrimaScena;
                // s.usc = riproduzione.int_UltimaScena;
                s.vel = riproduzione.int_Velocità;
                s.est = riproduzione.chr_Estremo;
                splbis.Add(s);
            }
            return JsonConvert.SerializeObject(splbis);


        }



        //prendo tutte le scene di un determinato periodo di riproduzione
        public String sp_GetTimelineRiproduzionePeriodo()
        {
            string fk_int_Riproduzione = Request.Query["fk_int_Riproduzione"].ToString();
            string fk_int_Periodo = Request.Query["fk_int_Periodo"].ToString();
          

            List<object> spl = DB.PopolaListaOggetti(typeof(tb_SceneRiproduzioniMaxischermo), "exec sp_GetTimelineRiproduzionePeriodo @fk_int_Riproduzione = " + fk_int_Riproduzione+ ", @fk_int_Periodo = " + fk_int_Periodo);

            List<scene_riprodotte_compresse_maxischermo> splbis = new List<scene_riprodotte_compresse_maxischermo>();
            foreach (tb_SceneRiproduzioniMaxischermo riproduzione in spl)
            {
                scene_riprodotte_compresse_maxischermo s = new scene_riprodotte_compresse_maxischermo();
                s.id = riproduzione.ID;
                s.nid = riproduzione.int_NextID;
                s.rid = riproduzione.fk_int_Riproduzione;
                s.sid = riproduzione.fk_int_Scena;
                s.msecd = riproduzione.int_Durata;
                s.msdi = riproduzione.int_DataOraInizio;
                s.msdf = riproduzione.int_DataOraFine;
                s.start = riproduzione.int_IniziaDa;
                s.stop = riproduzione.int_StopA;
                s.tsc = riproduzione.fk_int_TipoScena;
                s.iper = riproduzione.fk_int_Periodo;
                // s.psc = riproduzione.int_PrimaScena;
                // s.usc = riproduzione.int_UltimaScena;
                s.vel = riproduzione.int_Velocità;
                s.est = riproduzione.chr_Estremo;
                splbis.Add(s);
            }
            return JsonConvert.SerializeObject(splbis);


        }




        //prendo tutte le scene
        public String GetSceneRiproduzione()
        {

 
            string fk= Request.Query["fk_int_Riproduzione"].ToString();

            List<object> spl = DB.PopolaListaOggetti(typeof(tb_SceneRiproduzioniMaxischermo), "exec sp_GetTimeLineRiproduzione @fk_int_Riproduzione = " + fk);

            List<scene_riprodotte_compresse_maxischermo> splbis = new List<scene_riprodotte_compresse_maxischermo>();
            foreach (tb_SceneRiproduzioniMaxischermo riproduzione in spl)
            {
                scene_riprodotte_compresse_maxischermo s = new scene_riprodotte_compresse_maxischermo();
                s.id = riproduzione.ID;
                s.nid = riproduzione.int_NextID;
                s.rid = riproduzione.fk_int_Riproduzione ;
                s.sid = riproduzione.fk_int_Scena;
                s.msecd = riproduzione.int_Durata;
                s.msdi = riproduzione.int_DataOraInizio;
                s.msdf = riproduzione.int_DataOraFine;
                s.start = riproduzione.int_IniziaDa;
                s.stop = riproduzione.int_StopA;
                s.tsc = riproduzione.fk_int_TipoScena;
                s.iper = riproduzione.fk_int_Periodo;
               // s.psc = riproduzione.int_PrimaScena;
               // s.usc = riproduzione.int_UltimaScena;
                s.vel = riproduzione.int_Velocità;
                s.est = riproduzione.chr_Estremo;
                splbis.Add(s);
            }
            return JsonConvert.SerializeObject(splbis);
         

        }
       




    }
}
