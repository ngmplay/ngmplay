﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
//using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NgmPlay;
using NgmPlay.Classi;
using NgmPlay.Utility;
using RenderMachine.Models;
namespace RenderMachine.Controllers
{
    public class CrudController : Controller 
    {
        public IActionResult Index()
        {
            return View();
        }




        public string GetStadi()
        {
            try
            {
                return JsonConvert.SerializeObject(DB.GetDataFromSQL("select  * , case when tin_ConfigurazioneOK =1 then 'img/icon/checked_green.png' else 'img/icon/warning.png' end as img_ConfigurazioneOk   from [tb_Stadi] order by nva_Città "));
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name +" - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod()+": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }

        }

        public string GetTabellaListaGiocatori()
        {
            try
            {
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

                string m = "<table class='table' data-argomento='Giocatori'  id='TabellaCrud'><thead><tr><th>Immagine</th><th>Cognome</th><th>Nome</th><th>Numero</th><th>Squadra</th><th>Categoria</th><th>Azioni</th></tr></thead><tbody>";
                DataTable dt = DB.GetDataFromSQL("select  g.id, g.nva_Nome, g.nva_Cognome, g.nva_Numero, s.nva_Nome nva_NomeSquadra, s.nva_Categoria , s.nva_Directory   from tb_giocatori g join tb_Squadre s on s.id = g.fk_int_Squadra  where g.dtt_Cancellazione is null and s.dtt_Cancellazione is null");
                List<string> contenuto = new List<string>();
                foreach (DataRow dr in dt.Rows)
                {
                    string c = Directory.GetCurrentDirectory();
                    Console.WriteLine(c);
                    if(!System.IO.File.Exists(c+@"\wwwroot\assetsSquadre\" + dr["nva_Directory"] + @"\players\" + dr["nva_Numero"] + ".png"))
                    {

                        if(!System.IO.Directory.Exists(c + @"\wwwroot\assetsSquadre\" + dr["nva_Directory"]))
                        {
                            System.IO.Directory.CreateDirectory(c + @"\wwwroot\assetsSquadre\" + dr["nva_Directory"] + @"\players\");
                        }
                        else if (!System.IO.Directory.Exists(c + @"\wwwroot\assetsSquadre\" + dr["nva_Directory"] + @"\players\"))
                        {
                            System.IO.Directory.CreateDirectory(c + @"\wwwroot\assetsSquadre\" + dr["nva_Directory"] + @"\players\");
                        }
                      
                        System.IO.File.Copy(c + @"\wwwroot\img\GiocatoreSenzaImmagine.png", c + @"\wwwroot\assetsSquadre\" + dr["nva_Directory"] + @"\players\" + dr["nva_Numero"] + ".png");
                        
                    }
                    m = m + "<tr data-id='"+ dr["id"] + "'><td><img width='50' src='/assetsSquadre/" + dr["nva_Directory"] + "/players/" + dr["nva_Numero"] + ".png?"+ unixTimestamp.ToString() + "' /></td><td>" + dr["id"] + " - " + dr["nva_Cognome"] + "</td><td>" + dr["nva_Nome"] + "</td><td>" + dr["nva_Numero"] + "</td><td>" + dr["nva_NomeSquadra"] + "</td><td>" + dr["nva_Categoria"] + "</td><td><button type='button' class='btn btn-primary btn-modifica'>modifica</button> <button type='button' class='btn btn-primary btn-elimina'>elimina</button></td></tr>";
                }
                m = m + "</tbody></table>";
                return m;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }



        public string GetTabellaListaSquadre()
        {
            try
            {
                string m = "<table class='table' data-argomento='Squadre' id='tabellaCrud'><thead><tr><th>Logo</th><th>Nome</th><th>Path</th><th>Categoria</th><th>Azioni</th></tr></thead><tbody>";
                DataTable dt = DB.GetDataFromSQL("select  s.id, s.nva_Nome, s.nva_Categoria, s.nva_Directory   from  tb_Squadre s  where  s.dtt_Cancellazione is null");
                List<string> contenuto = new List<string>();
                foreach (DataRow dr in dt.Rows)
                {
                    string c = Directory.GetCurrentDirectory();
                    Console.WriteLine(c);
                    if (!System.IO.File.Exists(c + @"\wwwroot\assetsSquadre\" + dr["nva_Directory"] + @"\Grafica\logo.png"))
                    {
                        if (!System.IO.Directory.Exists(c + @"\wwwroot\assetsSquadre\" + dr["nva_Directory"]))
                        {
                            System.IO.Directory.CreateDirectory(c + @"\wwwroot\assetsSquadre\" + dr["nva_Directory"] );
                        }

                        if (!System.IO.Directory.Exists(c + @"\wwwroot\assetsSquadre\" + dr["nva_Directory"]+@"\Grafica\"))
                        {
                            System.IO.Directory.CreateDirectory(c + @"\wwwroot\assetsSquadre\" + dr["nva_Directory"] + @"\Grafica\");
                        }

                        if (!System.IO.Directory.Exists(c + @"\wwwroot\assetsSquadre\" + dr["nva_Directory"] + @"\Players\"))
                        {
                            System.IO.Directory.CreateDirectory(c + @"\wwwroot\assetsSquadre\" + dr["nva_Directory"] + @"\Players\");
                        }

                        System.IO.File.Copy(c + @"\wwwroot\img\SquadraSenzaLogo.png", c + @"\wwwroot\assetsSquadre\" + dr["nva_Directory"] + @"\Grafica\logo.png");
                    }
                    m = m + "<tr data-id='" + dr["id"] + "'><td><img width='50' src='/assetsSquadre/" + dr["nva_Directory"] + "/Grafica/logo.png' /></td><td>" + dr["nva_Nome"] + "</td><td>" + dr["nva_Directory"] + "</td><td>" + dr["nva_Categoria"] + "</td><td><button type='button' class='btn btn-primary btn-modifica'>modifica</button> <button type='button' class='btn btn-primary btn-elimina'>elimina</button></td></tr>";
                }
                m = m + "</tbody></table>";
                return m;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }







        public string GetTabellaListaMatch()
        {
            try
            {
                string m = "<table class='table' data-argomento='Match' id='tabellaCrud'><thead><tr><th>partita</th><th>casa</th><th>ospiti</th><th>data</th><th>data</th><th>Azioni</th></tr></thead><tbody>";

                DataTable dt = DB.GetDataFromSQL(@" select m.id, nva_Titolo, 
                            convert(varchar(25), dtt_DataOra, 120) as nva_DataOra ,
                            case when tin_TempiSupplementari =1 then 'SI' else 'NO' end nva_TempiSupplementari, sc.nva_Nome as nva_NomeCasa ,sc.nva_Directory as nva_DirectoryCasa, so.nva_Nome as nva_NomeOspiti,so.nva_Directory as nva_DirectoryOspiti    from tb_Squadre sc
                            join tb_match m  on m.fk_int_SquadraCasa = sc.id
                            join tb_Squadre so  on m.fk_int_SquadraOspite = so.id
                            where   so.dtt_Cancellazione is null and   sc.dtt_Cancellazione is null and   m.dtt_Cancellazione is null
                            ");
                List<string> contenuto = new List<string>();
                foreach (DataRow dr in dt.Rows)
                {
                    m = m + @"<tr data-id='" + dr["id"] + "'>" +
                    "<td><img width='50' src='/assetsSquadre/" + dr["nva_DirectoryCasa"] + "/Grafica/logo.png' /> vs <img width='50' src='/assetsSquadre/" + dr["nva_DirectoryOspiti"] + "/Grafica/logo.png' /></td>" +
                    "<td>" + dr["nva_NomeCasa"] + "</td>" +
                    "<td>" + dr["nva_NomeOspiti"] + "</td>" +
                    "<td>" + dr["nva_DataOra"] + "</td>" +
                    "<td>" + dr["nva_Titolo"] + "</td>" +
                    "<td><button type='button' class='btn btn-primary btn-modifica'>modifica</button></td></tr>";
                }
                m = m + "</tbody></table>";
                return m;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }


        private Boolean IsNumeroGiocatoreDuplicatoinSquadra(int int_numero ,int fk_int_Squadra,int fk_int_Giocatore=0)
        {
          if( Convert.ToInt32(DB.GetDataFromSQL("select count(*) from tb_giocatori where fk_int_Squadra = '" + fk_int_Squadra.ToString() + "' and nva_numero = '" + int_numero.ToString() + "' and (id <> " + fk_int_Giocatore.ToString()+ " or id=0) ").Rows[0][0]) > 0  )
            {
                return true;
            }
            else
            {
                return false;
            }
       
        }


        [HttpPost]
        public string SalvaGiocatore()
        {
            try
            {
                List<String> Errori = new List<string>();
                var f = HttpContext.Request.Form;
                int id = 0;
                tb_Giocatori g; 

                Risposta r = new Risposta();
                try
                {
                     id = Int32.Parse(f["id"]);
                }
                catch (Exception e)
                {
                    id = 0;
                }

                if (f["nva_Nome"].ToString()=="")
                {
                    Errori.Add("Nome richiesto");
                }
                if (f["nva_Cognome"].ToString() == "")
                {
                    Errori.Add(" Cognome richiesto");
                }

                if (!new Regex("^[0-9]{1,2}$").IsMatch(f["nva_Numero"].ToString()))
                {
                    Errori.Add("Campo Numero giocatore non valido (solo numeri massimo 99)");
                }else { 
                    if (IsNumeroGiocatoreDuplicatoinSquadra(Convert.ToInt32(f["nva_Numero"]), Convert.ToInt32(f["fk_int_Squadra"]), id))
                    {
                        Errori.Add("Numero maglia già assegnato ad altro giocatore");
                    }
                }

                if (Errori.Count == 0)
                {

                    if (id > 0)
                    {
                        g = new tb_Giocatori(id);
                        g.nva_Cognome = f["nva_Cognome"];
                        g.nva_Nome = f["nva_Nome"];
                        g.nva_Numero = f["nva_Numero"];
                        g.fk_int_Squadra = Int32.Parse(f["fk_int_Squadra"]);
                        g.DB_Update();
                    }
                    else
                    {

                        
                        g = new tb_Giocatori();
                        g.ID = DB.GetFirstFreeID("tb_Giocatori");
                        g.nva_Cognome = f["nva_Cognome"];
                        g.nva_Nome = f["nva_Nome"];
                        g.nva_Numero = f["nva_Numero"];
                        g.fk_int_Squadra = Int32.Parse(f["fk_int_Squadra"]);
                        id = g.DB_InsertInto();
                    }
             

                    tb_Squadre squadra = new tb_Squadre(g.fk_int_Squadra);
                    string c = Directory.GetCurrentDirectory();
                    var FileCaricato = f.Files["ImmagineGiocatore"];

                    if (!System.IO.Directory.Exists(c + @"\wwwroot\assetsSquadre\" + squadra.nva_Directory + @"\Players\"))
                    {
                        System.IO.Directory.CreateDirectory(c + @"\wwwroot\assetsSquadre\" +squadra.nva_Directory + @"\Players\");
                    }

                    if (FileCaricato is null)
                    {
                        if (!System.IO.File.Exists(c + @"\wwwroot\assetsSquadre\" + squadra.nva_Directory + @"\Players\" + g.nva_Numero + ".png"))
                        {
                            System.IO.File.Copy(c+ @"\wwwroot\img\GiocatoreSenzaImmagine.png",c+  @"\wwwroot\assetsSquadre\" + squadra.nva_Directory + @"\players\" + g.nva_Numero + ".png");
                        }

                    }
                    else
                    {
                        try {
          
                            if (System.IO.File.Exists(c + @"\wwwroot\assetsSquadre\" + squadra.nva_Directory + @"\Players\" + g.nva_Numero + ".png"))
                            {
                                System.IO.File.Delete(c + @"\wwwroot\assetsSquadre\" + squadra.nva_Directory + @"\Players\" + g.nva_Numero + ".png");
                            }
                            using (var stream = System.IO.File.Create(c + @"\wwwroot\assetsSquadre\" + squadra.nva_Directory + @"\Players\" + g.nva_Numero + ".png"))
                            {
                                FileCaricato.CopyTo(stream);
                            }
                        }
                        catch (Exception e)
                        {
                            Errori.Add("Errore caricamento immagine");
                        }
                    }




                }
               // GestioneLog.ArchiviaMessaggioLog("errore:" + r.Errori.ToString());


                r.id = id;
                r.Errori = Errori.ToArray();
                string ss = JsonConvert.SerializeObject(r);
                return ss;
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject("");
            }
        }


        public string eliminaGiocatore(int id = 0)
        {
            try
            {

                if (id == 0)
                {
                    GestioneLog.ArchiviaMessaggioLog(HttpContext.Request.Query["id"], System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                    try
                    {
                        id = Int32.Parse(HttpContext.Request.Query["id"]);
                    }
                    catch (Exception e)
                    {

                    }
                }
                if (id > 0)
                {
                    DB.EseguiSQL("delete from tb_giocatori where id = " + id.ToString());
                }

                return JsonConvert.SerializeObject("");
            }
            catch (Exception ex)
            {


                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }

        }


        public string rimuoviImmagineGiocatore(int id = 0)
        {
            try
            {

                if (id == 0)
                {
                    GestioneLog.ArchiviaMessaggioLog(HttpContext.Request.Query["id"], System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                    try
                    {
                        id = Int32.Parse(HttpContext.Request.Query["id"]);
                    }
                    catch (Exception e)
                    {

                    }
                }
                string c = Directory.GetCurrentDirectory();
                tb_Giocatori g = new tb_Giocatori(id);
                tb_Squadre s = new tb_Squadre(g.fk_int_Squadra);
                if (System.IO.File.Exists(c + @"\wwwroot\assetsSquadre\" + s.nva_Directory + @"\Players\" + g.nva_Numero + ".png"))
                {
                    System.IO.File.Delete(c + @"\wwwroot\assetsSquadre\" + s.nva_Directory + @"\Players\" + g.nva_Numero + ".png");

                }
           
                 System.IO.File.Copy(c + @"\wwwroot\img\GiocatoreSenzaImmagine.png", c + @"\wwwroot\assetsSquadre\" + s.nva_Directory + @"\players\" + g.nva_Numero + ".png");
            
                DataTable dt = DB.GetDataFromSQL(@"select  
                                            g.id, g.nva_Nome, g.nva_Cognome, g.nva_Numero, s.nva_Nome nva_NomeSquadra, s.nva_Categoria , s.nva_Directory, fk_int_Squadra 
                                            from tb_giocatori g join tb_Squadre s on s.id = g.fk_int_Squadra  
                                            where g.dtt_Cancellazione is null and s.dtt_Cancellazione is null and g.id = " + id.ToString());



       
                return JsonConvert.SerializeObject(dt.Rows[0].Table);
            }
            catch (Exception ex)
            {


                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }

        }



        public string GetGiocatore(int id = 0){
            try
            {
           
                if (id == 0)
                {
  GestioneLog.ArchiviaMessaggioLog(HttpContext.Request.Query["id"], System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                    id = Int32.Parse(HttpContext.Request.Query["id"]);
                }
                tb_Giocatori g = new tb_Giocatori(id);
                tb_Squadre s = new tb_Squadre(g.fk_int_Squadra);

                //if (System.IO.File.Exists(System.IO.Directory.GetCurrentDirectory() + @"\wwwroot\assetsSquadre\" + s.nva_Directory + @"\Players\" + g.nva_Numero + ".png"))
                //{
                //    string c = Directory.GetCurrentDirectory();
                //    System.IO.File.Delete(System.IO.Directory.GetCurrentDirectory() + @"\wwwroot\assetsSquadre\" + s.nva_Directory + @"\Players\" + g.nva_Numero + ".png");
                //    System.IO.File.Copy(c + @"\wwwroot\img\GiocatoreSenzaImmagine.png", c + @"\wwwroot\assetsSquadre\" + s.nva_Directory + @"\players\" + g.nva_Numero + ".png");
                //}


                GestioneLog.ArchiviaMessaggioLog("GetGiocatore id:" + id.ToString(), System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                DataTable dt = DB.GetDataFromSQL(@"select  
                                            g.id, g.nva_Nome, g.nva_Cognome, g.nva_Numero, s.nva_Nome nva_NomeSquadra, s.nva_Categoria , s.nva_Directory, fk_int_Squadra 
                                            from tb_giocatori g join tb_Squadre s on s.id = g.fk_int_Squadra  
                                            where g.dtt_Cancellazione is null and s.dtt_Cancellazione is null and g.id = " + id.ToString());



                GestioneLog.ArchiviaMessaggioLog(JsonConvert.SerializeObject(dt.Rows[0].Table), System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return JsonConvert.SerializeObject(dt.Rows[0].Table);
            }
            catch (Exception ex)
            {


                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();

            }

        }





        public string PopolaListaSquadre()
        {
            try
            {
              
                DataTable dt = DB.GetDataFromSQL(@"select id,nva_Nome  from tb_squadre order by nva_nome ");

                String m = "";

                foreach (DataRow dr in dt.Rows)
                {

                    m = m + @"<option value='"+dr["id"]+"' >"+ dr["nva_Nome"] + "</option>";


                }
            
                return m;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }






        public string CleanDisposizioneLedStadio()
        {
            try
            {
                string fk_int_stadio = Request.Query["fk_int_stadio"].ToString();
                DB.EseguiSQL("update tb_DisposizioneLed set dtt_Cancellazione = getdate() where fk_int_Stadio = " + fk_int_stadio);
                return "OK";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name +" - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod()+": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }




        [HttpPost]
        public string RicevoFormConfig()
        {
            try { 
                var FileCaricato = HttpContext.Request.Form.Files["FileDaCaricare"];
                int ID = Int32.Parse(HttpContext.Request.Form["ID"]);
                var result = string.Empty;
                using (var reader = new StreamReader(FileCaricato.OpenReadStream()))
                {
                    result = reader.ReadToEnd();
                }
                tb_ImportFileConfigurator ifc = new tb_ImportFileConfigurator();
                ifc.nva_Contenuto = result;
                ifc.fk_int_Stadio = ID;
                ImportazioneConfig.AcquisisciConfigurazioneStadio(ifc.fk_int_Stadio, ifc.nva_Contenuto);
                ifc.DB_InsertInto();
                return "File di configurazione importato correttamente"; // JsonConvert.SerializeObject(DB.GetDataFromSQL("SELECT (CONVERT(varchar,m.dtt_DataOra,103)+' '+s_casa.nva_Nome+' VS '+s_ospite.nva_Nome) nva_Titolo , m.ID, m.fk_int_SquadraCasa, m.fk_int_SquadraOspite, s_casa.nva_Nome as nva_NomeSquadraCasa, s_ospite.nva_Nome as nva_NomeSquadraOspite, CONVERT(varchar,m.dtt_DataOra,103) as nva_DataOra FROM [tb_Match] m join  tb_Squadre s_casa on s_casa.id = m.fk_int_SquadraCasa join tb_Squadre s_ospite on s_ospite.id = m.fk_int_SquadraOspite where dtt_DataOra >= cast(getdate() as date)"));

            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return "errore importazione file di configurazione"; // JsonConvert.SerializeObject(DB.GetDataFromSQL("SELECT (CONVERT(varchar,m.dtt_DataOra,103)+' '+s_casa.nva_Nome+' VS '+s_ospite.nva_Nome) nva_Titolo , m.ID, m.fk_int_SquadraCasa, m.fk_int_SquadraOspite, s_casa.nva_Nome as nva_NomeSquadraCasa, s_ospite.nva_Nome as nva_NomeSquadraOspite, CONVERT(varchar,m.dtt_DataOra,103) as nva_DataOra FROM [tb_Match] m join  tb_Squadre s_casa on s_casa.id = m.fk_int_SquadraCasa join tb_Squadre s_ospite on s_ospite.id = m.fk_int_SquadraOspite where dtt_DataOra >= cast(getdate() as date)"));
            }
        }



     //   [HttpPost]
        //public string RicevoFormPlaylist()
        //{
        //    try
        //    {
        //        var FileCaricato = HttpContext.Request.Form.Files["FileDaCaricare"];
        //        int ID = Int32.Parse(HttpContext.Request.Form["ID"]);
        //        var result = string.Empty;
        //        using (var reader = new StreamReader(FileCaricato.OpenReadStream()))
        //        {
        //            result = reader.ReadToEnd();
        //        }
        //        tb_ImportFileConfigurator ifc = new tb_ImportFileConfigurator();
        //        ifc.nva_Contenuto = result;
        //        ifc.fk_int_Stadio = ID;
        //        ImportazioneConfig.AcquisisciConfigurazioneStadio(ifc.fk_int_Stadio, ifc.nva_Contenuto);
        //        ifc.DB_InsertInto();
        //    }
        //    catch (Exception ex)
        //    {
        //        return "errore importazione file di configurazione"; // JsonConvert.SerializeObject(DB.GetDataFromSQL("SELECT (CONVERT(varchar,m.dtt_DataOra,103)+' '+s_casa.nva_Nome+' VS '+s_ospite.nva_Nome) nva_Titolo , m.ID, m.fk_int_SquadraCasa, m.fk_int_SquadraOspite, s_casa.nva_Nome as nva_NomeSquadraCasa, s_ospite.nva_Nome as nva_NomeSquadraOspite, CONVERT(varchar,m.dtt_DataOra,103) as nva_DataOra FROM [tb_Match] m join  tb_Squadre s_casa on s_casa.id = m.fk_int_SquadraCasa join tb_Squadre s_ospite on s_ospite.id = m.fk_int_SquadraOspite where dtt_DataOra >= cast(getdate() as date)"));
        //    }
        //    return "File di configurazione importato correttamente"; // JsonConvert.SerializeObject(DB.GetDataFromSQL("SELECT (CONVERT(varchar,m.dtt_DataOra,103)+' '+s_casa.nva_Nome+' VS '+s_ospite.nva_Nome) nva_Titolo , m.ID, m.fk_int_SquadraCasa, m.fk_int_SquadraOspite, s_casa.nva_Nome as nva_NomeSquadraCasa, s_ospite.nva_Nome as nva_NomeSquadraOspite, CONVERT(varchar,m.dtt_DataOra,103) as nva_DataOra FROM [tb_Match] m join  tb_Squadre s_casa on s_casa.id = m.fk_int_SquadraCasa join tb_Squadre s_ospite on s_ospite.id = m.fk_int_SquadraOspite where dtt_DataOra >= cast(getdate() as date)"));
        //}
    }
}
