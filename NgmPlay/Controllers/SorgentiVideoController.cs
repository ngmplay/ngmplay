﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
//using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NgmPlay.Classi;
using NgmPlay.Utility;
using RenderMachine.Models;
using ControlloLnsn;
namespace RenderMachine.Controllers
{



    public class SorgentiVideoController : Controller 
    {



        public IActionResult Index()
        {

            string m = "<table class='table table-bordered table-condensed tableLuminosita'><thead><tr><th width='13%'>Zone campo</th><th  width='2%'></th><th width='5%' ></th><th width='10%'>Luminosità %</th><th width='5%'></th><th width='47%'></th><th width='5%'></th><th width='8%'>Offset %</th><th width='5%'></th>  </tr></thead><tbody>";

            DataTable dtTot = DB.GetDataFromSQL("select l.int_Valore from tb_Luminosita l  where l.nva_Settore = 'Tutti' and dtt_Cancellazione is null");
            int LuminositaTutti = 0;
            if (dtTot.Rows.Count > 0)
            {
                LuminositaTutti = Convert.ToInt32(dtTot.Rows[0][0]);
            }
            m = m + "<tr Id='Luminosita_Tutti'><td><b>Tutti</b></td><td></td><td><button type='button'  class='btn btn-success btn-LuminositaDown Tutti'  >-</button></td><td class='valoreLuminosita Tutti'>0</td><td><button type='button'  class='btn btn-success btn-LuminositaUp Tutti'  >+</button></td><td><input  style='width:100%' type='range' min='10' max='100' value='10' step='1' class='form-control-range slider luminosita Tutti' ></td><td></td><td></td><td></td></tr>";

            tb_Stadi s = new tb_Stadi(StaticStartConfig.ID_int_Stadio);

            DataTable dt = DB.GetDataFromSQL("select distinct nva_Nome, min(int_MarginT), min(int_MarginL) from tb_DisposizioneLed where fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString() + " and fk_int_Layer = 6 and dtt_Cancellazione is null group by nva_nome  order by min(int_MarginT), min(int_MarginL), nva_nome");

         //   DataTable dt = DB.GetDataFromSQL("select distinct dl.nva_Nome, isnull(l.int_Valore,50) int_Valore, isnull(l.int_OffsetPercentualeVariazioneGlobale,0) int_OffsetPercentualeVariazioneGlobale, dl.id fk_int_DisposizioneLed from tb_DisposizioneLed dl left join tb_Luminosita l on l.fk_int_DisposizioneLed = dl.id  where dl.fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString() + " and dl.fk_int_Layer = 6 and dl.dtt_Cancellazione is null and l.dtt_Cancellazione is null  order by dl.int_MarginT, dl.int_MarginL");
           
     

            int numeroElementi = 0;
            //   int valoretotale = 0;
 
            foreach (DataRow dr in dt.Rows)
            {
                /*
                int int_Valore = 50;
                int int_OffsetPercentualeVariazioneGlobale = 0;
                DataTable dtl = DB.GetDataFromSQL("select top(1) isnull(int_Valore,50) int_Valore, isnull(int_OffsetPercentualeVariazioneGlobale,0) int_OffsetPercentualeVariazioneGlobale from tb_DisposizioneLed dl  where nva_Settore = '"+ dr["nva_Nome"].ToString().Replace(" ", "_") + "' dtt_Cancellazione is null");
                if (dtl.Rows.Count > 0)
                {

                    int_Valore = Convert.ToInt32(dtl.Rows[0]["int_Valore"]);
                    int_OffsetPercentualeVariazioneGlobale =  Convert.ToInt32(dtl.Rows[0]["int_OffsetPercentualeVariazioneGlobale"]);
                }
                */
                String NomeFormattato = dr["nva_Nome"].ToString().Replace(" ", "_");
               numeroElementi = numeroElementi + 1;
               // valoretotale = valoretotale + Convert.ToInt32(dr["int_Valore"]);
                m = m + "<tr  Id='Luminosita_" + NomeFormattato + "' ><td>" + dr["nva_Nome"].ToString().Replace("_"," ")  + "</td><td><input class='checkbox-2x spostaAncheMe' type='checkbox'/></td><td><button type='button'  class='btn btn-danger btn-LuminositaDown'  >-</button></td><td class='valoreLuminosita'>0</td><td><button type='button'  class='btn btn-danger btn-LuminositaUp'  >+</button></td><td><input   style='width:100%' type='range' min='10' max='100' value='10' step='1' class='form-control-range slider luminosita singolosettore luminosita_" + NomeFormattato + "'   data-settore='" + NomeFormattato + "'  data-target='" + NomeFormattato + "'></td><td><button type='button'  class='btn btn-warning btn-LuminositaOffsetDown'  >-</button></td><td class='valoreOffsetLuminosita'>0</td><td><button type='button'  class='btn btn-warning btn-LuminositaOffsetUp'  >+</button></td></tr>";
            }
          
            m = m + "</tbody></table>";


            try { 
            if (StaticStartConfig.Stadio.nva_SendingCardMaxischermo.ToLower() == "lnsn")
            {

                m = m + "<table class='table table-bordered table-condensed tableLuminosita'><tr Id='Luminosita_Maxischermo'><td width='13%'>Maxischermo</td><td  width='2%'></td><td  width='5%'><button type='button'  class='btn btn-success btn-LuminositaDown Maxischermo'  >-</button></td><td width='10%' class='valoreLuminosita Maxischermo'>0</td><td width='5%'><button type='button'  class='btn btn-success btn-LuminositaUp Maxischermo'  >+</button></td><td width='47%'><input  style='width:100%' type='range' min='10' max='100' value='10' step='1' class='form-control-range slider luminosita luminosita_Maxischermo Maxischermo' /></td><td width='18%'></td></tr></table>";            
            }
            }catch(Exception e )
            {

            }




            ViewBag.ModalRegolazioneLuminosita = m;

            



       
            ViewBag.InfoStadio = "Stadio:"+ StaticStartConfig.Stadio.nva_Nome + " (" + StaticStartConfig.Stadio.nva_Città + ") PC:" +  StaticStartConfig.int_IdentificazionePC.ToString();

            ViewBag.fk_int_Stadio = NgmPlay.Classi.StaticStartConfig.ID_int_Stadio.ToString();

            string eventi = "<table class='table'><thead><tr><th>Evento</th><th>Modalità riproduzione</th></tr></thead><tbody>";
            DataTable dte = DB.GetDataFromSQL("select id,nva_Codice from tb_Periodi  where fk_int_TipoPeriodo = 2 and dtt_Cancellazione is null ");

            if (dte.Rows.Count > 0) { 
                foreach (DataRow dr in dte.Rows)
                {
                    string Riproduzioni = "1";
                    string Nome = "";
                    if (dr["nva_Codice"].ToString().Split(":").Length > 1) {
                        Riproduzioni = dr["nva_Codice"].ToString().Split(":")[1].ToUpper(); 
                        Nome= dr["nva_Codice"].ToString().Split(":")[0].ToUpper();
                    }
                    else
                    {
                        Riproduzioni = "LOOP";
                        Nome = dr["nva_Codice"].ToString();
                    }
                    eventi = eventi + "<tr><td><button type=\"button\"  class=\"h-100 btn btn-lg flash-button btn-block btn-periodi idPeriodo_" + dr["id"] + " btn-danger\"   data-fk_int_periodo=\"" + dr["id"] + "\">" + Nome + "</button></td><td>"+Riproduzioni+"</td></tr>";
                }
                eventi = eventi + "</tbody></table>";
                ViewBag.ModalEventi = eventi;
            }else{
                ViewBag.ModalEventi = "";
            }



            string periodi = "";
            DataTable dtep = DB.GetDataFromSQL("select id,nva_Codice,nva_Nome from tb_Periodi  where fk_int_TipoPeriodo = 1 and dtt_Cancellazione is null");
            foreach (DataRow dr in dtep.Rows)
            {
  
                    string Nome = "";
                    Nome = dr["nva_Nome"].ToString();
                    periodi = periodi + "<div class='SuntoCaricamentoScene'  data-fk_int_periodo=\"" + dr["id"] + "\">" + Nome + " <span class='NumeroSceneCaricatePeriodo'></span>" +
                    "<button type=\"button\" class=\"btn btn-success btn-UnloadPeriodo  btn-lg flash-button  \" data-fk_int_periodo=\"" + dr["id"] + "\">Scarica</button>" +
                    "<button type=\"button\" class=\"btn btn-success btn-LoadPeriodo   btn-lg flash-button  \" data-fk_int_periodo=\"" + dr["id"] + "\">Carica</button>" +
                    "</div>";
            }
            periodi = periodi + "";
            ViewBag.ModalGestisciPeriodiPreload = periodi;



            return View();

        }



        [HttpPost]
        public string ArchiviaLuminosita([FromBody] List<tb_Luminosita> tb_LuminositaList)
        {
            try
            {
                foreach (tb_Luminosita l in tb_LuminositaList)
                {
                    GestioneLog.ArchiviaMessaggioLog("Setto Lux " + l.nva_Settore, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                    DB.EseguiSQL("exec sp_ArchiviaLuminosita @nva_Settore='" + l.nva_Settore + "', @int_Valore=" + l.int_Valore.ToString()+ ", @int_OffsetPercentualeVariazioneGlobale=" + l.int_OffsetPercentualeVariazioneGlobale.ToString());
                    if ((l.nva_Settore == "Maxischermo")&&(StaticStartConfig.Stadio.nva_SendingCardMaxischermo.ToLower()!=""))
                    {

                        GestioneLog.ArchiviaMessaggioLog("Setto Lux Maxischermo", System.Reflection.MethodInfo.GetCurrentMethod().ToString());

                        //SetLuminositaMaxischermo(l.int_Valore);
                    }
                }
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());

                return ex.Message.ToString();
            }
            return "OK";
        }





        void SetLuminositaMaxischermo(int val)
        {
            try
            {
                switch (StaticStartConfig.Stadio.nva_SendingCardMaxischermo.ToLower()){
                    case "lnsn":

                        ControlloLnsn.SendLnsn l = new ControlloLnsn.SendLnsn();
                        l.SetBrightness((byte)val);
            
                    break;
                }
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());

            }

        }

        public  String GetStatusMaxischermoFromCard()
        {
            try
            {
                switch (StaticStartConfig.Stadio.nva_SendingCardMaxischermo.ToLower())
                {
                    case "lnsn":
                        ControlloLnsn.SendLnsn l = new ControlloLnsn.SendLnsn();
                        //return l.GetStatus();
                        break;
                }

                return "";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());

            }
            return "";
        }


        public string GetLuminosita()
        {
            List<tb_Luminosita> tb_LuminositaList = new List<tb_Luminosita>();
            DataTable dt = DB.GetDataFromSQL("select ID from tb_Luminosita where dtt_Cancellazione is null");
            foreach (DataRow r in dt.Rows)
            {
                tb_LuminositaList.Add(new tb_Luminosita(Convert.ToInt32(r["ID"])));

            }
            return JsonConvert.SerializeObject(tb_LuminositaList);
        }



    }
}
