﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RenderMachine.Models;


namespace RenderMachine.Controllers
{
    public class acquisizione_sceneController : Controller
    {




        //acquisisco le scene
        public String Index()
        {


            string fk_int_Scena = Request.Query["fk_int_scena"].ToString();
            if (fk_int_Scena == "")
            {
                fk_int_Scena = "1";
            }
  

            string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GetSceneContenuti  @fk_int_Scena = " + fk_int_Scena));

            return json;
      
        }




        //prendo tutte le scene
        public String GetScenePlaylist()
        {

           // string fk_int_Match = Request.Query["fk_int_Match"].ToString();
            string fk_int_Periodo = Request.Query["fk_int_Periodo"].ToString();
            string fk_int_Playlist = Request.Query["fk_int_Playlist"].ToString();



            List<object> Scene = DB.PopolaListaOggetti(typeof(RenderMachine.Models.sp_GetTimeLine), DB.GetDataFromSQL("exec sp_GetTimeLine @fk_int_Periodo = " + fk_int_Periodo + ", @fk_int_Playlist = " + fk_int_Playlist ));

            Dictionary<Int64, object> dictScene = new Dictionary<Int64, object>();

            object[] myList= new object[Scene.Count];

            foreach (sp_GetTimeLine scena in Scene)
            {

                // myList.Add(JsonConvert.SerializeObject(new { ID = scena.ID, Durata = scena.int_Durata, Progressivo = scena.int_Progressivo, fk_int_TipoScena = scena.fk_int_TipoScena }));
               //  string obj = JsonConvert.SerializeObject(new { ID = scena.ID, Durata= scena.int_Durata, Progressivo = scena.int_Progressivo, fk_int_TipoScena = scena.fk_int_TipoScena  });

                myList[scena.int_Progressivo]= (new { ID = scena.ID, Durata = scena.int_Durata, IniziaDa = scena.int_IniziaDa, StopA = scena.int_StopA, Progressivo = scena.int_Progressivo, fk_int_TipoScena = scena.fk_int_TipoScena });


            }



            string json = JsonConvert.SerializeObject(myList);
            return json;
        }




        //public string GetProgressivoScenaSpecificoMinutaggio()
        //{

        //    //string fk_int_Periodi2Stadi = Request.Query["fk_int_Periodi2Stadi"].ToString();
        //    //string fk_int_Playlist = Request.Query["fk_int_Playlist"].ToString();
        //    //string int_SecondoRiproduzione = Request.Query["int_SecondoRiproduzione"].ToString();

        //    //sp_GetScenePlaylist Scene = DB.PopolaSingoloOggetto(typeof(sp_GetScenePlaylist), DB.GetDataFromSQL("exec sp_GetProgressivoScena @fk_int_Periodi2Stadi = " + fk_int_Periodi2Stadi + ", @fk_int_Playlist = " + fk_int_Playlist));

        //    //string json = JsonConvert.SerializeObject(dictScene);
        //    //return json;

        //}








        //prendo tutte le scene mettendo come chiave un incrementale temporale per far in modo di ricercare la scena con più facilità
        /*
        public String GetScenePlaylistIncrementaleInizio()
        {

            string fk_int_Periodi2Stadi = Request.Query["fk_int_Periodi2Stadi"].ToString();
            string fk_int_Playlist = Request.Query["fk_int_Playlist"].ToString();


            List<object> Scene = DB.PopolaListaOggetti(typeof(RenderMachine.Models.sp_GetScenePlaylist), DB.GetDataFromSQL("exec sp_GetTimeLine @fk_int_Periodi2Stadi = " + fk_int_Periodo + ", @fk_int_Playlist = " + fk_int_Playlist));

            Dictionary<Int64, object> dictScene = new Dictionary<Int64, object>();

            foreach (sp_GetScenePlaylist scena in Scene)
            {

                object obj = new { ID = scena.ID, Durata = scena.int_Durata, Progressivo = scena.int_Progressivo };
                dictScene.Add(scena.int_Progressivo, obj);

            }



            string json = JsonConvert.SerializeObject(dictScene);
            return json;
        }
        */




    }
}
