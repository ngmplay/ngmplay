﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
//using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NgmPlay.Classi;
using NgmPlay.Utility;
using RenderMachine.Models;
using ControlloLnsn;
namespace RenderMachine.Controllers
{
    public class HomeController : Controller 
    {
        public IActionResult Index()
        {

            string m = "<table class='table table-bordered table-condensed tableLuminosita'><thead><tr><th width='13%'>Zone campo</th><th  width='2%'></th><th width='5%' ></th><th width='10%'>Luminosità %</th><th width='5%'></th><th width='47%'></th><th width='5%'></th><th width='8%'>Offset %</th><th width='5%'></th>  </tr></thead><tbody>";

            DataTable dtTot = DB.GetDataFromSQL("select l.int_Valore from tb_Luminosita l  where l.nva_Settore = 'Tutti' and dtt_Cancellazione is null");
            int LuminositaTutti = 0;
            if (dtTot.Rows.Count > 0)
            {
                LuminositaTutti = Convert.ToInt32(dtTot.Rows[0][0]);
            }
            m = m + "<tr Id='Luminosita_Tutti'><td><b>Tutti</b></td><td></td><td><button type='button'  class='btn btn-success btn-LuminositaDown Tutti'  >-</button></td><td class='valoreLuminosita Tutti'>0</td><td><button type='button'  class='btn btn-success btn-LuminositaUp Tutti'  >+</button></td><td><input  style='width:100%' type='range' min='10' max='100' value='10' step='1' class='form-control-range slider luminosita Tutti' ></td><td></td><td></td><td></td></tr>";

            tb_Stadi s = new tb_Stadi(StaticStartConfig.ID_int_Stadio);

            DataTable dt = DB.GetDataFromSQL("select distinct nva_Nome, min(int_MarginT), min(int_MarginL) from tb_DisposizioneLed where fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString() + " and fk_int_Layer = 6 and dtt_Cancellazione is null group by nva_nome  order by min(int_MarginT), min(int_MarginL), nva_nome");

         //   DataTable dt = DB.GetDataFromSQL("select distinct dl.nva_Nome, isnull(l.int_Valore,50) int_Valore, isnull(l.int_OffsetPercentualeVariazioneGlobale,0) int_OffsetPercentualeVariazioneGlobale, dl.id fk_int_DisposizioneLed from tb_DisposizioneLed dl left join tb_Luminosita l on l.fk_int_DisposizioneLed = dl.id  where dl.fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString() + " and dl.fk_int_Layer = 6 and dl.dtt_Cancellazione is null and l.dtt_Cancellazione is null  order by dl.int_MarginT, dl.int_MarginL");
           
     

            int numeroElementi = 0;
            //   int valoretotale = 0;
 
            foreach (DataRow dr in dt.Rows)
            {

                /*
                int int_Valore = 50;
                int int_OffsetPercentualeVariazioneGlobale = 0;
                DataTable dtl = DB.GetDataFromSQL("select top(1) isnull(int_Valore,50) int_Valore, isnull(int_OffsetPercentualeVariazioneGlobale,0) int_OffsetPercentualeVariazioneGlobale from tb_DisposizioneLed dl  where nva_Settore = '"+ dr["nva_Nome"].ToString().Replace(" ", "_") + "' dtt_Cancellazione is null");
                if (dtl.Rows.Count > 0)
                {
                    int_Valore = Convert.ToInt32(dtl.Rows[0]["int_Valore"]);
                    int_OffsetPercentualeVariazioneGlobale =  Convert.ToInt32(dtl.Rows[0]["int_OffsetPercentualeVariazioneGlobale"]);
                }
                */

                String NomeFormattato = dr["nva_Nome"].ToString().Replace(" ", "_");
               numeroElementi = numeroElementi + 1;
               // valoretotale = valoretotale + Convert.ToInt32(dr["int_Valore"]);
                m = m + "<tr  Id='Luminosita_" + NomeFormattato + "' ><td>" + dr["nva_Nome"].ToString().Replace("_"," ")  + "</td><td><input class='checkbox-2x spostaAncheMe' type='checkbox'/></td><td><button type='button'  class='btn btn-danger btn-LuminositaDown'  >-</button></td><td class='valoreLuminosita'>0</td><td><button type='button'  class='btn btn-danger btn-LuminositaUp'  >+</button></td><td><input   style='width:100%' type='range' min='10' max='100' value='10' step='1' class='form-control-range slider luminosita singolosettore luminosita_" + NomeFormattato + "'   data-settore='" + NomeFormattato + "'  data-target='" + NomeFormattato + "'></td><td><button type='button'  class='btn btn-warning btn-LuminositaOffsetDown'  >-</button></td><td class='valoreOffsetLuminosita'>0</td><td><button type='button'  class='btn btn-warning btn-LuminositaOffsetUp'  >+</button></td></tr>";
            }
          
            m = m + "</tbody></table>";


            try { 
                if (StaticStartConfig.Stadio.nva_SendingCardMaxischermo.ToLower() == "lnsn"){
                    m = m + "<table class='table table-bordered table-condensed tableLuminosita'><tr Id='Luminosita_Maxischermo'><td width='13%'>Maxischermo</td><td  width='2%'></td><td  width='5%'><button type='button'  class='btn btn-success btn-LuminositaDown Maxischermo'  >-</button></td><td width='10%' class='valoreLuminosita Maxischermo'>0</td><td width='5%'><button type='button'  class='btn btn-success btn-LuminositaUp Maxischermo'  >+</button></td><td width='47%'><input  style='width:100%' type='range' min='10' max='100' value='10' step='1' class='form-control-range slider luminosita luminosita_Maxischermo Maxischermo' /></td><td width='18%'></td></tr></table>";            
                }
            }catch(Exception e ){

            }




            ViewBag.ModalRegolazioneLuminosita = m;

            



       
            ViewBag.InfoStadio = "Stadio:"+ StaticStartConfig.Stadio.nva_Nome + " (" + StaticStartConfig.Stadio.nva_Città + ") PC:" +  StaticStartConfig.int_IdentificazionePC.ToString();

            ViewBag.fk_int_Stadio = NgmPlay.Classi.StaticStartConfig.ID_int_Stadio.ToString();






            //string eventi = "<table class='table'><thead><tr><th>Evento</th><th>Modalità riproduzione</th></tr></thead><tbody>";
            //DataTable dte = DB.GetDataFromSQL("select id,nva_Codice from tb_Periodi  where fk_int_TipoPeriodo = 2 and id>100 and dtt_Cancellazione is null ");

            //if (dte.Rows.Count > 0) { 
            //    foreach (DataRow dr in dte.Rows)
            //    {
            //        string Riproduzioni = "1";
            //        string Nome = "";
            //        if (dr["nva_Codice"].ToString().Split(":").Length > 1) {
            //            Riproduzioni = dr["nva_Codice"].ToString().Split(":")[1].ToUpper(); 
            //            Nome= dr["nva_Codice"].ToString().Split(":")[0].ToUpper();
            //        }
            //        else
            //        {
            //            Riproduzioni = "LOOP";
            //            Nome = dr["nva_Codice"].ToString();
            //        }
            //        eventi = eventi + "<tr><td><button type=\"button\"  class=\"h-100 btn btn-lg flash-button btn-block btn-periodi idPeriodo_" + dr["id"] + " btn-danger\"   data-fk_int_periodo=\"" + dr["id"] + "\">" + Nome + "</button></td><td>"+Riproduzioni+"</td></tr>";
            //    }
            //    eventi = eventi + "</tbody></table>";
            //    ViewBag.ModalEventi = eventi;
            //}else{
            //    ViewBag.ModalEventi = "";
            //}







            string eventi = "<table class='table' style='width:100%'>";
            DataTable dte = DB.GetDataFromSQL(@"select distinct p.id,p.nva_Codice,p.nva_TipoTerminazione,p.nva_NumeroRiproduzioni, 
                            tap.dtt_TimerOraInizio, tap.fk_int_PeriodoContenitore, tap.int_TimerSecondiDaInizioContenitore,
                            p2.nva_Codice AS CodiceContenitore
                            from tb_Periodi p
                            join
                            (select fk_int_periodo from tb_SceneLed where fk_int_Playlist = (select top(1) id from tb_Playlist where IsSelected = 1)
                            UNION
                            select fk_int_periodo from tb_SceneMaxischermo where fk_int_Playlist = (select top(1) id from tb_Playlist where IsSelected = 1)) sl 
                            on sl.fk_int_Periodo = p.id
                            left join (SELECT * FROM tb_TimerAvvioPeriodi where fk_int_Playlist = (select top(1) id from tb_Playlist where IsSelected = 1)) tap on p.id=tap.fk_int_Periodo
                            LEFT JOIN tb_Periodi p2 on tap.fk_int_PeriodoContenitore = p2.id 
                            where p.fk_int_TipoPeriodo = 2 and p.id > 100 and  p.dtt_Cancellazione is null 
                            order by p.nva_Codice ");



            if (dte.Rows.Count > 0)
            {

                foreach (DataRow dr in dte.Rows)
                {
                    eventi += "<tr>";

                    string Riproduzioni = "1";
                    string Nome = "";
                    string TipoTerminazione = "bp";
                    bool avvioOraFissa = true;
                    bool avvioOraRelativa = true;
                    if (dr["nva_Codice"].ToString().Split(":").Length > 1)
                    {
                        Riproduzioni = dr["nva_Codice"].ToString().Split(":")[1].ToUpper();
                        Nome = dr["nva_Codice"].ToString().Split(":")[0].ToUpper();
                        TipoTerminazione = dr["nva_TipoTerminazione"].ToString();
                        if ("LOOP".Equals(dr["nva_NumeroRiproduzioni"].ToString()))
                        {
                            TipoTerminazione = "LOOP";
                            Riproduzioni = "LOOP";
                        }
                    }
                    else
                    {
                        Riproduzioni = "LOOP";
                        Nome = dr["nva_Codice"].ToString();
                        TipoTerminazione = dr["nva_TipoTerminazione"].ToString();
                        if ("LOOP".Equals(dr["nva_NumeroRiproduzioni"].ToString()))
                        {
                            TipoTerminazione = "LOOP";
                            Riproduzioni = "LOOP";
                        }
                    }

                    string oraInizioAut = "AVVIO ORE";
                    if(dr["dtt_TimerOraInizio"]!=null && dr["dtt_TimerOraInizio"] != DBNull.Value)
                    {
                        oraInizioAut = ((DateTime)dr["dtt_TimerOraInizio"]).ToString("HH:mm");
                        avvioOraFissa = true;
                        avvioOraRelativa = false;
                    }

                    string secondiInizioContenitoreAut = "AVVIO IN PER.";
                    int periodoContenitore = 0;
                    if (dr["int_TimerSecondiDaInizioContenitore"] != null && dr["int_TimerSecondiDaInizioContenitore"] != DBNull.Value &&
                        dr["fk_int_PeriodoContenitore"] != null && dr["fk_int_PeriodoContenitore"] != DBNull.Value)
                    {
                        periodoContenitore = ValueOperations.DbToInteger(dr["fk_int_PeriodoContenitore"]);
                        int minuti = (int)(ValueOperations.DbToInteger(dr["int_TimerSecondiDaInizioContenitore"]) / 60);
                        int secondi = ValueOperations.DbToInteger(dr["int_TimerSecondiDaInizioContenitore"]) % 60;
                        secondiInizioContenitoreAut = minuti.ToString().PadLeft(2,'0') + ":" + secondi.ToString().PadLeft(2, '0') + " " + dr["CodiceContenitore"].ToString();
                        avvioOraFissa = false;
                        avvioOraRelativa = true;
                    }

                    eventi += "<td style=\"width:30%;text-align:left;padding-left:40px\"><div class=\"btn-group\" role=\"group\" >";
                    eventi = eventi + "<button type=\"button\" class=\"btn btn-lg btn-secondary evento_TipoEsecuzione" + ("LOOP".Equals(TipoTerminazione) ? " active" : "") + "\" data-tipoesecuzione=\"LOOP\" data-idperiodo=\""+ dr["id"] + "\">Loop</button>";
                    eventi = eventi + "<button type=\"button\" class=\"btn btn-lg btn-secondary evento_TipoEsecuzione" + ("lf".Equals(TipoTerminazione) ? " active" : "") + "\" data-tipoesecuzione=\"lf\" data-idperiodo=\"" + dr["id"] + "\">Freeze</button>";
                    eventi = eventi + "<button type=\"button\" class=\"btn btn-lg btn-secondary evento_TipoEsecuzione" + ("bp".Equals(TipoTerminazione) ? " active" : "") + "\" data-tipoesecuzione=\"bp\" data-idperiodo=\"" + dr["id"] + "\">Playlist</button>";
                    eventi += "</div></td>";
                    eventi = eventi + "<td><button type=\"button\" class=\"h-100 w-40 btn btn-secondary btn-smallfont btn-lg flash-button btn-temporizzaEvento avvioIdPeriodo_" + dr["id"] + "\" data-fk_int_periodo=\"" + dr["id"] + "\""+(avvioOraFissa?"":" disabled")+">" + oraInizioAut + "</button></td>";
                    eventi = eventi + "<td><button type=\"button\" class=\"h-100 w-40 btn btn-secondary btn-smallfont btn-lg flash-button btn-temporizzaEventoInContenitore avvioIdPeriodoInContenitore_" + dr["id"] + "\" data-fk_int_periodo=\"" + dr["id"] + "\"" + (avvioOraRelativa ? "" : " disabled") + ">" + secondiInizioContenitoreAut + "</button></td>";
                    eventi = eventi + "<td style='width:50%; padding:15px 100px 15px 100px; text-align:left'><button type=\"button\"  class=\"h-100 btn  btn-primary  btn-lg flash-button btn-block  btn-periodi idPeriodo_" + dr["id"] + " btn-danger\"   data-fk_int_periodo=\"" + dr["id"] + "\">" + Nome + "</button></td>";
                    eventi += "<tr>";
                }

                eventi = eventi + "</table>";
                ViewBag.ModalEventi = eventi;
            }
            else
            {
                ViewBag.ModalEventi = "";
            }



            string periodi = "";
            DataTable dtep = DB.GetDataFromSQL("select id,nva_Codice,nva_Nome from tb_Periodi  where fk_int_TipoPeriodo = 1 and dtt_Cancellazione is null");
            foreach (DataRow dr in dtep.Rows)
            {

                string Nome = "";
                Nome = dr["nva_Nome"].ToString();
                periodi = periodi + "<div class='SuntoCaricamentoScene'  data-fk_int_periodo=\"" + dr["id"] + "\">" + Nome + " <span class='NumeroSceneCaricatePeriodo'></span>" +
                "<button type=\"button\" class=\"btn btn-success btn-UnloadPeriodo  btn-lg flash-button  \" data-fk_int_periodo=\"" + dr["id"] + "\">Scarica</button>" +
                "<button type=\"button\" class=\"btn btn-success btn-LoadPeriodo   btn-lg flash-button  \" data-fk_int_periodo=\"" + dr["id"] + "\">Carica</button>" +
                "</div>";
            }
            periodi = periodi + "";
            ViewBag.ModalGestisciPeriodiPreload = periodi;









            string parametri = "<table>";
            DataTable dtparametri = DB.GetDataFromSQL("select id,nva_NomeParametro,nva_ValoreParametro,nva_UserId,dtt_Modifica from tb_parametri where dtt_cancellazione is null and isnull(tin_IsEditabile,0) = 1");
            foreach (DataRow dr in dtparametri.Rows)
            {

                string nva_NomeParametro = dr["nva_NomeParametro"].ToString();
                string nva_ValoreParametro = dr["nva_ValoreParametro"].ToString();
                parametri = parametri + "<tr><td>" + nva_NomeParametro + "</td><td>" + nva_ValoreParametro + "</td></tr>";

            }
            parametri = parametri + "</table>";
            ViewBag.ModalParametri = parametri;


            /* Per vedere se le formazioni sono state selezionate, controllo quante squadre hanno giocatori selezionati in formazione 
             * per il match in corso. Se sono due, va bene.
             */
            string formazioniOK = "false";
            DataTable dtFormazioniOK = DB.GetDataFromSQL(@"SELECT COUNT(*) as NumSquadre FROM tb_Squadre WHERE id IN 
                                                            (SELECT fk_int_squadra FROM tb_Giocatori WHERE ID IN
                                                                (SELECT fk_int_giocatore FROM tb_Formazione WHERE dtt_Cancellazione IS NULL AND fk_int_Match IN
                                                                    (SELECT ID FROM tb_Match WHERE IsSelected = 1)))");
            foreach (DataRow dr in dtFormazioniOK.Rows)
            {
                if (int.Parse(dr["NumSquadre"].ToString()) == 2)
                    formazioniOK = "true";
            }

            ViewBag.FormazioniOK = formazioniOK;


            

            /*
             * Crea lo HTML per il pannello ModalOrarioPrepartita
             */
            string orarioPrepartita = "<table>";
            DataTable dtOrarioPrepartita = DB.GetDataFromSQL("SELECT dtt_TimerOraInizio FROM tb_TimerAvvioPeriodi WHERE fk_int_Playlist IN (SELECT ID FROM tb_Playlist WHERE IsSelected = 1) AND fk_int_Periodo=2");

            orarioPrepartita += "<tr><td style=\"text-align:center\"><span style=\"font-size:smaller;margin-left:20px;margin-right:20px\">Ore (24:00)</span></td><td></td><td style=\"text-align:center\"><span style=\"font-size:smaller;margin-left:20px;margin-right:20px\">Minuti</span></td></tr>";
            if (dtOrarioPrepartita.Rows.Count == 0 || dtOrarioPrepartita.Rows[0]["dtt_TimerOraInizio"] == DBNull.Value)
            {
                orarioPrepartita += "<tr><td><input type=\"number\" value=\"0\" min=\"0\" max=\"23\" class=\"InizioPrepartitaOre\" /></td><td><span style=\"font-size:larger;margin-left:20px;margin-right:20px\">:</span></td>";
                orarioPrepartita += "<td><input type=\"number\" value=\"0\" min=\"0\" max=\"55\" step=\"5\" class=\"InizioPrepartitaMinuti\" /></td></tr>";
                ViewBag.AvvioAutomaticoPrepartita = "";
            }
            else
            {
                DateTime OraInizio = Convert.ToDateTime(dtOrarioPrepartita.Rows[0]["dtt_TimerOraInizio"]);
                orarioPrepartita += "<tr><td><input type=\"number\" value=\"" + OraInizio.Hour + "\" min=\"0\" max=\"23\" class=\"InizioPrepartitaOre\" /></td><td><span style=\"font-size:larger;margin-left:20px;margin-right:20px\">:</span></td>";
                orarioPrepartita += "<td><input type=\"number\" value=\"" + OraInizio.Minute + "\" min=\"0\" max=\"55\" step=\"5\" class=\"InizioPrepartitaMinuti\" /></td></tr>";
                ViewBag.AvvioAutomaticoPrepartita = OraInizio.Hour+":"+OraInizio.Minute;
            }
            orarioPrepartita += "</table><br>";

            orarioPrepartita += "<div class='BottoniOraInizioPrepartita' style=\"margin-top:20px\">" +
                "<button type=\"button\" class=\"btn btn-success btn-SalvaOraInizioPrepartita btn-lg flash-button\">Salva</button>" +
                "<button type=\"button\" class=\"btn btn-success btn-CancellaOraInizioPrepartita btn-lg flash-button float-right\">Avvia Manualmente</button>" +
                "</div>";

            ViewBag.ModalOrarioPrepartita = orarioPrepartita;


            /*
             * Crea lo HTML per il pannello ModalOrarioEvento
             */
            string orarioEvento = "<table>";
            DataTable dtOrarioEvento = DB.GetDataFromSQL("SELECT dtt_TimerOraInizio FROM tb_TimerAvvioPeriodi WHERE fk_int_Playlist IN (SELECT ID FROM tb_Playlist WHERE IsSelected = 1) AND fk_int_Periodo=2");

            orarioEvento += "<tr><td style=\"text-align:center\"><span style=\"font-size:smaller;margin-left:20px;margin-right:20px\">Ore (24:00)</span></td><td></td><td style=\"text-align:center\"><span style=\"font-size:smaller;margin-left:20px;margin-right:20px\">Minuti</span></td></tr>";
            if (dtOrarioEvento.Rows.Count == 0 || dtOrarioEvento.Rows[0]["dtt_TimerOraInizio"] == DBNull.Value)
            {
                orarioEvento += "<tr><td><input type=\"number\" value=\"0\" min=\"0\" max=\"23\" class=\"InizioEventoOre\" /></td><td><span style=\"font-size:larger;margin-left:20px;margin-right:20px\">:</span></td>";
                orarioEvento += "<td><input type=\"number\" value=\"0\" min=\"0\" max=\"55\" step=\"5\" class=\"InizioEventoMinuti\" /></td></tr>";
                ViewBag.AvvioAutomaticoEvento = "";
            }
            else
            {
                DateTime OraInizio = Convert.ToDateTime(dtOrarioEvento.Rows[0]["dtt_TimerOraInizio"]);
                orarioEvento += "<tr><td><input type=\"number\" value=\"" + OraInizio.Hour + "\" min=\"0\" max=\"23\" class=\"InizioEventoOre\" /></td><td><span style=\"font-size:larger;margin-left:20px;margin-right:20px\">:</span></td>";
                orarioEvento += "<td><input type=\"number\" value=\"" + OraInizio.Minute + "\" min=\"0\" max=\"55\" step=\"5\" class=\"InizioEventoMinuti\" /></td></tr>";
                ViewBag.AvvioAutomaticoEvento = OraInizio.Hour + ":" + OraInizio.Minute;
            }
            orarioEvento += "</table><br>";

            orarioEvento += "<div class='BottoniOraInizioEvento' style=\"margin-top:20px\">" +
                "<button type=\"button\" class=\"btn btn-success btn-SalvaOraInizioEvento btn-lg flash-button\">Salva</button>" +
                "<button type=\"button\" class=\"btn btn-success btn-CancellaOraInizioEvento btn-lg flash-button float-right\">Avvia Manualmente</button>" +
                "</div>";

            orarioEvento += "<span class='InizioEventoIdPeriodo' style ='display:none'></span>";

            ViewBag.ModalOrarioEvento = orarioEvento;


            /*
             * Crea lo HTML per il pannello ModalOrarioEventoInContenitore
             */
            string orarioEventoInContenitore = "<table>";
            DataTable dtOrarioEventoInContenitore = DB.GetDataFromSQL("SELECT fk_int_PeriodoContenitore, int_TimerSecondiDaInizioContenitore FROM tb_TimerAvvioPeriodi WHERE fk_int_Playlist IN (SELECT ID FROM tb_Playlist WHERE IsSelected = 1) AND fk_int_Periodo=2");

            orarioEventoInContenitore += "<tr><td style=\"text-align:center\"><span style=\"font-size:smaller;margin-left:20px;margin-right:20px\">Minuti</span></td><td></td><td style=\"text-align:center\"><span style=\"font-size:smaller;margin-left:20px;margin-right:20px\">Secondi</span></td></tr>";
            if (dtOrarioEventoInContenitore.Rows.Count == 0 || dtOrarioEventoInContenitore.Rows[0]["int_TimerSecondiDaInizioContenitore"] == DBNull.Value)
            {
                orarioEventoInContenitore += "<tr><td><input type=\"number\" value=\"0\" min=\"0\" max=\"90\" class=\"InizioEventoInContenitoreMinuti\" /></td><td><span style=\"font-size:larger;margin-left:20px;margin-right:20px\">:</span></td>";
                orarioEventoInContenitore += "<td><input type=\"number\" value=\"0\" min=\"0\" max=\"55\" step=\"5\" class=\"InizioEventoInContenitoreSecondi\" /></td></tr>";
                ViewBag.AvvioAutomaticoEventoInContenitore = "";
            }
            else
            {
                int secondiDaInizioContenitore = ValueOperations.DbToInteger(dtOrarioEventoInContenitore.Rows[0]["int_TimerSecondiDaInizioContenitore"]);
                int minuti = (int)(secondiDaInizioContenitore / 60);
                int secondi = secondiDaInizioContenitore % 60;
                orarioEventoInContenitore += "<tr><td><input type=\"number\" value=\"" + minuti + "\" min=\"0\" max=\"90\" class=\"InizioEventoInContenitoreMinuti\" /></td><td><span style=\"font-size:larger;margin-left:20px;margin-right:20px\">:</span></td>";
                orarioEventoInContenitore += "<td><input type=\"number\" value=\"" + secondi + "\" min=\"0\" max=\"55\" step=\"5\" class=\"InizioEventoInContenitoreSecondi\" /></td></tr>";
                ViewBag.AvvioAutomaticoEventoInContenitore = minuti + ":" + secondi;
            }


            orarioEventoInContenitore += "<tr><td colspan=3 style=\"text-align:left\"><span style=\"font-size:smaller;margin-left:20px;margin-right:20px\">Dall'inizio del periodo</span></td></tr>";

            orarioEventoInContenitore += "<tr><td colspan=3 style=\"text-align:left\"><select id=\"InizioEventoInContenitoreIdPeriodoContenitore\" class=\"form-select InizioEventoInContenitoreIdPeriodoContenitore\" aria-label=\"Select periodi\">";
            DataTable periodiContenitori = DB.GetDataFromSQL(@"select id, nva_Codice, nva_Nome from tb_periodi where fk_int_TipoPeriodo=1 and id in
                                                                (select fk_int_Periodo from tb_SceneLed where fk_int_Playlist = (select id from tb_Playlist where IsSelected = 1)
                                                                union
                                                                select fk_int_Periodo from tb_SceneMaxischermo where fk_int_Playlist = (select id from tb_Playlist where IsSelected = 1))
                                                                ORDER BY id");
            if (periodiContenitori.Rows.Count > 0)
            {
                foreach (DataRow dr in periodiContenitori.Rows)
                {
                    bool isSelected = false;
                    if (dtOrarioEventoInContenitore.Rows.Count > 0)
                        dr["id"].ToString().Equals(dtOrarioEventoInContenitore.Rows[0]["fk_int_PeriodoContenitore"].ToString());
                    orarioEventoInContenitore += "<option " + (isSelected ? "selected" : "") + " value=\"" + dr["id"].ToString() + "\">" + dr["nva_Nome"].ToString() + " (" + dr["nva_Codice"].ToString() + ")</option>";
                }
            }
            orarioEventoInContenitore += "</select></td></tr>";

            orarioEventoInContenitore += "</table><br>";

            orarioEventoInContenitore += "<div class='BottoniOraInizioEventoInContenitore' style=\"margin-top:20px\">" +
                "<button type=\"button\" class=\"btn btn-success btn-SalvaOraInizioEventoInContenitore btn-lg flash-button\">Salva</button>" +
                "<button type=\"button\" class=\"btn btn-success btn-CancellaOraInizioEventoInContenitore btn-lg flash-button float-right\">Avvia Manualmente</button>" +
                "</div>";

            orarioEventoInContenitore += "<span class='InizioEventoInContenitoreIdPeriodo' style ='display:none'></span>";

            ViewBag.ModalOrarioEventoInContenitore = orarioEventoInContenitore;

            return View();
        }



        [HttpPost]
        public string ArchiviaLuminosita([FromBody] List<tb_Luminosita> tb_LuminositaList)
        {
            try
            {
                foreach (tb_Luminosita l in tb_LuminositaList)
                {
                    GestioneLog.ArchiviaMessaggioLog("Setto Lux " + l.nva_Settore, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                    DB.EseguiSQL("exec sp_ArchiviaLuminosita @nva_Settore='" + l.nva_Settore + "', @int_Valore=" + l.int_Valore.ToString()+ ", @int_OffsetPercentualeVariazioneGlobale=" + l.int_OffsetPercentualeVariazioneGlobale.ToString());
                    if ((l.nva_Settore == "Maxischermo")&&(StaticStartConfig.Stadio.nva_SendingCardMaxischermo.ToLower()!=""))
                    {

                        GestioneLog.ArchiviaMessaggioLog("Setto Lux Maxischermo", System.Reflection.MethodInfo.GetCurrentMethod().ToString());

                        SetLuminositaMaxischermo(l.int_Valore);
                    }
                }
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());

                return ex.Message.ToString();
            }
            return "OK";
        }





        void SetLuminositaMaxischermo(int val)
        {
            try
            {
                switch (StaticStartConfig.Stadio.nva_SendingCardMaxischermo.ToLower()){
                    case "lnsn":
                        ControlloLnsn.SendLnsn l = new ControlloLnsn.SendLnsn();
                        l.SetBrightness((byte)val);
                    break;
                }
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
            }

        }



        public  String GetStatusMaxischermoFromCard()
        {
            try
            {
                switch (StaticStartConfig.Stadio.nva_SendingCardMaxischermo.ToLower())
                {
                    case "lnsn":
                        ControlloLnsn.SendLnsn l = new ControlloLnsn.SendLnsn();
                        //return l.GetStatus();
                        break;
                }
                return "";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
            }
            return "";
        }


        public string GetLuminosita()
        {
            List<tb_Luminosita> tb_LuminositaList = new List<tb_Luminosita>();
            DataTable dt = DB.GetDataFromSQL("select ID from tb_Luminosita where dtt_Cancellazione is null");
            foreach (DataRow r in dt.Rows)
            {
                tb_LuminositaList.Add(new tb_Luminosita(Convert.ToInt32(r["ID"])));
            }
            return JsonConvert.SerializeObject(tb_LuminositaList);
        }










    }
}
