﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NgmPlay.Classi;
using RenderMachine.Models;

namespace RenderMachine.Controllers
{
    public class PlayerController : Controller
    {
        public IActionResult maxischermoTimerPunteggio()
        {
            return View();
        }

        public IActionResult maxischermo()
        {
            return View();
        }
        public IActionResult Animazione()
        {


            return View();

        }
        public IActionResult ProvaPausa()
        {


            return View();

        }

        public IActionResult memoryleak()
        {


            return View();

        }
        public IActionResult provastatico()
        {


            return View();

        }
        public IActionResult ProvaClona()
        {


            return View();

        }
        public IActionResult Punteggio()
        {


            return View();

        }


        public IActionResult Index()
        {

            string q = @"select
                        tv.*
                          from tb_DisposizioneTagVideo tv
                        join tb_DisposizioneLed dl on tv.fk_int_DisposizioneLed = dl.Id
                        where
                        (fk_int_TipoElemento = 4 or fk_int_TipoElemento = 2) and
                          fk_int_Layer = 1 and
                          fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString() + @" and
                        dl.dtt_Cancellazione is null and
                        tv.dtt_Cancellazione is null";

            List<object> Lista_tv = DB.PopolaListaOggetti(typeof(tb_DisposizioneTagVideo), DB.GetDataFromSQL(q));

            string HtmlClockScore = "";

            foreach (tb_DisposizioneTagVideo tv in Lista_tv)
            {
                HtmlClockScore = HtmlClockScore + @"<div class='" + tv.tb_DisposizioneLed.tb_TipoElemento.nva_Nome.Replace(" ", "_") + "' style='position: absolute; border:none; width:" + tv.int_W + "px; height:" + tv.int_H + "px; margin-left:" + tv.int_MarginL + "px; margin-top:" + tv.int_MarginT + "px;'>";

                if(tv.tb_DisposizioneLed.tb_TipoElemento.nva_Nome.ToLower() == "punteggio")
                {
                    HtmlClockScore = HtmlClockScore + @"<div class='div_logoPunteggio '><img class='img_punteggioCasa' /></div><div class='div_testoPunteggio'></div><div class='div_logoPunteggio'><img  class='img_punteggioOspiti' /></div>";
                    
                }
               
               HtmlClockScore = HtmlClockScore + "</div>";

            }

            ViewBag.HtmlClockScore = HtmlClockScore;




            List<object> Lista_tvGrafica = DB.PopolaListaOggetti(typeof(tb_DisposizioneTagVideo), DB.GetDataFromSQL("select tv.* from tb_DisposizioneTagVideo tv join tb_DisposizioneLed dl on tv.fk_int_DisposizioneLed = dl.Id where dl.fk_int_Stadio = 2 and fk_int_Layer = 5 order by int_ordine"));

            string HtmlGrafica = "";


            foreach (tb_DisposizioneTagVideo tv in Lista_tvGrafica)
            {
               
                    HtmlGrafica = HtmlGrafica + "<canvas width='" + tv.int_W + "' height='" + tv.int_H + "'  id='grafica_" + tv.fk_int_DisposizioneLed  + "_" + tv.Id  + "' style='position: absolute; border:none;  margin-left:" + tv.int_MarginL + "px; margin-top:" + tv.int_MarginT + "px;  '></canvas>";

               
            }
           

            ViewBag.HtmlGrafica = HtmlGrafica;





            ViewBag.DimensioneCanvasPerCss = DB.GetDataFromSQL("select 'width:' + cast(int_W as nvarchar(55)) + 'px;height:' + cast(int_H as nvarchar(55)) + 'px;' as size  from tb_Stadi where ID = " + StaticStartConfig.ID_int_Stadio.ToString()).Rows[0][0];


            //string Azioni = "";
            //string EventiPartita = @"<div class='EventiPartita' style='position:absolute; left:~left~px; top:~top~px; width:~width~px; height:~height~px;  '></div>";
            //DataTable dt = DB.GetDataFromSQL("select tv.id, tv.int_MarginL, tv.int_MarginT ,dl.int_W , dl.int_H   from tb_DisposizioneTagVideo tv join tb_DisposizioneLed  dl on dl.Id = tv.fk_int_DisposizioneLed where dl.dtt_Cancellazione is Null  and tv.dtt_Cancellazione is Null and dl.nva_Nome like 'Central' and dl.fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString());
            //foreach(DataRow r in dt.Rows)
            //{
            //    Azioni = Azioni + EventiPartita.Replace("~left~", r["int_MarginL"].ToString())
            //    .Replace("~top~", r["int_MarginT"].ToString())
            //    .Replace("~width~", r["int_W"].ToString())
            //    .Replace("~height~", r["int_H"].ToString());

            //}

            //ViewBag.EventiPartita = Azioni;





            List<object> Lista_tvResult = DB.PopolaListaOggetti(typeof(tb_DisposizioneTagVideo), DB.GetDataFromSQL("select distinct tv.* from tb_DisposizioneTagVideo tv join tb_DisposizioneLed dl on tv.fk_int_DisposizioneLed = dl.Id join tb_TipoElemento te on te.ID = dl.fk_int_TipoElemento where dl.fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString() + " and fk_int_Layer = 2  and dl.dtt_Cancellazione is null and tv.dtt_Cancellazione is null"));
            string EventiPartita = "";
            foreach (tb_DisposizioneTagVideo tv in Lista_tvResult)
            {
                EventiPartita = EventiPartita + "<div class='"+ tv.tb_DisposizioneLed.tb_TipoElemento.nva_Nome  + "' style='position: absolute; border:none; width:" + tv.int_W + "px; height:" + tv.int_H + "px; margin-left:" + tv.int_MarginL + "px; margin-top:" + tv.int_MarginT + "px;  '></div>";
            }

            ViewBag.HtmlEventiPartita = EventiPartita;









            List<object> Lista_tvLuminosità = DB.PopolaListaOggetti(typeof(tb_DisposizioneTagVideo), DB.GetDataFromSQL("select distinct tv.* from tb_DisposizioneTagVideo tv join tb_DisposizioneLed dl on tv.fk_int_DisposizioneLed = dl.Id where dl.fk_int_Stadio = 2 and fk_int_Layer = 4"));

            string htmlLuminosità = "";

            foreach (tb_DisposizioneTagVideo tv in Lista_tvLuminosità)
            {
                htmlLuminosità = htmlLuminosità + "<div class='luminosita_" + tv.fk_int_DisposizioneLed + "' style='position: absolute; opacity:0.5; border:none; width:" + tv.int_W + "px; height:" + tv.int_H + "px; margin-left:" + tv.int_MarginL + "px; margin-top:" + tv.int_MarginT + "px; background-color:black; '></div>";

            }

            ViewBag.HtmlLuminosità = htmlLuminosità;








            return View();



        }

    }
}
