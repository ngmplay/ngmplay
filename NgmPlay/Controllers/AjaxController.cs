﻿using System;
using System.Collections.Generic;
using System.Data;
//using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NgmPlay.Classi;
using NgmPlay.Utility;
using RenderMachine.Models;
//using LibreriaCondivisa;
using NgmPlay.Costanti;
using System.Diagnostics;
using OfficeOpenXml;
//using System.IO.Compression;
using NgmPlay.Classi;
using System.IO;
using System.IO.Compression;
using Microsoft.Extensions.Configuration;

namespace RenderMachine.Controllers
{
public class AjaxController : Controller
    {
        private readonly IConfiguration Configuration;


        public AjaxController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public String SincronizzaCategorie()
        {
            try
            {
                String NomeTabella = "tb_Categorie";
                String url = "http://empoli.ngmsport.it/" + NomeTabella;
                String str = GetPage(url);
                EsitoAggiornamento EA = InsMod(str, NomeTabella);
                return EA.Riepilogo(NomeTabella);
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }


        public String SincronizzaSquadre()
        {
            try
            {
                String NomeTabella = "tb_Squadre";
                String url = "http://empoli.ngmsport.it/" + NomeTabella;
                String str = GetPage(url);
                EsitoAggiornamento EA = InsMod(str, NomeTabella);
                return EA.Riepilogo(NomeTabella);
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }


        public String SincronizzaGiocatori()
        {
            try
            {
                String NomeTabella = "tb_giocatori";
                String url = "http://empoli.ngmsport.it/" + NomeTabella;
                String str = GetPage(url);
                EsitoAggiornamento EA = InsMod(str, NomeTabella);
                return EA.Riepilogo(NomeTabella);
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }


        public String CaricaFileZipAssets()
        {
            return "";
        }




        private void GestisciAggiornamentoImmagini(String[] Intn, String[] Cln, String NomeTabella)
        {
            
            String DirectorySquadra = "";
            if (NomeTabella.Contains("giocator"))
            {
                String NumeroGiocatore = "";
                int k_nva_NG = 0;
                int k_Fk_SQ = 0;
                for (int i = 0; i < Intn.Length; i++)
                {
                    if (Intn[i].ToLower() == "nva_numero") k_nva_NG = i;
                    if (Intn[i].ToLower() == "fk_int_squadra") k_Fk_SQ = i;
                }

                DirectorySquadra = DB.GetDataFromSQL("select nva_Directory from tb_squadre where id = " + (Cln[k_Fk_SQ])).Rows[0][0].ToString();
                NumeroGiocatore = Cln[k_nva_NG];


                GestisciCopiaImmaginiGiocatori(DirectorySquadra, NumeroGiocatore);
            }


            /*
            else if(NomeTabella.Contains("squadr"))
            {
              
                int k_nva_directory = 0;
                for (int i = 0; i < Intn.Length; i++)
                {
                    if (Intn[i].ToLower() == "nva_directory") k_nva_directory = i;
                }
                DirectorySquadra = Cln[k_nva_directory];
                if (fg.Exist("http://ngm-011/GestioneSpaziLed/" + DirectorySquadra + "/grafica/logo.png"))
                {
                    if (System.IO.File.Exists(DirectorySquadra + "/grafica/logo.png"))
                    {
                        System.IO.File.Delete(DirectorySquadra + "/grafica/logo.png");
                    }
                    fg.CopyImageFromWeb("http://ngm-011/GestioneSpaziLed/" + DirectorySquadra + "/grafica/logo.png", DirectorySquadra + "/grafica/logo.png");
                }
            }
            */
        }

        private void GestisciCopiaImmaginiGiocatori(String DirectorySquadra,String NumeroGiocatore)
        {

            String DirImmagine = StaticStartConfig.Parametro_nva_DirAssetsPlayers.Replace("[nomesquadra]", DirectorySquadra);

            try
            { 

                
                if (!Directory.Exists(DirImmagine )) Directory.CreateDirectory(DirImmagine );
                if (!Directory.Exists(DirImmagine + "led/")) Directory.CreateDirectory(DirImmagine + "led/");
                if (!Directory.Exists(DirImmagine + "maxischermo/")) Directory.CreateDirectory(DirImmagine + "maxischermo/");


                //esempio di download da computer in rete locale
                //fg.CopyImageFromWeb("http://ngm-011/GestioneSpaziLed/img/squadre/" + DirectorySquadra + "/players/led/" + NumeroGiocatore + ".png", DirImmagine +  "led/" + NumeroGiocatore + ".png");

                //Inizializzo il giocatore dandogli l'immagine "grigia", che eventualmente verrà sovrascritta al passo successivo
                System.IO.File.Copy(Path.Combine(Configuration["DirectoryBase"], @"publish\wwwroot\img\GiocatoreSenzaImmagine.png"), DirImmagine + NumeroGiocatore + ".png",false);
                System.IO.File.Copy(Path.Combine(Configuration["DirectoryBase"], @"publish\wwwroot\img\GiocatoreSenzaImmagine.png"), DirImmagine + "led/" + NumeroGiocatore + ".png", false);
                System.IO.File.Copy(Path.Combine(Configuration["DirectoryBase"], @"publish\wwwroot\img\GiocatoreSenzaImmagine.png"), DirImmagine + "maxischermo/" + NumeroGiocatore + ".png", false);

                //copio l'immagine dei led sia in sottocartella Led che nella cartella della squadra (serve per mostrarla nel menu Formazione Casa/Formazione ospiti)
                fg.CopyImageFromWeb("http://"+ DirectorySquadra.ToLower() + ".ngmsport.it/img/squadre/" + DirectorySquadra + "/players/led/" + NumeroGiocatore + ".png", DirImmagine + "led/" + NumeroGiocatore + ".png");
                fg.CopyImageFromWeb("http://" + DirectorySquadra.ToLower() + ".ngmsport.it/img/squadre/" + DirectorySquadra + "/players/led/" + NumeroGiocatore + ".png", DirImmagine + NumeroGiocatore + ".png");
                fg.CopyImageFromWeb("http://" + DirectorySquadra.ToLower() + ".ngmsport.it/img/squadre/" + DirectorySquadra + "/players/maxischermo/" + NumeroGiocatore + ".png", DirImmagine + "maxischermo/" + NumeroGiocatore + ".png");


            }catch(Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
            }
        }
         

        private EsitoAggiornamento InsMod(String str, String NomeTabella)
        {


            List<int> Sq = new List<int>();
            if (NomeTabella.ToLower() == "tb_giocatori")
            {
                DataTable dt = DB.GetDataFromSQL("select top(1) fk_int_SquadraCasa,fk_int_SquadraOspite from tb_match where IsSelected = 1 and dtt_cancellazione is null");
                if (dt.Rows.Count == 1)
                {
                    Sq.Add(Int32.Parse(dt.Rows[0][0].ToString()));
                    Sq.Add(Int32.Parse(dt.Rows[0][1].ToString()));
                }
            }


            String[] rows = str.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            int n = 0;
            String[] Cln = null;
            int NumeroColonnaDttModifica = 0;
            int NumeroColonnaFkSquadra = 0;
            int NCID = 0;
            String[] Intn = null;
            int NumeroAggiornati = 0;
            int NumeroInseriti = 0;
            int NumeroTotali = 0;

            foreach (String row in rows)
            {
                Cln = row.Split(";");
                if (n == 0)
                {
                    Intn = Cln;
                    for (int i = 0; i < Intn.Length; i++)
                    {
                        if (Intn[i].ToLower() == "dtt_modifica") NumeroColonnaDttModifica = i;
                        if (Intn[i].ToLower() == "id") NCID = i;
                        if (Intn[i].ToLower() == "fk_int_squadra") NumeroColonnaFkSquadra = i;

                    }
                }
                else
                {
                    NumeroTotali++;
                    if (double.TryParse(Cln[NCID].ToString(), out _))
                    {

                        int NR = Convert.ToInt32(DB.GetDataFromSQL("select count(*) from " + NomeTabella + " where id = " + Cln[NCID].ToString()).Rows[0][0]);
                        if (NR == 0)
                        {
                            NumeroInseriti++;
                            String Sql = "insert into " + NomeTabella + " (";
                            for (int i = 0; i < Cln.Length; i++)
                            {
                                Sql += Intn[i] + ",";
                            }
                            Sql = Sql.Remove(Sql.Length - 1);
                            Sql = Sql + " ) values ( ";
                            for (int i = 0; i < Cln.Length; i++)
                            {
                                if ((Cln[i] == "") && (Intn[i].ToLower().StartsWith("dtt_"))){
                                    Sql += "NULL,";
                                }else{
                                    Sql += "'" + Cln[i].Replace("'", "''") + "',";
                                }
                            }
                            Sql = Sql.Remove(Sql.Length - 1);
                            Sql = Sql + " );";
                            DB.EseguiSQL(Sql);



                        }
                        else if (NR == 1) {
                            DateTime d = (DateTime)DB.GetDataFromSQL("select isnull(dtt_Modifica,'01/01/1900') from " + NomeTabella + " where id = " + Cln[NCID].ToString()).Rows[0][0];
                            if (DateTime.Parse(Cln[NumeroColonnaDttModifica]) > d)
                            {
                                NumeroAggiornati++;
                                String Sql = "update " + NomeTabella + " set ";
                                for (int i = 0; i < Cln.Length; i++)
                                {
                                    if (i != NCID)
                                    {
                                        if ((Cln[i] == "") && (Intn[i].ToLower().StartsWith("dtt_")))
                                        {
                                            Sql += Intn[i] + " = NULL,";
                                        }else{
                                            Sql += Intn[i] + " = '" + Cln[i].Replace("'", "''") + "',";
                                        }
                                    }
                                }
                                Sql = Sql.Remove(Sql.Length - 1);
                                Sql += " where ID = " + Cln[NCID].ToString();
                                DB.EseguiSQL(Sql);
                            }
                            else
                            {
                             //   Console.WriteLine("mantengo valore vecchio");
                            }


                            if (NomeTabella.ToLower() == "tb_giocatori")
                            {
                               if (Sq.Contains(Int32.Parse(Cln[NumeroColonnaFkSquadra].ToString())))
                               {
                                    GestisciAggiornamentoImmagini(Intn, Cln, NomeTabella);
                                }
                            }
                        }
                        else
                        {
                        //    Console.WriteLine("Ricorrenze multiple?!");
                        }
                    }
                }
                n++;
            }
            EsitoAggiornamento es = new EsitoAggiornamento(NumeroTotali, NumeroInseriti, NumeroAggiornati);
            return es;
        }


        private String GetPage(String url)
        {

            String str ;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            request.Timeout = 5000;
            str = reader.ReadToEnd();
            GestioneLog.ArchiviaMessaggioLog("ho eseguito chiamata", "SincronizzaGiocatori");
            reader.Close();
            reader.Dispose();
            response.Close();
            return str;
        }





        public String sp_GetPeriodiInPlaylist()
        {
            try
            {
                string id = Request.Query["fk_int_Playlist"].ToString();
                var dictionary = new Dictionary<int, tb_Periodi>();
                List<object> Periodi = DB.PopolaListaOggetti(typeof(tb_Periodi), "exec sp_GetPeriodiInPlaylist" );
                foreach (tb_Periodi Periodo in Periodi)
                {
                    dictionary[Periodo.ID] = Periodo;
                }
                return JsonConvert.SerializeObject(dictionary);
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }


        public String sp_SettaRitardoClient()
        {
            try
            {
                string n = GetIpFormattato();
                string now = Request.Query["now"].ToString();

                Int64 i = Convert.ToInt64(DB.GetDataFromSQL("EXEC sp_SettaRitardoClient @msRemote="+ now + ", @nva_IP= '"+n+"'").Rows[0][0]);

                return i.ToString();
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }


        public String GetLogImportazione()
        {
       
            try
            {
                
                var dictionary = new Dictionary<int, tb_LogPlaylist>();
                List<object> ll = DB.PopolaListaOggetti(typeof(tb_LogPlaylist), "select * from tb_LogPlaylist where fk_int_Playlist = (select top(1) id from tb_playlist order by id desc) order by id desc");
                return JsonConvert.SerializeObject(ll);

            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "GetLogImportazione");
                return ex.Message.ToString();

            }
           
        }



        public String ListaAggiornamenti()
        {
            try
            {
                var dictionary = new Dictionary<int, tb_AggiornamentiPlaylist>();

                List<object> tb_AggiornamentiPlaylist = DB.PopolaListaOggetti(typeof(tb_AggiornamentiPlaylist), "select * from tb_AggiornamentiPlaylist where FORMAT(dtt_DataOraUltimoAggiornamento, 'yyyyMMdd') = (select FORMAT(dtt_DataOraUltimoAggiornamento, 'yyyyMMdd') from tb_Match where IsSelected = 1 )");
                foreach (tb_AggiornamentiPlaylist aggiornamento in tb_AggiornamentiPlaylist)
                {
                    dictionary[aggiornamento.ID] = aggiornamento;
                }
                return JsonConvert.SerializeObject(dictionary);
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "ListaAggiornamenti");
                return ex.Message.ToString();
            }
        }



        public String sp_GetGoalService()
        {
            try
            {
                DB.EseguiSQL("exec sp_GetGoalService");
                return "ok";
              
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "ListaAggiornamenti");
                return ex.Message.ToString();
            }
        }



        [HttpPost]
        public String InserisciAggiornamentoPlaylist()
        {
            try
            {
                string log = HttpContext.Request.Form["log"];
                string consistenza = HttpContext.Request.Form["consistenza"];
                DB.EseguiSQL("insert into tb_AggiornamentiPlaylist (fk_int_Match,dtt_DataOraUltimoAggiornamento,tin_Consistenza,nva_log,dtt_Modifica,nva_UserID) select id,getdate(),'"+consistenza+"','" + log + "',getdate(),'aaaa' from tb_Match where IsSelected = 1 and dtt_Cancellazione is null");
                return "ok";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "ListaAggiornamenti");
                return ex.Message.ToString();
            }
        }





        public String ControllaAggiornamenti()
        {
            string json = "";
            try
            {
                String DataOraUltimoAggiornamento = "19000101000000";
                String DataOraMatch = "19000101";
                DataTable dtm = DB.GetDataFromSQL("select FORMAT(dtt_DataOra, 'yyyyMMdd') nva_DataOraMatch from tb_Match where IsSelected = 1");
                if (dtm.Rows.Count > 0)
                {
                    DataOraMatch = dtm.Rows[0]["nva_DataOraMatch"].ToString();
                }
                //DataTable dt = DB.GetDataFromSQL("select top(1) FORMAT(dtt_DataOraUltimoAggiornamento, 'yyyyMMddhhmmss') nva_DataOraUltimoAggiornamento from tb_AggiornamentiPlaylist where FORMAT(dtt_DataOraUltimoAggiornamento, 'yyyyMMdd') = '"+ DataOraMatch + "' and isnull(tin_Consistenza,0) = 1 order by id desc");
                DataTable dt = DB.GetDataFromSQL("select top(1) FORMAT(a.dtt_DataOraUltimoAggiornamento, 'yyyyMMddHHmmss') nva_DataOraUltimoAggiornamento from tb_AggiornamentiPlaylist a join tb_match m on m.id = a.fk_int_match  where FORMAT(m.dtt_DataOra , 'yyyyMMdd') = '" + DataOraMatch + "' and isnull(tin_Consistenza,0) = 1 order by a.id desc");
               
                if (dt.Rows.Count > 0)
                {
                    DataOraUltimoAggiornamento = dt.Rows[0]["nva_DataOraUltimoAggiornamento"].ToString();
                }
                string url = StaticStartConfig.Stadio.nva_URLScaricoDati+ "?DataGiornata="+DataOraMatch+ "&DataRichiesta="+ DataOraUltimoAggiornamento;
                GestioneLog.ArchiviaMessaggioLog("chiamata server url: " + url, "ControllaAggiornamenti");
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                request.Timeout = 5000;
                json = reader.ReadToEnd();
                GestioneLog.ArchiviaMessaggioLog("chiamata server json: " + json, "ControllaAggiornamenti");
                reader.Close();
                reader.Dispose();
                response.Close();
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "ControllaAggiornamenti");
                return ex.Message.ToString();
            }

            return json;

        }






        public String ImportaPlaylist()
        {

            String PathVideoStadio = StaticStartConfig.Parametro_nva_DirVideoSorgenti.Replace("[nomesquadra]", StaticStartConfig.Stadio.nva_NomeDirectory);

           StepImportPlaylist(PathVideoStadio + "Playlist.xlsx", "Da Led Manager " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), 1);


           StepImportEventi();







            return "xlsx";
        }








        [HttpPost]
        public String DownloadFile(FileDownload fd)
        {

            try { 
      
                Uri uri = new Uri(fd.NomeFile);
                String PathVideoStadio = StaticStartConfig.Parametro_nva_DirVideoSorgenti.Replace("[nomesquadra]", StaticStartConfig.Stadio.nva_NomeDirectory);
                String TmpFileName = "tmp_" + System.IO.Path.GetFileName(uri.LocalPath);
                String FileName = System.IO.Path.GetFileName(uri.LocalPath);

                using (WebClient wc = new WebClient())
                {
                    wc.DownloadFile(uri, PathVideoStadio + TmpFileName);
                    if (System.IO.File.Exists(PathVideoStadio + FileName))
                    {
                        System.IO.File.Delete(PathVideoStadio + FileName);
                    }
                    System.IO.FileInfo fi = new FileInfo(PathVideoStadio + FileName);
                    System.IO.File.Move(PathVideoStadio + TmpFileName, PathVideoStadio + FileName);
                    if (System.IO.File.Exists(PathVideoStadio + FileName))
                    {
                        return fi.Extension;
                    }
                }


            }catch (Exception ex) {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "ControllaAggiornamenti");

                return "ko";
            }
            
           
            return "ko";
        }



        //public String ElaboraEventualiXlsx()
        //{
        //  /*
        //    string TmpFile = "tmp_Eventi_" + fg.PulisciStringa(DateTime.Now.ToString()) + ".xlsx";
        //    string PathFilePlaylistTmp = StaticStartConfig.Parametro_nva_DirPlaylist + TmpFile;
        //    System.IO.File.Move(PathVideoStadio + TmpFileName, PathFilePlaylistTmp);
        //    System.IO.File.Delete(PathVideoStadio + TmpFileName);
        //    StepImportEventi(PathFilePlaylistTmp, "Da Led Manager " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), 1);

        //    return "xlsx";

        //    string TmpFile = "tmp_Export_" + fg.PulisciStringa(DateTime.Now.ToString()) + ".xlsx";
        //    string PathFilePlaylistTmp = StaticStartConfig.Parametro_nva_DirPlaylist + TmpFile;
        //    System.IO.File.Move(PathVideoStadio + TmpFileName, PathFilePlaylistTmp);
        //    System.IO.File.Delete(PathVideoStadio + TmpFileName);

        //    StepImportPlaylist(PathFilePlaylistTmp, "Da Led Manager " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), 1);


        //    return "xlsx";
        //    */





        //}





        public String sp_SendToPlayerPreloadScenePresentazioneGiocatori()
        {
            DB.EseguiSQL("exec sp_SendToPlayerPreloadScenePresentazioneGiocatori ");
            return "ok";
        }




        public String sp_SendToPlayerUnloadScenePresentazioneGiocatori()
        {
            DB.EseguiSQL("exec sp_SendToPlayerUnloadScenePresentazioneGiocatori ");
            return "ok";
        }

        public String sp_GetTipoEventiMatch()
        {
            return JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GetTipoEventiMatch "));
           // return "ok";

        }

        public String sp_RipristinaAndamentoPlaylist()
        {
            string a = Request.Query["int_Adesso"].ToString();
            return JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_RipristinaAndamentoPlaylist @int_Adesso="+ a));
           // return "ok";

        }

        public String sp_AzioneTastiMaxischermo()
        {
            try
            {
                string oggetto = Request.Query["oggetto"].ToString();
                DB.EseguiSQL("exec sp_AzioneTastiMaxischermo @nva_Oggetto = '"+ oggetto + "' ");
                return "ok";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "AzioneTastiMaxischermo");
                return ex.Message.ToString();
            }
        }





        [HttpPost]
        public string InsertUpdateMatch(tb_Match _tb_Match)
        {
            try
            {
                string StringaMatch = DB.GetDataFromSQL(" select nva_Nome from tb_squadre where id = " + _tb_Match.fk_int_SquadraCasa.ToString()).Rows[0][0].ToString()+" VS "+ DB.GetDataFromSQL(" select nva_Nome from tb_squadre where id = " + _tb_Match.fk_int_SquadraOspite.ToString()).Rows[0][0].ToString();

                

                _tb_Match.nva_Titolo = StringaMatch;

                if (_tb_Match.Id > 0)
                {
                    tb_Match m = new tb_Match(_tb_Match.Id);
                     _tb_Match.IsSelected = m.IsSelected;
                    _tb_Match.DB_Update();
                }
                else
                {
                    _tb_Match.DB_InsertInto();
                }
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
            return "OK";
        }








        public String sp_GetListaScenePreloadLed()
        {
            try
            {
                string fk_int_Periodo = Request.Query["fk_int_Periodo"].ToString();
                string fk_int_TipoPeriodo = Request.Query["fk_int_TipoPeriodo"].ToString();
                if (fk_int_Periodo == "") fk_int_Periodo = "0";
                if (fk_int_TipoPeriodo == "") fk_int_TipoPeriodo = "0";
                string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GetListaScenePreloadLed  @fk_int_Periodo=" + fk_int_Periodo + ", @fk_int_TipoPeriodo=" + fk_int_TipoPeriodo));
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
        }




        public String sp_ResetTimelineLoadingScene()
        {
            try
            {

                string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_ResetTimelineLoadingScene"));
                return "";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
        }




        public String sp_SetupSchermataUI()
        {
            try
            {
                string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_SetupSchermataUI"));
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
        }




        public String sp_SendToPlayerArraySceneNonScaricabili()
        {
            try
            {
                DB.EseguiSQL("exec sp_SendToPlayerArraySceneNonScaricabili");
                return "ok" ;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
        }


        public String sp_GetListaSceneTestSimone()
        {
            string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("SELECT [ID],[int_Durata],[int_Ordine],[tin_IsLega] FROM [tb_SceneLedTestSimone] order by int_ordine"));
            return json;
        }

        public String sp_GetListaSceneLedPerOrdinamento()
        {
            try
            {
                string fk_int_periodo = HttpContext.Request.Form["fk_int_Periodo"].ToString();

                DataTable dt = DB.GetDataFromSQL("sp_GetListaSceneLedPerOrdinamento  @fk_int_periodo=" + fk_int_periodo);
                foreach (DataRow dr in dt.Rows)
                {
                    string label = dr["nva_ListaFile"].ToString();
                    string[] nomiSpot = label.Split(',');
                    List<string> nomiUnici = nomiSpot.Distinct().Where(x => !x.Equals("TRASPARENTE")).ToList();
                    string stringaUnita = String.Join(",", nomiUnici);
                    if (stringaUnita.Length > 50)
                        stringaUnita = stringaUnita.Substring(0, 50);
                    dr["nva_ListaFile"] = stringaUnita;
                }
                string json = JsonConvert.SerializeObject(dt);
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
          
        }

        public String sp_GetListaSceneMaxischermoPerOrdinamento()
        {
            try
            {
                string fk_int_periodo = HttpContext.Request.Form["fk_int_Periodo"].ToString();

                string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("sp_GetListaSceneMaxischermoPerOrdinamento  @fk_int_periodo=" + fk_int_periodo));
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
          
        }
        [HttpPost]
        public string sp_OrdinaSceneTestSimone()
        {
            string nva_Ordine = HttpContext.Request.Form["ordine"];
            return DB.GetDataFromSQL("sp_AggiornaOrdinamento '" + nva_Ordine + "'").Rows[0][0].ToString();
        }
        [HttpPost]
        public string sp_AggiornaOrdinamentoLed()
        {
            try
            {
                string nva_Ordine = HttpContext.Request.Form["ordine"];
                string fk_int_periodo = HttpContext.Request.Form["fk_int_periodo"];
                
                return DB.GetDataFromSQL("sp_AggiornaOrdinamentoLed @fk_int_periodo=" + fk_int_periodo + ",@nva_Ordine='" + nva_Ordine + "'").Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
          
        }
        [HttpPost]
        public string sp_AggiornaOrdinamentoMaxischermo()
        {
            try
            {
                string nva_Ordine = HttpContext.Request.Form["ordine"];
                string fk_int_periodo = HttpContext.Request.Form["fk_int_periodo"];
                
                return DB.GetDataFromSQL("sp_AggiornaOrdinamentoMaxischermo @fk_int_periodo=" + fk_int_periodo + ",@nva_Ordine='" + nva_Ordine + "'").Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
      
        }
        public String sp_GetListaScenePreloadMaxischermo()
        {
            try
            {
                string fk_int_Periodo = Request.Query["fk_int_Periodo"].ToString();
                string fk_int_TipoPeriodo = Request.Query["fk_int_TipoPeriodo"].ToString();
                if (fk_int_Periodo == "") fk_int_Periodo = "0";
                if (fk_int_TipoPeriodo == "") fk_int_TipoPeriodo = "0";
                string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GetListaScenePreloadMaxischermo  @fk_int_Periodo=" + fk_int_Periodo + ", @fk_int_TipoPeriodo="+ fk_int_TipoPeriodo));
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
          
        }
        public String sp_GetStatoAct()
        {
            try
            {
                string json = "";
                DataTable dt = DB.GetDataFromSQL("exec sp_GetStatoAct ");
                if (dt.Rows.Count > 0)
                {
                    json = JsonConvert.SerializeObject(dt);
                }
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
          
        }

        public string GenerazioneEventi()
        {
            String Destinazione = "maxischermo";
            try
            {
                Destinazione = (HttpContext.Request.Query["destinazione"]).ToString();
            }
            catch (Exception e)
            {

            }

            ProcessStartInfo psi = new ProcessStartInfo();
            psi.FileName = @"C:\NGMPlay\UCE\UCE.exe";
            psi.WorkingDirectory = Path.GetDirectoryName(psi.FileName);
            psi.Arguments = Destinazione;
            psi.UseShellExecute = true;
            var proc = Process.Start(psi);

            return "";
        }



        public String GetEventiFromPath()
        {
            try
            {
                String json = "";

                String[] ArrExtAmmese = StaticStartConfig.Parametro_nva_EstensioniConsentiteDentroCartellaEventi.Split(",");


                /*
                 * 
                 * nva_DirAssetsEventiLed
                 *  nva_DirAssetsEventiMaxischermo
                 */


                DataTable dt = DB.GetDataFromSQL("select top(1) nva_Directory  from tb_Match m  join tb_Squadre s on m.fk_int_SquadraCasa = s.ID where m.IsSelected = 1");
                if(dt.Rows.Count == 1)
                {
                    String PathEventiLed = StaticStartConfig.Parametro_nva_DirAssetsEventiLed.Replace("[nomesquadra]", dt.Rows[0][0].ToString());
                    String PathEventiMaxischermo = StaticStartConfig.Parametro_nva_DirAssetsEventiMaxischermo.Replace("[nomesquadra]", dt.Rows[0][0].ToString());

                    DirectoryInfo dm = Directory.GetParent(PathEventiMaxischermo);
                    DirectoryInfo dl = Directory.GetParent(PathEventiLed);

                    if (!dm.Exists) Directory.GetParent(PathEventiMaxischermo).Create();
                    if (!dl.Exists) Directory.GetParent(PathEventiLed).Create();
  
                    if (!Directory.Exists(PathEventiLed)) Directory.CreateDirectory(PathEventiLed);
                    if (!Directory.Exists(PathEventiMaxischermo)) Directory.CreateDirectory(PathEventiMaxischermo);




                    string[] eventiLed = Directory.GetFiles(PathEventiLed);
                    string[] eventiMaxischermo= Directory.GetFiles(PathEventiMaxischermo);
                    Dictionary<string, FileEvento> FileDentroLed = new Dictionary<string, FileEvento>();
                    Dictionary<string, FileEvento> FileDentroMaxischermo = new Dictionary<string, FileEvento>();

                    List<FileEvento> eventiValidati = new List<FileEvento>();


                    foreach (string a in eventiLed)
                    {
                        FileInfo fi = new FileInfo(a);
                        FileEvento fe = new FileEvento();
                        fe.Nome = fi.Name.ToUpper();
                        fe.Tipo = fi.Extension.Replace(".", "").ToLower();
                        fe.isLed = true;
                        fe.isMaxischermo = false;
                        if (ArrExtAmmese.Contains(fe.Tipo))
                        {
                            FileDentroLed.Add(fe.Nome,fe);
                        }
                    }
                    foreach (string a in eventiMaxischermo)
                    {
                        FileInfo fi = new FileInfo(a);
                        FileEvento fe = new FileEvento();
                        fe.Nome = fi.Name.ToUpper();
                        fe.Tipo = fi.Extension.Replace(".", "").ToLower();
                        fe.isLed = false;
                        fe.isMaxischermo =true;
                        if (ArrExtAmmese.Contains(fe.Tipo))
                        {
                            FileDentroMaxischermo.Add(fe.Nome,fe);
                        }
                    }

                    foreach (KeyValuePair<String, FileEvento> a in FileDentroLed)
                    {
                        if (FileDentroMaxischermo.ContainsKey(a.Key))
                        {
                            a.Value.isMaxischermo = true;
                        }
                        eventiValidati.Add(a.Value);
                    }

                    foreach (KeyValuePair<String, FileEvento> a in FileDentroMaxischermo)
                    {
                        if(FileDentroLed.ContainsKey(a.Key))
                        {
                           
                        }
                        else
                        {
                            eventiValidati.Add(a.Value);
                        }
                    }



                    json = JsonConvert.SerializeObject(eventiValidati);

                }


                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }

        }



        public String AvviaRiproduzionePlaylistDaPeriodo()
        {
            try
            {
                string fk_int_Riproduzione = Request.Query["fk_int_Riproduzione"].ToString();
                string fk_int_Periodo = Request.Query["fk_int_Periodo"].ToString();
                string int_DataOraRichiestaAvvio = Request.Query["int_DataOraRichiestaAvvio"].ToString();
                string int_SkipToTime = Request.Query["int_SkipToTime"].ToString();
                if (int_SkipToTime.Length == 0)
                    int_SkipToTime = "0";
                string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GeneraRiproduzionePeriodoLed @fk_int_Riproduzione=" + fk_int_Riproduzione.ToString() + ", @fk_int_Periodo=" + fk_int_Periodo.ToString() + ",@int_DataOraRichiestaAvvio=" + int_DataOraRichiestaAvvio.ToString() + ", @out= 2 ,@int_SkipToTime=" + int_SkipToTime));
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
        }



        //    http://localhost:5000/Ajax/sp_GestisciRiproduzioneLed?fk_int_PeriodoDaLanciare=66&int_DataOraJavascript=1594290711635&int_SkipToMillesimoSecondoSpecificoPeriodo=200000
        public String sp_GestisciRiproduzioneLed()
        {
            try
            {



                string int_IsScenaInterrotta = Request.Query["int_IsScenaInterrotta"].ToString();
                string fk_int_PeriodoDaLanciare = Request.Query["fk_int_PeriodoDaLanciare"].ToString();
                string int_DataOraJavascript = Request.Query["int_DataOraJavascript"].ToString();
                string int_SkipToMillesimoSecondoSpecificoPeriodo = Request.Query["int_SkipToMillesimoSecondoSpecificoPeriodo"].ToString();
                string id_int_SceneRiproduzioniLed_inizio = Request.Query["id_int_SceneRiproduzioniLed_inizio"].ToString();
                string id_int_SceneRiproduzioniLed_fine = Request.Query["id_int_SceneRiproduzioniLed_fine"].ToString();
                string id_int_SceneRiproduzioniLed_ultimo = Request.Query["id_int_SceneRiproduzioniLed_ultimo"].ToString();
                string fk_int_ScenaSpecificaPush = Request.Query["fk_int_ScenaSpecificaPush"].ToString();
                string int_IsFromPlayer = Request.Query["int_IsFromPlayer"].ToString();
                string forzaTimeskip = Request.Query["forzaTimeskip"].ToString();




                if (int_IsFromPlayer.Length == 0)
                    int_IsFromPlayer = "0";
                if (int_IsScenaInterrotta.Length == 0)
                    int_IsScenaInterrotta = "0";
                if (id_int_SceneRiproduzioniLed_inizio.Length == 0)
                    id_int_SceneRiproduzioniLed_inizio = "0";
                if (id_int_SceneRiproduzioniLed_fine.Length == 0)
                    id_int_SceneRiproduzioniLed_fine = "0";
                if (id_int_SceneRiproduzioniLed_ultimo.Length == 0)
                    id_int_SceneRiproduzioniLed_ultimo = "0";
                if (int_SkipToMillesimoSecondoSpecificoPeriodo.Length == 0)
                    int_SkipToMillesimoSecondoSpecificoPeriodo = "0";
                if (int_DataOraJavascript.Length == 0)
                    int_DataOraJavascript = "0";
                if (fk_int_ScenaSpecificaPush.Length == 0)
                    fk_int_ScenaSpecificaPush = "0";
                if (fk_int_PeriodoDaLanciare.Length == 0)
                    fk_int_PeriodoDaLanciare = "0";
                if (forzaTimeskip.Length == 0)
                    forzaTimeskip = "0";

                /*
                 * Salva l'ora di lancio dei periodi regolamentari (tipo periodo = 1), in modo che venga poi utilizzata dalla stored 
                 * SettaAvvioPeriodoStandard
                 */
                if ("0".Equals(int_IsFromPlayer) && !"0".Equals(int_DataOraJavascript))
                {
                    DataTable dt = DB.GetDataFromSQL("select fk_int_tipoPeriodo from tb_Periodi where dtt_Cancellazione is null and id="+ fk_int_PeriodoDaLanciare);
                    int fk_int_TipoPeriodoDaLanciare = 0;
                    if (dt.Rows.Count > 0)
                    {
                        fk_int_TipoPeriodoDaLanciare = Convert.ToInt32(dt.Rows[0][0]);
                        if (fk_int_TipoPeriodoDaLanciare == 1)
                        {
                            try
                            {
                                int res = DB.EseguiSQL("UPDATE tb_OrariPeriodiSalvati SET int_startPlayingLed="+ int_DataOraJavascript + ", int_startPlayingMaxischermo=" + int_DataOraJavascript + 
                                                        " where fk_int_Periodo = " + fk_int_PeriodoDaLanciare);
                                if (res == 0)
                                {
                                    DB.EseguiSQL("INSERT INTO tb_OrariPeriodiSalvati (fk_int_Periodo, int_startPlayingLed, int_startPlayingMaxischermo) VALUES (" +
                                                fk_int_PeriodoDaLanciare + "," + int_DataOraJavascript + "," + int_DataOraJavascript + ")");
                                }
                            }
                            catch (Exception ex)
                            {
                                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                            }
                        }
                    }
                }

                string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GestisciRiproduzioneLed @int_IsFromPlayer= " + int_IsFromPlayer + ", @fk_int_ScenaSpecificaPush=" + fk_int_ScenaSpecificaPush + ",        @fk_int_PeriodoDaLanciare=" + fk_int_PeriodoDaLanciare + ", @int_DataOraJavascript=" + int_DataOraJavascript.ToString() + ", @int_SkipToMillesimoSecondoSpecificoPeriodo=" + int_SkipToMillesimoSecondoSpecificoPeriodo.ToString() + ", @id_int_SceneRiproduzioniLed_inizio=" + id_int_SceneRiproduzioniLed_inizio + ", @id_int_SceneRiproduzioniLed_fine=" + id_int_SceneRiproduzioniLed_fine + ", @id_int_SceneRiproduzioniLed_ultimo=" + id_int_SceneRiproduzioniLed_ultimo + ", @int_IsScenaInterrotta = " + int_IsScenaInterrotta + ", @forzaTimeskip = " + forzaTimeskip));
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        public String sp_GestisciRiproduzioneMaxischermo()
        {
            try
            {
                string int_IsScenaInterrotta = Request.Query["int_IsScenaInterrotta"].ToString();
                string fk_int_PeriodoDaLanciare = Request.Query["fk_int_PeriodoDaLanciare"].ToString();
                string int_DataOraJavascript = Request.Query["int_DataOraJavascript"].ToString();
                string int_SkipToMillesimoSecondoSpecificoPeriodo = Request.Query["int_SkipToMillesimoSecondoSpecificoPeriodo"].ToString();
                string id_int_SceneRiproduzioniMaxischermo_inizio = Request.Query["id_int_SceneRiproduzioniMaxischermo_inizio"].ToString();
                string id_int_SceneRiproduzioniMaxischermo_fine = Request.Query["id_int_SceneRiproduzioniMaxischermo_fine"].ToString();
                string id_int_SceneRiproduzioniMaxischermo_ultimo = Request.Query["id_int_SceneRiproduzioniMaxischermo_ultimo"].ToString();
                string fk_int_ScenaSpecificaPush = Request.Query["fk_int_ScenaSpecificaPush"].ToString();
                string int_IsFromPlayer = Request.Query["int_IsFromPlayer"].ToString();
                string int_MillisecondiDaRecuperareSuHome = Request.Query["int_MillisecondiDaRecuperareSuHome"].ToString();
                string forzaTimeskip = Request.Query["forzaTimeskip"].ToString();



                if (int_IsFromPlayer.Length == 0)
                    int_IsFromPlayer = "0";
                if (int_IsScenaInterrotta.Length == 0)
                    int_IsScenaInterrotta = "0";
                if (id_int_SceneRiproduzioniMaxischermo_inizio.Length == 0)
                    id_int_SceneRiproduzioniMaxischermo_inizio = "0";
                if (id_int_SceneRiproduzioniMaxischermo_fine.Length == 0)
                    id_int_SceneRiproduzioniMaxischermo_fine = "0";
                if (id_int_SceneRiproduzioniMaxischermo_ultimo.Length == 0)
                    id_int_SceneRiproduzioniMaxischermo_ultimo = "0";
                if (int_SkipToMillesimoSecondoSpecificoPeriodo.Length == 0)
                    int_SkipToMillesimoSecondoSpecificoPeriodo = "0";
                if (int_DataOraJavascript.Length == 0)
                    int_DataOraJavascript = "0";
                if (fk_int_ScenaSpecificaPush.Length == 0)
                    fk_int_ScenaSpecificaPush = "0";
                if (fk_int_PeriodoDaLanciare.Length == 0)
                    fk_int_PeriodoDaLanciare = "0";
                if (int_MillisecondiDaRecuperareSuHome.Length == 0)
                    int_MillisecondiDaRecuperareSuHome = "0";
                if (forzaTimeskip.Length == 0)
                    forzaTimeskip = "0";

                string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GestisciRiproduzioneMaxischermo @int_IsFromPlayer= " + int_IsFromPlayer + ", @fk_int_ScenaSpecificaPush=" + fk_int_ScenaSpecificaPush + ",  @fk_int_PeriodoDaLanciare=" + fk_int_PeriodoDaLanciare + ", @int_DataOraJavascript=" + int_DataOraJavascript.ToString() + ", @int_SkipToMillesimoSecondoSpecificoPeriodo=" + int_SkipToMillesimoSecondoSpecificoPeriodo.ToString() + ", @id_int_SceneRiproduzioniMaxischermo_inizio=" + id_int_SceneRiproduzioniMaxischermo_inizio + ", @id_int_SceneRiproduzioniMaxischermo_fine=" + id_int_SceneRiproduzioniMaxischermo_fine + ", @id_int_SceneRiproduzioniMaxischermo_ultimo=" + id_int_SceneRiproduzioniMaxischermo_ultimo + ", @int_IsScenaInterrotta = " + int_IsScenaInterrotta + ", @int_MillisecondiDaRecuperareSuHome = " + int_MillisecondiDaRecuperareSuHome + ", @forzaTimeskip = " + forzaTimeskip));
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        public String sp_PreloadRiproduzionePeriodoLed()
        {
            try
            {
                string fk_int_Periodo = Request.Query["fk_int_Periodo"].ToString();
                string int_DataOraRichiestaAvvio = Request.Query["int_DataOraRichiestaAvvio"].ToString();
                string int_SkipToTime = Request.Query["int_SkipToTime"].ToString();
                string fk_int_TipoPeriodo = Request.Query["fk_int_TipoPeriodo"].ToString();
                string json = "";
                if (fk_int_TipoPeriodo != "" && fk_int_Periodo == "")
                {
                    json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_PreloadRiproduzionePeriodoLed  @fk_int_TipoPeriodo=" + fk_int_TipoPeriodo.ToString() + ", @int_DataOraRichiestaAvvio=" + int_DataOraRichiestaAvvio.ToString() + ", @int_SkipToTime=" + int_SkipToTime));
                }
                else
                {
                    json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_PreloadRiproduzionePeriodoLed  @fk_int_Periodo=" + fk_int_Periodo.ToString() + ", @int_DataOraRichiestaAvvio=" + int_DataOraRichiestaAvvio.ToString() + ",  @int_SkipToTime=" + int_SkipToTime));
                }
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        public String sp_PreloadRiproduzionePeriodoMaxischermo()
        {
            try
            {
                string fk_int_Periodo = Request.Query["fk_int_Periodo"].ToString();
                string int_DataOraRichiestaAvvio = Request.Query["int_DataOraRichiestaAvvio"].ToString();
                string int_SkipToTime = Request.Query["int_SkipToTime"].ToString();
                string fk_int_TipoPeriodo = Request.Query["fk_int_TipoPeriodo"].ToString();
                string json = "";
                if (fk_int_TipoPeriodo != "" && fk_int_Periodo == "")
                {
                    json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_PreloadRiproduzionePeriodoMaxischermo  @fk_int_TipoPeriodo=" + fk_int_TipoPeriodo.ToString() + ", @int_DataOraRichiestaAvvio=" + int_DataOraRichiestaAvvio.ToString() + ", @int_SkipToTime=" + int_SkipToTime));
                }
                else
                {
                    json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_PreloadRiproduzionePeriodoMaxischermo  @fk_int_Periodo=" + fk_int_Periodo.ToString() + ", @int_DataOraRichiestaAvvio=" + int_DataOraRichiestaAvvio.ToString() + ",  @int_SkipToTime=" + int_SkipToTime));
                }
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        public String sp_GeneraRiproduzione()
        {
            try
            {
                string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GeneraRiproduzione"));
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }

        public String sp_SetTimelineLoadingScene()
        {
            try
            {
                string id = Request.Query["id"].ToString();
                string azione = Request.Query["azione"].ToString();
                string soggetto = Request.Query["soggetto"].ToString();
                if (azione == "unload")
                {
                    DB.EseguiSQL("delete from tb_TimelineLoadingScene" + soggetto + " where fk_int_Scena  = "+ id);
                }
                else
                {
                    DB.EseguiSQL("insert into tb_TimelineLoadingScene"+ soggetto+ " (fk_int_Scena,dtt_Inserimento)values("+id+" ,getdate())");
                }
                return "ok";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }







        public void sp_GetListaEventiSpecialiPeriodiMatch()
        {
            try
            {
                string FK = Request.Query["fk_int_Periodo"].ToString();
                DB.GetDataFromSQL("exec sp_GetListaEventiSpecialiPeriodiMatch " + FK);
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
            }
        }


        public String sp_GetEventiMatch()
        {
            try
            {
                string id = Request.Query["id"].ToString();
                return JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GetEventiMatch " + id));
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }


        public String sp_GetDatiMaxischermo()
        {
            try
            {
                return JsonConvert.SerializeObject(DB.GetDataSetFromSQL("exec sp_GetDatiMaxischermo "));
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());

                return ex.Message.ToString();
            }
        }


        public String sp_EditaEventoMatch()
        {
            try
            {
                tb_EventiMatch ev = new tb_EventiMatch(Convert.ToInt32(Request.Query["id"].ToString()));
                ev.fk_int_Giocatore = Convert.ToInt32(Request.Query["fk_int_Giocatore"].ToString());
                ev.fk_int_GiocatoreSecondario = Convert.ToInt32(Request.Query["fk_int_GiocatoreSecondario"].ToString());
                ev.fk_int_TipoEventoMatch = Convert.ToInt32(Request.Query["fk_int_TipoEventoMatch"].ToString());
                ev.int_MinutoPartita = Convert.ToInt32(Request.Query["int_MinutoPartita"].ToString());
                ev.fk_int_Periodo  = Convert.ToInt32(Request.Query["fk_int_Periodo"].ToString());

                ev.DB_Update();
                return "ok";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return "ko";
            }
        }


        public String ArchiviaLog()
        {
            try
            {
                tb_Log l = new tb_Log();
                l.nva_messaggio = "ArchiviaLog: " + Request.Query["nva_Messaggio"].ToString();
                l.tin_Tipo = Convert.ToInt32(Request.Query["tin_Tipo"].ToString());
                l.DB_InsertInto();
                return "ok";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return "ko";
            }
        }





        public String sp_CancellaEventoMatch()
        {
            try
            {
                tb_EventiMatch ev = new tb_EventiMatch(Convert.ToInt32(Request.Query["id"].ToString()));
                ev.DB_Delete();
                DataRow d = DB.GetDataFromSQL("exec sp_GetNumeroGoalsMatch").Rows[0];
                tb_DatiToPlayer dtp = new tb_DatiToPlayer();
                dtp.nva_Dato = "AggiornaPunteggio({ 'casa': " + d["casa"].ToString() + ", 'ospiti': " + d["ospiti"].ToString() + " })";
                DB.DB_InsertInto(dtp);
                return "ok";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return "ko";
            }
        }


        


        [HttpPost]
        public string ArchiviaEvento(tb_EventiMatch Evento)
        {
            try
            {
                DataRow drMatch = DB.GetDataFromSQL("select top(1) id from  tb_Match  where isSelected = 1 ").Rows[0];
                DataRow drRiproduzione = DB.GetDataFromSQL("select top(1) id from tb_Riproduzioni where dtt_Cancellazione is null order by id desc ").Rows[0];

                DataTable dt = DB.GetDataFromSQL("select ID from tb_Periodi where int_StartPlayngLed > 0 and dtt_Cancellazione is null");
                int fk_int_Periodo=0;
                if (dt.Rows.Count > 0)
                {
                    fk_int_Periodo = Convert.ToInt32(dt.Rows[0][0]);
                }

                Evento.fk_int_Riproduzione = Convert.ToInt32(drRiproduzione["id"]);
                Evento.fk_int_Match = Convert.ToInt32(drMatch["id"]);
                Evento.fk_int_Periodo = fk_int_Periodo;
                DB.DB_InsertInto(Evento);
                if (Evento.fk_int_TipoEventoMatch == TipoEventi.Goal)
                {
                    DataRow d = DB.GetDataFromSQL("exec sp_GetNumeroGoalsMatch").Rows[0];
                    tb_DatiToPlayer dtp = new tb_DatiToPlayer();
                    dtp.nva_Dato = "AggiornaPunteggio({ 'casa': " + d["casa"].ToString() + ", 'ospiti': " + d["ospiti"].ToString() + " })";
                    DB.DB_InsertInto(dtp);
                }
                return "ok";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }




        public String tb_ProvenienzaPlaylist()
        {
            try
            {
                string FK = Request.Query["fk_int_ProvenienzaPlaylist"].ToString();
                if (FK == "")
                {
                    FK = "1";
                }
                return JsonConvert.SerializeObject(DB.GetDataFromSQL("select * from [tb_ProvenienzaPlaylist] where  ID_int_ProvenienzaPlaylist = " + FK));
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }

        public String GetPlaylist()
        {
            try
            {
                string FK = Request.Query["fk_int_Match"].ToString();
                if (FK == "")
                {
                    FK = "1";
                }
                return JsonConvert.SerializeObject(DB.GetDataFromSQL("sp_GetPlaylist @fk_int_Stadio=" + StaticStartConfig.ID_int_Stadio ));
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        public String sp_SendToPlayerDecodificaSquadre()
        {
            try
            {
                DB.GetDataFromSQL("exec sp_SendToPlayerDecodificaSquadre ");
                return "";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        public String sp_GetGiocatoriMatch()
        {
            try
            {
                string fk_int_Squadra = Request.Query["fk_int_Squadra"].ToString();
                return JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GetGiocatoriMatch @fk_int_Squadra=" + fk_int_Squadra));
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        public String GetRosaSquadra()
        {
            try
            {
                string fk_int_Squadra = Request.Query["fk_int_Squadra"].ToString();
                var dictionary = new Dictionary<int, tb_Giocatori>();
                List<object> Giocatori = DB.PopolaListaOggetti(typeof(tb_Giocatori), "select g.ID, g.nva_Nome,g.nva_Cognome ,g.nva_Numero, g.fk_int_Squadra from tb_giocatori g  where g.fk_int_Squadra = " + fk_int_Squadra);
                foreach (tb_Giocatori Giocatore in Giocatori)
                {
                    dictionary[Giocatore.ID] = Giocatore;
                }
                return JsonConvert.SerializeObject(dictionary);
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }

        public String GetSettingSquadra()
        {
            try
            {
                string fk_int_Squadra = Request.Query["fk_int_Squadra"].ToString();
                tb_Squadre squadra = new tb_Squadre(int.Parse(fk_int_Squadra));
                return JsonConvert.SerializeObject(squadra);
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }


        public String sp_GetSquadreCategoria()
        {
            try
            {
                return JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GetSquadreCategoria"));

            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }
        public String GetSquadre()
        {
            try
            {
                string id = Request.Query["id"].ToString();
                if (id.Length == 0 || id is null) id = "0";

                string fk_int_Stadio = Request.Query["fk_int_Stadio"].ToString();
                if (fk_int_Stadio.Length == 0 || fk_int_Stadio is null) fk_int_Stadio = "0";

                return JsonConvert.SerializeObject(DB.GetDataFromSQL("select * from tb_squadre where  (id= " + id + " or " + id + "=0) and  (fk_int_StadioDefault= " + fk_int_Stadio + " or " + fk_int_Stadio + "=0) and dtt_Cancellazione is null order by nva_Nome"));
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }





        public String GetMatch()
        {
            // return JsonConvert.SerializeObject(DB.GetDataFromSQL("SELECT (CONVERT(varchar,m.dtt_DataOra,103)+' Stadio: '+ st.nva_Nome +' '+ st.nva_Città  +' ('+s_casa.nva_Nome+' VS '+s_ospite.nva_Nome +')') nva_Titolo , m.ID, m.fk_int_SquadraCasa, m.fk_int_SquadraOspite, s_casa.nva_Nome as nva_NomeSquadraCasa, s_ospite.nva_Nome as nva_NomeSquadraOspite, CONVERT(varchar,m.dtt_DataOra,103) as nva_DataOra FROM [tb_Match] m join  tb_Squadre s_casa on s_casa.id = m.fk_int_SquadraCasa join tb_Squadre s_ospite on s_ospite.id = m.fk_int_SquadraOspite join tb_Stadi st on st.Id = m.Fk_int_Stadio where st.Id = " + StaticStartConfig.ID_int_Stadio.ToString() + " and dtt_DataOra >= cast(getdate() as date)"));
            
            try
            {
                string id = Request.Query["id"];
                if ((id == "") || (id == null)) id = "0";

                return JsonConvert.SerializeObject(DB.GetDataFromSQL("SELECT m.ID id, m.IsSelected , m.fk_int_SquadraCasa, m.fk_int_SquadraOspite, s_casa.nva_Nome as nva_NomeSquadraCasa, s_ospite.nva_Nome as nva_NomeSquadraOspite, DATEPART(yyyy,dtt_DataOra) yyyy,DATEPART(mm,dtt_DataOra) mm,DATEPART(dd,dtt_DataOra) dd,DATEPART(hh,dtt_DataOra) hh,DATEPART(n,dtt_DataOra) n   FROM [tb_Match] m join  tb_Squadre s_casa on s_casa.id = m.fk_int_SquadraCasa join tb_Squadre s_ospite on s_ospite.id = m.fk_int_SquadraOspite join tb_Stadi st on st.Id = m.Fk_int_Stadio where m.dtt_Cancellazione is null and ( m.id="+id+ " or " + id + "=0 ) and st.Id = " + StaticStartConfig.ID_int_Stadio.ToString()));
          
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }

        }





        public String sp_GetParametriIniziali()
        {
            try
            {
                Dictionary<string, string> Parametri = new Dictionary<string, string>();

                DataTable dt = DB.GetDataFromSQL("SELECT  * from tb_parametri " );
                foreach(DataRow dr in dt.Rows)
                {
                    Parametri.Add(dr["nva_NomeParametro"].ToString() , dr["nva_ValoreParametro"].ToString());
                }


                return JsonConvert.SerializeObject(Parametri);
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }

        public List<String> GetSheetsName(string PathFile)
        {
            FileInfo fileInfo = new FileInfo(PathFile);
            var excel = new ExcelPackage(fileInfo);
            List<String> fogli = new List<String>();
            foreach (var worksheet in excel.Workbook.Worksheets)
            {
                fogli.Add(worksheet.Name.ToUpper());
            }
            return fogli;
        }









        public string StepImportEventi()
        {
     
            String PathVideoStadio = StaticStartConfig.Parametro_nva_DirVideoSorgenti.Replace("[nomesquadra]", StaticStartConfig.Stadio.nva_NomeDirectory);

            if (System.IO.File.Exists(PathVideoStadio + "Eventi.xlsx"))
            {


                DB.EseguiSQL("delete from tb_periodi where id > 99; DBCC checkident ('tb_periodi', reseed, 100) ");

                int IdPlaylist = Convert.ToInt32(DB.GetDataFromSQL("select top(1) Id from tb_playlist order by id desc ").Rows[0][0]);

                tb_Match m = new tb_Match(Convert.ToInt32(GetSelected("tb_Match")));
                string NomeElaboratoEventi = "_Eventi_" + fg.PulisciStringa(m.SquadraCasa.nva_Nome + " vs " + m.SquadraOspite.nva_Nome + " " + DateTime.Now.ToString()) ;
                System.IO.File.Move(PathVideoStadio + "Eventi.xlsx", StaticStartConfig.Parametro_nva_DirPlaylist + NomeElaboratoEventi + ".xlsx");

                string PathFileEventi = StaticStartConfig.Parametro_nva_DirPlaylist + NomeElaboratoEventi + ".xlsx";

                List<string> ListaFogli = GetSheetsName(PathFileEventi);

                String NomePrimoFoglio = ListaFogli.First();


                //try
                //{
                //    new tb_LogPlaylist(IdPlaylist, "inizio creazione tabella Eventi" + "__xlsx_" + NomeElaboratoEventi);
                //    ImportazioneXlsx importLed = new ImportazioneXlsx(PathFilePlaylist, NomePrimoFoglio);
                //    DB.EseguiSQL(importLed.CreasqlDataTable("LED_" + p.nva_TableNamePlaylist));
                //    DB.InserisciDataTableNelDB(importLed.dtable, "__xlsx_" + NomeElaboratoEventi + "LED");
                //    new tb_LogPlaylist(IdPlaylist, "fine creazione tabella Eventi" + "__xlsx_" + NomeElaboratoEventi + "LED");
                //}
                //catch (Exception ex)
                //{
                //    //   new tb_LogPlaylist(IdPlaylist, "errore creazione tabella " + p.nva_TableName + "LED");
                //    GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                //    Console.WriteLine(ex.Message);
                //}


                try
                {
                    new tb_LogPlaylist(IdPlaylist, "inizio creazione tabella Playlist" + NomeElaboratoEventi);
                    ImportazioneXlsx importLed = new ImportazioneXlsx(PathFileEventi, NomePrimoFoglio);
                    DB.EseguiSQL(importLed.CreasqlDataTable("_LED" + NomeElaboratoEventi));
                    DB.InserisciDataTableNelDB(importLed.dtable, "_LED" + NomeElaboratoEventi);
                    DB.EseguiSQL("update tb_playlist set nva_TableNameEventi = '" + NomeElaboratoEventi + "' where id = " + IdPlaylist.ToString());
                    new tb_LogPlaylist(IdPlaylist, "fine creazione tabella " + "_LED" + NomeElaboratoEventi);
                }
                catch (Exception ex)
                {
                    new tb_LogPlaylist(IdPlaylist, "errore creazione tabella " + "_LED" + NomeElaboratoEventi);
                    GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                }



                new tb_LogPlaylist(IdPlaylist, "inizio Importazione contenuti Led");

                DB.EseguiSQL("sp_import_04_Importazione_Eventi_Led " + IdPlaylist.ToString());

                new tb_LogPlaylist(IdPlaylist, "fine Importazione contenuti Led");

                new tb_LogPlaylist(IdPlaylist, "inizio Importazione contenuti Maxischermo");

                DB.EseguiSQL("sp_import_05_Importazione_Eventi_Maxischermo " + IdPlaylist.ToString());

                new tb_LogPlaylist(IdPlaylist, "fine Importazione contenuti Maxischermo");


                GestioneLog.ArchiviaMessaggioLog("RicevoFormPlaylist dentro ajaxController OK fine metodo ", System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                
                
                return IdPlaylist.ToString();


            }
            return "";
        }






        //public String ImportaEventi()
        //{


        //    int IdPlaylist = Convert.ToInt32(DB.GetDataFromSQL("select top(1) Id from tb_playlist order by id desc ").Rows[0][0]);

        //    tb_Playlist p = new tb_Playlist(IdPlaylist);

        //    tb_Match m = new tb_Match(Convert.ToInt32(GetSelected("tb_Match")));

        //    p.nva_TableNameEventi = "_Eventi_" + fg.PulisciStringa(m.SquadraCasa.nva_Nome + " vs " + m.SquadraOspite.nva_Nome + " " + DateTime.Now.ToString()); ;

        //    String PathVideoStadio = StaticStartConfig.Parametro_nva_DirVideoSorgenti.Replace("[nomesquadra]", StaticStartConfig.Stadio.nva_NomeDirectory);

        //    string PathFileEventi = StaticStartConfig.Parametro_nva_DirPlaylist + p.nva_TableNameEventi + ".xlsx";

            

        //    p.DB_Update();

        //    new tb_LogPlaylist(IdPlaylist, "avvio importazione");


        //    List<string> ListaFogli = GetSheetsName(PathFileEventi);
        //    String NomePrimoFoglio = ListaFogli.First();
        //    try
        //    {
        //        new tb_LogPlaylist(IdPlaylist, "inizio creazione tabella Playlist" + p.nva_TableNamePlaylist);
        //        ImportazioneXlsx importLed = new ImportazioneXlsx(PathFileEventi, NomePrimoFoglio);
        //        DB.EseguiSQL(importLed.CreasqlDataTable("_Eventi" + p.nva_TableNamePlaylist));
        //        DB.InserisciDataTableNelDB(importLed.dtable, "_Eventi" + p.nva_TableNamePlaylist);
        //        new tb_LogPlaylist(IdPlaylist, "fine creazione tabella " + "Eventi" + p.nva_TableNamePlaylist);
        //    }
        //    catch (Exception ex)
        //    {
        //        new tb_LogPlaylist(IdPlaylist, "errore creazione tabella " + "Eventi" + p.nva_TableNamePlaylist);
        //        GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
        //        Console.WriteLine(ex.Message);
        //    }
        //    new tb_LogPlaylist(IdPlaylist, "inizio Importazione contenuti Led");
        //    DB.EseguiSQL("sp_import_02_Importazione_Playlist_Led " + IdPlaylist.ToString());
        //    new tb_LogPlaylist(IdPlaylist, "fine Importazione contenuti Led");
        //    new tb_LogPlaylist(IdPlaylist, "inizio Importazione contenuti Maxischermo");
        //    DB.EseguiSQL("sp_import_03_Importazione_Playlist_Maxischermo " + IdPlaylist.ToString());
        //    new tb_LogPlaylist(IdPlaylist, "fine Importazione contenuti Maxischermo");
        //    GestioneLog.ArchiviaMessaggioLog("RicevoFormPlaylist dentro ajaxController OK fine metodo ", System.Reflection.MethodInfo.GetCurrentMethod().ToString());
        //    StepImportEventi();
        //    return "xlsx";

        //}


        public string StepImportPlaylist(String PathFilePlaylistTmp, String nva_NomePlaylist, int FK_int_ProvenienzaPlaylist)
        {


           tb_Match m = new tb_Match(Convert.ToInt32(GetSelected("tb_Match")));
            string NomeElaboratoPlaylist = "_Playlist_" +fg.PulisciStringa(m.SquadraCasa.nva_Nome + " vs " + m.SquadraOspite.nva_Nome + " " + DateTime.Now.ToString());
          
            string PathFilePlaylist = StaticStartConfig.Parametro_nva_DirPlaylist + NomeElaboratoPlaylist+ ".xlsx" ;
            System.IO.File.Move(PathFilePlaylistTmp, PathFilePlaylist );
            DB.EseguiSQL("update tb_Playlist set tin_ElaboraScene = 0");
            tb_Playlist p = new tb_Playlist();
            p.nva_NomePlaylist = nva_NomePlaylist;
            p.fk_int_Stadio = m.fk_int_Stadio;
            p.tin_ElaboraScene = 1;
            p.fk_int_ProvenienzaPlaylist = FK_int_ProvenienzaPlaylist;
            p.nva_TableNamePlaylist = NomeElaboratoPlaylist;
            p.nva_TableNameEventi = "";

            p.dtt_DataCaricamento = DateTime.Now;
            int IdPlaylist = DB.DB_InsertInto(p);
            new tb_LogPlaylist(IdPlaylist, "avvio importazione");


            List<string> ListaFogli = GetSheetsName(PathFilePlaylist );

            String NomePrimoFoglio = ListaFogli.First();

            try
            {
                new tb_LogPlaylist(IdPlaylist, "inizio creazione tabella Playlist" + p.nva_TableNamePlaylist);
                ImportazioneXlsx importLed = new ImportazioneXlsx(PathFilePlaylist , NomePrimoFoglio);

                DB.EseguiSQL(importLed.CreasqlDataTable("_LED"+p.nva_TableNamePlaylist  ));
                DB.InserisciDataTableNelDB(importLed.dtable, "_LED"+ p.nva_TableNamePlaylist );

                new tb_LogPlaylist(IdPlaylist, "fine creazione tabella " + "LED"+ p.nva_TableNamePlaylist  );
            }
            catch (Exception ex)
            {
                new tb_LogPlaylist(IdPlaylist, "errore creazione tabella " + "LED" + p.nva_TableNamePlaylist );
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                Console.WriteLine(ex.Message);
            }

            new tb_LogPlaylist(IdPlaylist, "inizio Importazione contenuti Led");

            DB.EseguiSQL("sp_import_02_Importazione_Playlist_Led " + IdPlaylist.ToString());

            new tb_LogPlaylist(IdPlaylist, "fine Importazione contenuti Led");

            new tb_LogPlaylist(IdPlaylist, "inizio Importazione contenuti Maxischermo");

            DB.EseguiSQL("sp_import_03_Importazione_Playlist_Maxischermo " + IdPlaylist.ToString());

            new tb_LogPlaylist(IdPlaylist, "fine Importazione contenuti Maxischermo");

            GestioneLog.ArchiviaMessaggioLog("RicevoFormPlaylist dentro ajaxController OK fine metodo ", System.Reflection.MethodInfo.GetCurrentMethod().ToString());

            return IdPlaylist.ToString();
        }




        [HttpPost]
        public string RicevoFormPlaylistOld()
        {
            try
            {
                var file = HttpContext.Request.Form.Files["FileDaCaricare"];
                string nva_NomePlaylist = HttpContext.Request.Form["nva_NomePlaylist"];
                string TmpFile = "tmp_" + fg.PulisciStringa(DateTime.Now.ToString()) + ".xlsx";
                string PathFilePlaylistTmp = StaticStartConfig.Parametro_nva_DirPlaylist + "tmp_" + fg.PulisciStringa(DateTime.Now.ToString()) + ".xlsx";
                string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                string FileExtension = Path.GetExtension(fileName);
                if (FileExtension.Contains("xlsx"))
                {
                    using (FileStream fs = System.IO.File.Create(PathFilePlaylistTmp))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }

                    return StepImportPlaylist(PathFilePlaylistTmp, nva_NomePlaylist, 2);
                }
                else
                {
                    return "File Non XLSX";
                }

            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }

        }



        [HttpPost]
        [RequestSizeLimit(5000000000)]
        public string RicevoFormAssets()
        {
            try
            {
                var file = HttpContext.Request.Form.Files["FileAssetsDaCaricare"];
                string TmpFile = "assets_" + fg.PulisciStringa(DateTime.Now.ToString()) + ".zip";

                string PathFilePlaylistTmp = StaticStartConfig.Parametro_nva_DirPlaylist + TmpFile;
                if (System.IO.File.Exists(PathFilePlaylistTmp)) fg.DeleteDirectory(PathFilePlaylistTmp);

                string PathFilePlaylistTmpDecompattata = @"C:\NGMPlay\publish\wwwroot\tmp_extract\";
                if (Directory.Exists(PathFilePlaylistTmpDecompattata)) fg.DeleteDirectory(PathFilePlaylistTmpDecompattata);

                if (!Directory.Exists(PathFilePlaylistTmpDecompattata)) Directory.CreateDirectory(PathFilePlaylistTmpDecompattata);
                string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                string FileExtension = Path.GetExtension(fileName);
                if (FileExtension.Contains("zip"))
                {
                    using (FileStream fs = System.IO.File.Create(PathFilePlaylistTmp))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }

                    ZipFile.ExtractToDirectory(PathFilePlaylistTmp, PathFilePlaylistTmpDecompattata);
                    System.IO.File.Delete(PathFilePlaylistTmp);

                    string[] ListaDir = Directory.GetDirectories( PathFilePlaylistTmpDecompattata.TrimEnd('/').TrimEnd('\\') + @"\Squadre\");

                    String DirSquadraPlayers;
                    String DirSquadra;

                    foreach (string L in ListaDir)
                    {
                        DirSquadraPlayers = StaticStartConfig.Parametro_nva_DirAssetsPlayers.Replace("[nomesquadra]", L.Split(Path.DirectorySeparatorChar).Last()).TrimEnd('/').TrimEnd('\\');
                        DirSquadra = DirSquadraPlayers.Substring(0, DirSquadraPlayers.LastIndexOf(Path.DirectorySeparatorChar)) + @"\";
                        DirSquadraPlayers = DirSquadraPlayers + @"\";

                        if (Directory.Exists(DirSquadra)){
                            if (Directory.Exists(DirSquadraPlayers))
                            {
                                if(!Directory.Exists(DirSquadraPlayers + @"\led\")) Directory.CreateDirectory(DirSquadraPlayers + @"\led\");
                                if(!Directory.Exists(DirSquadraPlayers + @"\maxischermo\")) Directory.CreateDirectory(DirSquadraPlayers + @"\maxischermo\");

                            }
                            else
                            {
                                Directory.CreateDirectory(DirSquadraPlayers);
                                Directory.CreateDirectory(DirSquadraPlayers + @"\led\" );
                                Directory.CreateDirectory(DirSquadraPlayers + @"\maxischermo\" );
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(DirSquadra);
                            Directory.CreateDirectory(DirSquadraPlayers);
                            Directory.CreateDirectory(DirSquadraPlayers + @"\led\");
                            Directory.CreateDirectory(DirSquadraPlayers + @"\maxischermo\");
                        }

                    }



                    foreach (string L in ListaDir)
                    {
                        DirSquadraPlayers = StaticStartConfig.Parametro_nva_DirAssetsPlayers.Replace("[nomesquadra]", L.Split(Path.DirectorySeparatorChar).Last()).TrimEnd('/').TrimEnd('\\');
                        DirSquadra = DirSquadraPlayers.Substring(0, DirSquadraPlayers.LastIndexOf(Path.DirectorySeparatorChar)) + @"\";
                        DirSquadraPlayers = DirSquadraPlayers + @"\";
                        String DirSquadraPlayersLed = DirSquadraPlayers + @"Led\";
                        String DirSquadraPlayersMaxischermo = DirSquadraPlayers + @"Maxischermo\";
                        if (Directory.Exists(L + @"\players\Led\")) {
                            foreach (string pathFile in Directory.GetFiles(L + @"\players\Led\"))
                            {
                                CopiaImmagini(pathFile, DirSquadraPlayersLed);
                            }
                        }

                        if (Directory.Exists(L + @"\players\Maxischermo\"))
                        {
                            foreach (string pathFile in Directory.GetFiles(L + @"\players\Led\"))
                            {
                                CopiaImmagini(pathFile, DirSquadraPlayersMaxischermo);
                            }
                        }

                    }





                    return "Upload terminato";
                }
                else
                {
                    return "File Non ZIP";
                }
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }

        }





        private void CopiaImmagini(String PathFileSource, String PathDestination)
        {
            FileInfo fnew = new FileInfo(PathFileSource);
            if (System.IO.File.Exists(PathDestination + fnew.Name))
            {
                FileInfo fold = new FileInfo(PathFileSource);
                if (fnew.LastWriteTime > fold.LastWriteTime)
                {
                    System.IO.File.Delete(PathDestination + fnew.Name);
                    System.IO.File.Move(fnew.FullName, PathDestination + fnew.Name);
                }
            }
            else
            {
                System.IO.File.Move(fnew.FullName, PathDestination + fnew.Name);
            }
           
        }












        [HttpPost]
        [RequestSizeLimit(500000000)]
        public string RicevoFormPlaylist()
        {
            try
            {
                var file = HttpContext.Request.Form.Files["FileDaCaricare"];
                string nva_NomePlaylist = HttpContext.Request.Form["nva_NomePlaylist"];
                string TmpFile = "tmp_"+fg.PulisciStringa(DateTime.Now.ToString())+".zip";

                

                string PathFilePlaylistTmp = StaticStartConfig.Parametro_nva_DirPlaylist+"tmp_" + fg.PulisciStringa(DateTime.Now.ToString()) + ".zip";
                if (System.IO.File.Exists(PathFilePlaylistTmp)) fg.DeleteDirectory(PathFilePlaylistTmp);
                
                string PathFilePlaylistTmpDecompattata = StaticStartConfig.Parametro_nva_DirPlaylist + "uncompressed";
                if (Directory.Exists(PathFilePlaylistTmpDecompattata)) fg.DeleteDirectory(PathFilePlaylistTmpDecompattata);

                if (!Directory.Exists(PathFilePlaylistTmpDecompattata)) Directory.CreateDirectory(PathFilePlaylistTmpDecompattata);
                string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                string FileExtension = Path.GetExtension(fileName);
                if (FileExtension.Contains("zip")) { 
                    using (FileStream fs = System.IO.File.Create(PathFilePlaylistTmp))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                    ZipFile.ExtractToDirectory(PathFilePlaylistTmp, PathFilePlaylistTmpDecompattata);
                    System.IO.File.Delete(PathFilePlaylistTmp);


                    String[] mp4 = Directory.GetFiles(PathFilePlaylistTmpDecompattata, "*.mp4");


                    string nva_DirVideoSorgenti =  StaticStartConfig.Parametro_nva_DirVideoSorgenti.Replace("[nomesquadra]", StaticStartConfig.Stadio.nva_NomeDirectory);

                    int NumeroTotale = 0;
                    int NumeroIgnorati = 0;
                    int NumeroAggiornati = 0;
                    int NumeroNuovi = 0;

                    foreach (String s in mp4 )
                    {
                        NumeroTotale++;
                        FileInfo fiNew = new FileInfo(s);
                        if(System.IO.File.Exists(nva_DirVideoSorgenti + fiNew.Name))
                        {
                            FileInfo fiOld = new FileInfo(nva_DirVideoSorgenti + fiNew.Name);
                            if(fiOld.GetHashCode() != fiNew.GetHashCode())
                            {
                                NumeroAggiornati++;
                                System.IO.File.Delete(nva_DirVideoSorgenti + fiNew.Name);
                                System.IO.File.Move(s, nva_DirVideoSorgenti + fiNew.Name);
                             //   Console.WriteLine("AGGIORNATO: "+ fiNew.Name);
                            }
                            else
                            {
                                NumeroIgnorati++;
                              //  Console.WriteLine("IGNORATO: " + fiNew.Name);
                            }

                        }
                        else
                        {
                            NumeroNuovi++;
                            System.IO.File.Move(s, nva_DirVideoSorgenti + fiNew.Name);
                           // Console.WriteLine("NUOVO: " + fiNew.Name);
                        }
                        System.IO.File.Delete(s);
                    }


                    GestioneLog.ArchiviaMessaggioLog("Totali:"+ NumeroTotale + " Aggiornati:" + NumeroAggiornati +" Ignorati:" + NumeroIgnorati +" Nuovi:" + NumeroNuovi  , System.Reflection.MethodInfo.GetCurrentMethod().ToString());

                    String[] xlsx = Directory.GetFiles(PathFilePlaylistTmpDecompattata, "*.xlsx");
                    if (xlsx.Length > 1)
                    {
                        if (Directory.Exists(PathFilePlaylistTmpDecompattata)) fg.DeleteDirectory(PathFilePlaylistTmpDecompattata);
                        return "Il file zip contiene molteplici file xlsx";
                    }
                    else if (xlsx.Length == 1)
                    {
                        PathFilePlaylistTmp = xlsx[0];
                        StepImportPlaylist(PathFilePlaylistTmp, "Da ZIP: "+ nva_NomePlaylist +" " +DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), 1);
                    }


                    // return StepImportPlaylist(PathFilePlaylistTmp, nva_NomePlaylist, 2); 

                    return "Procedura di upload terminata, attendi il caricamento delle scene";
                }
                else
                {
                    return "File Non ZIP";
                }


               
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }

        }




        [HttpPost]
        public string ScriviLog()
        {
            try
            {
                string msg = HttpContext.Request.Form["msg"];
                string ts = HttpContext.Request.Form["ts"];
                GestioneLog.ArchiviaErroreLog("Errore javascript: " + System.Web.HttpUtility.UrlDecode(msg).Replace("'", "''"), System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                // DB.EseguiSQL("insert into [tb_LogFromJs] ([nva_messaggio],int_timestamp,dtt_Modifica) values ('" + System.Web.HttpUtility.UrlDecode(msg).Replace("'", "''") + "','" + System.Web.HttpUtility.UrlDecode(ts).Replace("'", "''") + "',getdate())");
                return "ok";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }





        [HttpPost]
        public string ArchiviaRecordDebug()
        {
            string nva_Messaggio = HttpContext.Request.Form["nva_Messaggio"];
            string nva_NomeArray = HttpContext.Request.Form["nva_NomeArray"];
            string nva_Contenuto = HttpContext.Request.Form["nva_Contenuto"];
            if (nva_Contenuto == null)
            {
                nva_Contenuto = "";
            }
            string nva_NumeroElementi = HttpContext.Request.Form["nva_NumeroElementi"];
            if (nva_NumeroElementi == null)
            {
                nva_NumeroElementi = "";
            }
            DB.EseguiSQL("insert into [tb_Debug] values ('" + nva_Messaggio.Replace("'", "") + "','" + nva_NomeArray.Replace("'", "") + "','" + nva_Contenuto.Replace("'", "") + "','" + nva_NumeroElementi.Replace("'", "") + "',getdate())");
            
            /*
            [id]
            ,[nva_Messaggio]
            ,[nva_NomeArray]
            ,[nva_Contenuto]
            ,[nva_NumeroElementi]
            ,[dtt_Dataora]  
            */

            return "ok";
        }



        [HttpPost]
        public string SetFormazione([FromBody] List<tb_Formazione> tb_FormazioneList)
        {
            try
            {
                foreach (tb_Formazione f in tb_FormazioneList)
                {
                    GestioneLog.ArchiviaMessaggioLog("delete from tb_Formazione where fk_int_Match = " + f.fk_int_Match + " and  fk_int_Giocatore = " + f.fk_int_Giocatore, "SetFormazione");


                    DB.EseguiSQL("delete from tb_Formazione where fk_int_Match = " + f.fk_int_Match + " and  fk_int_Giocatore = " + f.fk_int_Giocatore);
                    if (f.int_Ordine > 0)
                    {
                        DB.DB_InsertInto(f);
                        GestioneLog.ArchiviaMessaggioLog("DB_InsertInto", "SetFormazione");
                    }

                    // DB.DB_InsertInto(f);
                }
            }
            catch (Exception ex){
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "SetFormazione");
                return ex.Message.ToString();
            }
            return "OK";
        }



        [HttpPost]
        public string SetDatiToUI([FromBody] List<tb_DatiToUi> dati)
        {
            try
            {
                foreach (tb_DatiToUi f in dati)
                {
                    f.dtt_Scrittura = DateTime.Now;
                    DB.DB_InsertInto(f);
                }
                return "OK";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }


        [HttpPost]
        public string SetDatiToPlayer([FromBody] List<tb_DatiToPlayer> dati)
        {

            if (DB.GetDataFromSQL(" select * from [tb_PC] where nva_LocaleRemoto = 'Locale' and (( tin_IsOnline = 0 or  tin_IsMaster =1) or (DATEDIFF(minute,[dtt_UltimoControlloOnline] , getdate())>15) )").Rows.Count > 0)
            {

                try
                {
                    foreach (tb_DatiToPlayer f in dati)
                    {
                        f.dtt_Scrittura = DateTime.Now;
                        DB.DB_InsertInto(f);
                    }
                }
                catch (Exception ex)
                {
                    GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                    return ex.Message.ToString();
                }
            }

            return "OK";
        }

        private String GetIpFormattato()
        {
            return HttpContext.Connection.RemoteIpAddress.ToString().Replace("::1", "127.0.0.1").Replace("::ffff:", "").Replace(":", "_").Replace(".", "_");
        }

        public String GetDatiToUi()
        {
            try
            {
                String ColonnaDateTimeLetturaDevice = "dtt_Lettura_" + GetIpFormattato(); // HttpContext.Connection.RemoteIpAddress.ToString().Replace("::1","127.0.0.1").Replace("::ffff:","").Replace(":", "_").Replace(".", "_");
                String S;
                DataTable dt;
                try
                {
                    dt = DB.GetDataFromSQL("select ID, nva_Dato from tb_DatiToUi where "+ ColonnaDateTimeLetturaDevice + " is NULL order by ID");
                }catch (Exception ex){
                    DB.EseguiSQL("alter table tb_DatiToUi add " + ColonnaDateTimeLetturaDevice + " datetime NULL;   ");
                    DB.EseguiSQL("update      tb_DatiToUi set " + ColonnaDateTimeLetturaDevice + " =   getdate()  ");
                    dt = DB.GetDataFromSQL("select ID, nva_Dato from tb_DatiToUi where " + ColonnaDateTimeLetturaDevice + " is NULL order by ID");
                }
                S = JsonConvert.SerializeObject(dt);
                DB.EseguiSQL("update tb_DatiToUi set "+ ColonnaDateTimeLetturaDevice + " = getdate() where " + ColonnaDateTimeLetturaDevice + " is NULL");
                return S;
            }catch (Exception ex){
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }



        public String GetDatiToPlayer()
        {
            try
            {
                String S;
                S = JsonConvert.SerializeObject(DB.GetDataFromSQL("select ID, nva_Dato from tb_DatiToPlayer where dtt_LetturaPC"+ StaticStartConfig.int_IdentificazionePC.ToString() + " is NULL and dtt_Cancellazione is NULL order by ID"));
                DB.EseguiSQL("update tb_DatiToPlayer set dtt_LetturaPC" + StaticStartConfig.int_IdentificazionePC.ToString() + "  = getdate() where dtt_LetturaPC" + StaticStartConfig.int_IdentificazionePC.ToString() + "  is NULL");
                return S;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }


        public String SetExecutedFromUiToPlayer()
        {
            try
            {
                string ID = Request.Query["ID"].ToString();
                DB.EseguiSQL("update tb_DatiToPlayer set dtt_LetturaPC" + StaticStartConfig.int_IdentificazionePC.ToString() + "  = GETDATE() where ID = " + ID);
                return "ok";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }

        //public String SetExecutedFromPlayerToUi()
        //{
        //    try
        //    {
        //        string ID = Request.Query["ID"].ToString();
        //        DB.EseguiSQL("update tb_DatiToUi set dtt_Lettura  = GETDATE() where ID = " + ID);
        //        return "ok";
        //    }
        //    catch (Exception ex)
        //    {
        //        GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
        //        return ex.Message.ToString();
        //    }
        //}

        public String CancellaPlaylist()
        {
            try
            {
                int ID = int.Parse(Request.Query["ID"].ToString());
                DB.EseguiSQL("update tb_ContenutiLed set dtt_Cancellazione = getdate() where fk_int_scena in(select ID  from tb_sceneLed where fk_int_Playlist = " + ID.ToString() + " )");
                DB.EseguiSQL("update tb_sceneLed set dtt_Cancellazione = getdate() where fk_int_Playlist = " + ID.ToString() + " ");
                new tb_Playlist(ID).DB_Delete();
                return "ok";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }


        public String SetSelected()
        {
            try
            {
                int ID = int.Parse(Request.Query["ID"].ToString());
                string tb = Request.Query["tb"].ToString();

                DB.EseguiSQL("update " + tb + " set IsSelected = 0 where ID <> " + ID);
                DB.EseguiSQL("update " + tb + " set IsSelected = 1 where ID = " + ID);
                return "ok";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }


        public String SetDeleted()
        {
            try
            {
                int ID = int.Parse(Request.Query["ID"].ToString());
                string tb = Request.Query["tb"].ToString();
                DB.EseguiSQL("update " + tb + " set dtt_Cancellazione = getdate() where ID = " + ID);
                return "ok";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }


        public String GetRecord()
        {
            try
            {
                string ID = Request.Query["ID"].ToString();
                string tb = Request.Query["tb"].ToString();
                return JArray.FromObject(DB.GetDataFromSQL("select top(1) * from " + tb + " where ID = " + ID.ToString()), JsonSerializer.CreateDefault(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })).FirstOrDefault().ToString();
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }


        public String sp_GetSelectedMatch()
        {
            try
            {
                // return JsonConvert.SerializeObject(DB.GetDataFromSQL("select top(1) * from " + tb + " where ID = " + ID.ToString()) );
                return JArray.FromObject(DB.GetDataFromSQL("exec sp_GetSelectedMatch"), JsonSerializer.CreateDefault(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })).FirstOrDefault().ToString();
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }




        public String sp_GetAssociazioneGiocatorePeriodo()
        {
            try
            {
                string fk_int_Squadra = Request.Query["fk_int_Squadra"].ToString();
                DataTable dt = DB.GetDataFromSQL("exec sp_GetAssociazioneGiocatorePeriodo " + fk_int_Squadra);
                var pt = new Dictionary<int, int>();
                var ngp = new Dictionary<int, Array>();
                foreach (DataRow dr in dt.Rows)
                {
                    ngp.Add(int.Parse(dr[0].ToString()), dr.ItemArray);
                }
                return JsonConvert.SerializeObject(ngp);
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.ToString();
            }
        }




        public String sp_GetSelectedPlaylist()
        {
            try
            {
                return JArray.FromObject(DB.GetDataFromSQL("exec sp_GetSelectedPlaylist"), JsonSerializer.CreateDefault(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })).FirstOrDefault().ToString();
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.ToString();
            }
        }


        public String sp_GetUltimaRiproduzione()
        {
            try
            {
                string json = JsonConvert.SerializeObject(DB.GetDataFromSQL("exec sp_GetUltimaRiproduzione"));
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.ToString();
            }
        }


        public String InviaSelezionePC()
        {
            try
            {
                string json = "";
                string s = Request.Query["soggetto"].ToString();
                if (s.Contains("p0"))
                {
                    s = s.Replace("p0", "p1") + "=0&" + s.Replace("p0", "p2") + "=0";
                }
                else if (s.Contains("p1"))
                {
                    s = s.Replace("p1", "p2") + "=0&" + s + "=1";
                }
                else if (s.Contains("p2"))
                {
                    s = s.Replace("p2", "p1") + "=0&" + s + "=1";
                }
                string url = "http://"+ StaticStartConfig.Parametro_nva_IPSelettore + "/?" + s;
                GestioneLog.ArchiviaMessaggioLog("InviaSelezionePC url: " + url, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                json = reader.ReadToEnd();
                GestioneLog.ArchiviaMessaggioLog("InviaSelezionePC json: " + json, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                reader.Close();
                reader.Dispose();
                response.Close();
                return json;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message;
            }
        }


        public String SimulaPressioneTastoScaler()
        {
            string json="";
            try
            {
                if (fg.IsYes(StaticStartConfig.Parametro_nva_EseguiChiamateScaler)) { 
                    try
                    {
                        string pin = (Request.Query["pin"]).ToString().PadLeft(2, '0');
                        string url = "http://" + StaticStartConfig.Parametro_nva_IPScaler + "/?pin_" + pin;
                        GestioneLog.ArchiviaMessaggioLog("SimulaPressioneTastoScaler url: " + url, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        StreamReader reader = new StreamReader(response.GetResponseStream());
                        request.Timeout = 5000;
                        json = reader.ReadToEnd();
                        GestioneLog.ArchiviaMessaggioLog("SimulaPressioneTastoScaler json: " + json, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                        reader.Close();
                        reader.Dispose();
                        response.Close();
                        return Request.Query["pin"].ToString();
                    }
                    catch (Exception ex)
                    {

                        return "0";
                    }

                }
                return "0";
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message+" json:"+ json, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return "0";
            }
        }


        public String GetSelected(String tb = "")
        {
            try
            {
                if(tb=="")tb = Request.Query["tb"].ToString();

                DataTable DT = DB.GetDataFromSQL("select ID from " + tb + " where IsSelected = 1");
                if (DT.Rows.Count > 0)
                {
                    return DT.Rows[0][0].ToString();
                }
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
            return "0";
        }


        public String GetPeriodiPresentiNellaPlaylist()
        {
            try
            {
                string fk_int_Playlist = Request.Query["fk_int_Playlist"].ToString();
                return JsonConvert.SerializeObject(DB.GetDataFromSQL("select distinct p.ID, lower(p.nva_Nome) nva_Nome, p.ID as IDPeriodo, p.nva_DivTipoPeriodo  from  tb_Periodi p join tb_SceneLed sc on sc.fk_int_Periodo = p.id   where p.dtt_Cancellazione is null and sc.dtt_Cancellazione is null and sc.fk_int_Playlist = " + fk_int_Playlist.ToString() + " order by p.ID "));
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());
                return ex.Message.ToString();
            }
        }



        /*
        [HttpPost]
        public String SetFormazione_ok([FromBody] tb_Formazione f)
        {
            //string fk_int_giocatore = Request.Query["fk_int_giocatore"].ToString();
            //string fk_int_match = Request.Query["fk_int_match"].ToString();
            //int nr = Convert.ToInt32(DB.GetDataFromSQL("select count(*) as PresenzaGiocatore from tb_Formazione where fk_int_Match = '" + fk_int_match + "' and fk_int_Giocatore = '" + fk_int_giocatore + "'").Rows[0][0]);
            //if (nr > 0)
            //{
            //    DB.EseguiSQL("delete from tb_Formazione where fk_int_Match = '" + fk_int_match + "' and fk_int_Giocatore = '" + fk_int_giocatore + "'");
            //    return "panchina";
            //}
            //else
            //{
            //    DB.EseguiSQL("insert into tb_Formazione (fk_int_Match,fk_int_Giocatore)values('" + fk_int_match + "', '" + fk_int_giocatore + "')");
            //    return "titolare";
            //}
            DB.DB_InsertInto(f);
         
            return "ok";
        }
        */

        public String SalvaOraInizioPrepartita()
        {
            try
            {
                string ora = Request.Query["ora"].ToString();
                DateTime oraAvvio = DateTime.Now.Date;
                if (!string.IsNullOrWhiteSpace(ora) && ora.Contains(':')) 
                {
                    string[] componentiOra = ora.Split(':');
                    int ore = int.Parse(componentiOra[0]);
                    int minuti = int.Parse(componentiOra[1]);
                    oraAvvio = oraAvvio.AddHours(ore);
                    oraAvvio = oraAvvio.AddMinutes(minuti);
                }

                int res = DB.EseguiSQL("UPDATE tb_TimerAvvioPeriodi SET int_TimerInizioEseguito=0, dtt_TimerOraInizio='" + oraAvvio.ToString("s") + "' WHERE fk_int_playlist IN (SELECT Id FROM tb_Playlist WHERE IsSelected=1) AND fk_int_Periodo=2");
                if (res == 0)
                {
                    //non c'era la riga, va inserita
                    DB.EseguiSQL("INSERT INTO tb_TimerAvvioPeriodi (fk_int_playlist, fk_int_Periodo, dtt_TimerOraInizio) (SELECT ID, '2', '" + oraAvvio.ToString("s") + "' FROM tb_Playlist WHERE IsSelected=1)");
                }
                return "ok";

            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "ListaAggiornamenti");
                return ex.Message.ToString();
            }
        }

        public String CancellaOraInizioPrepartita()
        {
            try
            {
                DB.EseguiSQL("DELETE FROM tb_TimerAvvioPeriodi WHERE fk_int_playlist IN (SELECT Id FROM tb_Playlist WHERE IsSelected=1) AND fk_int_Periodo=2");
                return "ok";

            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "ListaAggiornamenti");
                return ex.Message.ToString();
            }
        }


        public String SalvaOraInizioEvento()
        {
            try
            {
                string ora = Request.Query["ora"].ToString();
                string idPeriodo = Request.Query["idPeriodo"].ToString();
                DateTime oraAvvio = DateTime.Now.Date;
                if (!string.IsNullOrWhiteSpace(ora) && ora.Contains(':'))
                {
                    string[] componentiOra = ora.Split(':');
                    int ore = int.Parse(componentiOra[0]);
                    int minuti = int.Parse(componentiOra[1]);
                    oraAvvio = oraAvvio.AddHours(ore);
                    oraAvvio = oraAvvio.AddMinutes(minuti);
                }

                int res = DB.EseguiSQL("UPDATE tb_TimerAvvioPeriodi SET int_TimerInizioEseguito=0, dtt_TimerOraInizio='" + oraAvvio.ToString("s") + "' WHERE fk_int_playlist IN (SELECT Id FROM tb_Playlist WHERE IsSelected=1) AND fk_int_Periodo="+idPeriodo);
                if (res == 0)
                {
                    //non c'era la riga, va inserita
                    DB.EseguiSQL("INSERT INTO tb_TimerAvvioPeriodi (fk_int_playlist, fk_int_Periodo, dtt_TimerOraInizio) (SELECT ID, '"+ idPeriodo + "', '" + oraAvvio.ToString("s") + "' FROM tb_Playlist WHERE IsSelected=1)");
                }
                return "ok";

            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "ListaAggiornamenti");
                return ex.Message.ToString();
            }
        }


        public String SalvaInizioEventoInContenitore()
        {
            string ret = "ok";
            try
            {
                string secondi = Request.Query["secondi"].ToString();
                string idPeriodo = Request.Query["idPeriodo"].ToString();
                string idPeriodoContenitore = Request.Query["idPeriodoContenitore"].ToString();
                if (!string.IsNullOrWhiteSpace(secondi))
                {
                    int res = DB.EseguiSQL("UPDATE tb_TimerAvvioPeriodi SET int_TimerInizioEseguito=0, dtt_TimerOraInizio=NULL, fk_int_PeriodoContenitore=" + idPeriodoContenitore+ ", int_TimerSecondiDaInizioContenitore="+secondi+ " WHERE fk_int_playlist IN (SELECT Id FROM tb_Playlist WHERE IsSelected=1) AND fk_int_Periodo=" + idPeriodo);
                    if (res == 0)
                    {
                        //non c'era la riga, va inserita
                        DB.EseguiSQL("INSERT INTO tb_TimerAvvioPeriodi (fk_int_playlist, fk_int_Periodo, dtt_TimerOraInizio, fk_int_PeriodoContenitore, int_TimerSecondiDaInizioContenitore) (SELECT ID, '" + idPeriodo + "', NULL, " + idPeriodoContenitore + ", " + secondi + " FROM tb_Playlist WHERE IsSelected=1)");
                    }

                    DataTable dt = DB.GetDataFromSQL("SELECT * FROM tb_Periodi WHERE id='" + idPeriodoContenitore + "'");
                    if (dt.Rows.Count > 0)
                    {
                        ret += "-" + dt.Rows[0]["nva_Codice"].ToString();
                    }

                }
                return ret;

            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "ListaAggiornamenti");
                return ex.Message.ToString();
            }
        }


        public String CancellaOraInizioEvento()
        {
            try
            {
                string idPeriodo = Request.Query["idPeriodo"].ToString();
                DB.EseguiSQL("DELETE FROM tb_TimerAvvioPeriodi WHERE fk_int_playlist IN (SELECT Id FROM tb_Playlist WHERE IsSelected=1) AND fk_int_Periodo="+ idPeriodo);
                return "ok";

            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "ListaAggiornamenti");
                return ex.Message.ToString();
            }
        }

        public String AcquisisciPeriodiOraFissa()
        {
            try
            {
                DataTable dt = DB.GetDataFromSQL(@" SELECT TOP(1) Id, fk_int_Periodo FROM tb_TimerAvvioPeriodi 
                                                    WHERE fk_int_playlist IN (SELECT Id FROM tb_Playlist WHERE IsSelected=1)
                                                    AND int_TimerInizioEseguito=0 AND dtt_TimerOraInizio IS NOT NULL 
                                                    AND dtt_TimerOraInizio<GETDATE() 
                                                    ORDER BY dtt_TimerOraInizio ASC");
                if (dt.Rows.Count > 0)
                {
                    int timerId = Convert.ToInt32(dt.Rows[0][0]);
                    int idPeriodo = Convert.ToInt32(dt.Rows[0][1]);

                    //imposta il timer come eseguito in modo che non venga piu' trovato tra quelli da avviare
                    DB.EseguiSQL("UPDATE tb_TimerAvvioPeriodi SET int_TimerInizioEseguito=1 WHERE Id=" + timerId);
                    return JsonConvert.SerializeObject(dt);
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "ListaAggiornamenti");
                return ex.Message.ToString();
            }
        }

        public String AcquisisciPeriodiOraRelativa()
        {
            String ret = null;
            try
            {
                long adesso = long.Parse(Request.Query["adesso"].ToString());
                DataTable dtPeriodo = DB.GetDataFromSQL(@" select top(1) * from tb_periodi order by int_StartPlayngLed desc, int_StartPlayngMaxischermo desc");
                if (dtPeriodo.Rows.Count > 0)
                {
                    long startPlayingLed = ValueOperations.DbToLong(dtPeriodo.Rows[0]["int_StartPlayngLed"]);
                    long startPlayingMaxischermo = ValueOperations.DbToLong(dtPeriodo.Rows[0]["int_StartPlayngMaxischermo"]);
                    long oraInizioPeriodoJavascript = Math.Max(startPlayingLed, startPlayingMaxischermo);
                    if (oraInizioPeriodoJavascript > 0)
                    {
                        //il periodo è effettivamente in corso
                        int idPeriodoCorrente = ValueOperations.DbToInteger(dtPeriodo.Rows[0]["id"]);

                        DataTable dt = DB.GetDataFromSQL(@" SELECT TOP(1) Id, fk_int_Periodo, int_TimerSecondiDaInizioContenitore FROM tb_TimerAvvioPeriodi 
                                                    WHERE fk_int_playlist IN (SELECT Id FROM tb_Playlist WHERE IsSelected=1)
                                                    AND int_TimerInizioEseguito=0 AND fk_int_PeriodoContenitore=" + idPeriodoCorrente+ @" 
                                                    ORDER BY int_TimerSecondiDaInizioContenitore ASC");
                        if (dt.Rows.Count > 0)
                        {
                            int timerId = Convert.ToInt32(dt.Rows[0][0]);
                            int idPeriodo = Convert.ToInt32(dt.Rows[0][1]);
                            int secondiDaInizio = Convert.ToInt32(dt.Rows[0][2]);

                            if(adesso > oraInizioPeriodoJavascript + (secondiDaInizio * 1000))
                            {
                                //il periodo è iniziato da più del numero di secondi necessario a far scattare l'evento
                                //imposta il timer come eseguito in modo che non venga piu' trovato tra quelli da avviare
                                DB.EseguiSQL("UPDATE tb_TimerAvvioPeriodi SET int_TimerInizioEseguito=1 WHERE Id=" + timerId);
                                ret = JsonConvert.SerializeObject(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "ListaAggiornamenti");
                ret = ex.Message.ToString();
            }
            return ret;
        }

        public String SalvaTipoEsecuzioneEvento()
        {
            try
            {
                string idPeriodo = Request.Query["idPeriodo"].ToString();
                string tipoEsecuzione = Request.Query["tipoEsecuzione"].ToString();

                int res = DB.EseguiSQL("UPDATE tb_Periodi SET nva_TipoTerminazione='"+tipoEsecuzione+"', tin_IsLoop="+("LOOP".Equals(tipoEsecuzione)?1:0)+ 
                                        ", nva_NumeroRiproduzioni='" + ("LOOP".Equals(tipoEsecuzione) ? 0 : 1) + "' WHERE id=" + idPeriodo);
                if (res == 0)
                {
                    return "periodo non aggiornato";
                }
                return "ok";

            }
            catch (Exception ex)
            {
                GestioneLog.ArchiviaErroreLog("Exception metodo classe " + this.GetType().Name + " - metodo: " + System.Reflection.MethodInfo.GetCurrentMethod() + ": " + ex.Message, "ListaAggiornamenti");
                return ex.Message.ToString();
            }
        }
    }
}