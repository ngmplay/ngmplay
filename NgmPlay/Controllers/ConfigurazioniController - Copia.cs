﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
//using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NgmPlay;
using NgmPlay.Utility;
using RenderMachine.Models;

namespace RenderMachine.Controllers
{



    public class CrudController : Controller 
    {




        public IActionResult Index()
        {




            return View();

        }


        public string GetStadi()
        {


            return JsonConvert.SerializeObject(DB.GetDataFromSQL("select  * , case when tin_ConfigurazioneOK =1 then 'img/icon/checked_green.png' else 'img/icon/warning.png' end as img_ConfigurazioneOk   from [tb_Stadi] order by nva_Città "));


        }


        public string CleanDisposizioneLedStadio()
        {


            string fk_int_stadio = Request.Query["fk_int_stadio"].ToString();
            DB.EseguiSQL("update tb_DisposizioneLed set dtt_Cancellazione = getdate() where fk_int_Stadio = " + fk_int_stadio);


            return "OK";


        }

        [HttpPost]
        public string RicevoFormConfig()
        {

            try { 

                var FileCaricato = HttpContext.Request.Form.Files["FileDaCaricare"];
                int ID = Int32.Parse(HttpContext.Request.Form["ID"]);

                var result = string.Empty;
                using (var reader = new StreamReader(FileCaricato.OpenReadStream()))
                {
                    result = reader.ReadToEnd();
                }

                tb_ImportFileConfigurator ifc = new tb_ImportFileConfigurator();
                ifc.nva_Contenuto = result;
                ifc.fk_int_Stadio = ID;

                ImportazioneConfig.AcquisisciConfigurazioneStadio(ifc.fk_int_Stadio, ifc.nva_Contenuto);

                ifc.DB_InsertInto();

            }
            catch (Exception ex)
            {

                return "errore importazione file di configurazione"; // JsonConvert.SerializeObject(DB.GetDataFromSQL("SELECT (CONVERT(varchar,m.dtt_DataOra,103)+' '+s_casa.nva_Nome+' VS '+s_ospite.nva_Nome) nva_Titolo , m.ID, m.fk_int_SquadraCasa, m.fk_int_SquadraOspite, s_casa.nva_Nome as nva_NomeSquadraCasa, s_ospite.nva_Nome as nva_NomeSquadraOspite, CONVERT(varchar,m.dtt_DataOra,103) as nva_DataOra FROM [tb_Match] m join  tb_Squadre s_casa on s_casa.id = m.fk_int_SquadraCasa join tb_Squadre s_ospite on s_ospite.id = m.fk_int_SquadraOspite where dtt_DataOra >= cast(getdate() as date)"));

            }

            return "File di configurazione importato correttamente"; // JsonConvert.SerializeObject(DB.GetDataFromSQL("SELECT (CONVERT(varchar,m.dtt_DataOra,103)+' '+s_casa.nva_Nome+' VS '+s_ospite.nva_Nome) nva_Titolo , m.ID, m.fk_int_SquadraCasa, m.fk_int_SquadraOspite, s_casa.nva_Nome as nva_NomeSquadraCasa, s_ospite.nva_Nome as nva_NomeSquadraOspite, CONVERT(varchar,m.dtt_DataOra,103) as nva_DataOra FROM [tb_Match] m join  tb_Squadre s_casa on s_casa.id = m.fk_int_SquadraCasa join tb_Squadre s_ospite on s_ospite.id = m.fk_int_SquadraOspite where dtt_DataOra >= cast(getdate() as date)"));

        }





     //   [HttpPost]
        //public string RicevoFormPlaylist()
        //{

        //    try
        //    {

        //        var FileCaricato = HttpContext.Request.Form.Files["FileDaCaricare"];
        //        int ID = Int32.Parse(HttpContext.Request.Form["ID"]);

        //        var result = string.Empty;
        //        using (var reader = new StreamReader(FileCaricato.OpenReadStream()))
        //        {
        //            result = reader.ReadToEnd();
        //        }

        //        tb_ImportFileConfigurator ifc = new tb_ImportFileConfigurator();
        //        ifc.nva_Contenuto = result;
        //        ifc.fk_int_Stadio = ID;

        //        ImportazioneConfig.AcquisisciConfigurazioneStadio(ifc.fk_int_Stadio, ifc.nva_Contenuto);




        //        ifc.DB_InsertInto();

        //    }
        //    catch (Exception ex)
        //    {

        //        return "errore importazione file di configurazione"; // JsonConvert.SerializeObject(DB.GetDataFromSQL("SELECT (CONVERT(varchar,m.dtt_DataOra,103)+' '+s_casa.nva_Nome+' VS '+s_ospite.nva_Nome) nva_Titolo , m.ID, m.fk_int_SquadraCasa, m.fk_int_SquadraOspite, s_casa.nva_Nome as nva_NomeSquadraCasa, s_ospite.nva_Nome as nva_NomeSquadraOspite, CONVERT(varchar,m.dtt_DataOra,103) as nva_DataOra FROM [tb_Match] m join  tb_Squadre s_casa on s_casa.id = m.fk_int_SquadraCasa join tb_Squadre s_ospite on s_ospite.id = m.fk_int_SquadraOspite where dtt_DataOra >= cast(getdate() as date)"));

        //    }

        //    return "File di configurazione importato correttamente"; // JsonConvert.SerializeObject(DB.GetDataFromSQL("SELECT (CONVERT(varchar,m.dtt_DataOra,103)+' '+s_casa.nva_Nome+' VS '+s_ospite.nva_Nome) nva_Titolo , m.ID, m.fk_int_SquadraCasa, m.fk_int_SquadraOspite, s_casa.nva_Nome as nva_NomeSquadraCasa, s_ospite.nva_Nome as nva_NomeSquadraOspite, CONVERT(varchar,m.dtt_DataOra,103) as nva_DataOra FROM [tb_Match] m join  tb_Squadre s_casa on s_casa.id = m.fk_int_SquadraCasa join tb_Squadre s_ospite on s_ospite.id = m.fk_int_SquadraOspite where dtt_DataOra >= cast(getdate() as date)"));

        //}








    }
}
