﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NgmPlay.Classi;
using NgmPlay.Utility;
using RenderMachine.Models;

namespace RenderMachine.Controllers
{
    public class PlayerController : Controller
    {
        public IActionResult maxischermoTimerPunteggio()
        {
            return View();
        }

        public IActionResult maxischermo()
        {
            return View();
        }

        public IActionResult nuovaTemporizzazione()
        {
            try { 
            string q = @"   
                            select
                            tv.*
                            from tb_DisposizioneTagVideo tv
                            join tb_DisposizioneLed dl on tv.fk_int_DisposizioneLed = dl.Id
                            where
                            fk_int_Layer = 2 and
                            fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString() + @" and
                            dl.dtt_Cancellazione is null and
                            tv.dtt_Cancellazione is null and 
                            not dl.nva_Nome like 'trasparente'
                        ";

            List<object> Lista_tv = DB.PopolaListaOggetti(typeof(tb_DisposizioneTagVideo), DB.GetDataFromSQL(q));

            string HtmlClockScore = "";

            foreach (tb_DisposizioneTagVideo tv in Lista_tv)
            {
                HtmlClockScore = HtmlClockScore + @"<div class='TileClockScore " + tv.tb_DisposizioneLed.nva_Nome.Replace(" ", "_") + "' style='width:" + tv.int_W + "px; height:" + tv.int_H + "px; margin-left:" + tv.int_MarginL + "px; margin-top:" + tv.int_MarginT + "px;'>";

                //if(tv.tb_DisposizioneLed.nva_Nome.ToLower() == "timer") {
                //    HtmlClockScore = HtmlClockScore + "<div class='LogoTimerLed'><img src = '/img/logo.png'  /></div><div class='TempoOrologioLed' ></div>";
                //}

                //if (tv.tb_DisposizioneLed.nva_Nome.ToLower() == "punteggio")
                //{
                //    HtmlClockScore = HtmlClockScore + "<div class='PunteggioLed'></div>";
                //}


                HtmlClockScore = HtmlClockScore + "</div>";
            }
            ViewBag.HtmlClockScore = HtmlClockScore;




            List<object> Lista_tvGrafica = DB.PopolaListaOggetti(typeof(tb_DisposizioneTagVideo), DB.GetDataFromSQL("select tv.* from tb_DisposizioneTagVideo tv join tb_DisposizioneLed dl on tv.fk_int_DisposizioneLed = dl.Id where dl.fk_int_Stadio = 2 and fk_int_Layer = 5 order by int_ordine"));

            string HtmlGrafica = "";


            foreach (tb_DisposizioneTagVideo tv in Lista_tvGrafica)
            {

                HtmlGrafica = HtmlGrafica + "<canvas width='" + tv.int_W + "' height='" + tv.int_H + "'  id='grafica_" + tv.fk_int_DisposizioneLed + "_" + tv.Id + "' style='position: absolute; border:none;  margin-left:" + tv.int_MarginL + "px; margin-top:" + tv.int_MarginT + "px;  '></canvas>";


            }


            ViewBag.HtmlGrafica = HtmlGrafica;





            ViewBag.DimensioneLedCanvasPerCss = DB.GetDataFromSQL("select 'width:' + cast(int_W as nvarchar(55)) + 'px;height:'+ cast(int_H as nvarchar(55)) + 'px;' as size  from tb_Stadi where ID = " + StaticStartConfig.ID_int_Stadio.ToString()).Rows[0][0];


            //string Azioni = "";
            //string EventiPartita = @"<div class='EventiPartita' style='position:absolute; left:~left~px; top:~top~px; width:~width~px; height:~height~px;  '></div>";
            //DataTable dt = DB.GetDataFromSQL("select tv.id, tv.int_MarginL, tv.int_MarginT ,dl.int_W , dl.int_H   from tb_DisposizioneTagVideo tv join tb_DisposizioneLed  dl on dl.Id = tv.fk_int_DisposizioneLed where dl.dtt_Cancellazione is Null  and tv.dtt_Cancellazione is Null and dl.nva_Nome like 'Central' and dl.fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString());
            //foreach(DataRow r in dt.Rows)
            //{
            //    Azioni = Azioni + EventiPartita.Replace("~left~", r["int_MarginL"].ToString())
            //    .Replace("~top~", r["int_MarginT"].ToString())
            //    .Replace("~width~", r["int_W"].ToString())
            //    .Replace("~height~", r["int_H"].ToString());
            //}
            //ViewBag.EventiPartita = Azioni;





            List<object> Lista_tvResult = DB.PopolaListaOggetti(typeof(tb_DisposizioneTagVideo), DB.GetDataFromSQL("select distinct tv.* from tb_DisposizioneTagVideo tv join tb_DisposizioneLed dl on tv.fk_int_DisposizioneLed = dl.Id where dl.fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString() + " and fk_int_Layer = 3  and dl.dtt_Cancellazione is null and tv.dtt_Cancellazione is null and  not dl.nva_Nome like 'trasparente'"));
            string EventiPartita = "";
            foreach (tb_DisposizioneTagVideo tv in Lista_tvResult)
            {
                EventiPartita = EventiPartita + "<div class='TileEventiPartita " + tv.tb_DisposizioneLed.nva_Nome + "' style='width:" + tv.int_W + "px; height:" + tv.int_H + "px; margin-left:" + tv.int_MarginL + "px; margin-top:" + tv.int_MarginT + "px;  '></div>";
            }

            ViewBag.HtmlEventiPartita = EventiPartita;









            //List<object> Lista_tvLuminosità = DB.PopolaListaOggetti(typeof(tb_DisposizioneTagVideo), DB.GetDataFromSQL("select  tv.* from tb_DisposizioneTagVideo tv join tb_DisposizioneLed dl on tv.fk_int_DisposizioneLed = dl.Id join tb_layer l on l.id = dl.fk_int_layer   where dl.fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString() + " and l.nva_Nome like 'Luminosità'  and dl.dtt_Cancellazione is null and tv.dtt_Cancellazione is null    order by tv.int_MarginT, tv.int_MarginL"));
            //string htmlLuminosità = "";

            //foreach (tb_DisposizioneTagVideo tv in Lista_tvLuminosità)
            //{
            //  //  htmlLuminosità = htmlLuminosità + "<div class='TileLuminosita luminosita_" + tv.tb_DisposizioneLed.Id.ToString() + "' style='width:" + tv.int_W + "px; height:" + tv.int_H + "px; margin-left:" + tv.int_MarginL + "px; margin-top:" + tv.int_MarginT + "px; '></div>";

            //}

            //ViewBag.HtmlLuminosità = htmlLuminosità;

        }
            catch (Exception e)
            {
                GestioneLog.ArchiviaErroreLog("Exception PlayerController 139:" +e.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());

            }


            return View();
         
        }
        public IActionResult Animazione()
        {


            return View();

        }
        public IActionResult ProvaPausa()
        {


            return View();

        }

        public IActionResult memoryleak()
        {


            return View();

        }
        public IActionResult provastatico()
        {


            return View();

        }
        public IActionResult ProvaClona()
        {


            return View();

        }
        public IActionResult Punteggio()
        {


            return View();

        }


        public IActionResult Index()
        {
            try { 

            string q = @"select
                        tv.*
                        from tb_DisposizioneTagVideo tv
                        join tb_DisposizioneLed dl on tv.fk_int_DisposizioneLed = dl.Id
                        where
                        fk_int_Layer = 2 and
                        fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString() + @" and
                        dl.dtt_Cancellazione is null and
                        tv.dtt_Cancellazione is null and 
                        not dl.nva_Nome like 'trasparente'
                        ";

            List<object> Lista_tv = DB.PopolaListaOggetti(typeof(tb_DisposizioneTagVideo), DB.GetDataFromSQL(q));

            string HtmlClockScore = "";

            foreach (tb_DisposizioneTagVideo tv in Lista_tv)
            {
                HtmlClockScore = HtmlClockScore + @"<div class='TileClockScore " + tv.tb_DisposizioneLed.nva_Nome.Replace(" ", "_") + "' style='width:" + tv.int_W + "px; height:" + tv.int_H + "px; margin-left:" + tv.int_MarginL + "px; margin-top:" + tv.int_MarginT + "px;'>";
                HtmlClockScore = HtmlClockScore + "</div>";
            }
            ViewBag.HtmlClockScore = HtmlClockScore;




            List<object> Lista_tvGrafica = DB.PopolaListaOggetti(typeof(tb_DisposizioneTagVideo), DB.GetDataFromSQL("select tv.* from tb_DisposizioneTagVideo tv join tb_DisposizioneLed dl on tv.fk_int_DisposizioneLed = dl.Id where dl.fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString() + "  and fk_int_Layer = 4 and dl.dtt_Cancellazione is null  and tv.dtt_Cancellazione is null order by int_ordine"));

            string HtmlGrafica = "";


            foreach (tb_DisposizioneTagVideo tv in Lista_tvGrafica)
            {
                HtmlGrafica = HtmlGrafica + "<canvas width='" + tv.int_W + "' height='" + tv.int_H + "'  id='grafica_" + tv.fk_int_DisposizioneLed + "_" + tv.Id + "' style='position: absolute; border:none;  margin-left:" + tv.int_MarginL + "px; margin-top:" + tv.int_MarginT + "px;  '></canvas>";
            }
            ViewBag.HtmlGrafica = HtmlGrafica;



            string HtmlDiv = "";
            foreach (tb_DisposizioneTagVideo tv in Lista_tvGrafica)
            {
                HtmlDiv = HtmlDiv + "<div class='div_h" + tv.int_H + "'  id='div_" + tv.fk_int_DisposizioneLed + "_" + tv.Id + "' style='width:" + tv.int_W + "px;height:" + tv.int_H + "px; position: absolute; border:none;  margin-left:" + tv.int_MarginL + "px; margin-top:" + tv.int_MarginT + "px;  '></div>";
            }
            ViewBag.HtmlDiv = HtmlDiv;




            ViewBag.DimensioneLedCanvasPerCss = DB.GetDataFromSQL("select 'width:' + cast(int_W as nvarchar(55)) + 'px;height:' + cast(int_H as nvarchar(55)) + 'px;' as size  from tb_Stadi where ID = " + StaticStartConfig.ID_int_Stadio.ToString()).Rows[0][0];

            ViewBag.DimensionePosizioneMaxischermoPerCss = DB.GetDataFromSQL("select 'width:' + cast(int_WMaxischermo as nvarchar(55)) + 'px;height:' + cast(int_HMaxischermo as nvarchar(55)) + 'px;left:' + cast(int_XMaxischermo as nvarchar(55)) + 'px;top:' + cast(int_YMaxischermo as nvarchar(55)) + 'px;' as size  from tb_Stadi where ID = " + StaticStartConfig.ID_int_Stadio.ToString()).Rows[0][0];

            //string Azioni = "";
            //string EventiPartita = @"<div class='EventiPartita' style='position:absolute; left:~left~px; top:~top~px; width:~width~px; height:~height~px;  '></div>";
            //DataTable dt = DB.GetDataFromSQL("select tv.id, tv.int_MarginL, tv.int_MarginT ,dl.int_W , dl.int_H   from tb_DisposizioneTagVideo tv join tb_DisposizioneLed  dl on dl.Id = tv.fk_int_DisposizioneLed where dl.dtt_Cancellazione is Null  and tv.dtt_Cancellazione is Null and dl.nva_Nome like 'Central' and dl.fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString());
            //foreach(DataRow r in dt.Rows)
            //{
            //    Azioni = Azioni + EventiPartita.Replace("~left~", r["int_MarginL"].ToString())
            //    .Replace("~top~", r["int_MarginT"].ToString())
            //    .Replace("~width~", r["int_W"].ToString())
            //    .Replace("~height~", r["int_H"].ToString());

            //}

            //ViewBag.EventiPartita = Azioni;
            String Act = DB.GetDataFromSQL("select nva_NomeDirectory from tb_Stadi where ID = " + StaticStartConfig.ID_int_Stadio.ToString()).Rows[0][0].ToString();

            String Dir = DB.GetDataFromSQL("select nva_Directory from tb_Squadre s join tb_match m on m.fk_int_SquadraCasa = s.id  where m.IsSelected = 1").Rows[0][0].ToString();

              // ViewBag.HtmlAnimazioniLedMaxischermo = Action();
             Console.WriteLine(System.IO.Directory.GetCurrentDirectory());

            ViewBag.htmlMaxischermo = System.IO.File.ReadAllText(System.IO.Directory.GetCurrentDirectory() + "/wwwroot/assetsSquadre/" + Dir + "/Layout/htmlMaxischermo.html");
            ViewBag.jsLedMaxischermo = System.IO.File.ReadAllText(System.IO.Directory.GetCurrentDirectory() + "/wwwroot/assetsSquadre/" + Dir + "/Layout/jsLedMaxischermo.html");
            ViewBag.cssLedMaxischermo = System.IO.File.ReadAllText(System.IO.Directory.GetCurrentDirectory() + "/wwwroot/assetsSquadre/" + Dir + "/Layout/cssLedMaxischermo.html");



            List<object> Lista_tvResult = DB.PopolaListaOggetti(typeof(tb_DisposizioneTagVideo), DB.GetDataFromSQL("select distinct tv.* from tb_DisposizioneTagVideo tv join tb_DisposizioneLed dl on tv.fk_int_DisposizioneLed = dl.Id where dl.fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString() + " and fk_int_Layer = 3  and dl.dtt_Cancellazione is null and tv.dtt_Cancellazione is null and  not dl.nva_Nome like 'trasparente'"));
            string EventiPartita = "";
            foreach (tb_DisposizioneTagVideo tv in Lista_tvResult)
            {
                EventiPartita = EventiPartita + "<div class='TileEventiPartita " + tv.tb_DisposizioneLed.nva_Nome + "' style='width:" + tv.int_W + "px; height:" + tv.int_H + "px; margin-left:" + tv.int_MarginL + "px; margin-top:" + tv.int_MarginT + "px;  '></div>";
            }

            ViewBag.HtmlEventiPartita = EventiPartita;




                /*
                string[] colori = { "red", "green", "blue", "yellow", "purple", "white" };
               // background - color:"+ colori[i] + ";

                List<object> Lista_tvLuminosità = DB.PopolaListaOggetti(typeof(tb_DisposizioneTagVideo), DB.GetDataFromSQL("select  tv.* from tb_DisposizioneTagVideo tv join tb_DisposizioneLed dl on tv.fk_int_DisposizioneLed = dl.Id join tb_layer l on l.id = dl.fk_int_layer   where dl.fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString() + " and l.nva_Nome like 'Luminosità'  and dl.dtt_Cancellazione is null and tv.dtt_Cancellazione is null    order by tv.int_MarginT, tv.int_MarginL"));
                string htmlLuminosità = "";
                int i = 0;
                foreach (tb_DisposizioneTagVideo tv in Lista_tvLuminosità)
                {
                     tb_DisposizioneLed dl = new tb_DisposizioneLed(tv.fk_int_DisposizioneLed);

                    String valore = "0";
                    DataTable dt = DB.GetDataFromSQL("select top(1) isnull(int_Valore,0) valore from tb_Luminosita where fk_int_DisposizioneLed = " + dl.Id + " and dtt_Cancellazione is null");
                    if (dt.Rows.Count > 0)
                    {
                        try
                        {
                            valore = (1- (double.Parse(dt.Rows[0][0].ToString())/100)).ToString("F").Replace(",",".");
                        }
                        catch (Exception e)
                        {
                            GestioneLog.ArchiviaErroreLog("Exception PlayerController 309:" + e.Message);
                        }
                    }
                    i = i + 1;
                    if (i > (colori.Length-1)) i = 0;

                    valore = ".5";
                    htmlLuminosità = htmlLuminosità + "<div class='TileLuminosita luminosita_" + tv.tb_DisposizioneLed.Id.ToString() + "' style='text-align:left; background-color:"+ colori[i] + "; width:" + tv.int_W + "px; height:" + tv.int_H + "px; margin-left:" + tv.int_MarginL + "px; margin-top:" + tv.int_MarginT + "px; opacity:"+ valore + ";'>" + tv.tb_DisposizioneLed.nva_Nome.ToString() +": " + tv.int_MarginL + "</div>";
                    
                
                }
                */






                List<object> Lista_tvLuminosita = DB.PopolaListaOggetti(typeof(tb_DisposizioneTagVideo), DB.GetDataFromSQL("select distinct tv.* from tb_DisposizioneTagVideo tv join tb_DisposizioneLed dl on tv.fk_int_DisposizioneLed = dl.Id where dl.fk_int_Stadio = " + StaticStartConfig.ID_int_Stadio.ToString() + " and fk_int_Layer = 6  and dl.dtt_Cancellazione is null and tv.dtt_Cancellazione is null and  not dl.nva_Nome like 'trasparente'  order by tv.int_MarginT, tv.int_MarginL"));
                string htmlLuminosità = "";
                foreach (tb_DisposizioneTagVideo tv in Lista_tvLuminosita)
                {
                    String l = "";
                    DataTable dt = DB.GetDataFromSQL("select top(1) int_Valore from tb_Luminosita where dtt_Cancellazione is null and nva_Settore like '" + tv.tb_DisposizioneLed.nva_Nome.ToString().Replace(" ", "_") + "'");
                    if (dt.Rows.Count > 0)
                    {
                        decimal d = (((100 - Convert.ToDecimal(dt.Rows[0][0]))) / 100);
                        l = d.ToString("0.##").Replace(",",".");
                    }
                    htmlLuminosità = htmlLuminosità + "<div class='TileLuminosita luminosita_" + tv.tb_DisposizioneLed.nva_Nome.ToString().Replace(" ","_") + "' style='opacity:"+l+"; width:" + tv.int_W + "px; height:" + tv.int_H + "px; margin-left:" + tv.int_MarginL + "px; margin-top:" + tv.int_MarginT + "px; '></div>";
                }


                ViewBag.HtmlLuminosità = htmlLuminosità;

            }
            catch (Exception e)
            {
                GestioneLog.ArchiviaErroreLog("Exception PlayerController 312:" +e.Message, System.Reflection.MethodInfo.GetCurrentMethod().ToString());

            }




            return View();




        }

    }
}
