﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OfficeOpenXml;
using System.IO;
using System.Data;
using Microsoft.Win32;
using System.Collections;


namespace NgmImport
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class Import : Window
    {

        private DataTable dt;
        private String filePath;
        private int RigaConIntestazioni = 3;
        private int PrimaColonna = 1;

        //modifica test sync



        public Import()
        {
            InitializeComponent();
           // dt =  XlsToDatatable();
            //   SalvaDatatableToXls(dt); 
          //  dt = XlsToDatatable(tb_PathFile.Text);
            btn_ApriFile.Click += btnOpenFile_Click;
            btn_Importa.Click += btnImporta_Click;
                   tb_PathFile.Text = @"C:\Users\mpetrini\Desktop\Playlist PERUGIA - LECCE.xlsx";
        }


        private void btnOpenFile_Click(object sender, RoutedEventArgs e) 
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop).ToString();
            openFileDialog.Filter = "xlsx file| *.xlsx";
            openFileDialog.Multiselect = false;

            if (openFileDialog.ShowDialog() == true)
                tb_PathFile.Text = openFileDialog.FileName;
 




        }


        private void btnImporta_Click(object sender, RoutedEventArgs e)
        {
            dt = XlsToDatatable();

            dg_dati.ItemsSource = dt.DefaultView ;
        }





        private string FindFirtsNoEmptyCell(ExcelWorksheet workSheet,int colonna )
        {
            for (int r = RigaConIntestazioni; r <= 100; r++)
            {
                string s = workSheet.Cells[r, colonna].Text;
                if (s.Length > 0)
                    return s;
            }
            return "";
        }







        private DataTable CreateDataTableForData(ExcelWorksheet workSheet)
        {
            DataTable dt = new DataTable();
            var end = workSheet.Dimension.End;
            try
            {
                dt.Columns.Add(new DataColumn { ColumnName = "Periodo" });
                dt.Columns.Add(new DataColumn {ColumnName = "Tempo" });
                dt.Columns.Add(new DataColumn { ColumnName = "Durata" });


                for (int col = 3; col <= end.Column; col++)
                {
                    string NomeColonna = workSheet.Cells[RigaConIntestazioni, col].Text.Trim().Replace(" ", "_");
                    int.TryParse(workSheet.Cells[(RigaConIntestazioni + 1), col].Text.Trim(), out int Size);
                    DataColumn  d = new DataColumn();
                    d.ColumnName = NomeColonna;
                    d.ExtendedProperties["size"] = Size;
                    dt.Columns.Add(d);
                }
            }
            catch(Exception  ex)
            {
                MessageBox.Show("Controllare che le celle nella prima riga non siano vuote o abbiano nomi univoci");
            }
            return dt;
        }



        /*
        private void SalvaDatatableToXls(DataTable dt)
        {
            FileInfo file = new FileInfo(filePath);
            using (ExcelPackage package = new ExcelPackage(file))
            {
                int r = 2;
                foreach (DataRow dr in dt.Rows)
                {
                    int c = 3;
                    foreach (DataColumn col in dt.Columns)
                    {
                        object _act = workSheet.Cells[r, c].Value;
                        object _new = dr[col];
                        Console.WriteLine("----------------------------------");
                        Console.WriteLine("act:");
                        Console.WriteLine(_act);
                        Console.WriteLine("new:");
                        Console.WriteLine(_new);
                        Console.WriteLine("----------------------------------");
                        if (_act != _new)
                        {
                            workSheet.Cells[r, c].Value = _new;
                        }
                        c++;
                    }
                    r++;
                }
                package.Save();
            }
        }
        */



        private DataTable XlsToDatatable()
        {
            FileInfo file = new FileInfo(tb_PathFile.Text);
            using (ExcelPackage package = new ExcelPackage(file)) {
            ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
            var start = workSheet.Dimension.Start;
            var end = workSheet.Dimension.End;
            DataTable dt = CreateDataTableForData(workSheet);
                for (int row = (RigaConIntestazioni+1); row <= end.Row; row++)
                {
                    


                    int c = 0 ;
                    DataRow dr = dt.NewRow();
                   foreach(DataColumn col in dt.Columns )
                    {
                        object o = null;

                        switch (col.ColumnName)
                        {
                            case "Periodo":
                                if ((workSheet.Cells[row, 1].Text != "") && (workSheet.Cells[row, 2].Text == ""))
                                {
                                    o = workSheet.Cells[row, 1].Text;
                                }
                                else if (dt.Rows.Count > 1)
                                {
                                   o = dt.Rows[dt.Rows.Count - 1][col];
                                }

                                break;
                            case "Durata":


                                break;

                            default:
                                 o = workSheet.Cells[row, c].Value;
                                
                                break;
                        }


                        dr[col] = o;
                        
                        c++;

                    }
                    dt.Rows.Add(dr);
                }

                //var removeColumns = dt.Columns.Cast<DataColumn>().Where(c => c.ColumnName.StartsWith("column", StringComparison.InvariantCultureIgnoreCase));

                DataTable dt2 = new DataTable();
                dt2 = dt.Copy();

                //foreach (DataColumn colToRemove in removeColumns)
                //{
                //    dt2.Columns.Remove(colToRemove.ColumnName);
                //}
                                   

                return dt2;
            }
        }






    }



}



