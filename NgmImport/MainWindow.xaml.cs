﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OfficeOpenXml;
using System.IO;
using System.Data;
using Microsoft.Win32;

namespace NgmImport
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ExcelWorksheet workSheet;

        private DataTable dt;
        private String filePath;


        public MainWindow()
        {
            InitializeComponent();
            //   dt =  XlsToDatatable();
            //   SalvaDatatableToXls(dt); 
          //  dt = XlsToDatatable(tb_PathFile.Text);
            btn_ApriFile.Click += btnOpenFile_Click;
            btn_Importa.Click += btnImporta_Click;
            
        }


        private void btnOpenFile_Click(object sender, RoutedEventArgs e) 
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop).ToString();
            openFileDialog.Filter = "xlsx file| *.xlsx";
            openFileDialog.Multiselect = false;


            if (openFileDialog.ShowDialog() == true)


                tb_PathFile.Text = openFileDialog.FileName;
 

        }


        private void btnImporta_Click(object sender, RoutedEventArgs e)
        {
            dt = XlsToDatatable();

            dg_dati.ItemsSource = dt.DefaultView ;
        }













        private DataTable CreateDataTableForData(ExcelWorksheet workSheet)
        {
            DataTable dt = new DataTable();
            var end = workSheet.Dimension.End;
            try
            {
                for (int col = 1; col <= end.Column; col++)
                {
                    dt.Columns.Add(workSheet.Cells[1, col].Text.Trim().Replace(" ","_"));
                }
            }
            catch( Exception  ex)
            {
                MessageBox.Show("Controllare che le celle nella prima riga non siano vuote o abbiano nomi univoci");
            }
            return dt;
        }




        private void SalvaDatatableToXls(DataTable dt)
        {
            FileInfo file = new FileInfo(filePath);
            using (ExcelPackage package = new ExcelPackage(file))
            {
                int r = 2;
                foreach (DataRow dr in dt.Rows)
                {
                    int c = 1;
                    foreach (DataColumn col in dt.Columns)
                    {
                        object _act = workSheet.Cells[r, c].Value;
                        object _new = dr[col];
                        Console.WriteLine("----------------------------------");
                        Console.WriteLine("act:");
                        Console.WriteLine(_act);
                        Console.WriteLine("new:");
                        Console.WriteLine(_new);
                        Console.WriteLine("----------------------------------");
                        if (_act != _new)
                        {
                            workSheet.Cells[r, c].Value = _new;
                        }
                        c++;
                    }
                    r++;
                }
                package.Save();
            }
        }




        private DataTable XlsToDatatable()
        {
            FileInfo file = new FileInfo(tb_PathFile.Text);
            using (ExcelPackage package = new ExcelPackage(file)) { 
            workSheet = package.Workbook.Worksheets.First();
            var start = workSheet.Dimension.Start;
            var end = workSheet.Dimension.End;
            DataTable dt = CreateDataTableForData(workSheet);
                for (int row = 2; row <= end.Row; row++)
                {
                    int c = 1;
                    DataRow dr = dt.NewRow();
                   foreach(DataColumn col in dt.Columns)
                    {
                        object o = workSheet.Cells[row, c].Value ;
                        dr[col] = o;
                        c++;
                    }
                    dt.Rows.Add(dr);
                }
                return dt;
            }
        }






    }
}
