﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestartApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Wait for the process to terminate
            foreach (var process in Process.GetProcessesByName("SyncPC"))
            {
                process.Kill();
            }
            Process.Start(@"SyncPC.exe");
        }
    }
}
